USE [master]
GO
/****** Object:  Database [BookingSystemDB]    Script Date: 05-Aug-17 9:14:08 AM ******/
CREATE DATABASE [BookingSystemDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BookingSystemDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\BookingSystemDB.mdf' , SIZE = 12480KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'BookingSystemDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\BookingSystemDB_log.ldf' , SIZE = 1344KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [BookingSystemDB] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BookingSystemDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BookingSystemDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BookingSystemDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BookingSystemDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BookingSystemDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BookingSystemDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [BookingSystemDB] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [BookingSystemDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BookingSystemDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BookingSystemDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BookingSystemDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BookingSystemDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BookingSystemDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BookingSystemDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BookingSystemDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BookingSystemDB] SET  ENABLE_BROKER 
GO
ALTER DATABASE [BookingSystemDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BookingSystemDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BookingSystemDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BookingSystemDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BookingSystemDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BookingSystemDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BookingSystemDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BookingSystemDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BookingSystemDB] SET  MULTI_USER 
GO
ALTER DATABASE [BookingSystemDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BookingSystemDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BookingSystemDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BookingSystemDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [BookingSystemDB] SET DELAYED_DURABILITY = DISABLED 
GO
USE [BookingSystemDB]
GO
/****** Object:  Schema [del]    Script Date: 05-Aug-17 9:14:11 AM ******/
CREATE SCHEMA [del]
GO
/****** Object:  UserDefinedFunction [dbo].[getPromotionRates]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create function [dbo].[getPromotionRates] (	
	@start date, --checkin_date
	@end date, --checkout_date
	--additional optional parameters in case an outer apply is required (no join allowed for outer apply)
	@provider int =0,  
	@market int =0,
	@room int =0,
	@fulldate datetime='1900-01-01'
	)
returns @PromoInfo  table
(
fulldate datetime,
provider_id int,
market_id int,
market_name nvarchar(50),
room_id int,
room_name nvarchar(50),
final_discount float
)
AS
BEGIN
Insert @PromoInfo
select 
d.fulldate,
p.provider_id,
markets.market_id,
markets.market_name,
rooms.room_id,
rooms.room_name,
EXP(SUM(LOG((100 - coalesce(p.discount,0)))))/100   final_discount
from  [dbo].[promotion] p 

inner join dbo.date d
on d.fulldate between p.valid_from and p.valid_from
inner join  --get all markets applicable 
(
	select 
	p.promotion_id,
	p.apply_all_markets,
	m.market_id,
	m.market_name,
	m.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_market] pm
	on p.promotion_id=pm.promotion_id
	right join [dbo].[market] m
	on (m.market_id=pm.market_id  --either it applies to a specific market or 
	or  p.apply_all_markets=1   --to all markets 
	)
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) markets 
on p.promotion_id=markets.promotion_id
and markets.provider_id=p.provider_id
inner join   --get all rooms applicable 
(
	select 
	p.promotion_id,
	p.apply_all_rooms,
	r.room_id,
	r.name room_name,
	r.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_room] pr
	on p.promotion_id=pr.promotion_id
	right join [dbo].[room] r 
	on (r.room_id=pr.room_id  --either it applies to a specific room or 
	or  p.apply_all_rooms=1   ---or to all rooms 
	)
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) rooms 
on 
rooms.promotion_id=markets.promotion_id
and rooms.provider_id=p.provider_id

where
p.is_active=1
and 
d.fulldate between  @start and  @end    --only return prices for dates within booking
and  /* handle additional parameters if required */
(@provider=0 or @provider =p.provider_id)
and 
( @market=0 or @market= markets.market_id)
and
(  @room= 0 or @room=rooms.room_id)
and
(@fulldate ='1900-01-01' or @fulldate =d.fulldate )
and  
--apply all logic for promotion filtering 
(
	
		--if booking is made between the promotion booking dates 
	getdate() between coalesce(p.valid_from,'1990-01-01') and coalesce(p.valid_to ,'1990-01-01')
	or
		--the travel dates must match 
		(
			@start between coalesce(p.[travel_from_date],'1990-01-01') and coalesce(p.[travel_until_date],'1990-01-01')
			and @end between coalesce(p.[travel_from_date],'1990-01-01') and coalesce(p.[travel_until_date],'1990-01-01')
		)
)
	and datediff(day,@start,@end) >=coalesce(p.min_nights,0)
group by
d.fulldate,
p.provider_id,
markets.market_id,
markets.market_name,
rooms.room_id,
rooms.room_name

return 
end
GO
/****** Object:  UserDefinedFunction [dbo].[InitCap]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[InitCap] ( @InputString varchar(4000) ) 
RETURNS VARCHAR(4000)
AS
BEGIN

DECLARE @Index          INT
DECLARE @Char           CHAR(1)
DECLARE @PrevChar       CHAR(1)
DECLARE @OutputString   VARCHAR(255)

SET @OutputString = LOWER(@InputString)
SET @Index = 1

WHILE @Index <= LEN(@InputString)
BEGIN
    SET @Char     = SUBSTRING(@InputString, @Index, 1)
    SET @PrevChar = CASE WHEN @Index = 1 THEN ' '
                         ELSE SUBSTRING(@InputString, @Index - 1, 1)
                    END

    IF @PrevChar IN (' ', ';', ':', '!', '?', ',', '.', '_', '-', '/', '&', '''', '(')
    BEGIN
        IF @PrevChar != '''' OR UPPER(@Char) != 'S'
            SET @OutputString = STUFF(@OutputString, @Index, 1, UPPER(@Char))
    END

    SET @Index = @Index + 1
END

RETURN @OutputString

END

GO
/****** Object:  Table [dbo].[activity]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[activity](
	[activity_id] [int] IDENTITY(100,1) NOT NULL,
	[activity_name] [nvarchar](200) NULL,
	[description] [nvarchar](max) NULL,
	[cancellation_policy] [nvarchar](max) NULL,
	[conditions] [nvarchar](max) NULL,
	[category] [nvarchar](50) NULL,
	[duration] [nvarchar](50) NULL,
	[min_age] [int] NULL,
	[max_child_age] [int] NULL,
	[is_active] [bit] NOT NULL,
	[valid_from] [date] NULL,
	[valid_to] [date] NULL,
	[features] [nvarchar](1000) NULL,
	[includes_meal] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[activity_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[activity_picture]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[activity_picture](
	[picture_id] [int] NOT NULL,
	[activity_id] [int] NOT NULL,
	[display_order] [int] NULL,
	[section] [varchar](15) NULL,
 CONSTRAINT [PK_activity_picture] PRIMARY KEY CLUSTERED 
(
	[activity_id] ASC,
	[picture_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[activity_pricing]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[activity_pricing](
	[activity_pricing_id] [int] IDENTITY(100,1) NOT NULL,
	[activity_id] [int] NOT NULL,
	[currency_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[price_child] [float] NULL,
	[price_adult] [float] NULL,
	[price_child_incl_sic_transfer] [float] NULL,
	[price_adult_incl_sic_transfer] [float] NULL,
	[price_child_incl_private_transfer] [float] NULL,
	[price_adult_incl_private_transfer] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[activity_id] ASC,
	[currency_id] ASC,
	[provider_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[audit]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[audit](
	[audit_id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[entity_type] [nvarchar](100) NULL,
	[entity_id] [int] NULL,
	[operation] [nvarchar](30) NULL,
	[description] [nvarchar](255) NULL,
	[created_by] [int] NULL,
	[create_date] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.AuditEntries] PRIMARY KEY CLUSTERED 
(
	[audit_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[base_price]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[base_price](
	[base_price_id] [int] IDENTITY(100,1) NOT NULL,
	[room_id] [int] NOT NULL,
	[market_id] [int] NOT NULL,
	[meal_plan_id] [int] NOT NULL,
	[start_date] [date] NOT NULL,
	[end_date] [date] NOT NULL,
	[price_single] [float] NULL,
	[price_twin] [float] NULL,
	[price_triple] [float] NULL,
	[price_infant] [float] NULL,
	[price_child] [float] NULL,
	[price_teen] [float] NULL,
	[price_quadruple] [float] NULL,
	[price_without_bed] [float] NULL,
	[price_honeymoon] [float] NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_base_price] PRIMARY KEY CLUSTERED 
(
	[base_price_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[booking]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[booking](
	[booking_id] [int] IDENTITY(100,1) NOT NULL,
	[market_id] [int] NOT NULL,
	[agent_user_id] [int] NOT NULL,
	[agent_name] [nvarchar](100) NULL,
	[booking_name] [varchar](300) NOT NULL,
	[checkin_date] [datetime] NULL,
	[checkout_date] [datetime] NULL,
	[num_nights] [int] NULL,
	[create_date] [datetime] NULL,
	[booking_date] [datetime] NULL,
	[status] [int] NULL,
	[booking_expiry_date] [datetime] NULL,
	[backoffice_user_id] [int] NULL,
	[backoffice_user_name] [nvarchar](100) NULL,
	[booking_ref] [nvarchar](32) NULL,
	[adults] [int] NULL,
	[infants] [int] NULL,
	[children] [int] NULL,
	[teens] [int] NULL,
	[rooms] [int] NULL,
	[phone_number] [nvarchar](20) NULL,
	[email] [nvarchar](75) NULL,
	[provider_id] [int] NULL,
	[discount_amt] [float] NULL,
	[discount_reason] [nvarchar](2000) NULL,
	[discounted_by_user_id] [int] NULL,
	[booking_type] [int] NULL,
	[package_id] [int] NULL,
	[booking_amt] [float] NULL,
	[markup_amt] [float] NULL,
	[credit_amt] [float] NULL,
	[paid_amt] [float] NULL,
	[provider_name] [nvarchar](100) NULL,
	[market_name] [nvarchar](50) NULL,
	[booking_file_number] [nvarchar](100) NULL,
	[package_name] [varchar](100) NULL,
	[free_nights] [int] NULL,
	[remarks] [nvarchar](1000) NULL,
	[is_timelimit] [bit] NOT NULL DEFAULT ((0)),
	[is_new_timelimit] [bit] NOT NULL DEFAULT ((0)),
	[timelimit_date] [date] NULL,
	[currency_code] [char](3) NULL,
	[symbol] [nvarchar](10) NULL,
	[is_credit] [bit] NOT NULL DEFAULT ((0)),
	[compulsory_charges_amt] [float] NULL,
	[charge_name] [nvarchar](100) NULL,
	[cancellation_fee] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[booking_activity]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_activity](
	[booking_activity_id] [int] IDENTITY(100,1) NOT NULL,
	[booking_id] [int] NOT NULL,
	[activity_id] [int] NOT NULL,
	[adults] [int] NULL,
	[children] [int] NULL,
	[price_per_adult] [float] NULL,
	[price_per_child] [float] NULL,
	[total_price] [float] NULL,
	[markup_amt] [float] NULL,
	[is_aftersales] [bit] NOT NULL DEFAULT ((0)),
	[aftersales_admin] [int] NULL,
	[provider_name] [nvarchar](100) NULL,
	[activity_name] [nvarchar](200) NULL,
	[transfer_type] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_activity_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[booking_guest]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[booking_guest](
	[booking_guest_id] [int] IDENTITY(100,1) NOT NULL,
	[booking_room_id] [int] NOT NULL,
	[package_id] [int] NULL,
	[salutation] [varchar](15) NULL,
	[firstname] [varchar](60) NULL,
	[lastname] [varchar](60) NULL,
	[date_of_birth] [date] NULL,
	[nationality] [varchar](50) NULL,
	[age] [int] NULL,
	[guest_type] [int] NULL,
	[arrival_flight_no] [varchar](20) NULL,
	[departure_flight_no] [varchar](20) NULL,
	[arrival_datetime] [datetime] NULL,
	[departure_datetime] [datetime] NULL,
	[price_per_night] [float] NULL,
	[markup_amt] [float] NULL,
	[booking_ref] [varchar](32) NULL,
	[passport_number] [nvarchar](20) NULL,
	[passport_expiry_date] [date] NULL,
	[gender] [int] NULL,
	[booking_id] [int] NULL,
	[package_name] [nvarchar](100) NULL,
	[compulsory_charge_amt] [float] NULL,
	[ground_handling_amt] [float] NULL,
	[promotional_discount_amt] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_guest_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[booking_payment]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_payment](
	[booking_payment_id] [int] IDENTITY(100,1) NOT NULL,
	[booking_id] [int] NOT NULL,
	[payment_amt] [float] NULL,
	[payment_date] [datetime] NULL,
	[payment_method] [int] NULL,
	[payment_details] [nvarchar](1000) NULL,
	[received_by] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_payment_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[booking_proforma]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_proforma](
	[booking_proforma_id] [int] IDENTITY(100,1) NOT NULL,
	[booking_id] [int] NOT NULL,
	[invoice_num] [nvarchar](50) NULL,
	[agent_details] [nvarchar](500) NULL,
	[booking_details] [nvarchar](1500) NULL,
	[reference] [nvarchar](50) NULL,
	[due_date] [date] NULL,
	[sales_category] [nvarchar](200) NULL,
	[sales_description] [nvarchar](1000) NULL,
	[currency] [nvarchar](10) NULL,
	[nett_price_accomodation] [float] NULL,
	[nett_price_supplement] [float] NULL,
	[nett_price_transfer] [float] NULL,
	[nett_price_activity] [float] NULL,
	[nett_price_rental] [float] NULL,
	[nett_price] [float] NULL,
	[handling_fees] [float] NULL,
	[sold_by] [nvarchar](100) NULL,
	[issued_by] [nvarchar](100) NULL,
	[checked_by] [nvarchar](100) NULL,
	[approved_by] [nvarchar](100) NULL,
	[remarks] [nvarchar](1000) NULL,
	[issue_date] [date] NULL,
	[finalized] [bit] NOT NULL DEFAULT ((0)),
	[bankcharges] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_proforma_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[booking_promotion]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_promotion](
	[booking_promotion_id] [int] IDENTITY(100,1) NOT NULL,
	[booking_id] [int] NOT NULL,
	[booking_room_id] [int] NULL,
	[promotion_id] [int] NULL,
	[promotion_name] [nvarchar](100) NULL,
	[promotion_description] [nvarchar](max) NULL,
	[free_nights] [int] NULL,
	[discount] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_promotion_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[booking_rental]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[booking_rental](
	[booking_rental_id] [int] IDENTITY(100,1) NOT NULL,
	[booking_id] [int] NOT NULL,
	[vehicle_id] [int] NOT NULL,
	[booking_guest_id] [int] NULL,
	[num_days] [float] NULL,
	[total_price] [float] NULL,
	[markup_amt] [float] NULL,
	[pickup_date] [datetime] NULL,
	[return_date] [datetime] NULL,
	[pickup_location] [varchar](100) NULL,
	[return_location] [varchar](100) NULL,
	[is_aftersales] [bit] NOT NULL,
	[aftersales_admin] [int] NULL,
	[has_driver] [bit] NOT NULL,
	[provider_name] [nvarchar](100) NULL,
	[vehicle_model] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_rental_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[booking_room]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_room](
	[booking_room_id] [int] IDENTITY(100,1) NOT NULL,
	[booking_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[base_price_id] [int] NOT NULL,
	[meal_plan_id] [int] NULL,
	[room_number] [int] NOT NULL,
	[booking_ref] [nvarchar](32) NULL,
	[adults] [int] NULL,
	[infants] [int] NULL,
	[children] [int] NULL,
	[teens] [int] NULL,
	[room_name] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_room_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[booking_search]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_search](
	[booking_ref] [nvarchar](32) NOT NULL,
	[search_details] [xml] NULL,
	[hotel_id] [int] NULL,
	[timestamp] [datetime] NULL,
	[package_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_ref] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[booking_supplement]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_supplement](
	[booking_supplement_id] [int] IDENTITY(100,1) NOT NULL,
	[booking_id] [int] NOT NULL,
	[supplement_id] [int] NULL,
	[quantity] [int] NULL,
	[price] [float] NULL,
	[booking_room_id] [int] NULL,
	[infants] [int] NULL,
	[children] [int] NULL,
	[teens] [int] NULL,
	[adults] [int] NULL,
	[markup_amt] [float] NULL,
	[supplement_name] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_supplement_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[booking_transfer]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_transfer](
	[booking_transfer_id] [int] IDENTITY(100,1) NOT NULL,
	[booking_id] [int] NOT NULL,
	[transfer_id] [int] NOT NULL,
	[children] [int] NULL,
	[teens] [int] NULL,
	[adults] [int] NULL,
	[total_price] [float] NULL,
	[markup_amt] [float] NULL,
	[is_aftersales] [bit] NOT NULL DEFAULT ((0)),
	[aftersales_admin] [int] NULL,
	[transfer_pricing_id] [int] NULL,
	[provider_name] [nvarchar](100) NULL,
	[transfer_name] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[booking_transfer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[booking_voucher]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking_voucher](
	[booking_voucher_id] [int] IDENTITY(100,1) NOT NULL,
	[booking_id] [int] NOT NULL,
	[booking_file_number] [nvarchar](100) NULL,
	[guest_names] [nvarchar](1000) NULL,
	[num_guest] [int] NULL,
	[flight_details] [nvarchar](200) NULL,
	[hotel_name] [nvarchar](150) NULL,
	[hotel_address] [nvarchar](350) NULL,
	[room_types] [nvarchar](300) NULL,
	[check_in] [nvarchar](100) NULL,
	[check_out] [nvarchar](100) NULL,
	[num_nights] [int] NULL,
	[services_details] [nvarchar](4000) NULL,
	[offer] [nvarchar](150) NULL,
	[remarks] [nvarchar](1000) NULL,
	[cost_per_pax] [nvarchar](500) NULL,
	[total_cost] [nvarchar](150) NULL,
	[note] [nvarchar](2000) NULL,
	[disclaimer] [nvarchar](2000) NULL,
	[package_name] [nvarchar](200) NULL,
	[package_description] [nvarchar](max) NULL,
	[finalized] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[booking_voucher_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[company_info]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[company_info](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NULL,
	[addr_line1] [nvarchar](200) NULL,
	[addr_line2] [nvarchar](200) NULL,
	[addr_line3] [nvarchar](200) NULL,
	[town] [nvarchar](100) NULL,
	[country] [nvarchar](100) NULL,
	[vat_reg_num] [nvarchar](100) NULL,
	[business_reg_num] [nvarchar](100) NULL,
	[phone_num] [nvarchar](20) NULL,
	[fax_num] [nvarchar](20) NULL,
	[email] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[configuration]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[configuration](
	[configuration_id] [int] IDENTITY(100,1) NOT NULL,
	[conf_key] [nvarchar](50) NULL,
	[conf_type] [nvarchar](50) NULL,
	[conf_value] [nvarchar](300) NULL,
	[system_based] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[configuration_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[contact]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[contact](
	[contact_id] [int] IDENTITY(100,1) NOT NULL,
	[provider_id] [int] NULL,
	[salutation] [nvarchar](10) NULL,
	[firstname] [nvarchar](100) NULL,
	[lastname] [nvarchar](300) NULL,
	[job_title] [nvarchar](100) NULL,
	[office_phone] [nvarchar](50) NULL,
	[mobile] [nvarchar](50) NULL,
	[email] [nvarchar](100) NULL,
	[notes] [nvarchar](max) NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_contact] PRIMARY KEY CLUSTERED 
(
	[contact_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[country]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[country](
	[country_id] [int] IDENTITY(100,1) NOT NULL,
	[country_code] [nchar](2) NOT NULL,
	[name] [nvarchar](50) NULL,
	[currency] [nvarchar](3) NULL,
	[currency_name] [nvarchar](30) NULL,
	[is_independent] [nvarchar](30) NULL,
	[language] [char](2) NULL,
	[nationality] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[country_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[currency]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[currency](
	[currency_id] [int] IDENTITY(100,1) NOT NULL,
	[currency_code] [char](3) NOT NULL,
	[currency_desc] [nvarchar](50) NULL,
	[symbol] [nvarchar](10) NULL,
	[code_desc]  AS (([currency_code]+' - ')+[currency_desc]),
	[is_active] [bit] NOT NULL DEFAULT ((1)),
	[exchange_rate] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[currency_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[date]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[date](
	[fulldate] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[fulldate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[facility]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[facility](
	[facility_id] [int] IDENTITY(100,1) NOT NULL,
	[provider_id] [int] NOT NULL,
	[name] [nvarchar](100) NULL,
	[type] [nvarchar](100) NULL,
	[description] [nvarchar](300) NULL,
	[opening_hours] [nvarchar](300) NULL,
	[conditions] [nvarchar](300) NULL,
 CONSTRAINT [PK_facility] PRIMARY KEY CLUSTERED 
(
	[facility_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[feature]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feature](
	[feature_id] [int] IDENTITY(100,1) NOT NULL,
	[category] [nvarchar](300) NULL,
	[description] [nvarchar](300) NULL,
	[feature_type] [nchar](1) NULL,
	[name] [nvarchar](100) NOT NULL,
	[icon] [nvarchar](100) NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_feature] PRIMARY KEY CLUSTERED 
(
	[feature_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[featured_hotel]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[featured_hotel](
	[featured_hotel_id] [int] IDENTITY(100,1) NOT NULL,
	[provider_id] [int] NOT NULL,
	[title] [nvarchar](300) NULL,
	[description] [nvarchar](max) NULL,
	[section] [nvarchar](30) NOT NULL,
	[from_date] [date] NULL,
	[to_date] [date] NULL,
	[picture_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[section] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[file_upload]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[file_upload](
	[file_id] [int] IDENTITY(100,1) NOT NULL,
	[provider_id] [int] NULL,
	[filename] [nvarchar](200) NULL,
	[title] [nvarchar](100) NULL,
	[description] [nvarchar](500) NULL,
	[filetype] [nvarchar](50) NULL,
	[file_guid] [uniqueidentifier] NULL,
	[display_order] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[file_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[guest_type]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[guest_type](
	[guest_type_id] [int] IDENTITY(100,1) NOT NULL,
	[provider_id] [int] NOT NULL,
	[infant_min_age] [int] NULL,
	[infant_max_age] [int] NULL,
	[child_min_age] [int] NULL,
	[child_max_age] [int] NULL,
	[teen_min_age] [int] NULL,
	[teen_max_age] [int] NULL,
	[adult_min_age] [int] NULL,
	[adult_max_age] [int] NULL,
	[senior_min_age] [int] NULL,
	[senior_max_age] [int] NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[guest_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[hotel_feature]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[hotel_feature](
	[feature_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[quantity] [int] NULL,
 CONSTRAINT [PK_hotel_feature] PRIMARY KEY CLUSTERED 
(
	[feature_id] ASC,
	[provider_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[log]    Script Date: 05-Aug-17 9:14:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[log](
	[log_id] [int] IDENTITY(100,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[operation] [nvarchar](30) NULL,
	[item_type] [nvarchar](50) NULL,
	[item_id] [int] NULL,
	[description] [nvarchar](300) NULL,
	[created_on] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[log_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[market]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[market](
	[market_id] [int] IDENTITY(100,1) NOT NULL,
	[market_name] [nvarchar](50) NULL,
	[currency_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
	[is_default] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[market_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[market_country]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[market_country](
	[market_id] [int] NOT NULL,
	[country_id] [int] NOT NULL,
 CONSTRAINT [PK_market_country] PRIMARY KEY CLUSTERED 
(
	[country_id] ASC,
	[market_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[meal_plan]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[meal_plan](
	[meal_plan_id] [int] IDENTITY(100,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](max) NULL,
	[meal_order] [smallint] NULL,
	[code] [nchar](2) NOT NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[meal_plan_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[note]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[note](
	[note_id] [int] IDENTITY(100,1) NOT NULL,
	[item_type] [nvarchar](30) NULL,
	[item_id] [int] NULL,
	[category] [nvarchar](100) NULL,
	[detail] [nvarchar](1000) NULL,
	[user_id] [int] NOT NULL,
	[create_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[note_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[occupancy]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[occupancy](
	[occupancy_id] [int] IDENTITY(100,1) NOT NULL,
	[name] [nvarchar](30) NULL,
	[description] [nvarchar](max) NULL,
	[infant] [int] NULL,
	[child] [int] NULL,
	[teen] [int] NULL,
	[adult] [int] NULL,
	[senior] [int] NULL,
	[without_bed] [int] NULL,
 CONSTRAINT [PK_occupancy] PRIMARY KEY CLUSTERED 
(
	[occupancy_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[package]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[package](
	[package_id] [int] IDENTITY(100,1) NOT NULL,
	[provider_id] [int] NOT NULL,
	[market_id] [int] NOT NULL DEFAULT ((100)),
	[valid_from] [date] NOT NULL,
	[valid_to] [date] NOT NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[number_nights] [int] NULL,
	[price_ground_handling_adult] [float] NULL,
	[price_ground_handling_child] [float] NULL,
	[price_ground_handling_teen] [float] NULL,
	[contracted_offer] [nvarchar](max) NULL,
	[cancellation_policy] [nvarchar](max) NULL,
	[apply_all_rooms] [bit] NOT NULL DEFAULT ((0)),
	[is_active] [bit] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_package] PRIMARY KEY CLUSTERED 
(
	[package_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[package_activity]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[package_activity](
	[package_id] [int] NOT NULL,
	[activity_id] [int] NOT NULL,
 CONSTRAINT [PK_package_activity] PRIMARY KEY CLUSTERED 
(
	[package_id] ASC,
	[activity_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[package_room]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[package_room](
	[package_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
 CONSTRAINT [PK_package_room] PRIMARY KEY CLUSTERED 
(
	[package_id] ASC,
	[room_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[package_transfer]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[package_transfer](
	[package_id] [int] NOT NULL,
	[transfer_id] [int] NOT NULL,
 CONSTRAINT [PK_package_transfer] PRIMARY KEY CLUSTERED 
(
	[package_id] ASC,
	[transfer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[picture]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[picture](
	[picture_id] [int] IDENTITY(100,1) NOT NULL,
	[file_name] [nvarchar](200) NULL,
	[title] [nvarchar](100) NULL,
	[description] [nvarchar](150) NULL,
	[picture_guid] [uniqueidentifier] NULL,
	[display_order] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[picture_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[policy]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[policy](
	[policy_id] [int] IDENTITY(100,1) NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](300) NULL,
	[type] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[policy_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[promo_set_promotions]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[promo_set_promotions](
	[promotion_set_id] [int] NOT NULL,
	[promotion_id] [int] NOT NULL,
 CONSTRAINT [PK_promo_set_promo] PRIMARY KEY CLUSTERED 
(
	[promotion_set_id] ASC,
	[promotion_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[promotion]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[promotion](
	[promotion_id] [int] IDENTITY(100,1) NOT NULL,
	[provider_id] [int] NOT NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[code] [nvarchar](100) NULL,
	[contracted_offer] [nvarchar](max) NULL,
	[cancellation_policy] [nvarchar](max) NULL,
	[book_before_days_min] [int] NULL,
	[book_before_days_max] [int] NULL,
	[book_from_date] [date] NULL,
	[book_until_date] [date] NULL,
	[travel_from_date] [date] NULL,
	[travel_until_date] [date] NULL,
	[valid_from] [date] NOT NULL,
	[valid_to] [date] NOT NULL,
	[min_nights] [int] NULL,
	[discount] [float] NOT NULL DEFAULT ((1.0)),
	[apply_all_markets] [bit] NOT NULL DEFAULT ((0)),
	[apply_all_rooms] [bit] NOT NULL DEFAULT ((0)),
	[is_active] [bit] NOT NULL DEFAULT ((1)),
	[is_honeymoon] [bit] NOT NULL DEFAULT ((0)),
	[free_nights] [int] NULL,
	[promotion_group_id] [int] NULL,
	[flat_single] [float] NULL,
	[flat_twin] [float] NULL,
	[flat_triple] [float] NULL,
	[flat_quadruple] [float] NULL,
	[flat_child1] [float] NULL,
	[flat_child2] [float] NULL,
	[flat_teen1] [float] NULL,
	[flat_teen2] [float] NULL,
 CONSTRAINT [PK_promotion] PRIMARY KEY CLUSTERED 
(
	[promotion_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[promotion_market]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[promotion_market](
	[promotion_id] [int] NOT NULL,
	[market_id] [int] NOT NULL,
 CONSTRAINT [PK_promo_market] PRIMARY KEY CLUSTERED 
(
	[promotion_id] ASC,
	[market_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[promotion_room]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[promotion_room](
	[promotion_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
 CONSTRAINT [PK_promo_room] PRIMARY KEY CLUSTERED 
(
	[promotion_id] ASC,
	[room_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[promotion_set]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[promotion_set](
	[promotion_set_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[description] [nvarchar](300) NULL,
	[provider_id] [int] NOT NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
PRIMARY KEY CLUSTERED 
(
	[promotion_set_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[provider]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[provider](
	[provider_id] [int] IDENTITY(100,1) NOT NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[location] [nvarchar](100) NULL,
	[company] [nvarchar](100) NULL,
	[website] [nvarchar](100) NULL,
	[commission_rate] [float] NULL,
	[street1] [nvarchar](100) NULL,
	[street2] [nvarchar](100) NULL,
	[town] [nvarchar](100) NULL,
	[country] [nvarchar](100) NULL,
	[phone1] [nvarchar](50) NULL,
	[phone2] [nvarchar](50) NULL,
	[fax] [nvarchar](50) NULL,
	[email] [nvarchar](75) NULL,
	[google_location] [nvarchar](50) NULL,
	[provider_type] [char](1) NOT NULL,
	[postcode] [varchar](20) NULL,
	[is_active] [bit] NOT NULL,
	[star_rating] [int] NULL,
 CONSTRAINT [PK_provider] PRIMARY KEY CLUSTERED 
(
	[provider_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[provider_picture]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[provider_picture](
	[picture_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[display_order] [int] NULL,
	[section] [varchar](15) NULL,
 CONSTRAINT [PK_provider_picture] PRIMARY KEY CLUSTERED 
(
	[provider_id] ASC,
	[picture_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[provider_policy]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[provider_policy](
	[provider_id] [int] NOT NULL,
	[policy_id] [int] NOT NULL,
 CONSTRAINT [PK_provider_policy] PRIMARY KEY CLUSTERED 
(
	[provider_id] ASC,
	[policy_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[room]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[room](
	[room_id] [int] IDENTITY(100,1) NOT NULL,
	[provider_id] [int] NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
	[min_adult] [int] NULL,
	[max_adult] [int] NULL,
	[room_size] [nvarchar](30) NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_room] PRIMARY KEY CLUSTERED 
(
	[room_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[room_feature]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[room_feature](
	[feature_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[quantity] [int] NULL,
 CONSTRAINT [PK_room_feature] PRIMARY KEY CLUSTERED 
(
	[feature_id] ASC,
	[room_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[room_occupancy]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[room_occupancy](
	[room_id] [int] NOT NULL,
	[occupancy_id] [int] NOT NULL,
 CONSTRAINT [PK_room_occupancy] PRIMARY KEY CLUSTERED 
(
	[room_id] ASC,
	[occupancy_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[room_picture]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[room_picture](
	[picture_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[display_order] [int] NULL,
	[section] [varchar](15) NULL,
 CONSTRAINT [PK_room_picture] PRIMARY KEY CLUSTERED 
(
	[room_id] ASC,
	[picture_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[room_unavailability]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[room_unavailability](
	[room_unavailability_id] [int] IDENTITY(100,1) NOT NULL,
	[room_id] [int] NOT NULL,
	[start_date] [date] NOT NULL,
	[end_date] [date] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[room_unavailability_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[supplement]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[supplement](
	[supplement_id] [int] IDENTITY(100,1) NOT NULL,
	[provider_id] [int] NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[is_per_person] [bit] NOT NULL DEFAULT ((0)),
	[is_per_room] [bit] NOT NULL DEFAULT ((0)),
	[is_per_night] [bit] NOT NULL DEFAULT ((0)),
	[is_per_item] [bit] NOT NULL DEFAULT ((0)),
	[is_compulsory] [bit] NOT NULL DEFAULT ((0)),
	[valid_all_rooms] [bit] NOT NULL DEFAULT ((0)),
	[is_active] [bit] NOT NULL DEFAULT ((1)),
	[meal_plan_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[supplement_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[supplement_pricing]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[supplement_pricing](
	[supplement_pricing_id] [int] IDENTITY(100,1) NOT NULL,
	[supplement_id] [int] NOT NULL,
	[market_id] [int] NOT NULL,
	[valid_from] [datetime] NULL DEFAULT (getdate()),
	[valid_to] [datetime] NULL DEFAULT ('2999-12-31'),
	[price_per_item] [float] NULL,
	[price_infant] [float] NULL,
	[price_child] [float] NULL,
	[price_teen] [float] NULL,
	[price_adult] [float] NULL,
	[price_senior] [float] NULL,
	[price_honeymoon] [float] NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_supplement_pricing] PRIMARY KEY CLUSTERED 
(
	[supplement_pricing_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[supplement_room]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[supplement_room](
	[supplement_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
 CONSTRAINT [PK_supplement_room] PRIMARY KEY CLUSTERED 
(
	[supplement_id] ASC,
	[room_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[transfer]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transfer](
	[transfer_id] [int] IDENTITY(100,1) NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[description] [nvarchar](max) NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
	[category] [nvarchar](100) NULL,
	[is_upgrade] [bit] NOT NULL DEFAULT ((0)),
PRIMARY KEY CLUSTERED 
(
	[transfer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[transfer_pricing]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transfer_pricing](
	[transfer_pricing_id] [int] IDENTITY(100,1) NOT NULL,
	[transfer_id] [int] NOT NULL,
	[currency_id] [int] NOT NULL,
	[child_price] [float] NULL,
	[teen_price] [float] NULL,
	[adult_price] [float] NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
	[provider_id] [int] NULL,
	[is_selling_price] [bit] NOT NULL,
 CONSTRAINT [PK_transfer_pricing] PRIMARY KEY CLUSTERED 
(
	[transfer_pricing_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[user_id] [int] IDENTITY(100,1) NOT NULL,
	[title] [nvarchar](20) NULL,
	[firstname] [nvarchar](50) NULL,
	[lastname] [nvarchar](50) NULL,
	[date_of_birth] [date] NULL,
	[gender] [int] NULL,
	[nationality] [nvarchar](60) NULL,
	[role] [int] NOT NULL,
	[officephone] [nvarchar](20) NULL,
	[mobile] [nvarchar](20) NULL,
	[email] [nvarchar](100) NULL,
	[company] [nvarchar](100) NULL,
	[job_title] [nvarchar](100) NULL,
	[password] [nvarchar](200) NULL,
	[markup_percentage] [float] NULL,
	[markup_fixed] [float] NULL,
	[username] [nvarchar](50) NULL,
	[credit_limit] [float] NULL,
	[credit_used] [float] NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
	[temp_password_expiry] [date] NULL,
	[temp_password] [nvarchar](200) NULL,
	[is_credit_based] [bit] NOT NULL DEFAULT ((0)),
	[currency_id] [int] NOT NULL DEFAULT ((100)),
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[user_picture]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user_picture](
	[picture_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[display_order] [int] NULL,
	[section] [varchar](15) NULL,
 CONSTRAINT [PK_user_picture] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[picture_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vehicle]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vehicle](
	[vehicle_id] [int] IDENTITY(100,1) NOT NULL,
	[provider_id] [int] NOT NULL,
	[service_name] [nvarchar](200) NULL,
	[vehicle_model] [nvarchar](100) NULL,
	[vehicle_type] [nvarchar](50) NULL,
	[service_description] [nvarchar](max) NULL,
	[max_seat] [int] NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
	[is_transfer] [bit] NOT NULL DEFAULT ((0)),
	[includes_driver] [bit] NOT NULL CONSTRAINT [DF_vehicle_includes_driver]  DEFAULT ((0)),
	[engine_size] [smallint] NULL,
	[year] [smallint] NULL,
	[is_automatic] [bit] NOT NULL DEFAULT ((0)),
	[doors] [tinyint] NULL,
	[fuel_type] [tinyint] NULL,
PRIMARY KEY CLUSTERED 
(
	[vehicle_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[vehicle_feature]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vehicle_feature](
	[feature_id] [int] NOT NULL,
	[vehicle_id] [int] NOT NULL,
	[quantity] [int] NULL,
 CONSTRAINT [PK_vehicle_feature] PRIMARY KEY CLUSTERED 
(
	[feature_id] ASC,
	[vehicle_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[vehicle_picture]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[vehicle_picture](
	[picture_id] [int] NOT NULL,
	[vehicle_id] [int] NOT NULL,
	[display_order] [int] NULL,
	[section] [varchar](15) NULL,
 CONSTRAINT [PK_vehicle_picture] PRIMARY KEY CLUSTERED 
(
	[vehicle_id] ASC,
	[picture_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[vehicle_pricing]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vehicle_pricing](
	[vehicle_pricing_id] [int] IDENTITY(100,1) NOT NULL,
	[vehicle_id] [int] NOT NULL,
	[currency_id] [int] NOT NULL,
	[half_day_price_with_driver] [float] NULL,
	[half_day_price] [float] NULL,
	[daily_price_with_driver] [float] NULL,
	[daily_price] [float] NULL,
	[is_active] [bit] NOT NULL DEFAULT ((1)),
 CONSTRAINT [PK_vehicle_pricing] PRIMARY KEY CLUSTERED 
(
	[vehicle_pricing_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [del].[__RefactorLog_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[__RefactorLog_hist](
	[OperationKey] [uniqueidentifier] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[activity_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[activity_hist](
	[activity_id] [int] NOT NULL,
	[activity_name] [nvarchar](200) NULL,
	[description] [nvarchar](max) NULL,
	[cancellation_policy] [nvarchar](max) NULL,
	[conditions] [nvarchar](max) NULL,
	[category] [nvarchar](50) NULL,
	[duration] [nvarchar](50) NULL,
	[min_age] [int] NULL,
	[max_child_age] [int] NULL,
	[is_active] [bit] NOT NULL,
	[valid_from] [date] NULL,
	[valid_to] [date] NULL,
	[features] [nvarchar](1000) NULL,
	[includes_meal] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[activity_picture_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[activity_picture_hist](
	[picture_id] [int] NOT NULL,
	[activity_id] [int] NOT NULL,
	[display_order] [int] NULL,
	[section] [varchar](15) NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[activity_pricing_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[activity_pricing_hist](
	[activity_pricing_id] [int] NOT NULL,
	[activity_id] [int] NOT NULL,
	[currency_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[price_child] [float] NULL,
	[price_adult] [float] NULL,
	[price_child_incl_sic_transfer] [float] NULL,
	[price_adult_incl_sic_transfer] [float] NULL,
	[price_child_incl_private_transfer] [float] NULL,
	[price_adult_incl_private_transfer] [float] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[audit_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[audit_hist](
	[audit_id] [bigint] NOT NULL,
	[user_id] [int] NULL,
	[entity_type] [nvarchar](100) NULL,
	[entity_id] [int] NULL,
	[operation] [nvarchar](30) NULL,
	[description] [nvarchar](255) NULL,
	[created_by] [int] NULL,
	[create_date] [datetime] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[base_price_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[base_price_hist](
	[base_price_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[market_id] [int] NOT NULL,
	[meal_plan_id] [int] NOT NULL,
	[start_date] [date] NOT NULL,
	[end_date] [date] NOT NULL,
	[price_single] [float] NULL,
	[price_twin] [float] NULL,
	[price_triple] [float] NULL,
	[price_infant] [float] NULL,
	[price_child] [float] NULL,
	[price_teen] [float] NULL,
	[price_quadruple] [float] NULL,
	[price_without_bed] [float] NULL,
	[price_honeymoon] [float] NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[booking_activity_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[booking_activity_hist](
	[booking_activity_id] [int] NOT NULL,
	[booking_id] [int] NOT NULL,
	[activity_id] [int] NOT NULL,
	[adults] [int] NULL,
	[children] [int] NULL,
	[price_per_adult] [float] NULL,
	[price_per_child] [float] NULL,
	[total_price] [float] NULL,
	[markup_amt] [float] NULL,
	[is_aftersales] [bit] NOT NULL,
	[aftersales_admin] [int] NULL,
	[provider_name] [nvarchar](100) NULL,
	[activity_name] [nvarchar](200) NULL,
	[transfer_type] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[booking_guest_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[booking_guest_hist](
	[booking_guest_id] [int] NOT NULL,
	[booking_room_id] [int] NOT NULL,
	[package_id] [int] NULL,
	[salutation] [varchar](15) NULL,
	[firstname] [varchar](60) NULL,
	[lastname] [varchar](60) NULL,
	[date_of_birth] [date] NULL,
	[nationality] [varchar](50) NULL,
	[age] [int] NULL,
	[guest_type] [int] NULL,
	[arrival_flight_no] [varchar](20) NULL,
	[departure_flight_no] [varchar](20) NULL,
	[arrival_datetime] [datetime] NULL,
	[departure_datetime] [datetime] NULL,
	[price_per_night] [float] NULL,
	[markup_amt] [float] NULL,
	[booking_ref] [varchar](32) NULL,
	[passport_number] [nvarchar](20) NULL,
	[passport_expiry_date] [date] NULL,
	[gender] [int] NULL,
	[booking_id] [int] NULL,
	[package_name] [nvarchar](100) NULL,
	[compulsory_charge_amt] [float] NULL,
	[ground_handling_amt] [float] NULL,
	[promotional_discount_amt] [float] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[booking_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[booking_hist](
	[booking_id] [int] NOT NULL,
	[market_id] [int] NOT NULL,
	[agent_user_id] [int] NOT NULL,
	[agent_name] [nvarchar](100) NULL,
	[booking_name] [varchar](300) NOT NULL,
	[checkin_date] [datetime] NULL,
	[checkout_date] [datetime] NULL,
	[num_nights] [int] NULL,
	[create_date] [datetime] NULL,
	[booking_date] [datetime] NULL,
	[status] [int] NULL,
	[booking_expiry_date] [datetime] NULL,
	[backoffice_user_id] [int] NULL,
	[backoffice_user_name] [nvarchar](100) NULL,
	[booking_ref] [nvarchar](32) NULL,
	[adults] [int] NULL,
	[infants] [int] NULL,
	[children] [int] NULL,
	[teens] [int] NULL,
	[rooms] [int] NULL,
	[phone_number] [nvarchar](20) NULL,
	[email] [nvarchar](75) NULL,
	[provider_id] [int] NULL,
	[discount_amt] [float] NULL,
	[discount_reason] [nvarchar](2000) NULL,
	[discounted_by_user_id] [int] NULL,
	[booking_type] [int] NULL,
	[package_id] [int] NULL,
	[booking_amt] [float] NULL,
	[markup_amt] [float] NULL,
	[credit_amt] [float] NULL,
	[paid_amt] [float] NULL,
	[provider_name] [nvarchar](100) NULL,
	[market_name] [nvarchar](50) NULL,
	[booking_file_number] [nvarchar](100) NULL,
	[package_name] [varchar](100) NULL,
	[free_nights] [int] NULL,
	[remarks] [nvarchar](1000) NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[booking_payment_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[booking_payment_hist](
	[booking_payment_id] [int] NOT NULL,
	[booking_id] [int] NOT NULL,
	[payment_amt] [float] NULL,
	[payment_date] [datetime] NULL,
	[payment_method] [int] NULL,
	[payment_details] [nvarchar](1000) NULL,
	[received_by] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[booking_proforma_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[booking_proforma_hist](
	[booking_proforma_id] [int] NOT NULL,
	[booking_id] [int] NOT NULL,
	[invoice_num] [nvarchar](50) NULL,
	[agent_details] [nvarchar](500) NULL,
	[booking_details] [nvarchar](1500) NULL,
	[reference] [nvarchar](50) NULL,
	[due_date] [date] NULL,
	[sales_category] [nvarchar](200) NULL,
	[sales_description] [nvarchar](1000) NULL,
	[currency] [nvarchar](10) NULL,
	[nett_price_accomodation] [float] NULL,
	[nett_price_supplement] [float] NULL,
	[nett_price_transfer] [float] NULL,
	[nett_price_activity] [float] NULL,
	[nett_price_rental] [float] NULL,
	[nett_price] [float] NULL,
	[handling_fees] [float] NULL,
	[sold_by] [nvarchar](100) NULL,
	[issued_by] [nvarchar](100) NULL,
	[checked_by] [nvarchar](100) NULL,
	[approved_by] [nvarchar](100) NULL,
	[remarks] [nvarchar](1000) NULL,
	[issue_date] [date] NULL,
	[finalized] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[booking_promotion_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[booking_promotion_hist](
	[booking_promotion_id] [int] NOT NULL,
	[booking_id] [int] NOT NULL,
	[booking_room_id] [int] NULL,
	[promotion_id] [int] NULL,
	[promotion_name] [nvarchar](100) NULL,
	[promotion_description] [nvarchar](max) NULL,
	[free_nights] [int] NULL,
	[discount] [float] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[booking_rental_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[booking_rental_hist](
	[booking_rental_id] [int] NOT NULL,
	[booking_id] [int] NOT NULL,
	[vehicle_id] [int] NOT NULL,
	[booking_guest_id] [int] NULL,
	[num_days] [float] NULL,
	[total_price] [float] NULL,
	[markup_amt] [float] NULL,
	[pickup_date] [datetime] NULL,
	[return_date] [datetime] NULL,
	[pickup_location] [varchar](100) NULL,
	[return_location] [varchar](100) NULL,
	[is_aftersales] [bit] NOT NULL,
	[aftersales_admin] [int] NULL,
	[has_driver] [bit] NOT NULL,
	[provider_name] [nvarchar](100) NULL,
	[vehicle_model] [nvarchar](100) NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[booking_room_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[booking_room_hist](
	[booking_room_id] [int] NOT NULL,
	[booking_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[base_price_id] [int] NOT NULL,
	[meal_plan_id] [int] NULL,
	[room_number] [int] NOT NULL,
	[booking_ref] [nvarchar](32) NULL,
	[adults] [int] NULL,
	[infants] [int] NULL,
	[children] [int] NULL,
	[teens] [int] NULL,
	[room_name] [nvarchar](50) NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[booking_search_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[booking_search_hist](
	[booking_ref] [nvarchar](32) NOT NULL,
	[search_details] [xml] NULL,
	[hotel_id] [int] NULL,
	[timestamp] [datetime] NULL,
	[package_id] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[booking_supplement_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[booking_supplement_hist](
	[booking_supplement_id] [int] NOT NULL,
	[booking_id] [int] NOT NULL,
	[supplement_id] [int] NULL,
	[quantity] [int] NULL,
	[price] [float] NULL,
	[booking_room_id] [int] NULL,
	[infants] [int] NULL,
	[children] [int] NULL,
	[teens] [int] NULL,
	[adults] [int] NULL,
	[markup_amt] [float] NULL,
	[supplement_name] [nvarchar](100) NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[booking_transfer_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[booking_transfer_hist](
	[booking_transfer_id] [int] NOT NULL,
	[booking_id] [int] NOT NULL,
	[transfer_id] [int] NOT NULL,
	[children] [int] NULL,
	[teens] [int] NULL,
	[adults] [int] NULL,
	[total_price] [float] NULL,
	[markup_amt] [float] NULL,
	[is_aftersales] [bit] NOT NULL,
	[aftersales_admin] [int] NULL,
	[transfer_pricing_id] [int] NULL,
	[provider_name] [nvarchar](100) NULL,
	[transfer_name] [nvarchar](100) NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[booking_voucher_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[booking_voucher_hist](
	[booking_voucher_id] [int] NOT NULL,
	[booking_id] [int] NOT NULL,
	[booking_file_number] [nvarchar](100) NULL,
	[guest_names] [nvarchar](1000) NULL,
	[num_guest] [int] NULL,
	[flight_details] [nvarchar](200) NULL,
	[hotel_name] [nvarchar](150) NULL,
	[hotel_address] [nvarchar](350) NULL,
	[room_types] [nvarchar](300) NULL,
	[check_in] [nvarchar](100) NULL,
	[check_out] [nvarchar](100) NULL,
	[num_nights] [int] NULL,
	[services_details] [nvarchar](4000) NULL,
	[offer] [nvarchar](150) NULL,
	[remarks] [nvarchar](1000) NULL,
	[cost_per_pax] [nvarchar](500) NULL,
	[total_cost] [nvarchar](150) NULL,
	[note] [nvarchar](2000) NULL,
	[disclaimer] [nvarchar](2000) NULL,
	[package_name] [nvarchar](200) NULL,
	[package_description] [nvarchar](max) NULL,
	[finalized] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[company_info_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[company_info_hist](
	[Id] [int] NOT NULL,
	[name] [nvarchar](100) NULL,
	[addr_line1] [nvarchar](200) NULL,
	[addr_line2] [nvarchar](200) NULL,
	[addr_line3] [nvarchar](200) NULL,
	[town] [nvarchar](100) NULL,
	[country] [nvarchar](100) NULL,
	[vat_reg_num] [nvarchar](100) NULL,
	[business_reg_num] [nvarchar](100) NULL,
	[phone_num] [nvarchar](20) NULL,
	[fax_num] [nvarchar](20) NULL,
	[email] [nvarchar](100) NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[configuration_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[configuration_hist](
	[configuration_id] [int] NOT NULL,
	[conf_key] [nvarchar](50) NULL,
	[conf_type] [nvarchar](50) NULL,
	[conf_value] [nvarchar](300) NULL,
	[system_based] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[contact_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[contact_hist](
	[contact_id] [int] NOT NULL,
	[provider_id] [int] NULL,
	[salutation] [nvarchar](10) NULL,
	[firstname] [nvarchar](100) NULL,
	[lastname] [nvarchar](300) NULL,
	[job_title] [nvarchar](100) NULL,
	[office_phone] [nvarchar](50) NULL,
	[mobile] [nvarchar](50) NULL,
	[email] [nvarchar](100) NULL,
	[notes] [nvarchar](max) NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[country_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[country_hist](
	[country_id] [int] NOT NULL,
	[country_code] [nchar](2) NOT NULL,
	[name] [nvarchar](50) NULL,
	[currency] [nvarchar](3) NULL,
	[currency_name] [nvarchar](30) NULL,
	[is_independent] [nvarchar](30) NULL,
	[language] [char](2) NULL,
	[nationality] [nvarchar](100) NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[currency_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[currency_hist](
	[currency_id] [int] NOT NULL,
	[currency_code] [char](3) NOT NULL,
	[currency_desc] [nvarchar](50) NULL,
	[symbol] [nvarchar](10) NULL,
	[code_desc] [nvarchar](56) NULL,
	[is_active] [bit] NOT NULL,
	[exchange_rate] [float] NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[date_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[date_hist](
	[fulldate] [date] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[facility_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[facility_hist](
	[facility_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[name] [nvarchar](100) NULL,
	[type] [nvarchar](100) NULL,
	[description] [nvarchar](300) NULL,
	[opening_hours] [nvarchar](300) NULL,
	[conditions] [nvarchar](300) NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[feature_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[feature_hist](
	[feature_id] [int] NOT NULL,
	[category] [nvarchar](300) NULL,
	[description] [nvarchar](300) NULL,
	[feature_type] [nchar](1) NULL,
	[name] [nvarchar](100) NOT NULL,
	[icon] [nvarchar](100) NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[featured_hotel_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[featured_hotel_hist](
	[featured_hotel_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[title] [nvarchar](300) NULL,
	[description] [nvarchar](max) NULL,
	[section] [nvarchar](30) NOT NULL,
	[from_date] [date] NULL,
	[to_date] [date] NULL,
	[picture_id] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[file_upload_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[file_upload_hist](
	[file_id] [int] NOT NULL,
	[provider_id] [int] NULL,
	[filename] [nvarchar](200) NULL,
	[title] [nvarchar](100) NULL,
	[description] [nvarchar](500) NULL,
	[filetype] [nvarchar](50) NULL,
	[file_guid] [uniqueidentifier] NULL,
	[display_order] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[guest_type_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[guest_type_hist](
	[guest_type_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[infant_min_age] [int] NULL,
	[infant_max_age] [int] NULL,
	[child_min_age] [int] NULL,
	[child_max_age] [int] NULL,
	[teen_min_age] [int] NULL,
	[teen_max_age] [int] NULL,
	[adult_min_age] [int] NULL,
	[adult_max_age] [int] NULL,
	[senior_min_age] [int] NULL,
	[senior_max_age] [int] NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[hotel_feature_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[hotel_feature_hist](
	[feature_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[quantity] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[log_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[log_hist](
	[log_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[operation] [nvarchar](30) NULL,
	[item_type] [nvarchar](50) NULL,
	[item_id] [int] NULL,
	[description] [nvarchar](300) NULL,
	[created_on] [datetime] NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[market_country_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[market_country_hist](
	[market_id] [int] NOT NULL,
	[country_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[market_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[market_hist](
	[market_id] [int] NOT NULL,
	[market_name] [nvarchar](50) NULL,
	[currency_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[is_active] [bit] NOT NULL,
	[is_default] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[meal_plan_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[meal_plan_hist](
	[meal_plan_id] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[description] [nvarchar](max) NULL,
	[meal_order] [smallint] NULL,
	[code] [nchar](2) NOT NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[note_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[note_hist](
	[note_id] [int] NOT NULL,
	[item_type] [nvarchar](30) NULL,
	[item_id] [int] NULL,
	[category] [nvarchar](100) NULL,
	[detail] [nvarchar](1000) NULL,
	[user_id] [int] NOT NULL,
	[create_date] [datetime] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[occupancy_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[occupancy_hist](
	[occupancy_id] [int] NOT NULL,
	[name] [nvarchar](30) NULL,
	[description] [nvarchar](max) NULL,
	[infant] [int] NULL,
	[child] [int] NULL,
	[teen] [int] NULL,
	[adult] [int] NULL,
	[senior] [int] NULL,
	[without_bed] [int] NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[occupant_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[occupant_hist](
	[occupant_id] [int] NOT NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[min_adult] [int] NULL,
	[ground_handling_price] [float] NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[package_activity_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[package_activity_hist](
	[package_id] [int] NOT NULL,
	[activity_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[package_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[package_hist](
	[package_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[market_id] [int] NOT NULL,
	[valid_from] [date] NOT NULL,
	[valid_to] [date] NOT NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[number_nights] [int] NULL,
	[price_ground_handling_adult] [float] NULL,
	[price_ground_handling_child] [float] NULL,
	[price_ground_handling_teen] [float] NULL,
	[contracted_offer] [nvarchar](max) NULL,
	[cancellation_policy] [nvarchar](max) NULL,
	[apply_all_rooms] [bit] NOT NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[package_room_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[package_room_hist](
	[package_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[package_transfer_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[package_transfer_hist](
	[package_id] [int] NOT NULL,
	[transfer_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[picture_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[picture_hist](
	[picture_id] [int] NOT NULL,
	[file_name] [nvarchar](200) NULL,
	[title] [nvarchar](100) NULL,
	[description] [nvarchar](150) NULL,
	[picture_guid] [uniqueidentifier] NULL,
	[display_order] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[policy_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[policy_hist](
	[policy_id] [int] NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](300) NULL,
	[type] [nvarchar](100) NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[promo_set_promotions_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[promo_set_promotions_hist](
	[promotion_set_id] [int] NOT NULL,
	[promotion_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[promotion_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[promotion_hist](
	[promotion_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[contracted_offer] [nvarchar](max) NULL,
	[cancellation_policy] [nvarchar](max) NULL,
	[book_before_days_min] [int] NULL,
	[book_before_days_max] [int] NULL,
	[book_from_date] [date] NULL,
	[book_until_date] [date] NULL,
	[travel_from_date] [date] NULL,
	[travel_until_date] [date] NULL,
	[valid_from] [date] NOT NULL,
	[valid_to] [date] NOT NULL,
	[min_nights] [int] NULL,
	[discount] [float] NOT NULL,
	[apply_all_markets] [bit] NOT NULL,
	[apply_all_rooms] [bit] NOT NULL,
	[is_active] [bit] NOT NULL,
	[is_honeymoon] [bit] NOT NULL,
	[free_nights] [int] NULL,
	[promotion_group_id] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[promotion_market_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[promotion_market_hist](
	[promotion_id] [int] NOT NULL,
	[market_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[promotion_room_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[promotion_room_hist](
	[promotion_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[promotion_set_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[promotion_set_hist](
	[promotion_set_id] [int] NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[description] [nvarchar](300) NULL,
	[provider_id] [int] NOT NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[provider_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[provider_hist](
	[provider_id] [int] NOT NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[location] [nvarchar](100) NULL,
	[company] [nvarchar](100) NULL,
	[website] [nvarchar](100) NULL,
	[commission_rate] [float] NULL,
	[street1] [nvarchar](100) NULL,
	[street2] [nvarchar](100) NULL,
	[town] [nvarchar](100) NULL,
	[country] [nvarchar](100) NULL,
	[phone1] [nvarchar](50) NULL,
	[phone2] [nvarchar](50) NULL,
	[fax] [nvarchar](50) NULL,
	[email] [nvarchar](75) NULL,
	[google_location] [nvarchar](50) NULL,
	[provider_type] [char](1) NOT NULL,
	[postcode] [varchar](20) NULL,
	[is_active] [bit] NOT NULL,
	[star_rating] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[provider_picture_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[provider_picture_hist](
	[picture_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[display_order] [int] NULL,
	[section] [varchar](15) NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[provider_policy_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[provider_policy_hist](
	[provider_id] [int] NOT NULL,
	[policy_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[room_feature_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[room_feature_hist](
	[feature_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[quantity] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[room_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[room_hist](
	[room_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[name] [nvarchar](50) NULL,
	[description] [nvarchar](max) NULL,
	[min_adult] [int] NULL,
	[max_adult] [int] NULL,
	[room_size] [nvarchar](30) NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[room_occupancy_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[room_occupancy_hist](
	[room_id] [int] NOT NULL,
	[occupancy_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[room_picture_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[room_picture_hist](
	[picture_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[display_order] [int] NULL,
	[section] [varchar](15) NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[supplement_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[supplement_hist](
	[supplement_id] [int] NOT NULL,
	[provider_id] [int] NULL,
	[name] [nvarchar](100) NULL,
	[description] [nvarchar](max) NULL,
	[is_per_person] [bit] NOT NULL,
	[is_per_room] [bit] NOT NULL,
	[is_per_night] [bit] NOT NULL,
	[is_per_item] [bit] NOT NULL,
	[is_compulsory] [bit] NOT NULL,
	[valid_all_rooms] [bit] NOT NULL,
	[is_active] [bit] NOT NULL,
	[meal_plan_id] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[supplement_package_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[supplement_package_hist](
	[supplement_id] [int] NOT NULL,
	[package_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[supplement_pricing_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[supplement_pricing_hist](
	[supplement_pricing_id] [int] NOT NULL,
	[supplement_id] [int] NOT NULL,
	[market_id] [int] NOT NULL,
	[valid_from] [datetime] NULL,
	[valid_to] [datetime] NULL,
	[price_per_item] [float] NULL,
	[price_infant] [float] NULL,
	[price_child] [float] NULL,
	[price_teen] [float] NULL,
	[price_adult] [float] NULL,
	[price_senior] [float] NULL,
	[price_honeymoon] [float] NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[supplement_room_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[supplement_room_hist](
	[supplement_id] [int] NOT NULL,
	[room_id] [int] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[transfer_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[transfer_hist](
	[transfer_id] [int] NOT NULL,
	[name] [nvarchar](100) NOT NULL,
	[description] [nvarchar](max) NULL,
	[is_active] [bit] NOT NULL,
	[category] [nvarchar](100) NULL,
	[is_upgrade] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[transfer_pricing_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[transfer_pricing_hist](
	[transfer_pricing_id] [int] NOT NULL,
	[transfer_id] [int] NOT NULL,
	[currency_id] [int] NOT NULL,
	[child_price] [float] NULL,
	[teen_price] [float] NULL,
	[adult_price] [float] NULL,
	[is_active] [bit] NOT NULL,
	[provider_id] [int] NULL,
	[is_selling_price] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[user_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[user_hist](
	[user_id] [int] NOT NULL,
	[title] [nvarchar](20) NULL,
	[firstname] [nvarchar](50) NULL,
	[lastname] [nvarchar](50) NULL,
	[date_of_birth] [date] NULL,
	[gender] [int] NULL,
	[nationality] [nvarchar](60) NULL,
	[role] [int] NOT NULL,
	[officephone] [nvarchar](20) NULL,
	[mobile] [nvarchar](20) NULL,
	[email] [nvarchar](100) NULL,
	[company] [nvarchar](100) NULL,
	[job_title] [nvarchar](100) NULL,
	[password] [nvarchar](200) NULL,
	[markup_percentage] [float] NULL,
	[markup_fixed] [float] NULL,
	[username] [nvarchar](50) NULL,
	[credit_limit] [float] NULL,
	[credit_used] [float] NULL,
	[is_active] [bit] NOT NULL,
	[temp_password_expiry] [date] NULL,
	[temp_password] [nvarchar](200) NULL,
	[is_credit_based] [bit] NULL,
	[currency_id] [int] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  Table [del].[user_picture_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[user_picture_hist](
	[picture_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[display_order] [int] NULL,
	[section] [varchar](15) NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[vehicle_feature_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[vehicle_feature_hist](
	[feature_id] [int] NOT NULL,
	[vehicle_id] [int] NOT NULL,
	[quantity] [int] NULL,
	[load_timestamp] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [del].[vehicle_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[vehicle_hist](
	[vehicle_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
	[service_name] [nvarchar](200) NULL,
	[vehicle_model] [nvarchar](100) NULL,
	[vehicle_type] [nvarchar](50) NULL,
	[service_description] [nvarchar](max) NULL,
	[max_seat] [int] NULL,
	[is_active] [bit] NOT NULL,
	[is_transfer] [bit] NOT NULL,
	[includes_driver] [bit] NOT NULL,
	[engine_size] [smallint] NULL,
	[year] [smallint] NULL,
	[is_automatic] [bit] NOT NULL,
	[doors] [tinyint] NULL,
	[fuel_type] [tinyint] NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [del].[vehicle_picture_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [del].[vehicle_picture_hist](
	[picture_id] [int] NOT NULL,
	[vehicle_id] [int] NOT NULL,
	[display_order] [int] NULL,
	[section] [varchar](15) NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [del].[vehicle_pricing_hist]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [del].[vehicle_pricing_hist](
	[vehicle_pricing_id] [int] NOT NULL,
	[vehicle_id] [int] NOT NULL,
	[currency_id] [int] NOT NULL,
	[half_day_price_with_driver] [float] NULL,
	[half_day_price] [float] NULL,
	[daily_price_with_driver] [float] NULL,
	[daily_price] [float] NULL,
	[is_active] [bit] NOT NULL,
	[load_timestamp] [datetime] NULL DEFAULT (getdate())
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[v_package]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







--select * from v_package

CREATE view [dbo].[v_package] as

select 
p.package_id,
p.number_nights,
p.name package_name,
r.room_id,
r.name room_name,
h.provider_id,
h.name provider_name,
b.base_price_id,
p.description,

m.market_id,
c.currency_code,
c.currency_id,
c.symbol,


 b.price_single ,
 
 b.price_twin   ,

 b.price_triple  , 
 b.price_quadruple  ,
 b.price_honeymoon, 
 b.price_infant ,
 b.price_child   , 
 b.price_teen   , 

price_ground_handling_adult ,
price_ground_handling_teen,
price_ground_handling_child,

chg.price_infant chg_infant, chg.price_child chg_child, chg.price_teen chg_teen, chg.price_adult chg_adult,


dt.fulldate,

case when p.price_ground_handling_adult > 0 then 1 else 0 end as incl_grnd_handling,
p.is_active

--select count(1)
from 
base_price b

inner join date dt
on dt.fulldate between b.start_date and b.end_date


inner join room r
on b.room_id = r.room_id

inner join provider h
on r.provider_id = h.provider_id

inner join market m
on b.market_id = m.market_id

inner join currency c
on m.currency_id = c.currency_id

left join 

(
select s.supplement_id, s.provider_id, sp.market_id, d.fulldate, sp.price_infant, sp.price_child, sp.price_teen, sp.price_adult
from supplement s

inner join supplement_pricing sp
on s.supplement_id = sp.supplement_id

inner join [date] d
on d.fulldate between sp.valid_from and sp.valid_to

where s.is_compulsory =1 
and s.is_active = 1
and sp.is_active = 1
) chg

on chg.provider_id = h.provider_id
and dt.fulldate = chg.fulldate
and chg.market_id = m.market_id



inner join   --get all rooms applicable 
(
	select 
	m.market_id,
	p.package_id,
	p.apply_all_rooms,
	r.room_id,
	r.name room_name,
	r.provider_id

	from 
	[dbo].[package] p
	inner join dbo.market m
	on p.market_id=m.market_id
	left join [dbo].[package_room] pr
	on p.package_id=pr.package_id
	right join [dbo].[room] r 
	on (r.room_id=pr.room_id  --either it applies to a specific room or 
	or  p.apply_all_rooms=1)
	   ---or to all rooms )
	where p.package_id is not null --there must be a promotion associated
	and p.is_active=1
) rooms 
on rooms.room_id=b.room_id
and rooms.provider_id=r.provider_id
and rooms.market_id=m.market_id

inner join [dbo].[package] p 
on p.package_id=rooms.package_id
where
p.is_active=1 and 
m.is_active=1 and
p.is_active =1 and
r.is_active =1 and
b.is_active =1 and
h.is_active = 1 and
dt.fulldate between  p.valid_from and p.valid_to    --only return prices for dates within booking
  --and p.valid_from between b.start_date and b.end_date  --give records only if present in base price for given period
--and p.valid_to   between b.start_date and b.end_date
GO
/****** Object:  View [dbo].[v_price]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[v_price] as

select bp.base_price_id, bp.market_id, bp.room_id, r.name room,  p.provider_id, p.name  hotel,  bp.meal_plan_id, dt.fulldate, 
bp.price_single, bp.price_twin, bp.price_triple, bp.price_quadruple, bp.price_honeymoon,
bp.price_infant, bp.price_child, bp.price_teen, bp.price_without_bed,
c.currency_code, c.symbol, c.currency_id,
chg.price_infant chg_infant, chg.price_child chg_child, chg.price_teen chg_teen, chg.price_adult chg_adult

from base_price bp

inner join [date] dt
on dt.fulldate between bp.start_date and bp.end_date

inner join room r 
on bp.room_id = r.room_id

inner join provider p
on r.provider_id = p.provider_id

inner join market m
on bp.market_id = m.market_id

inner join currency c
on m.currency_id = c.currency_id

left join 

(
select s.supplement_id, s.provider_id, sp.market_id, d.fulldate, sp.price_infant, sp.price_child, sp.price_teen, sp.price_adult
from supplement s

inner join supplement_pricing sp
on s.supplement_id = sp.supplement_id

inner join [date] d
on d.fulldate between sp.valid_from and sp.valid_to

where s.is_compulsory =1 
and s.is_active = 1
--and sp.is_active = 1
) chg

on chg.provider_id = p.provider_id
and dt.fulldate = chg.fulldate
and chg.market_id = m.market_id



where bp.is_active = 1
and r.is_active = 1
and p.is_active = 1
and m.is_active =1 
and c.is_active =1 
and dt.fulldate > dateAdd(day,-100,getDate())


--select * from v_baseprice
--select * from base_price
GO
/****** Object:  View [dbo].[v_rate_sheet]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_rate_sheet] as 
select 
	   data.price_type
	  ,bp.start_date
	  ,bp.end_date
	  ,data.[package_id]
      ,data.[package_name]
	  ,data.[number_nights]
      ,data.[room_id]
      ,data.[room_name]
      ,data.[provider_id]
      ,data.[provider_name]
      ,data.[base_price_id]
      ,data.[description]
      ,data.[market_id]
      ,data.[currency_code]
      ,data.[currency_id]
      ,data.[symbol]
      ,data.[price_single]
      ,data.[price_twin]
      ,data.[price_triple]
      ,data.[price_quadruple]
      ,data.[price_honeymoon]
      ,data.[price_infant]
      ,data.[price_child]
      ,data.[price_teen]
      ,data.[price_ground_handling_adult]
      ,data.[price_ground_handling_teen]
      ,data.[price_ground_handling_child]
      ,data.[chg_infant]
      ,data.[chg_child]
      ,data.[chg_teen]
      ,data.[chg_adult]	  
      ,data.[incl_grnd_handling]
      ,data.[is_active]
	  ,data.[contracted_offer]
	  from
(
SELECT 
'PC' price_type
		,v.[package_id]
      ,v.[package_name]
	  ,v.[number_nights]
      ,v.[room_id]
      ,v.[room_name]
      ,v.[provider_id]
      ,v.[provider_name]
      ,v.[base_price_id]
      ,v.[description]
      ,v.[market_id]
      ,v.[currency_code]
      ,v.[currency_id]
      ,v.[symbol]
      ,v.[price_single]
      ,v.[price_twin]
      ,v.[price_triple]
      ,v.[price_quadruple]
      ,v.[price_honeymoon]
      ,v.[price_infant]
      ,v.[price_child]
      ,v.[price_teen]
      ,v.[price_ground_handling_adult]
      ,v.[price_ground_handling_teen]
      ,v.[price_ground_handling_child]
      ,v.[chg_infant]
      ,v.[chg_child]
      ,v.[chg_teen]
      ,v.[chg_adult]  
      ,v.[incl_grnd_handling]
      ,v.[is_active]
	  ,p.[contracted_offer]
  FROM [dbo].[v_package] v
  inner join dbo.package p 
  on p.package_id=v.package_id

  union all

SELECT 
'BP' price_type
,0 package_id
	   ,'Base price' package_name
	   ,'0' number_nights
	   ,[room_id]
	   ,[room]      
      ,[provider_id]
      ,[hotel]
	  ,[base_price_id]
	   ,'Flat rate' price_name
	   ,[market_id]     
	     ,[currency_code]
		  ,[currency_id]
		    ,[symbol]

     -- ,[fulldate]
      ,[price_single]
      ,[price_twin]
      ,[price_triple]
      ,[price_quadruple]
      ,[price_honeymoon]
      ,[price_infant]
      ,[price_child]
      ,[price_teen]
     -- ,[price_without_bed]
     , 0 [price_ground_handling_adult]
      ,0 [price_ground_handling_teen]
      ,0 [price_ground_handling_child]        
      ,[chg_infant]
      ,[chg_child]
      ,[chg_teen]
      ,[chg_adult]
	  , 0 [incl_grnd_handling]
      ,1 [is_active]
	  , null [contracted_offer]
  FROM [dbo].[v_price]
  ) data
  inner join [dbo].[base_price] bp
  on bp.[base_price_id]=data.base_price_id
  where bp.is_active=1
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetGuestDefinition]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Giovanni L'Etourdi
-- Create date: 25-Jun=2016
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetGuestDefinition]
(	
	-- Add the parameters for the function here
	@provider_id int,
	@age1 int=-1,
	@age2 int=-1,
	@age3 int=-1,
	@age4 int=-1,
	@age5 int=-1
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT 
	gt.provider_id,
	case when @age1 >-1 and @age1 <= infant_max_age then 1 else 0 end+
	case when @age2 >-1 and @age2 <= infant_max_age then 1 else 0 end+
	case when @age3 >-1 and @age3 <= infant_max_age then 1 else 0 end+
	case when @age4 >-1 and @age4 <= infant_max_age then 1 else 0 end+
	case when @age5 >-1 and @age5 <= infant_max_age then 1 else 0 end infant,
					
	case when @age1 >-1 and @age1 > infant_max_Age and @age1 <= child_max_age then 1 else 0 end +
	case when @age2 >-1 and @age2 > infant_max_Age and @age2 <= child_max_age then 1 else 0 end +
	case when @age3 >-1 and @age3 > infant_max_Age and @age3 <= child_max_age then 1 else 0 end +
	case when @age4 >-1 and @age4 > infant_max_Age and @age4 <= child_max_age then 1 else 0 end +
	case when @age5 >-1 and @age5 > infant_max_Age and @age5 <= child_max_age then 1 else 0 end child,
					
	case when @age1 >-1 and @age1 > child_max_Age and @age1<= teen_max_age then 1 else 0 end +
	case when @age2 >-1 and @age2 > child_max_Age and @age2<= teen_max_age then 1 else 0 end +
	case when @age3 >-1 and @age3 > child_max_Age and @age3<= teen_max_age then 1 else 0 end +
	case when @age4 >-1 and @age4 > child_max_Age and @age4<= teen_max_age then 1 else 0 end +
	case when @age5 >-1 and @age5 > child_max_Age and @age5<= teen_max_age then 1 else 0 end teen

	from guest_type gt
	where gt.provider_id = @provider_id
)
GO
/****** Object:  View [dbo].[all_rates]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[all_rates] as 
select '1' one
--select 
--dt.fulldate checkin_date,
--m.market_id,
--h.hotel_id,
--h.star_rating,
--p.package_id ,
--p.name package_name,
----p.valid_from package_valid_from,
----p.valid_to  package_valid_until,  
--dt.fulldate package_book_start_date,
----dateAdd(day,p.paid_night + p.free_night,dt.fulldate) package_book_end_date,
----p.paid_night,
----p.free_night,
--o.occupant_id,
--o.name occupancy_type,
--o.min_adult min_occupancy,
--rt.room_size,
--rt.min_adult min_adult_room_size,
--rt.max_adult max_adult_room_size,
----pp.[price] price_pax_per_day,
----(pp.[price] *p.paid_night )  package_price_pax, --price of package for the whole paid nights
--o.ground_handling_price
--from 
--package p 
--inner join dbo.Package_pricing pp
--on pp.package_id=p.package_id
--inner join dbo.market m
--on m.market_id=pp.market_id
----inner join [dbo].[hotel] h 
----on h.hotel_id=p.hotel_id
--inner join [dbo].[room] rt
----on rt.hotel_id=h.hotel_id
----and rt.room_id=pp.room_id
----inner join [date] dt
----on dt.fulldate <= p.valid_to
----inner join dbo.package_occupant op
----on op.package_id=p.package_id
----inner join [dbo].[occupant] o
----on o.occupant_id=op.occupant_id
--where m.is_active='Y'  --make sure that the market is active 
--and dateAdd(day,p.paid_night + p.free_night,dt.fulldate) <= p.valid_to -- make sure that only booking which falls within the package validity period is accepted

GO
/****** Object:  View [dbo].[v_activity]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[v_activity]
 as
select a.activity_id, ap.activity_pricing_id,  a.activity_name, a.description,a.category,  a.min_age, a.max_child_age, 
a.features, a.includes_meal,

c.currency_id, c.symbol, c.currency_code, 
ap.price_adult, ap.price_child
,ap.price_adult_incl_sic_transfer, ap.price_child_incl_sic_transfer
,ap.price_adult_incl_private_transfer, ap.price_child_incl_private_transfer
from activity a
--inner join provider p 
--on p.provider_id = a.provider_id
inner join activity_pricing ap
on a.activity_id = ap.activity_id
inner join currency c
on c.currency_id = ap.currency_id
GO
/****** Object:  View [dbo].[v_baseprice]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[v_baseprice] as
select 
cast(p.provider_id as varchar(3))+'-'+ p.name as hotel,
cast(r.room_id as varchar(3))+'-'+ r.name as room,
cast(bp.price_single as varchar(5)) +'/'+ cast(bp.price_twin as varchar(5)) +'/'+ cast(bp.price_triple as varchar(5)) as price_adult, 
bp.price_infant, bp.price_child,bp.price_teen,
bp.start_date, bp.end_date
 from base_price bp
 inner join room r 
 on bp.room_id = r.room_id
 inner join provider p 
 on p.provider_id = r.provider_id
 where bp.is_active=1 and r.is_active=1 and p.is_active=1
GO
/****** Object:  View [dbo].[v_booking]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE view [dbo].[v_booking] as

SELECT b.rooms num_rooms
	,isnull(b.booking_id,-999) booking_id
	,b.booking_ref
	,b.booking_name
	,p.provider_id
	,b.package_id
	,p.name
	,b.checkin_date
	,b.checkout_date
	,coalesce(b.num_nights,0) as num_nights
	,b.create_date created_on
	,coalesce(b.adults,0) as adults
	,coalesce(b.infants,0) as infants
	,coalesce(b.children,0) as children
	,coalesce(b.teens,0) as teens
	,coalesce(bg.price,0) AS price
	,coalesce(bg.price_per_night,0) price_per_night
	,coalesce(bg.markup_amt,0) markup_amt_room
	,c.symbol
	,b.agent_user_id
	,b.status
	,coalesce(r.cnt,0) as num_rental
	,coalesce(r.total_price,0) AS price_rental
	,coalesce(r.total_price_no_after_sales,0) as price_rental_no_after_sales
	,coalesce(a.cnt,0) as num_activity
	,coalesce(a.total_price,0) AS price_activity
	,coalesce(a.total_price_no_after_sales,0) as price_activity_no_after_sales
	,coalesce(s.cnt,0) as num_supplement
	,coalesce(s.total_price,0) AS price_supplement
	,coalesce(t.cnt,0) as num_transfer
	,coalesce(t.total_price,0) AS price_transfer
	,coalesce(t.total_price_no_after_sales,0) as price_transfer_no_after_sales
	,coalesce(b.discount_amt,0) as discount_amt
	,coalesce(bg.ground_handling_amt,0) as ground_handling_amt
	,coalesce(bg.compulsory_charge_amt,0) as compulsory_charge_amt
	,coalesce(bg.promotional_discount_amt,0) as promotional_discount_amt
	,coalesce(b.free_nights,0) free_nights
	,agent.is_credit_based
	,coalesce(proforma.has_proforma_finalized, 0) has_proforma_finalized
	,b.remarks
	,b.timelimit_date
	,b.is_timelimit
	,b.is_new_timelimit
	,b.cancellation_fee
	,voucher.has_voucher_generated
FROM booking b
--INNER JOIN booking_search bs ON b.booking_ref = bs.booking_ref
INNER JOIN market m ON b.market_id = m.market_id
INNER JOIN currency c ON m.currency_id = c.currency_id
INNER JOIN provider p ON b.provider_id = p.provider_id
INNER JOIN [user] agent on agent.user_id = b.agent_user_id

LEFT JOIN (
	SELECT booking_id
		,sum((coalesce(price_per_night,0) + coalesce(markup_amt,0)) - coalesce(promotional_discount_amt,0)) price
		,sum(coalesce(price_per_night,0)) price_per_night
		,sum(coalesce(markup_amt,0)) markup_amt
		,sum(coalesce(ground_handling_amt,0)) ground_handling_amt
		,sum(coalesce(promotional_discount_amt,0)) promotional_discount_amt
		,sum(coalesce(compulsory_charge_amt,0)) compulsory_charge_amt
	FROM booking_guest bg
	GROUP BY booking_id
	) bg ON b.booking_id = bg.booking_id
LEFT JOIN (
	SELECT count(1) cnt
		,booking_id
		,sum(coalesce(total_price,0) * coalesce(num_days,0)) AS total_price
		,sum(coalesce(markup_amt,0)) markup_amt
		,sum(case when coalesce(is_aftersales,0) = 0 then coalesce(total_price,0) * coalesce(num_days,0) else 0 end) AS total_price_no_after_sales
		
	FROM booking_rental
	GROUP BY booking_id
	) r ON b.booking_id = r.booking_id
LEFT JOIN (
	SELECT count(1) cnt
		,booking_id
		,sum(coalesce(total_price,0)) AS total_price
		,sum(case when coalesce(is_aftersales,0) = 0 then coalesce(total_price,0) else 0 end) AS total_price_no_after_sales
		,sum(coalesce(markup_amt,0)) markup_amt
	FROM booking_activity
	GROUP BY booking_id
	) a ON b.booking_id = a.booking_id
LEFT JOIN (
	SELECT count(1) cnt
		,booking_id
		,sum(coalesce(price,0)) AS total_price
		,sum(coalesce(markup_amt,0)) markup_amt
	FROM booking_supplement
	GROUP BY booking_id
	) s ON b.booking_id = s.booking_id
LEFT JOIN (
	SELECT count(1) cnt
		,booking_id
		,sum(coalesce(total_price,0)) AS total_price
		,sum(case when coalesce(is_aftersales,0) = 0 then coalesce(total_price,0) else 0 end) AS total_price_no_after_sales
		,sum(coalesce(markup_amt,0)) markup_amt
	FROM booking_transfer
	GROUP BY booking_id
	) t ON b.booking_id = t.booking_id
	
LEFT JOIN (
	SELECT 1 has_proforma_finalized
		,booking_id
	FROM booking_proforma

	GROUP BY booking_id
	) proforma ON proforma.booking_id = b.booking_id

LEFT JOIN (
	select 1 has_voucher_generated, booking_id 
	from booking_voucher
	group by booking_id
) voucher on voucher.booking_id = b.booking_id
GO
/****** Object:  View [dbo].[v_supplement]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[v_supplement]

as

select 
distinct s.supplement_id, s.provider_id, s.name, s.description, s.is_per_person, s.is_per_room, s.is_per_night, s.is_per_item,
s.valid_all_rooms, s.meal_plan_id, sr.room_id, sp.market_id, c.symbol, c.currency_code, sp.valid_from, sp.valid_to,
sp.price_per_item, sp.price_adult, sp.price_infant,  sp.price_child, sp.price_teen ,s.is_compulsory


from supplement s

left join supplement_room sr 
on s.supplement_id = sr.supplement_id

inner join supplement_pricing sp
on sp.supplement_id = s.supplement_id

inner join market m
on sp.market_id = m.market_id

inner join currency c 
on m.currency_id = c.currency_id

where 
sp.is_active = 1 
and s.is_active = 1
and m.is_active =1
and c.is_active =1
and sp.is_active =1
GO
/****** Object:  View [dbo].[v_unavailability]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_unavailability]
	AS 
	
	SELECT  
	distinct
	u.room_id , d.fulldate unavailable_date
	from [dbo].[room_unavailability] u
	inner join dbo.date d
	on d.fulldate between u.start_date and u.end_date
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [IX_configuration_keytype]    Script Date: 05-Aug-17 9:14:12 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_configuration_keytype] ON [dbo].[configuration]
(
	[conf_key] ASC,
	[conf_type] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[booking_rental] ADD  DEFAULT ((0)) FOR [is_aftersales]
GO
ALTER TABLE [dbo].[booking_rental] ADD  DEFAULT ((0)) FOR [has_driver]
GO
ALTER TABLE [dbo].[log] ADD  DEFAULT (getdate()) FOR [created_on]
GO
ALTER TABLE [del].[__RefactorLog_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[company_info_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[configuration_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[country_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[currency_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[date_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[facility_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[guest_type_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[log_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[meal_plan_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[occupancy_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[occupant_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[package_activity_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[package_transfer_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[policy_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[provider_policy_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[supplement_package_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[supplement_room_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[transfer_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [del].[vehicle_feature_hist] ADD  DEFAULT (getdate()) FOR [load_timestamp]
GO
ALTER TABLE [dbo].[activity_picture]  WITH CHECK ADD  CONSTRAINT [FK_activity_picture_activity_id] FOREIGN KEY([activity_id])
REFERENCES [dbo].[activity] ([activity_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[activity_picture] CHECK CONSTRAINT [FK_activity_picture_activity_id]
GO
ALTER TABLE [dbo].[activity_picture]  WITH CHECK ADD  CONSTRAINT [FK_activity_picture_picture_id] FOREIGN KEY([picture_id])
REFERENCES [dbo].[picture] ([picture_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[activity_picture] CHECK CONSTRAINT [FK_activity_picture_picture_id]
GO
ALTER TABLE [dbo].[activity_pricing]  WITH CHECK ADD  CONSTRAINT [FK_activity_pricing_activity_id] FOREIGN KEY([activity_id])
REFERENCES [dbo].[activity] ([activity_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[activity_pricing] CHECK CONSTRAINT [FK_activity_pricing_activity_id]
GO
ALTER TABLE [dbo].[activity_pricing]  WITH CHECK ADD  CONSTRAINT [FK_activity_pricing_currency_id] FOREIGN KEY([currency_id])
REFERENCES [dbo].[currency] ([currency_id])
GO
ALTER TABLE [dbo].[activity_pricing] CHECK CONSTRAINT [FK_activity_pricing_currency_id]
GO
ALTER TABLE [dbo].[activity_pricing]  WITH CHECK ADD  CONSTRAINT [FK_activity_pricing_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
GO
ALTER TABLE [dbo].[activity_pricing] CHECK CONSTRAINT [FK_activity_pricing_provider_id]
GO
ALTER TABLE [dbo].[base_price]  WITH CHECK ADD  CONSTRAINT [FK_base_price_market_id] FOREIGN KEY([market_id])
REFERENCES [dbo].[market] ([market_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[base_price] CHECK CONSTRAINT [FK_base_price_market_id]
GO
ALTER TABLE [dbo].[base_price]  WITH CHECK ADD  CONSTRAINT [FK_base_price_room_id] FOREIGN KEY([room_id])
REFERENCES [dbo].[room] ([room_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[base_price] CHECK CONSTRAINT [FK_base_price_room_id]
GO
ALTER TABLE [dbo].[base_price]  WITH CHECK ADD  CONSTRAINT [FK_meal_plan_base_price] FOREIGN KEY([meal_plan_id])
REFERENCES [dbo].[meal_plan] ([meal_plan_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[base_price] CHECK CONSTRAINT [FK_meal_plan_base_price]
GO
ALTER TABLE [dbo].[booking]  WITH CHECK ADD  CONSTRAINT [FK_booking_backoffice_user_id] FOREIGN KEY([backoffice_user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[booking] CHECK CONSTRAINT [FK_booking_backoffice_user_id]
GO
ALTER TABLE [dbo].[booking]  WITH CHECK ADD  CONSTRAINT [FK_booking_booking_ref] FOREIGN KEY([booking_ref])
REFERENCES [dbo].[booking_search] ([booking_ref])
GO
ALTER TABLE [dbo].[booking] CHECK CONSTRAINT [FK_booking_booking_ref]
GO
ALTER TABLE [dbo].[booking]  WITH CHECK ADD  CONSTRAINT [FK_booking_discount_user_id] FOREIGN KEY([discounted_by_user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[booking] CHECK CONSTRAINT [FK_booking_discount_user_id]
GO
ALTER TABLE [dbo].[booking]  WITH CHECK ADD  CONSTRAINT [FK_booking_user_id] FOREIGN KEY([agent_user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[booking] CHECK CONSTRAINT [FK_booking_user_id]
GO
ALTER TABLE [dbo].[booking_activity]  WITH CHECK ADD  CONSTRAINT [FK_booking_activity_booking_id] FOREIGN KEY([booking_id])
REFERENCES [dbo].[booking] ([booking_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[booking_activity] CHECK CONSTRAINT [FK_booking_activity_booking_id]
GO
ALTER TABLE [dbo].[booking_guest]  WITH CHECK ADD  CONSTRAINT [FK_booking_guest_booking_id] FOREIGN KEY([booking_id])
REFERENCES [dbo].[booking] ([booking_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[booking_guest] CHECK CONSTRAINT [FK_booking_guest_booking_id]
GO
ALTER TABLE [dbo].[booking_guest]  WITH CHECK ADD  CONSTRAINT [FK_booking_guest_booking_room_id] FOREIGN KEY([booking_room_id])
REFERENCES [dbo].[booking_room] ([booking_room_id])
GO
ALTER TABLE [dbo].[booking_guest] CHECK CONSTRAINT [FK_booking_guest_booking_room_id]
GO
ALTER TABLE [dbo].[booking_payment]  WITH CHECK ADD  CONSTRAINT [FK_booking_payment_booking_id] FOREIGN KEY([booking_id])
REFERENCES [dbo].[booking] ([booking_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[booking_payment] CHECK CONSTRAINT [FK_booking_payment_booking_id]
GO
ALTER TABLE [dbo].[booking_proforma]  WITH CHECK ADD  CONSTRAINT [FK_booking_proforma_booking_id] FOREIGN KEY([booking_id])
REFERENCES [dbo].[booking] ([booking_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[booking_proforma] CHECK CONSTRAINT [FK_booking_proforma_booking_id]
GO
ALTER TABLE [dbo].[booking_promotion]  WITH CHECK ADD  CONSTRAINT [FK_booking_promotion_booking_id] FOREIGN KEY([booking_id])
REFERENCES [dbo].[booking] ([booking_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[booking_promotion] CHECK CONSTRAINT [FK_booking_promotion_booking_id]
GO
ALTER TABLE [dbo].[booking_promotion]  WITH CHECK ADD  CONSTRAINT [FK_booking_promotion_booking_room_id] FOREIGN KEY([booking_room_id])
REFERENCES [dbo].[booking_room] ([booking_room_id])
GO
ALTER TABLE [dbo].[booking_promotion] CHECK CONSTRAINT [FK_booking_promotion_booking_room_id]
GO
ALTER TABLE [dbo].[booking_rental]  WITH CHECK ADD  CONSTRAINT [FK_booking_rental_booking_id] FOREIGN KEY([booking_id])
REFERENCES [dbo].[booking] ([booking_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[booking_rental] CHECK CONSTRAINT [FK_booking_rental_booking_id]
GO
ALTER TABLE [dbo].[booking_room]  WITH CHECK ADD  CONSTRAINT [FK_booking_room_booking_id] FOREIGN KEY([booking_id])
REFERENCES [dbo].[booking] ([booking_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[booking_room] CHECK CONSTRAINT [FK_booking_room_booking_id]
GO
ALTER TABLE [dbo].[booking_supplement]  WITH CHECK ADD  CONSTRAINT [FK_booking_supplement_booking_id] FOREIGN KEY([booking_id])
REFERENCES [dbo].[booking] ([booking_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[booking_supplement] CHECK CONSTRAINT [FK_booking_supplement_booking_id]
GO
ALTER TABLE [dbo].[booking_supplement]  WITH CHECK ADD  CONSTRAINT [FK_booking_supplement_booking_room_id] FOREIGN KEY([booking_room_id])
REFERENCES [dbo].[booking_room] ([booking_room_id])
GO
ALTER TABLE [dbo].[booking_supplement] CHECK CONSTRAINT [FK_booking_supplement_booking_room_id]
GO
ALTER TABLE [dbo].[booking_transfer]  WITH CHECK ADD  CONSTRAINT [FK_booking_transfer_booking_id] FOREIGN KEY([booking_id])
REFERENCES [dbo].[booking] ([booking_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[booking_transfer] CHECK CONSTRAINT [FK_booking_transfer_booking_id]
GO
ALTER TABLE [dbo].[booking_voucher]  WITH CHECK ADD  CONSTRAINT [FK_booking_voucher_booking_id] FOREIGN KEY([booking_id])
REFERENCES [dbo].[booking] ([booking_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[booking_voucher] CHECK CONSTRAINT [FK_booking_voucher_booking_id]
GO
ALTER TABLE [dbo].[contact]  WITH CHECK ADD  CONSTRAINT [FK_contact_ToProvider] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[contact] CHECK CONSTRAINT [FK_contact_ToProvider]
GO
ALTER TABLE [dbo].[facility]  WITH CHECK ADD  CONSTRAINT [FK_facility_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[facility] CHECK CONSTRAINT [FK_facility_provider_id]
GO
ALTER TABLE [dbo].[featured_hotel]  WITH CHECK ADD  CONSTRAINT [FK_featured_hotel_picture_id] FOREIGN KEY([picture_id])
REFERENCES [dbo].[picture] ([picture_id])
GO
ALTER TABLE [dbo].[featured_hotel] CHECK CONSTRAINT [FK_featured_hotel_picture_id]
GO
ALTER TABLE [dbo].[featured_hotel]  WITH CHECK ADD  CONSTRAINT [FK_featured_hotel_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
GO
ALTER TABLE [dbo].[featured_hotel] CHECK CONSTRAINT [FK_featured_hotel_provider_id]
GO
ALTER TABLE [dbo].[file_upload]  WITH CHECK ADD  CONSTRAINT [FK_file_upload_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[file_upload] CHECK CONSTRAINT [FK_file_upload_provider_id]
GO
ALTER TABLE [dbo].[guest_type]  WITH CHECK ADD  CONSTRAINT [FK_guest_type_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[guest_type] CHECK CONSTRAINT [FK_guest_type_provider_id]
GO
ALTER TABLE [dbo].[hotel_feature]  WITH CHECK ADD  CONSTRAINT [FK_hotel_feature_To_feature] FOREIGN KEY([feature_id])
REFERENCES [dbo].[feature] ([feature_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[hotel_feature] CHECK CONSTRAINT [FK_hotel_feature_To_feature]
GO
ALTER TABLE [dbo].[hotel_feature]  WITH CHECK ADD  CONSTRAINT [FK_hotel_feature_To_provider] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[hotel_feature] CHECK CONSTRAINT [FK_hotel_feature_To_provider]
GO
ALTER TABLE [dbo].[log]  WITH CHECK ADD  CONSTRAINT [FK_log_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
GO
ALTER TABLE [dbo].[log] CHECK CONSTRAINT [FK_log_user_id]
GO
ALTER TABLE [dbo].[market]  WITH CHECK ADD  CONSTRAINT [FK_market_currency_id] FOREIGN KEY([currency_id])
REFERENCES [dbo].[currency] ([currency_id])
GO
ALTER TABLE [dbo].[market] CHECK CONSTRAINT [FK_market_currency_id]
GO
ALTER TABLE [dbo].[market]  WITH CHECK ADD  CONSTRAINT [FK_market_ToProvider] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[market] CHECK CONSTRAINT [FK_market_ToProvider]
GO
ALTER TABLE [dbo].[market_country]  WITH CHECK ADD  CONSTRAINT [FK_market_country_country_id] FOREIGN KEY([country_id])
REFERENCES [dbo].[country] ([country_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[market_country] CHECK CONSTRAINT [FK_market_country_country_id]
GO
ALTER TABLE [dbo].[market_country]  WITH CHECK ADD  CONSTRAINT [FK_market_country_market_id] FOREIGN KEY([market_id])
REFERENCES [dbo].[market] ([market_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[market_country] CHECK CONSTRAINT [FK_market_country_market_id]
GO
ALTER TABLE [dbo].[note]  WITH CHECK ADD  CONSTRAINT [FK_note_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[note] CHECK CONSTRAINT [FK_note_user_id]
GO
ALTER TABLE [dbo].[package]  WITH CHECK ADD  CONSTRAINT [FK_package_market] FOREIGN KEY([market_id])
REFERENCES [dbo].[market] ([market_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[package] CHECK CONSTRAINT [FK_package_market]
GO
ALTER TABLE [dbo].[package]  WITH CHECK ADD  CONSTRAINT [FK_package_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
GO
ALTER TABLE [dbo].[package] CHECK CONSTRAINT [FK_package_provider_id]
GO
ALTER TABLE [dbo].[package_activity]  WITH CHECK ADD  CONSTRAINT [FK_package_activity_activity_id] FOREIGN KEY([activity_id])
REFERENCES [dbo].[activity] ([activity_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[package_activity] CHECK CONSTRAINT [FK_package_activity_activity_id]
GO
ALTER TABLE [dbo].[package_activity]  WITH CHECK ADD  CONSTRAINT [FK_package_activity_package_id] FOREIGN KEY([package_id])
REFERENCES [dbo].[package] ([package_id])
GO
ALTER TABLE [dbo].[package_activity] CHECK CONSTRAINT [FK_package_activity_package_id]
GO
ALTER TABLE [dbo].[package_room]  WITH CHECK ADD  CONSTRAINT [FK_package_market_room] FOREIGN KEY([room_id])
REFERENCES [dbo].[room] ([room_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[package_room] CHECK CONSTRAINT [FK_package_market_room]
GO
ALTER TABLE [dbo].[package_room]  WITH CHECK ADD  CONSTRAINT [FK_package_room_package] FOREIGN KEY([package_id])
REFERENCES [dbo].[package] ([package_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[package_room] CHECK CONSTRAINT [FK_package_room_package]
GO
ALTER TABLE [dbo].[package_transfer]  WITH CHECK ADD  CONSTRAINT [FK_package_transfer_package_id] FOREIGN KEY([package_id])
REFERENCES [dbo].[package] ([package_id])
GO
ALTER TABLE [dbo].[package_transfer] CHECK CONSTRAINT [FK_package_transfer_package_id]
GO
ALTER TABLE [dbo].[package_transfer]  WITH CHECK ADD  CONSTRAINT [FK_package_transfer_transfer_id] FOREIGN KEY([transfer_id])
REFERENCES [dbo].[transfer] ([transfer_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[package_transfer] CHECK CONSTRAINT [FK_package_transfer_transfer_id]
GO
ALTER TABLE [dbo].[promo_set_promotions]  WITH CHECK ADD  CONSTRAINT [FK_promoset] FOREIGN KEY([promotion_set_id])
REFERENCES [dbo].[promotion_set] ([promotion_set_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[promo_set_promotions] CHECK CONSTRAINT [FK_promoset]
GO
ALTER TABLE [dbo].[promo_set_promotions]  WITH CHECK ADD  CONSTRAINT [FK_promoset_promo] FOREIGN KEY([promotion_id])
REFERENCES [dbo].[promotion] ([promotion_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[promo_set_promotions] CHECK CONSTRAINT [FK_promoset_promo]
GO
ALTER TABLE [dbo].[promotion]  WITH CHECK ADD  CONSTRAINT [FK_promotion_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
GO
ALTER TABLE [dbo].[promotion] CHECK CONSTRAINT [FK_promotion_provider_id]
GO
ALTER TABLE [dbo].[promotion_market]  WITH CHECK ADD  CONSTRAINT [FK_promo_market_market] FOREIGN KEY([market_id])
REFERENCES [dbo].[market] ([market_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[promotion_market] CHECK CONSTRAINT [FK_promo_market_market]
GO
ALTER TABLE [dbo].[promotion_market]  WITH CHECK ADD  CONSTRAINT [FK_promo_market_promo] FOREIGN KEY([promotion_id])
REFERENCES [dbo].[promotion] ([promotion_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[promotion_market] CHECK CONSTRAINT [FK_promo_market_promo]
GO
ALTER TABLE [dbo].[promotion_room]  WITH CHECK ADD  CONSTRAINT [FK_promo_room_promo] FOREIGN KEY([promotion_id])
REFERENCES [dbo].[promotion] ([promotion_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[promotion_room] CHECK CONSTRAINT [FK_promo_room_promo]
GO
ALTER TABLE [dbo].[promotion_room]  WITH CHECK ADD  CONSTRAINT [FK_promo_room_room] FOREIGN KEY([room_id])
REFERENCES [dbo].[room] ([room_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[promotion_room] CHECK CONSTRAINT [FK_promo_room_room]
GO
ALTER TABLE [dbo].[promotion_set]  WITH CHECK ADD  CONSTRAINT [FK_promo_set_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
GO
ALTER TABLE [dbo].[promotion_set] CHECK CONSTRAINT [FK_promo_set_provider_id]
GO
ALTER TABLE [dbo].[provider_picture]  WITH CHECK ADD  CONSTRAINT [FK_provider_picture_picture_id] FOREIGN KEY([picture_id])
REFERENCES [dbo].[picture] ([picture_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[provider_picture] CHECK CONSTRAINT [FK_provider_picture_picture_id]
GO
ALTER TABLE [dbo].[provider_picture]  WITH CHECK ADD  CONSTRAINT [FK_provider_picture_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[provider_picture] CHECK CONSTRAINT [FK_provider_picture_provider_id]
GO
ALTER TABLE [dbo].[provider_policy]  WITH CHECK ADD  CONSTRAINT [FK_provider_policy_policy_id] FOREIGN KEY([policy_id])
REFERENCES [dbo].[policy] ([policy_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[provider_policy] CHECK CONSTRAINT [FK_provider_policy_policy_id]
GO
ALTER TABLE [dbo].[provider_policy]  WITH CHECK ADD  CONSTRAINT [FK_provider_policy_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[provider_policy] CHECK CONSTRAINT [FK_provider_policy_provider_id]
GO
ALTER TABLE [dbo].[room]  WITH CHECK ADD  CONSTRAINT [FK_room_type_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
GO
ALTER TABLE [dbo].[room] CHECK CONSTRAINT [FK_room_type_provider_id]
GO
ALTER TABLE [dbo].[room_feature]  WITH CHECK ADD  CONSTRAINT [FK_room_feature_feature_id] FOREIGN KEY([feature_id])
REFERENCES [dbo].[feature] ([feature_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[room_feature] CHECK CONSTRAINT [FK_room_feature_feature_id]
GO
ALTER TABLE [dbo].[room_feature]  WITH CHECK ADD  CONSTRAINT [FK_room_feature_room_id] FOREIGN KEY([room_id])
REFERENCES [dbo].[room] ([room_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[room_feature] CHECK CONSTRAINT [FK_room_feature_room_id]
GO
ALTER TABLE [dbo].[room_occupancy]  WITH CHECK ADD  CONSTRAINT [FK_room_occupancy_occupancy_id] FOREIGN KEY([occupancy_id])
REFERENCES [dbo].[occupancy] ([occupancy_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[room_occupancy] CHECK CONSTRAINT [FK_room_occupancy_occupancy_id]
GO
ALTER TABLE [dbo].[room_occupancy]  WITH CHECK ADD  CONSTRAINT [FK_room_occupancy_room_type_id] FOREIGN KEY([room_id])
REFERENCES [dbo].[room] ([room_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[room_occupancy] CHECK CONSTRAINT [FK_room_occupancy_room_type_id]
GO
ALTER TABLE [dbo].[room_picture]  WITH CHECK ADD  CONSTRAINT [FK_room_picture_picture_id] FOREIGN KEY([picture_id])
REFERENCES [dbo].[picture] ([picture_id])
GO
ALTER TABLE [dbo].[room_picture] CHECK CONSTRAINT [FK_room_picture_picture_id]
GO
ALTER TABLE [dbo].[room_picture]  WITH CHECK ADD  CONSTRAINT [FK_room_picture_room_id] FOREIGN KEY([room_id])
REFERENCES [dbo].[room] ([room_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[room_picture] CHECK CONSTRAINT [FK_room_picture_room_id]
GO
ALTER TABLE [dbo].[room_unavailability]  WITH CHECK ADD  CONSTRAINT [FK_unavailability_room_id] FOREIGN KEY([room_id])
REFERENCES [dbo].[room] ([room_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[room_unavailability] CHECK CONSTRAINT [FK_unavailability_room_id]
GO
ALTER TABLE [dbo].[supplement]  WITH CHECK ADD  CONSTRAINT [FK_supplement_meal_plan_id] FOREIGN KEY([meal_plan_id])
REFERENCES [dbo].[meal_plan] ([meal_plan_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[supplement] CHECK CONSTRAINT [FK_supplement_meal_plan_id]
GO
ALTER TABLE [dbo].[supplement]  WITH CHECK ADD  CONSTRAINT [FK_supplement_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[supplement] CHECK CONSTRAINT [FK_supplement_provider_id]
GO
ALTER TABLE [dbo].[supplement_pricing]  WITH CHECK ADD  CONSTRAINT [FK_supplement_id] FOREIGN KEY([supplement_id])
REFERENCES [dbo].[supplement] ([supplement_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[supplement_pricing] CHECK CONSTRAINT [FK_supplement_id]
GO
ALTER TABLE [dbo].[supplement_pricing]  WITH CHECK ADD  CONSTRAINT [FK_supplement_market_id] FOREIGN KEY([market_id])
REFERENCES [dbo].[market] ([market_id])
GO
ALTER TABLE [dbo].[supplement_pricing] CHECK CONSTRAINT [FK_supplement_market_id]
GO
ALTER TABLE [dbo].[supplement_room]  WITH CHECK ADD  CONSTRAINT [FK_supplement_room_ToRoom] FOREIGN KEY([room_id])
REFERENCES [dbo].[room] ([room_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[supplement_room] CHECK CONSTRAINT [FK_supplement_room_ToRoom]
GO
ALTER TABLE [dbo].[supplement_room]  WITH CHECK ADD  CONSTRAINT [FK_supplement_room_Tosupplement] FOREIGN KEY([supplement_id])
REFERENCES [dbo].[supplement] ([supplement_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[supplement_room] CHECK CONSTRAINT [FK_supplement_room_Tosupplement]
GO
ALTER TABLE [dbo].[transfer_pricing]  WITH CHECK ADD  CONSTRAINT [FK_transfer_pricing_currency_id] FOREIGN KEY([currency_id])
REFERENCES [dbo].[currency] ([currency_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[transfer_pricing] CHECK CONSTRAINT [FK_transfer_pricing_currency_id]
GO
ALTER TABLE [dbo].[transfer_pricing]  WITH CHECK ADD  CONSTRAINT [FK_transfer_pricing_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[transfer_pricing] CHECK CONSTRAINT [FK_transfer_pricing_provider_id]
GO
ALTER TABLE [dbo].[transfer_pricing]  WITH CHECK ADD  CONSTRAINT [FK_transfer_pricing_transfer_id] FOREIGN KEY([transfer_id])
REFERENCES [dbo].[transfer] ([transfer_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[transfer_pricing] CHECK CONSTRAINT [FK_transfer_pricing_transfer_id]
GO
ALTER TABLE [dbo].[user]  WITH CHECK ADD  CONSTRAINT [FK_user_credit_currency_id] FOREIGN KEY([currency_id])
REFERENCES [dbo].[currency] ([currency_id])
GO
ALTER TABLE [dbo].[user] CHECK CONSTRAINT [FK_user_credit_currency_id]
GO
ALTER TABLE [dbo].[user_picture]  WITH CHECK ADD  CONSTRAINT [FK_user_picture_picture_id] FOREIGN KEY([picture_id])
REFERENCES [dbo].[picture] ([picture_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user_picture] CHECK CONSTRAINT [FK_user_picture_picture_id]
GO
ALTER TABLE [dbo].[user_picture]  WITH CHECK ADD  CONSTRAINT [FK_user_picture_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([user_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[user_picture] CHECK CONSTRAINT [FK_user_picture_user_id]
GO
ALTER TABLE [dbo].[vehicle]  WITH CHECK ADD  CONSTRAINT [FK_vehicle_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([provider_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[vehicle] CHECK CONSTRAINT [FK_vehicle_provider_id]
GO
ALTER TABLE [dbo].[vehicle_feature]  WITH CHECK ADD  CONSTRAINT [FK_vehicle_feature_feature_id] FOREIGN KEY([feature_id])
REFERENCES [dbo].[feature] ([feature_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[vehicle_feature] CHECK CONSTRAINT [FK_vehicle_feature_feature_id]
GO
ALTER TABLE [dbo].[vehicle_feature]  WITH CHECK ADD  CONSTRAINT [FK_vehicle_feature_vehicle_id] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[vehicle] ([vehicle_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[vehicle_feature] CHECK CONSTRAINT [FK_vehicle_feature_vehicle_id]
GO
ALTER TABLE [dbo].[vehicle_picture]  WITH CHECK ADD  CONSTRAINT [FK_vehicle_picture_picture_id] FOREIGN KEY([picture_id])
REFERENCES [dbo].[picture] ([picture_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[vehicle_picture] CHECK CONSTRAINT [FK_vehicle_picture_picture_id]
GO
ALTER TABLE [dbo].[vehicle_picture]  WITH CHECK ADD  CONSTRAINT [FK_vehicle_picture_vehicle_id] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[vehicle] ([vehicle_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[vehicle_picture] CHECK CONSTRAINT [FK_vehicle_picture_vehicle_id]
GO
ALTER TABLE [dbo].[vehicle_pricing]  WITH CHECK ADD  CONSTRAINT [FK_vehicle_pricing_currency_id] FOREIGN KEY([currency_id])
REFERENCES [dbo].[currency] ([currency_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[vehicle_pricing] CHECK CONSTRAINT [FK_vehicle_pricing_currency_id]
GO
ALTER TABLE [dbo].[vehicle_pricing]  WITH CHECK ADD  CONSTRAINT [FK_vehicle_pricing_vehicle_id] FOREIGN KEY([vehicle_id])
REFERENCES [dbo].[vehicle] ([vehicle_id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[vehicle_pricing] CHECK CONSTRAINT [FK_vehicle_pricing_vehicle_id]
GO
/****** Object:  StoredProcedure [dbo].[FindChargeName]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FindChargeName] 
	-- Add the parameters for the stored procedure here
	(
		@booking_id int = 0,
		@start datetime = '2000-01-01',
		@end datetime = '2000-01-01',
		@provider_id int = 0,
		@currency_id int =0
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--declare @start datetime,
	--@end datetime,
	--@provider_id int,
	--@market_id int
	SET NOCOUNT ON;


	DECLARE @Parameters TABLE
(
   start date,
   [end] date,
   provider_id int,
   market_id int
);



if(@booking_id >0)

INSERT INTO 
    @Parameters(start, [end], provider_id, market_id)
	select cast(checkin_date as date), cast(checkout_date as date), provider_id , market_id from booking where booking_id = @booking_id; 

	else
INSERT INTO 
    @Parameters(start, [end], provider_id, market_id)
	
	select cast(@start as date), cast(@end as date), @provider_id, 
	(select market_id from market where provider_id = @provider_id and currency_id =  @currency_id) ;
    -- Insert statements for procedure here



	select s.supplement_id, s.provider_id, sp.market_id, d.fulldate,
	case when 
coalesce(sp.price_infant,0)+ coalesce(sp.price_child,0)+ coalesce(sp.price_teen,0)+ 
coalesce(sp.price_adult,0) + coalesce(price_honeymoon,0) + coalesce(price_per_item,0) +
coalesce(sp.price_infant,0) > 0 then 1 else 0 end hasprice, 
 s.name, s.[description]

 

from supplement s

inner join supplement_pricing sp
on s.supplement_id = sp.supplement_id

inner join [date] d
on d.fulldate between sp.valid_from and sp.valid_to

where s.is_compulsory =1 
and s.is_active = 1
and s.provider_id = (select provider_id from @Parameters)
and sp.market_id = (select market_id from @Parameters)
and d.fulldate between (select start from @Parameters) and (select [end] from @Parameters)


END
GO
/****** Object:  StoredProcedure [dbo].[FindPackage]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[FindPackage](
	-- Add the parameters for the stored procedure here
	@start date,
	@adults int,
	@age1 int=-1,
	@age2 int=-1,
	@age3 int=-1,
	@age4 int=-1,
	@age5 int=-1,		
	@nights int =0,
	@currency_id int=0,
	@hotel_id int=0,
	@room_num int = 0,
	@package_id int = 0,
	@is_honeymoon int = 0,
	@onlyBestPrice int = 0


	)
AS
BEGIN

select * from
(

select T.* ,

rank() over(partition by provider_id, nights order by T.total_price) as rn,
STUFF(( SELECT  '|'+ convert(varchar(10),unavailable_date,103) FROM dbo.v_unavailability a
WHERE T.room_id = a.room_id and a.unavailable_date between @start and dateadd(day,@nights-1,@start) FOR XML PATH('')),1 ,1, '')   unavailable_dates




from
(
select 
pk.package_id,
pk.package_name,
pk.room_id,
pk.room_name,
pk.provider_id,
pk.provider_name,
pk.description,
pk.market_id,
pk.currency_code,
pk.currency_id,
pk.symbol,
max(pk.base_price_id) base_price_id,
min(pk.fulldate) from_date,
--max(pk.fulldate) to_date,
DATEADD(DAY,1,max(pk.fulldate)) to_date,
(DATEDIFF(DAY,min(pk.fulldate),max(pk.fulldate))+1) nights,

sum(case @adults 
when 1 then pk.price_single
when 2 then coalesce(pk.price_twin,pk.price_single)
when 3 then coalesce(pk.price_triple,pk.price_single)
when 4 then coalesce(pk.price_quadruple,pk.price_single)
else pk.price_single end) total_per_adult,

sum(coalesce(pk.price_honeymoon,pk.price_twin, pk.price_single)) total_per_honeymooner,
sum(coalesce(pk.price_infant,0.0)) total_per_infant,
sum(coalesce(pk.price_child,0.0)) total_per_child,
sum(coalesce(pk.price_teen,0.0)) total_per_teen,



sum(coalesce(pk.chg_adult,0.0)) charges_adult,
sum(coalesce(pk.chg_infant,0.0)) charges_infant,
sum(coalesce(pk.chg_child,0.0)) charges_child,
sum(coalesce(pk.chg_teen,0.0)) charges_teen,



max(coalesce(pk.price_ground_handling_adult,0.0)) ground_handling_adult,
max(coalesce(pk.price_ground_handling_teen,0.0)) ground_handling_teen,
max(coalesce(pk.price_ground_handling_child,0.0)) ground_handling_child,



pk.incl_grnd_handling includes_ground_handling,

case @is_honeymoon when 1 then

sum(coalesce(pk.price_honeymoon,pk.price_twin, pk.price_single)) 
+ max(coalesce(pk.price_ground_handling_adult,0.0)) 
+ sum(coalesce(pk.chg_adult,0.0)) 
else 

sum(case @adults 
when 1 then pk.price_single
when 2 then coalesce(pk.price_twin,pk.price_single)
when 3 then coalesce(pk.price_triple,pk.price_single)
when 4 then coalesce(pk.price_quadruple,pk.price_single)
else pk.price_single end)  

+ max(coalesce(pk.price_ground_handling_adult,0.0)) 

+ sum(coalesce(pk.chg_adult,0.0)) 

end

as total_price,

 max(coalesce(pk.price_ground_handling_adult,0.0)) 
+ max(coalesce(pk.price_ground_handling_teen,0.0)) 
+ max(coalesce(pk.price_ground_handling_child,0.0))
as total_cost_ground_handling,
@room_num as room_sequence,
	@adults num_adults,
	max(gt.infant) num_infants,
	max(gt.child) num_children,
	max(gt.teen) num_teens,
	case when sum(case when una.room_id is not null then 1 else 0 end )>0 then 'Y' else 'N' end as unavailable_flag
	
	

from v_package pk


inner join [date] dt
on dt.fulldate between @start and DATEADD(DAY,pk.number_nights-1,@start)
and dt.fulldate = pk.fulldate


inner join room r 
on pk.room_id = r.room_id


left join room_occupancy ro
on r.room_id = ro.room_id

left join occupancy o 
on o.occupancy_id = ro.occupancy_id


left join dbo.v_unavailability una 
on una.room_id=pk.room_id
and una.unavailable_date=pk.fulldate

Cross apply  [dbo].[fn_GetGuestDefinition](r.provider_id,@age1,@age2,@age3,@age4,@age5) gt


where r.is_active = 1
and pk.is_active = 1
and (@nights = 0 or (@nights = pk.number_nights))
and (@currency_id = 0 or (@currency_id = pk.currency_id))
and (@hotel_id = 0 or (@hotel_id = pk.provider_id))
and (@package_id = 0 or (@package_id = pk.package_id))

and o.adult = @adults
 and o.infant = gt.infant
 and o.teen = gt.teen
 and o.child = gt.child


group by 
pk.package_id,
pk.package_name,
pk.room_id,
pk.room_name,
pk.provider_id,
pk.provider_name,
pk.description,
pk.market_id,
pk.currency_code,
pk.currency_id,
--pk.base_price_id,
pk.symbol,
pk.incl_grnd_handling,
pk.number_nights 


having  DATEDIFF(DAY,min(pk.fulldate),max(pk.fulldate)) >= (pk.number_nights -1)

) as T

) as OT
where 
	(@onlyBestPrice = 0  or OT.rn = @onlyBestPrice)

END
GO
/****** Object:  StoredProcedure [dbo].[FindRoom]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[FindRoom](
	-- Add the parameters for the stored procedure here
	@start date,
	@end date,
	@adults int,
	@age1 int=-1,
	@age2 int=-1,
	@age3 int=-1,
	@age4 int=-1,
	@age5 int=-1,	
	@occNum int,
	@ishoneymoon int = 0,		
	@providerId int = 0,
	@currencyId int = 0,
	@onlyBestPrice int =0

	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
		-- onlyBestPrice (0 or 1)
		--0: returns all rooms found
		--1: returns lowest priced room per hotel

	SET NOCOUNT ON;

	declare @ds int = Datediff(day,@start,@end)

select * from

(

select 
	rank() over(partition by provider_id order by T.total_price) as rn,
T.*,
STUFF(( SELECT  '|'+ convert(varchar(10),unavailable_date,103) FROM dbo.v_unavailability a
WHERE T.room_id = a.room_id and a.unavailable_date between @start and @end FOR XML PATH('')),1 ,1, '')   unavailable_dates

from
(
	
select  

	@occNum as room_sequence,
	p.provider_id,
	p.name provider_name,
	min(bp.base_price_id) base_price_id,
	m.market_id,
	c.currency_id,
	c.currency_code,
	c.symbol,
	r.room_id, 
	r.name room_name,
	
	@ds nights,

	/* fields below added to match return type of package */

		'' as [description],
		@start from_date,
		null ground_handling_adult,
		null ground_handling_child,
		null ground_handling_teen,
		0 includes_ground_handling,
		0 package_id,
		'' package_name,
		@end to_date,
		null total_cost_ground_handling,



	
	sum(case @ishoneymoon when 1 then 
	/* honeymoon */
	(coalesce(price_honeymoon, price_twin, price_single)) * 2
	else 
	(
		(case @adults when 1 then (price_single) 
	when 2 then (coalesce(price_twin, price_single))  
	When 3 then (coalesce(price_triple, price_single)) 
	when 4 then (coalesce(price_quadruple, price_single)) 
	else (price_single) end) +
	(coalesce(price_infant,0)) * gt.infant+
	(coalesce(price_child,0)) *gt.child +
	(coalesce(price_teen,0) ) * gt.teen +

	(coalesce(bp.chg_infant,0)) * gt.infant+
	(coalesce(bp.chg_child,0)) * gt.child+
	(coalesce(bp.chg_child,0)) * gt.teen+
	(coalesce(bp.chg_adult,0)) * @adults
	)
	end) total_price,

	sum(case @adults when 1 then (price_single) 
	when 2 then (coalesce(price_twin, price_single))  
	When 3 then (coalesce(price_triple, price_single)) 
	when 4 then (coalesce(price_quadruple, price_single)) 
	else (price_single) end) total_per_adult,
	
	sum(coalesce(price_honeymoon, price_twin, price_single)) total_per_honeymooner,

	sum(coalesce(price_infant,0)) total_per_infant,
	sum(coalesce(price_child,0)) total_per_child,
	sum(coalesce(price_teen,0) ) total_per_teen,

	sum(coalesce(bp.chg_infant,0)) charges_infant,
	sum(coalesce(bp.chg_child,0)) charges_child,
	sum(coalesce(bp.chg_child,0)) charges_teen,
	sum(coalesce(bp.chg_adult,0)) charges_adult,
	@adults num_adults,
	max(gt.infant) num_infants,
	max(gt.child) num_children,
	max(gt.teen) num_teens,
	case when sum(case when una.room_id is not null then 1 else 0 end )>0 then 'Y' else 'N' end as unavailable_flag
	
from v_price bp

inner join market m
on bp.market_id = m.market_id

inner join currency c
on m.currency_id = c.currency_id

inner join room r 
on bp.room_id = r.room_id

inner join provider p 
on r.provider_id = p.provider_id

left join room_occupancy ro
on bp.room_id = ro.room_id

left join occupancy o 
on o.occupancy_id = ro.occupancy_id

left join dbo.v_unavailability una 
on una.room_id=bp.room_id
and una.unavailable_date=bp.fulldate

Cross apply  [dbo].[fn_GetGuestDefinition](r.provider_id,@age1,@age2,@age3,@age4,@age5) gt

where 

bp.fulldate between @start and dateadd(DAY,-1,@end)
 

 and o.adult = @adults
 and o.infant = gt.infant
 and o.teen = gt.teen
 and o.child = gt.child

and (@providerId = 0 or p.provider_id = @providerId)
and (@currencyId = 0 or c.currency_id = @currencyId)


and r.is_active = 1
and p.is_active = 1
and m.is_active =1 

group by 
c.currency_id,
c.currency_code,
c.symbol,
	r.room_id,
	r.name,
	m.market_id,
	p.provider_id,
	p.name
	having count(bp.fulldate) = @ds

	)  as T

) OT
	where 
	(@onlyBestPrice = 0  or OT.rn = @onlyBestPrice)

	
END
GO
/****** Object:  StoredProcedure [dbo].[GetCharges]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CReate PROCEDURE [dbo].[GetCharges](
	-- Add the parameters for the stored procedure here
	@start date,
	@end date,
	@provider_id int,	
	@market_id int = 0
	

	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;
	Select 
supplement_id, name, description, sum(price_infant) price_infant, sum(price_child) price_child, 
sum(price_teen) price_teen, sum(price_adult) price_adult 

from 


(

select s.supplement_id, s.name, s.description,  s.provider_id, sp.market_id, d.fulldate, sp.price_infant, sp.price_child, sp.price_teen, sp.price_adult
from supplement s

inner join supplement_pricing sp
on s.supplement_id = sp.supplement_id

inner join [date] d
on d.fulldate between sp.valid_from and sp.valid_to

where s.is_compulsory =1 
and s.is_active = 1
and sp.is_active = 1

) T

inner join [date] dt
on dt.fulldate between @start and dateadd(day,-1,@end)



where T.provider_id = @provider_id
and (T.market_id = 0 or T.market_id = @market_id)
and T.fulldate = dt.fulldate

group by
supplement_id, name, description


END
GO
/****** Object:  StoredProcedure [dbo].[GetPromotions]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[GetPromotions](
	-- Add the parameters for the stored procedure here
	@start date,
	@end date,	
	@provider_id int ,
	@room_id int =0 ,
	@market_id int =0 ,
	@is_honeymoon int = 0
	)
AS
BEGIN
with promo_data as 
(
select distinct
p.promotion_id, 
p.name, 
p.description, 
p.contracted_offer, 
p.cancellation_policy,
markets.market_id,
markets.market_name,
rooms.room_id,
rooms.room_name,
p.min_nights,
p.free_nights,
p.discount,
p.is_honeymoon,
p.apply_all_rooms,
p.apply_all_markets,
coalesce(c.conf_key,'NA') promo_categ
 from promotion p 
 inner join  --get all markets applicable 
(
	select 
	p.promotion_id,
	p.apply_all_markets,
	m.market_id,
	m.market_name,
	m.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_market] pm
	on p.promotion_id=pm.promotion_id
	right join [dbo].[market] m
	on ( (p.apply_all_markets=0 and  m.market_id=pm.market_id)  --either it applies to a specific market or 
	or  p.apply_all_markets=1   --to all markets 
	)
	and p.provider_id=m.provider_id
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) markets 
on markets.provider_id=p.provider_id
inner join   --get all rooms applicable 
(
	select 
	p.promotion_id,
	p.apply_all_rooms,
	r.room_id,
	r.name room_name,
	r.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_room] pr
	on p.promotion_id=pr.promotion_id
	right join [dbo].[room] r 
	on (
	(r.room_id=pr.room_id and p.apply_all_rooms=0)  --either it applies to a specific room or 
	or  p.apply_all_rooms=1   ---or to all rooms 
	)
	and r.provider_id=p.provider_id
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
	
) rooms 
on rooms.promotion_id=markets.promotion_id
and rooms.provider_id=p.provider_id
and p.promotion_id=rooms.promotion_id


--START OF SECTION USED TO REMOVE DUPLICATE PROMO CATEGORIZED IN GROUPS
left join dbo.[configuration] c
on  p.promotion_group_id= c.configuration_id
and c.conf_type='PROMOCATEG'


where  p.provider_id = @provider_id
and is_active = 1
and getDate() between coalesce(book_from_date,'1900-01-01') and coalesce(book_until_date,'2500-12-31')
and @start >= coalesce(travel_from_date,'1900-01-01') 
and @end <= coalesce(travel_until_date,'2500-12-31')
and @start >= valid_from
and @end <= valid_to
and DATEDIFF(day,@start,@end) >= coalesce(min_nights,0)
and 
Datediff(day,GetDate(),@start) between coalesce(p.book_before_days_min,0) and coalesce(p.book_before_days_max,Datediff(day,GetDate(),@start))

and (@room_id = 0 or @room_id=rooms.room_id) --if room is not specified take all else filter 
and (@market_id =0 or @market_id = markets.market_id) --if market is not specified take all else filter 
and (is_honeymoon = @is_honeymoon or @is_honeymoon= 1)

)


select 
p.promotion_id, 
p.name, 
p.description, 
p.contracted_offer, 
p.cancellation_policy,
p.market_id,
p.market_name,
p.room_id,
p.room_name,
p.min_nights,
p.free_nights,
p.discount,
p.is_honeymoon,
p.apply_all_rooms,
p.apply_all_markets

 from promo_data p 
 
left join
(
select
promotion_id,promo_categ,room_id,market_id
from
(
select promotion_id,room_id,market_id,coalesce(promo_categ,'NA') promo_categ, (coalesce(free_nights,0)*100) + coalesce(discount,0) score,
ROW_NUMBER() 
	over (partition by room_id,market_id,coalesce(promo_categ,'NA') 
	order by (coalesce(free_nights,0)*100) + coalesce(discount,0) desc)
as  score_rank 
from promo_data p
where  coalesce(promo_categ,'NA')<>'NA'

) top_promo 
where top_promo.score_rank=1 --get only the first one

) catpromo
on 
	catpromo.promotion_id=p.promotion_id
and catpromo.promo_categ=p.promo_categ
and catpromo.room_id=p.room_id
and catpromo.market_id=p.market_id
where  
((catpromo.promo_categ is null and coalesce(p.promo_categ,'NA')='NA') or (catpromo.promo_categ is not null ))

END
GO
/****** Object:  StoredProcedure [dbo].[GetPromotionsWithSets]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetPromotionsWithSets](
	-- Add the parameters for the stored procedure here
	@start date,
	@end date,	
	@provider_id int ,
	@room_id int =0 ,
	@market_id int =0 ,
	@is_honeymoon int = 0
	)
AS
BEGIN
with promo_data as 
(
select distinct
p.promotion_id, 
p.name,
p.code, 
p.description, 
p.contracted_offer, 
p.cancellation_policy,
markets.market_id,
markets.market_name,
rooms.room_id,
rooms.room_name,
p.min_nights,
p.free_nights,
p.discount,
p.is_honeymoon,
p.apply_all_rooms,
p.apply_all_markets,
p.flat_single,
p.flat_twin,
p.flat_triple,
p.flat_quadruple,
p.flat_child1,
p.flat_child2,
p.flat_teen1,
p.flat_teen2,
coalesce(c.conf_key,'NA') promo_categ,
coalesce(ps.promotion_set_id,-1 * p.promotion_id) promotion_set_id, --added a * -1 to make all promotions mutually exclusive by default without overlapping with any other promoset
coalesce(ps.name,p.name + '(Default)') promotion_set_name
 from promotion p 
 inner join  --get all markets applicable 
(
	select 
	p.promotion_id,
	p.apply_all_markets,
	m.market_id,
	m.market_name,
	m.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_market] pm
	on p.promotion_id=pm.promotion_id
	right join [dbo].[market] m
	on ( (p.apply_all_markets=0 and  m.market_id=pm.market_id)  --either it applies to a specific market or 
	or  p.apply_all_markets=1   --to all markets 
	)
	and p.provider_id=m.provider_id
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) markets 
on markets.provider_id=p.provider_id
inner join   --get all rooms applicable 
(
	select 
	p.promotion_id,
	p.apply_all_rooms,
	r.room_id,
	r.name room_name,
	r.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_room] pr
	on p.promotion_id=pr.promotion_id
	right join [dbo].[room] r 
	on (
	(r.room_id=pr.room_id and p.apply_all_rooms=0)  --either it applies to a specific room or 
	or  p.apply_all_rooms=1   ---or to all rooms 
	)
	and r.provider_id=p.provider_id
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
	
) rooms 
on rooms.promotion_id=markets.promotion_id
and rooms.provider_id=p.provider_id
and p.promotion_id=rooms.promotion_id

--START OF SECTION USED TO REMOVE DUPLICATE PROMO CATEGORIZED IN GROUPS
left join dbo.[configuration] c
on  p.promotion_group_id= c.configuration_id
and c.conf_type='PROMOCATEG'


left join dbo.promo_set_promotions psp
on psp.promotion_id=p.promotion_id

left join dbo.promotion_set ps
on ps.promotion_set_id=psp.promotion_set_id
and ps.is_active=1

where  p.provider_id = @provider_id
and p.is_active = 1
and getDate() between coalesce(book_from_date,'1900-01-01') and coalesce(book_until_date,'2500-12-31')
and @start >= coalesce(travel_from_date,'1900-01-01') 
and @end <= coalesce(travel_until_date,'2500-12-31')
and @start >= valid_from
and (coalesce(min_nights,0) = 
case when coalesce(free_nights,0) > 0 then 
(
SELECT  top(1) min_nights FROM [promotion] where provider_id = @provider_id and coalesce(min_nights,0) <= DATEDIFF(day,@start,@end) order by min_nights desc) 
end
or
coalesce(min_nights,0) <=  case when coalesce(free_nights,0)=0 then
DATEDIFF(day,@start,@end)
end
)
and @end <= valid_to
--and (DATEDIFF(day,@start,@end) >= coalesce(min_nights,0) and  coalesce(free_nights,0)=0) 
--and (min_nights = 6 or coalesce(min_nights,0)=0)
and Datediff(day,GetDate(),@start) between coalesce(p.book_before_days_min,0) and coalesce(p.book_before_days_max,Datediff(day,GetDate(),@start))
and (@room_id = 0 or @room_id=rooms.room_id) --if room is not specified take all else filter 
and (@market_id =0 or @market_id = markets.market_id) --if market is not specified take all else filter 
and (is_honeymoon = @is_honeymoon or @is_honeymoon= 1)

),

--for all sets containing only one promotion, return only the first set, to eliminate duplicates at booking level
uniquepromosets as 
(

select 
promotion_set_id,
promotion_id
from 
(
select 
promotion_set_id,
promotion_id,
row_number() over ( partition by promotion_id order by promotion_set_id,promotion_id) rec_cnt

from
(
	SELECT 
	p.[promotion_set_id]
	,p.[promotion_id]
	,count(p.promotion_id) over (partition by p.[promotion_set_id]) num_promo
	FROM 
			(
			select 
			distinct
			p.[promotion_set_id]
			,p.[promotion_id]
			from
			promo_data p
			) p
	)
 a 
 where a.num_promo=1
 ) b 
 where rec_cnt>1

)


select 
p.promotion_id, 
p.name,
p.code, 
p.description, 
p.contracted_offer, 
p.cancellation_policy,
p.market_id,
p.market_name,
p.room_id,
p.room_name,
p.min_nights,
p.free_nights,
p.discount,
p.is_honeymoon,
p.apply_all_rooms,
p.apply_all_markets,
p.flat_single,
p.flat_twin,
p.flat_triple,
p.flat_quadruple,
p.flat_child1,
p.flat_child2,
p.flat_teen1,
p.flat_teen2,
p.promo_categ,
p.promotion_set_id,
p.promotion_set_name
 from promo_data p 
 --join with unique promotions and matching records 
 left join uniquepromosets up
 on up.promotion_set_id=p.promotion_set_id
 and up.promotion_id=up.promotion_id
 
left join
(
select
promotion_id,promo_categ,room_id,market_id,promotion_set_id
from
(
select promotion_id,room_id,market_id,p.promotion_set_id,coalesce(promo_categ,'NA') promo_categ, ((coalesce(free_nights,0)/DATEDIFF(day,@start,@end))*100) + coalesce(discount,0) score,
ROW_NUMBER() 
	over (partition by room_id,market_id,p.promotion_set_id,coalesce(promo_categ,'NA') 
	order by ((coalesce(free_nights,0)/DATEDIFF(day,@start,@end))*100) + coalesce(discount,0) desc)
as  score_rank 
from promo_data p
where  coalesce(promo_categ,'NA')<>'NA'

) top_promo 
where top_promo.score_rank=1 --get only the first one

) catpromo
on 
	catpromo.promotion_id=p.promotion_id
and catpromo.promo_categ=p.promo_categ
and catpromo.room_id=p.room_id
and catpromo.market_id=p.market_id
and catpromo.promotion_set_id=p.promotion_set_id
where  
((catpromo.promo_categ is null and coalesce(p.promo_categ,'NA')='NA') or (catpromo.promo_categ is not null ))
and up.promotion_id is null --eliminate all promo returned in the uniquepromo dataset
END
GO
/****** Object:  StoredProcedure [dbo].[GetSupplements]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetSupplements]
	@start date, --checkin_date
	@end date, --checkout_date
	@compulsory bit =0 , 
	--additional optional parameters in case an outer apply is required
	@provider int =0,  
	@market int =0,
	@room int =0,	
	@fulldate datetime='1900-01-01',
	@adults int=0,
	@infants int=0,
	@children int=0,
	@teens int=0,
	@room_num int = 0
AS

BEGIN
	select 
	distinct
	[supplement_id]
      ,[provider_id]
      ,[name]
      ,[description]
      ,[is_per_person]
      ,[is_per_room]
      ,[is_per_night]
      ,[is_per_item]
      ,[valid_all_rooms]
      ,[meal_plan_id]
      ,[room_id]
      ,[market_id]
      ,[symbol]
      ,[currency_code]
      ,[valid_from]
      ,[valid_to]
      ,[price_per_item]
      ,[price_adult]
      ,[price_infant]
      ,[price_child]
      ,[price_teen]
	  ,case when [is_per_room] =1 then @room_num else 1 end * case when [is_per_night] =1 then datediff(day,@start,@end) else 1 end * [price_adult]	 * @adults as		[total_price_adult]
      ,case when [is_per_room] =1 then @room_num else 1 end * case when [is_per_night] =1 then datediff(day,@start,@end) else 1 end * [price_infant]	 * @infants as  [total_price_infant]
      ,case when [is_per_room] =1 then @room_num else 1 end * case when [is_per_night] =1 then datediff(day,@start,@end) else 1 end * [price_child]	 * @children as		[total_price_child]	
      ,case when [is_per_room] =1 then @room_num else 1 end * case when [is_per_night] =1 then datediff(day,@start,@end) else 1 end * [price_teen]		 * @teens  as	[total_price_teen]


      ,[is_compulsory]
	from 
	[dbo].[v_supplement]
	where is_compulsory=@compulsory 	
	and 
	@start >= valid_from and @start <=valid_to
	and 
	@end >= valid_from and @end <=valid_to
	 --only return prices for dates within booking
	and  /* handle additional parameters if required */
	(@provider=0 or @provider =provider_id)
	and 
	( @market=0 or @market= market_id)
	and
	(  case when [valid_all_rooms]=1 then 1 when [valid_all_rooms]=0  and( @room=room_id or @room=0 ) then 1 else 0 end =1 )
	
end
GO
/****** Object:  StoredProcedure [dbo].[PopulateDates]    Script Date: 05-Aug-17 9:14:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PopulateDates]
	-- Add the parameters for the stored procedure here
@StartDate		DATETIME = '2014-01-01',
@NumberOfDays	INT = 3000
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

 
	INSERT
	INTO	dbo.date
			(fulldate)
	SELECT	DATEADD(DAY, RowNum, @StartDate) AS DateValue
	FROM	(SELECT	TOP (@NumberOfDays)
					p1.PARAMETER_ID,
					ROW_NUMBER() OVER (ORDER BY p1.PARAMETER_ID) - 1 AS RowNum
			 FROM	sys.all_parameters p1
			 CROSS	JOIN sys.all_parameters p2) NumberTable
END

GO
USE [master]
GO
ALTER DATABASE [BookingSystemDB] SET  READ_WRITE 
GO
