//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Booking_System.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class user
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public user()
        {
            this.logs = new HashSet<log>();
            this.notes = new HashSet<note>();
            this.user_picture = new HashSet<userPicture>();
            this.bookings = new HashSet<booking>();
            this.bookings1 = new HashSet<booking>();
            this.bookings2 = new HashSet<booking>();
        }
    
        public int user_id { get; set; }
        public string title { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public Nullable<System.DateTime> date_of_birth { get; set; }
        public Nullable<int> gender { get; set; }
        public string nationality { get; set; }
        public int role { get; set; }
        public string officephone { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public string company { get; set; }
        public string job_title { get; set; }
        public string password { get; set; }
        public Nullable<double> markup_percentage { get; set; }
        public Nullable<double> markup_fixed { get; set; }
        public string username { get; set; }
        public Nullable<double> credit_limit { get; set; }
        public Nullable<double> credit_used { get; set; }
        public bool is_active { get; set; }
        public Nullable<System.DateTime> temp_password_expiry { get; set; }
        public string temp_password { get; set; }
        public bool is_credit_based { get; set; }
        public int currency_id { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<log> logs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<note> notes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<userPicture> user_picture { get; set; }
        public virtual currency currency { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<booking> bookings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<booking> bookings1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<booking> bookings2 { get; set; }
    }
}
