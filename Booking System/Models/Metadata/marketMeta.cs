﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    
    [MetadataType(typeof(marketMeta))]
    public partial class market
    {

    }


    public class marketMeta
    {

        [DisplayName("Market Code")]
        public int market_id { get; set; }

        [DisplayName("Market Name")]
        [Required]
        [MaxLength(50)]
        public string market_name { get; set; }

        [DisplayName("Currency")]
        [Required]
        public int currency_id { get; set; }

        [DisplayName("Hotel")]
        [Required]
        public int provider_id { get; set; }

        [DisplayName("Active")]
        public bool is_active { get; set; }

        [DisplayName("Default Market")]
        public bool is_default { get; set; }
    }
}