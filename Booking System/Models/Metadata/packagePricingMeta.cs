﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.ComponentModel.DataAnnotations;
//using System.Linq;
//using System.Web;

//namespace Booking_System.Models
//{

//    [MetadataType(typeof(packagePricingMeta))]
//    public partial class packagePricing { }


//    public class packagePricingMeta
//    {
//        public int package_pricing_id { get; set; }

//        [DisplayName("Package")]
//        [Required]
//        public int package_id { get; set; }

//        [DisplayName("Market")]
//        [Required]
//        public int market_id { get; set; }

//        [DisplayName("Valid From")]
//        [Required]
//        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
//               ApplyFormatInEditMode = true)]
//        public System.DateTime valid_from { get; set; }

//        [DisplayName("Valid Until")]
//        [Required]
//        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}",
//               ApplyFormatInEditMode = true)]
//        public System.DateTime valid_to { get; set; }


//        [DisplayName("Single Price")]
//        public Nullable<double> price_single { get; set; }

//        [DisplayName("Twin Price")]
//        public Nullable<double> price_twin { get; set; }

//        [DisplayName("Triple Price")]
//        public Nullable<double> price_triple { get; set; }

//        [DisplayName("Infant Price")]
//        public Nullable<double> price_infant { get; set; }

//        [DisplayName("Child Price")]
//        public Nullable<double> price_child { get; set; }

//        [DisplayName("Child Without bed Price")]
//        public Nullable<double> price_child_without_bed { get; set; }
//        [DisplayName("Teen Price")]

//        public Nullable<double> price_teen { get; set; }

//        [DisplayName("Senior Price")]
//        public Nullable<double> price_senior { get; set; }


//        [DisplayName("Honeymoon Price")]
//        public Nullable<double> price_honeymoon { get; set; }


//        [DisplayName("Ground Handling Price")]
//        public Nullable<double> price_ground_handling	{ get; set; }
        
//        [DisplayName("Active")]
//        public bool is_active { get; set; }
//    }
//}