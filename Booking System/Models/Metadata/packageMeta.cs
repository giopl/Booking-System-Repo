﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{

    [MetadataType(typeof(packageMeta))]
    public partial class package { }


    public class packageMeta
    {

        [DisplayName("Package id")]
        [Required]
        public int package_id { get; set; }

        [DisplayName("Hotel")]
        [Required]
        public int provider_id { get; set; }

        [DisplayName("Market")]
        [Required]
        public int market_id { get; set; }


        [DisplayName("Package Name")]
        [Required]
        [MaxLength(100)]
        public string name { get; set; }

        [DisplayName("Description")]
        [AllowHtml]
       
        [DataType(DataType.MultilineText)]
        public string description { get; set; }

        [DisplayName("Number of Nights")]
        public Nullable<int> number_nights { get; set; }

        [DisplayName("Valid From")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
        ApplyFormatInEditMode = true)]
        public System.DateTime valid_from { get; set; }

        [DisplayName("Valid Until")]
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
        ApplyFormatInEditMode = true)]
        public System.DateTime valid_to { get; set; }

        [DisplayName("Contracted Offer")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string contracted_offer { get; set; }

        [DisplayName("Cancellation policy")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string cancellation_policy { get; set; }


       [DisplayName("Ground Handling Adult")]
        public Nullable<double> price_ground_handling_adult { get; set; }

       [DisplayName("Ground Handling Child")]
        public Nullable<double> price_ground_handling_child { get; set; }

        [DisplayName("Ground Handling Teen")]
        public Nullable<double> price_ground_handling_teen { get; set; }



        [DisplayName("Apply to all rooms")]
        public bool apply_all_rooms { get; set; }

        [DisplayName("Active")]
        public bool is_active { get; set; }


       /* [DisplayName("Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Required]
        public System.DateTime valid_from { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Required]
        public System.DateTime valid_to { get; set; }
        */
    }
}