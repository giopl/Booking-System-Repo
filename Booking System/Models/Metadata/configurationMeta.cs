﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    public class configurationMeta
    {
        [DisplayName("ID")]
        public int configuration_id { get; set; }
      
        [DisplayName("Code")]
        [Required]
        public string conf_key { get; set; }
        [DisplayName("Type")]
        [Required]
        public string conf_type { get; set; }
        [DisplayName("Description")]
        [Required]
        public string conf_value { get; set; }
    }


    [MetadataType(typeof(configurationMeta))]
    public partial class configuration
    {
    
    }
}