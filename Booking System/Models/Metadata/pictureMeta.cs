﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    [MetadataType(typeof(pictureMeta))]
    public partial class picture
    {
        public ImagesType imgType { get; set; }

        public int SectionId { get; set; }

        public string FullFileName
        {
            get
            {
                string ext = string.Empty;

                string fileName = string.Empty;

                bool hasExt = false;

                if (!String.IsNullOrWhiteSpace(this.file_name))
                {
                    var str = this.file_name.Split('.');

                    if (str != null && str.Length > 0)
                    {
                        ext = str.Last();
                        hasExt = true;
                    }
                }

                if (this.picture_guid != null && this.picture_guid.HasValue)
                {
                    fileName = this.picture_guid.ToString();
                }

                if (hasExt)
                {
                    return string.Concat(fileName, ".", ext);

                }

                return fileName;
            }
        }

        /// <summary>
        /// Carrier property for other objects
        /// </summary>
        public ImgSection Section { get; set; }

        public string DisplayImageFileName
        {
            get
            {
                string imgFileName = string.Empty;
                if(!String.IsNullOrWhiteSpace(this.file_name))
                {
                    imgFileName = this.file_name;
                    if(this.file_name.Length > 23)
                    {
                        imgFileName = String.Concat(this.file_name.Substring(0, 20), "...");
                    }
                }

                return imgFileName;
            }
        }

        public string DisplayImageTitle
        {
            get
            {
                string imgTitle = string.Empty;
                if (!String.IsNullOrWhiteSpace(this.title))
                {
                    imgTitle = this.title;
                    if (this.title.Length > 23)
                    {
                        imgTitle = String.Concat(this.title.Substring(0, 20), "...");
                    }
                }

                return imgTitle;
            }
        }

    }

    public class pictureMeta
    {
        [DisplayName("File Name")]
        [MaxLength(200)]
        public string file_name { get; set; }

        [DisplayName("Title")]
        [MaxLength(100)]
        public string title { get; set; }

        [DisplayName("Description")]
        [MaxLength(150)]
        public string description { get; set; }

    }



    public enum ImagesType
    {
        User = 0,
        Hotel,
        Provider,
        Room,
        Vehicle,
        Activity,
        TourOperator
    }

    public enum ImgSection
    {
        None = 0,
        Search,
        Gallery,
        Main,
        Featured
    }

}