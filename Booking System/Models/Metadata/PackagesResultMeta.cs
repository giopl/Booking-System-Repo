﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    public class PackagesResultMeta
    {
        public Nullable<double> pp_adult { get; set; }
        public Nullable<double> pp_infant { get; set; }
        public Nullable<double> pp_child { get; set; }
        public Nullable<double> pp_teen { get; set; }
        public Nullable<double> total_price { get; set; }
        public Nullable<int> number_nights { get; set; }
        public string name { get; set; }
        public int provider_id { get; set; }
        public string currency_code { get; set; }
        public string symbol { get; set; }
        public int room_id { get; set; }
        public int incl_grnd_handling { get; set; }
        public int base_price_id { get; set; }
    }

    [MetadataType(typeof(PackagesResultMeta))]
    public partial class PackagesResult
    {
        public int NumInfant { get; set; }
        public int NumChild { get; set; }
        public int NumTeen { get; set; }
        public int NumAdult { get; set; }
        public int RoomNumber { get; set; }


    }
}