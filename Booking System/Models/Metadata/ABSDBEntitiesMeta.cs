﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Z.EntityFramework.Plus;
using System.Data.Entity;
using Booking_System.Helpers;
using System.Data.Entity.Validation;
using System.ComponentModel.DataAnnotations;

namespace Booking_System.Models
{
    [MetadataType(typeof(ABSDBEntitiesMeta))]
    public partial class ABSDBEntities : DbContext
    {

        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    log.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        log.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

            catch (Exception)
            {

                throw;
            }
            
        }
        
 

    }



    public class ABSDBEntitiesMeta
    {
    }
}