﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    [MetadataType(typeof(supplementPricingMeta))]
    public partial class supplementPricing { }

    public class supplementPricingMeta
    {
        [DisplayName("ID")]
        public int supplement_id { get; set; }

       
        [DisplayName("Market")]
        [Required]
        public Nullable<int> market_id { get; set; }

        [DisplayName("Valid From")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Required]
        public Nullable<System.DateTime> valid_from { get; set; }

        [DisplayName("Valid Until")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Required]
        public Nullable<System.DateTime> valid_to { get; set; }

        [DisplayName("Price per item")]
        [Range(0, double.MaxValue)]
        public Nullable<double> price_per_item { get; set; }

        [DisplayName("Price for infant")]
        [Range(0, double.MaxValue)]
        public Nullable<double> price_infant { get; set; }

        [DisplayName("Price for child")]
        [Range(0, double.MaxValue)]
        public Nullable<double> price_child { get; set; }

        [DisplayName("Price for teen")]
        [Range(0, double.MaxValue)]
        public Nullable<double> price_teen { get; set; }

        [DisplayName("Price for adult")]
        [Range(0, double.MaxValue)]
        public Nullable<double> price_adult { get; set; }

        [DisplayName("Price for senior")]
        [Range(0, double.MaxValue)]
        public Nullable<double> price_senior { get; set; }

        [DisplayName("Honeymoon Price")]
        public Nullable<double> price_honeymoon { get; set; }


    }
}