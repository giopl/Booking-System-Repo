﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{
    public class errorLogMeta
    {

        [DisplayName("Encountered By")]
        public string user_name { get; set; }
        public string controller { get; set; }
        public string action { get; set; }
        public string inner_exception { get; set; }
        public string message { get; set; }
        [DisplayName("Stacktrace")]
        public string stack_trace { get; set; }
        [DisplayName("Occurred On")]
        public Nullable<System.DateTime> error_date_time { get; set; }

    }

    [MetadataType(typeof(errorLogMeta))]
    public partial class errorLog
    {
       }

}