﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{

    [MetadataType(typeof(providerMeta))]
    public partial class provider {
        


        [DisplayName("Latitude Coordinates:")]
        public string GoogleLatitude { get; set;}

        [DisplayName("Longitude Coordinates:")]
        public string GoogleLongitude { get; set; }

        public bool IsHotel
        {
            get
            {
                return provider_type == "H";
            }
        }

        public bool IsTourOperator
        {
            get
            {
                return provider_type == "A";
            }
        }
        public bool IsRental
        {
            get
            {
                return provider_type == "R";
            }
        }
        public bool IsTransfer
        {
            get
            {
                return provider_type == "R";
            }
        }

        
    }




    public class providerMeta
    {
        [DisplayName("Id")]    
        public int provider_id { get; set; }

        [Required]
        [DisplayName("Provider Name")]
        [MaxLength(100)]
        public string name { get; set; }

        [DisplayName("Description")]
        [AllowHtml]
        
        [DataType(DataType.MultilineText)]
        public string description { get; set; }

        [DisplayName("Region")]
        [MaxLength(100)]
        public string location { get; set; }

        [DisplayName("Parent Company")]
        [MaxLength(100)]
        public string company { get; set; }

        [DataType(DataType.Url)]
        [MaxLength(100)]
        public string website { get; set; }


        [DisplayName("Commission")]
        public Nullable<double> commission_rate { get; set; }

        [DisplayName("Street Address")]
        [MaxLength(100)]
        public string street1 { get; set; }

        [DisplayName("Address Line 2")]
        [MaxLength(100)]
        public string street2 { get; set; }

        [DisplayName("Town")]
        [MaxLength(100)]
        public string town { get; set; }

        [DisplayName("Country")]
        [MaxLength(100)]
        public string country { get; set; }

        [DisplayName("Phone 1")]
        [MaxLength(50)]
        [RegularExpression(@"\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})?", ErrorMessage = "Provided phone number not valid.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Provided phone number not valid")]
        //[RegularExpression("([0-9]+)", ErrorMessage = "Provided phone number not valid")]
        public string phone1 { get; set; }

        [DisplayName("Phone2")]
        [MaxLength(50)]

        [DataType(DataType.PhoneNumber, ErrorMessage = "Provided phone number not valid")]
        [RegularExpression(@"\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})?", ErrorMessage = "Provided phone number not valid.")]
        public string phone2 { get; set; }

        [DisplayName("Fax")]
        [MaxLength(50)]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Provided phone number not valid")]
        [RegularExpression(@"\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})?", ErrorMessage = "Provided phone number not valid.")]
        public string fax { get; set; }


        [DisplayName("Email")]
        [MaxLength(75)]
        [DataType(DataType.EmailAddress)]
        public string email { get; set; }

        [DisplayName("Google Coordinates")]
        [MaxLength(50)]
        public string google_location { get; set; }

        [Required]
        [DisplayName("Type")]
        [MaxLength(1)]
        public string provider_type { get; set; }

        [DisplayName("Post Code")]
        [MaxLength(20)]
        public string postcode { get; set; }

        [DisplayName("Enabled")]        
        public byte is_active { get; set; }


        [Range(1, 5, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [DisplayName("Rating:")]
        public string star_rating { get; set; }

        [DisplayName("Cancellation policy")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string cancellation_policy { get; set; }


    }





}