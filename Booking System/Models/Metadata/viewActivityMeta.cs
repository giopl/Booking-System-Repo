﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    public class viewActivityMeta
    {
        public int activity_id { get; set; }
        public string activity_name { get; set; }
        public Nullable<int> min_age { get; set; }
        public int currency_id { get; set; }
        public string currency_code { get; set; }
        public Nullable<double> price_adult { get; set; }
        public Nullable<double> price_child { get; set; }


    }


    [MetadataType(typeof(viewActivityMeta))]
    public partial class viewActivity
    {
        public int _adult { get; set; }
        public int _child { get; set; }
        public int _teen { get; set; }
        public double _total_price { get; set; }
        public Nullable<double> _after_sales_price_adult { get; set; }
        public Nullable<double> _after_sales_price_child { get; set; }
        public Nullable<double> _final_price_adult
        {
            get
            {
                double? result = null;
                if(_after_sales_price_adult.HasValue)
                {
                    return _after_sales_price_adult.Value;
                }

                if(price_adult.HasValue)
                {
                    return price_adult.Value;
                }

                return result;

            }
        }

        public bool has_price_child {
            get
            {
                return (price_child.HasValue ? price_child.Value : 0) +
                    (price_child_incl_sic_transfer.HasValue ? price_child_incl_sic_transfer.Value : 0) +
                    (price_child_incl_private_transfer.HasValue ? price_child_incl_private_transfer.Value : 0) > 0;
            }
        }

        public bool has_price_adult
        {
            get
            {
                return (price_adult.HasValue ? price_adult.Value : 0) +
                    (price_adult_incl_sic_transfer.HasValue ? price_adult_incl_sic_transfer.Value : 0) +
                    (price_adult_incl_private_transfer.HasValue ? price_adult_incl_private_transfer.Value : 0) > 0;
            }
        }

        public Nullable<double> _final_price_child
        {
            get
            {
                double? result = null;
                if (_after_sales_price_child.HasValue)
                {
                    return _after_sales_price_child.Value;
                }

                if (price_child.HasValue)
                {
                    return price_child.Value;
                }

                return result;


            }
        }

        public int transfer_type { get; set; }
        public bool _isSelected { get; set; }
        public bool _SoldInFrontEnd { get; set; }
    }

}