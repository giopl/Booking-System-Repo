﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{

    [MetadataType(typeof(featureMeta))]
    public partial class feature {


        //public int feature_id { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var t = obj as feature;
            if (t == null)
                return false;
            if (feature_id == t.feature_id)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash += (hash * 43) + feature_id.GetHashCode();

            return hash;

        }

    }

    public class featureMeta
    {
        [DisplayName("Id")]
        public int feature_id { get; set; }

        [DisplayName("Category")]
        [MaxLength(300)]
        public string category { get; set; }

        [DisplayName("Description")]
       
        [DataType(DataType.MultilineText)]
        [AllowHtml]
        public string description { get; set; }

        [DisplayName("Type")]
        [MaxLength(1)]
        [Required]
        public string feature_type { get; set; }

        [DisplayName("Name")]
        [Required]
        [MaxLength(100)]
        public string name { get; set; }

        [DisplayName("Icon")]
        [MaxLength(100)]
        public string icon { get; set; }

        [DisplayName("Is Active")]
        public bool is_active { get; set; }

        //public override bool Equals(object obj)
        //{
        //    if (obj == null)
        //        return false;
        //    var t = obj as featureMeta;
        //    if (t == null)
        //        return false;
        //    if (feature_id == t.feature_id)
        //        return true;
        //    return false;
        //}

        //public override int GetHashCode()
        //{
        //    int hash = 13;
        //    hash += (hash * 43) + feature_id.GetHashCode();

        //    return hash;

        //}

    }


}
 