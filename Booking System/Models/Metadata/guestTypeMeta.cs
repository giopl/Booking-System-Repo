﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{

    [MetadataType(typeof(guestTypeMeta))]
    public partial class guestType
    {

        public class guestTypeMeta
        {
            public int guest_type_id { get; set; }
            [Required]
            public int provider_id { get; set; }

            [DisplayName("Min Age Infant")]
            [Range(0, 150)]
            public Nullable<int> infant_min_age { get; set; }

            [DisplayName("Max Age Infant")]
            [Range(0, 150)]
            public Nullable<int> infant_max_age { get; set; }

            [DisplayName("Min Age Child")]
            [Range(0, 150)]
            public Nullable<int> child_min_age { get; set; }

            [DisplayName("Max Age Child")]
            [Range(0, 150)]
            public Nullable<int> child_max_age { get; set; }

            [DisplayName("Min Age Teen")]
            [Range(0, 150)]
            public Nullable<int> teen_min_age { get; set; }

            [DisplayName("Max Age Teen")]
            [Range(0, 150)]
            public Nullable<int> teen_max_age { get; set; }

            [DisplayName("Min Age Adult")]
            [Range(0, 150)]
            public Nullable<int> adult_min_age { get; set; }

            [DisplayName("Max Age Adult")]
            [Range(0, 150)]
            public Nullable<int> adult_max_age { get; set; }

            [DisplayName("Min Age Senior")]
            [Range(0, 150)]
            public Nullable<int> senior_min_age { get; set; }

            [DisplayName("Max Age Senior")]
            [Range(0, 150)]
            public Nullable<int> senior_max_age { get; set; }

            [DisplayName("Is Active")]
            public bool is_active { get; set; }

            public virtual provider provider{ get; set; }
        }





    }   
}