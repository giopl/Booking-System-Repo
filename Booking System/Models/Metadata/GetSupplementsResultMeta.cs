﻿using Booking_System.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Booking_System.Models
{

    public class GetSupplementsResultMeta
    {
        public int supplement_id { get; set; }
        public Nullable<int> provider_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool is_per_person { get; set; }
        public bool is_per_room { get; set; }
        public bool is_per_night { get; set; }
        public bool is_per_item { get; set; }
        public bool valid_all_rooms { get; set; }
        public Nullable<int> meal_plan_id { get; set; }
        public Nullable<int> room_id { get; set; }
        public int market_id { get; set; }
        public string symbol { get; set; }
        public string currency_code { get; set; }
        public Nullable<System.DateTime> valid_from { get; set; }
        public Nullable<System.DateTime> valid_to { get; set; }
        public Nullable<double> price_per_item { get; set; }
        public Nullable<double> price_adult { get; set; }
        public Nullable<double> price_infant { get; set; }
        public Nullable<double> price_child { get; set; }
        public Nullable<double> price_teen { get; set; }
        public Nullable<double> total_price_adult { get; set; }
        public Nullable<double> total_price_infant { get; set; }
        public Nullable<double> total_price_child { get; set; }
        public Nullable<double> total_price_teen { get; set; }
        public bool is_compulsory { get; set; }
    }

    [MetadataType(typeof(GetSupplementsResultMeta))]
    [Serializable]
    public partial class GetSupplementsResult
    {

        public bool IsSelected { get; set; }

        public int booking_room_id { get; set; }

        public string _room_name { get; set; }
        public int _room_number { get; set; }
        public int? _adults { get; set; }
        public int? _infants { get; set; }
        public int? _children { get; set; }
        public int? _teens { get; set; }

        public int _showMealFirst {
            get
            {
                return IsMealPlan ? 1 : 2;
            }
        }

        public double? supp_price { get; set; }

        [AllowHtml]
        public string _OccupancyIcons { get; set; }

        public bool IsMealPlan
        {
            get
            {
                return meal_plan_id > 100;
            }
        }

        public AppEnums.PricingType PricingType
        {

            get
            {

                var result = AppEnums.PricingType.PER_ITEM;
                if (is_per_person && !is_per_night)
                {
                    result = AppEnums.PricingType.PER_PERSON;
                }
                else if (is_per_person && is_per_night)
                {
                    result = AppEnums.PricingType.PER_PERSON_PER_NIGHT;

                }
                else if (is_per_room && !is_per_night)
                {
                    result = AppEnums.PricingType.PER_ROOM;

                }
                else if (is_per_room && is_per_night)
                {
                    result = AppEnums.PricingType.PER_ROOM_PER_NIGHT;

                }


                return result;
            }
        }


        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var t = obj as GetSupplementsResult;
            if (t == null)
                return false;
            if (supplement_id == t.supplement_id)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash += (hash * 43) + supplement_id.GetHashCode();

            return hash;

        }

    }


}