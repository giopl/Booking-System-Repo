﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{



	[MetadataType(typeof(promotionMeta))]
	public partial class promotion
	{

	}


	public class promotionMeta
	{

		[DisplayName("ID")]
		public int promotion_id { get; set; }
        //    [DisplayName("Market")]
        //[Required]
        //public int market_id { get; set; }

        [DisplayName("Hotel")]
        [Required]
        public int provider_id { get; set; }

        //[DisplayName("Room")]
        //    ///if room id is null, it means that all rooms are applicable... need to handle it in DB
        //public Nullable<int> room_id { get; set; }



        [DisplayName("Name")]
		[Required]
		[StringLength(100)]
		public string name { get; set; }

        [DisplayName("Promotion Code")]
        [Required]
        [StringLength(100)]
        public string code { get; set; }

		[DisplayName("Description")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
		[Required]
	
		public string description { get; set; }

		[DisplayName("Contracted Offer")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
		public string contracted_offer { get; set; }

		[DisplayName("Cancellation policy")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
		public string cancellation_policy { get; set; }

        [DisplayName("Booking Start date")]
 
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public Nullable<System.DateTime> book_from_date { get; set; }

        [DisplayName("Booking End date")]
 
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public Nullable<System.DateTime> book_until_date { get; set; }

        [DisplayName("Travel Start date")]
 
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public Nullable<System.DateTime> travel_from_date { get; set; }

        [DisplayName("Travel End date")]
 
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        public Nullable<System.DateTime> travel_until_date { get; set; }


		[DisplayName("Valid From")]
		[Required]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
	    ApplyFormatInEditMode = true)]
		public System.DateTime valid_from { get; set; }

		[DisplayName("Valid Until")]
		[Required]
		[DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
	    ApplyFormatInEditMode = true)]
		public System.DateTime valid_to { get; set; }

		[DisplayName("Min book before days")]
		[Range(0, int.MaxValue)]
		public Nullable<int> book_before_days_min { get; set; }
        [DisplayName("Max book before days")]
        [Range(0, int.MaxValue)]
        public Nullable<int> book_before_days_max { get; set; }

        [DisplayName("Free Nights")]
		[Range(0, int.MaxValue)]
        public Nullable<int> free_nights { get; set; }

        [DisplayName("Minimum Nights")]
        [Range(0, int.MaxValue)]
        public Nullable<int> min_nights { get; set; }
        [DisplayName("Active")]
		public bool is_active { get; set; }

        [DisplayName("Apply to all markets")]
        public bool apply_all_markets { get; set; }


        [DisplayName("Promotion Category")]
        public Nullable<int> promotion_group_id { get; set; }


        [DisplayName("Apply to all rooms")]
        public bool apply_all_rooms { get; set; }

        [DisplayName("For honeymoon")]
        public bool is_honeymoon
        {
            get; set;
        }

        [DisplayName("Discounted Rate %")]
        [Range(0, 100.0)]
        [Required]
        public double discount { get; set; }

        [DisplayName("FR Single")]
        public Nullable<double> flat_single { get; set; }

        [DisplayName("FR Twin")]
        public Nullable<double> flat_twin { get; set; }
        
        [DisplayName("FR Triple")]
        public Nullable<double> flat_triple { get; set; }
        [DisplayName("FR Quad")]
        public Nullable<double> flat_quadruple { get; set; }
        [DisplayName("FR Chld1")]
        public Nullable<double> flat_child1 { get; set; }
        [DisplayName("FR Chld2")]
        public Nullable<double> flat_child2 { get; set; }
        [DisplayName("FR Teen1")]
        public Nullable<double> flat_teen1 { get; set; }
        [DisplayName("FR Teen2")]
        public Nullable<double> flat_teen2 { get; set; }

        }


}