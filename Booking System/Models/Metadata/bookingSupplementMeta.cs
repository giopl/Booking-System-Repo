﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    public class bookingSupplementMeta
    {


    }


    [MetadataType(typeof(bookingSupplementMeta))]
    public partial class bookingSupplement
    {
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var t = obj as bookingSupplement;
            if (t == null)
                return false;
            if (supplement_id == t.supplement_id)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash += (hash * 43) + supplement_id.GetHashCode();

            return hash;

        }

    }
}