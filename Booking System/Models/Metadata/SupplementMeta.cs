﻿using Booking_System.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{
    [MetadataType(typeof(supplementMeta))]
    public partial class supplement {

        public AppEnums.HotelTab hoteltab
        {
            get
            {
                return is_compulsory ? AppEnums.HotelTab.CHARGES : AppEnums.HotelTab.SUPPLEMENTS;
            }
        }

        public AppEnums.PricingType PricingType
        {

            get
            {

                var result = AppEnums.PricingType.PER_ITEM;
                if (is_per_person && !is_per_night)
                {
                    result = AppEnums.PricingType.PER_PERSON;
                } else if (is_per_person && is_per_night)
                {
                    result = AppEnums.PricingType.PER_PERSON_PER_NIGHT;
                        
                }
                else if (is_per_room && !is_per_night)
                {
                    result = AppEnums.PricingType.PER_ROOM;

                }
                else if (is_per_room && is_per_night)
                {
                    result = AppEnums.PricingType.PER_ROOM_PER_NIGHT;

                }
                else if (!is_per_room && !is_per_person && is_per_night )
                {
                    result = AppEnums.PricingType.PER_NIGHT;

                }



                return result;
            }
        }

    }

    public class supplementMeta
    {
        [DisplayName("ID")]
        public int supplement_id { get; set; }

        [DisplayName("Hotel")]
        [Required]
        public Nullable<int> provider_id { get; set; }

        
        [DisplayName("Name")]
        [Required]
        [MaxLength(100)]
        public string name { get; set; }

        [DisplayName("Description")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
       
        public string description { get; set; }

        
        [DisplayName("Per person")]
        public bool is_per_person { get; set; }

        [DisplayName("Per room")]
        public bool is_per_room { get; set; }

        [DisplayName("Per night")]
        public bool is_per_night { get; set; }

        [DisplayName("Per Item")]
        public bool is_per_item { get; set; }

        [DisplayName("Compulsory")]
        public bool is_compulsory { get; set; }

        [DisplayName("Apply to Hotel")]
        public bool valid_all_rooms { get; set; }

        [DisplayName("Active")]
        public bool is_active { get; set; }

        [DisplayName("Meal Plan Upgrade")]
        public int meal_plan_id { get; set; }

    }
}