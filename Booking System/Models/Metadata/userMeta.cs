﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Booking_System.Models
{
    [MetadataType(typeof(userMeta))]
    public partial class user
    {
        public string FullName
        {
            get
            {
                string name = string.Empty;

                if (!string.IsNullOrWhiteSpace(this.firstname))
                {
                    name = this.firstname;
                }

                if (!string.IsNullOrWhiteSpace(this.lastname))
                {
                    name = string.Concat(name, " ", this.lastname);
                }

                return Helpers.Utils.ToTitleCase(name.Trim());
            }
        }
        [StringLength(200, ErrorMessage = "Text length is too long.")]
        [DisplayName("Old Password")]
        public string OldPassword { get; set; }

        [StringLength(200, ErrorMessage = "Text length is too long.")]
        [DisplayName("New Password")]
        public string NewPassword { get; set; }

        [StringLength(200, ErrorMessage = "Text length is too long.")]
        [DisplayName("Confirm Password")]
        public string ConfirmedPassword { get; set; }

        private string _salutation;

        [Required]
        [DisplayName("Title")]
        public string Salutation
        {
            get
            {
                _salutation = this.title;
                return _salutation;
            }
            set
            {
                _salutation = value;
                this.title = _salutation;
            }
        }


        public string RoleText
        {
            get
            {
                return Enum.GetName(typeof(UserRoles), this.role);
            }
        }

        [DisplayName("Credit Remaining")]
        public Nullable<double> credit_remaining
        {
            get
            {
                return (credit_limit.HasValue ? credit_limit.Value : 0) - (credit_used.HasValue ? credit_used.Value : 0);

            }
        }


        public string GenderText
        {
            get
            {
                string _gender = "";
                if(this.gender == 1)
                {
                    _gender = "Male";
                }
                else if(this.gender == 2)
                {
                    _gender = "Female";
                }

                return _gender;
            }
        }

    }

    public class userMeta
    {
        //[Required]
        //[DisplayName("Title")]

        [StringLength(20, ErrorMessage = "Text length is too long.")]
        [DisplayName("Title")]
        public string title { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Text length is too long.")]
        [DisplayName("First Name")]
        public string firstname { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Text length is too long.")]
        [DisplayName("Last Name")]
        public string lastname { get; set; }

        [DisplayName("Date Of Birth")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
               ApplyFormatInEditMode = true)]
        public Nullable<DateTime> date_of_birth { get; set; }

        [Required]
        [DisplayName("Gender")]
        public int gender { get; set; }

        [DisplayName("Nationality")]
        [StringLength(60, ErrorMessage = "Text length is too long.")]
        public string nationality { get; set; }

        [DisplayName("Role")]
        public int role { get; set; }

        [StringLength(20, ErrorMessage = "Text length is too long.")]
        [DisplayName("Office Phone")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Provided office number not valid")]
        //[RegularExpression(@"\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})?", ErrorMessage = "Provided phone number not valid.")]
        public string officephone { get; set; }

        [DisplayName("Mobile")]
        [StringLength(20, ErrorMessage = "Text length is too long.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Provided mobile number not valid")]
        //[RegularExpression(@"\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})?", ErrorMessage = "Provided phone number not valid.")]
        public string mobile { get; set; }

        [Required]
        [DisplayName("Email")]
        [StringLength(100, ErrorMessage = "Text length is too long.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string email { get; set; }

        [DisplayName("Company")]
        [StringLength(100, ErrorMessage = "Text length is too long.")]
        public string company { get; set; }

        [DisplayName("Job Title")]
        [StringLength(100, ErrorMessage = "Text length is too long.")]
        public string job_title { get; set; }


        [DisplayName("Is Active")]
        public bool is_active { get; set; }

        [DisplayName("Password")]
        [StringLength(200, ErrorMessage = "Text length is too long.")]
        public string password { get; set; }

        [Range(0, 100)]
        [DisplayName("Markup Percentage")]
        public Nullable<double> markup_percentage { get; set; }

        [DisplayName("Fixed Markup")]
        public Nullable<double> markup_fixed { get; set; }

        [DisplayName("UserName")]
        [StringLength(50, ErrorMessage = "Text length is too long.")]
        public string username { get; set; }

        [DisplayName("Credit Currency")]
        public int currency_id { get; set; }

        [DisplayName("Credit Limit")]
        public Nullable<double> credit_limit { get; set; }

        

        [DisplayName("Credit Used")]
        public Nullable<double> credit_used { get; set; }


    }


    public enum UserRoles
    {
        None = 0, //No roles Defined
        Agent = 1, // front end
        Sales,
        Accounts,
        Planning,
        Administrator,
        SuperAdministrator //all rights

    }
}