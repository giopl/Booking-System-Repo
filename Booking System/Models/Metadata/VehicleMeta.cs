﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{
    [MetadataType(typeof(vehicleMeta))]
    public partial class vehicle
    {
        public string fuel_type_desc
        {
            get
            {
                int fuel_int = (int)(fuel_type.HasValue?fuel_type.Value:0);
                Helpers.AppEnums.FuelType fuel_enum = (Helpers.AppEnums.FuelType)fuel_int;
                string fuel_string = Helpers.Utils.EnumToString(fuel_enum.ToString());
                return fuel_string;
            }

        }

        public bool _isSelected { get; set; }

        /// <summary>
        /// rental day could be half day as well, reason for using double
        /// </summary>
        public double? _rentalDays { get; set; }

    }

    public class vehicleMeta
    {
        [DisplayName("Vehicle ID")]
        public int vehicle_id { get; set; }

        [DisplayName("Provider")]
        [Required]
        public int provider_id { get; set; }

        [DisplayName("Service Name")]        
        [MaxLength(200)]
        public string service_name { get; set; }

        [DisplayName("Type")]
        [Required]
        [MaxLength(50)]
        public string vehicle_type { get; set; }

        [DisplayName("Model")]
        [Required]
        [MaxLength(100)]
        public string vehicle_model { get; set; }

        [DisplayName("Service Description")]
       
        [AllowHtml]
        [DataType(DataType.MultilineText)]
        public string service_description { get; set; }

        

        [DisplayName("Max. Seat")]
        [Range(1, 40)]
        public Nullable<int> max_seat { get; set; }

        [DisplayName("With driver")]
        public byte includes_driver { get; set; }

        [DisplayName("Is Active")]
        public byte is_active { get; set; }

        [DisplayName("Is Transfer")]
        public byte is_transfer { get; set; }

        [DisplayName("Cc")]
        [Range(50, 5000)]
        public Nullable<short> engine_size { get; set; }

        [DisplayName("Is Year")]
        [Range(1980, 2999)]
        public Nullable<short> year { get; set; }

        [DisplayName("Auto Transmission")]
        public bool is_automatic { get; set; }

        [DisplayName("Doors")]
        [Range(1, 10)]
        public Nullable<byte> doors { get; set; }

        [DisplayName("Fuel Type")]
        public Nullable<byte> fuel_type { get; set; }


    }
}