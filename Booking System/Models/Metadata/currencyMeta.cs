﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    [MetadataType(typeof(currencyMeta))]
    public partial class currency
    {
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var t = obj as currency;
            if (t == null)
                return false;
            if (currency_id == t.currency_id)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash += (hash * 43) + currency_id.GetHashCode();

            return hash;

        }
    }


    public class currencyMeta
    {
        [DisplayName("ID")]
        
        public int currency_id { get; set; }

        [DisplayName("Currency Code")]
        [StringLength(3)]
        [Required]
        public string currency_code { get; set; }


        [DisplayName("Exchange Rate")]
        public Nullable<double> exchange_rate { get; set; }


        [DisplayName("Currency Name")]
        [StringLength(50)]
        [Required]
        public string currency_desc { get; set; }

        [DisplayName("Symbol")]
        [StringLength(10)]
        public string symbol { get; set; }

        [DisplayName("Is Active")]
        public bool is_active { get; set; }

    }
}