﻿using Booking_System.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    [MetadataType(typeof(packageAllotmentMeta))]
    
    public partial class packageAllotment
    {
        //[DisplayName("Selected Dates")]
        //[NotMapped]
        //public string MultipleDates { get; set; }

        
        //[NotMapped]
        // public int[] rooms { get; set; }

    }


    public class packageAllotmentMeta
    {
        public int package_allotment_id { get; set; }


        public int package_id { get; set; }
        public int room_id { get; set; }


        [DisplayName("Allotment Date")]        
        public System.DateTime allotment_date { get; set; }

        [Required]
        [DisplayName("Number of Allotments")]        
        public int allotment { get; set; }

        public virtual package package { get; set; }
        public virtual room room { get; set; }


    }
}