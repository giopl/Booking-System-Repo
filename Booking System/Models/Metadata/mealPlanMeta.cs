﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{
    [MetadataType(typeof(mealPlanMeta))]
    public partial class mealPlan
    {
        
 
    }



    public class mealPlanMeta
    {
        [DisplayName("ID")]
        public int meal_plan_id { get; set; }

        [Required]
        [DisplayName("Name")]
        [StringLength(50)]
        public string name { get; set; }

        [DisplayName("Description")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
       
        public string description { get; set; }

        [Required]
        [DisplayName("Meal Display Order")]
        public Nullable<short> meal_order { get; set; }

        [Required]
        [DisplayName("Code")]
        [StringLength(2)]
        public string code { get; set; }

        [DisplayName("Is Active")]
        public bool is_active { get; set; }
    }
}