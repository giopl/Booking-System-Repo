﻿using Booking_System.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{



    [MetadataType(typeof(roomMeta))]
    public partial class room
    {



    }


    public class roomMeta
    {

        [DisplayName("Room")]
        public int room_id { get; set; }

        [DisplayName("Hotel")]
        public int provider_id { get; set; }

        [DisplayName("Name")]
        [Required]
        [MaxLength(50)]
        public string name { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Description")]
        [AllowHtml]
      
        public string description { get; set; }

        [DisplayName("Min Adult Occupancy")]
        [Range(1, int.MaxValue)]
        public Nullable<int> min_adult { get; set; }

        [DisplayName("Max Adult Occupancy")]
        [Range(1, int.MaxValue)]
        public Nullable<int> max_adult { get; set; }

        [DisplayName("Room Area")]
        [Range(0, double.MaxValue)]
        public string room_size { get; set; }

    }
}