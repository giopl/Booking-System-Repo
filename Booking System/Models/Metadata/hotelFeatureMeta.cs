﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{

    [MetadataType(typeof(hotelFeatureMeta))]
    public partial class hotelFeature
    {

    }

    public class hotelFeatureMeta
    {

        [Required]
        public int feature_id { get; set; }

        [Required]
        [DisplayName("Hotel")]
        public int provider_id { get; set; }

        [RegularExpression(@"^\d$", ErrorMessage = "Count must be a natural number")]
        [Range(0, int.MaxValue)]
        public Nullable<int> quantity { get; set; }
        

    }
}