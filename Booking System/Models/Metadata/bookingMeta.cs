﻿using Booking_System.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    public class bookingMeta
    {

        [DisplayName("Booking ID")]
        public int booking_id { get; set; }

        [DisplayName("Market")]
        public int market_id { get; set; }

        [DisplayName("Agent")]
        public int agent_user_id { get; set; }

        [DisplayName("Booking Name")]
        public string booking_name { get; set; }

        [DisplayName("Checkin Date")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
               ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> checkin_date { get; set; }

        [DisplayName("Checkout Date")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
               ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> checkout_date { get; set; }

        [DisplayName("Nights")]
        public Nullable<int> num_nights { get; set; }

        [DisplayName("Created Date")]
        public Nullable<System.DateTime> create_date { get; set; }

        [DisplayName("Booking Date")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
               ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> booking_date { get; set; }

        [DisplayName("Status")]
        public Nullable<int> status { get; set; }

        [DisplayName("BFN")]
        public string booking_file_number { get; set; }

        [DisplayName("Booking Expiry Date")]
        public Nullable<System.DateTime> booking_expiry_date { get; set; }

        [DisplayName("Back Office User")]
        public Nullable<int> backoffice_user_id { get; set; }

        [DisplayName("Booking Reference")]
        public string booking_ref { get; set; }

        [DisplayName("Number Of Adults")]
        public Nullable<int> adults { get; set; }

        [DisplayName("Number Of Infants")]
        public Nullable<int> infants { get; set; }

        [DisplayName("Number Of Children")]
        public Nullable<int> children { get; set; }

        [DisplayName("Number Of Teens")]
        public Nullable<int> teens { get; set; }

        [DisplayName("Number Of Rooms")]
        public Nullable<int> rooms { get; set; }

        [DisplayName("Phone Number")]
        public string phone_number { get; set; }

        [DisplayName("Email")]
        public string email { get; set; }

        [DisplayName("Provider")]
        public Nullable<int> provider_id { get; set; }

        [DisplayName("Discount Amount")]
        public Nullable<double> discount_amt { get; set; }

        [DisplayName("Discount Reason")]

        public string discount_reason { get; set; }

        [DisplayName("Discounted By")]
        public Nullable<int> discounted_by_user_id { get; set; }
    }

    [MetadataType(typeof(bookingMeta))]
    public partial class booking
    {

        public AppEnums.BookingStatus StatusEnum
        {
            get
            {
                return (AppEnums.BookingStatus)status;
            }
        }

        public user agent { get; set; }

        public market market { get; set; }

        public provider provider { get; set; }

        public package package { get; set; }

        public currency currency { get; set; }

        public string HotelName { get; set; }

        public bool isPackage
        {
            get
            {
                return package_id > 0;
            }
        }

        [DisplayName("No. of Guests")]
        public int _totalGuests
        {
            get
            {
                return (adults.HasValue ? adults.Value : 0)
                    + (infants.HasValue ? infants.Value : 0)
                    + (children.HasValue ? children.Value : 0)
                    + (teens.HasValue ? teens.Value : 0);
            }
        }

        public int _totalChildren
        {
            get
            {
                return
                     (infants.HasValue ? infants.Value : 0)
                    + (children.HasValue ? children.Value : 0)
                    + (teens.HasValue ? teens.Value : 0);
            }
        }

        public int _totalChildrenAndTeens
        {
            get
            {
                return

                    (children.HasValue ? children.Value : 0)
                    + (teens.HasValue ? teens.Value : 0);
            }
        }



        [DisplayName("Total Cost Rooms")]
        public double TotalCostRooms
        {
            get
            {

                double? cost = null;
                if (this.booking_room != null && this.num_nights.HasValue)
                {
                    cost = (this.booking_room.Where(e => e.booking_guest != null).Sum(e => e.booking_guest.Sum(p => ((p.price_per_night.HasValue ? p.price_per_night : 0)
                        + (p.markup_amt.HasValue ? p.markup_amt : 0)) - (p.promotional_discount_amt.HasValue ? p.promotional_discount_amt : 0)))) * ((this.num_nights.HasValue ? num_nights.Value : 0) - (this.free_nights.HasValue ? free_nights.Value : 0))
                        + (this.booking_room.Where(e => e.booking_guest != null).Sum(e => e.booking_guest.Sum(p => (p.ground_handling_amt.HasValue ? p.ground_handling_amt : 0))));
                }


                if (!cost.HasValue)
                {
                    cost = 0;
                }



                return Math.Round(cost.Value,2);
            }
        }

        [DisplayName("Total Markup")]
        public double TotalMarkup
        {
            get
            {
                double? cost = null;
                if (this.booking_room != null && this.num_nights.HasValue)
                {
                    cost = (this.booking_room.Where(e => e.booking_guest != null).
                        Sum(e => e.booking_guest.Sum(p => p.markup_amt))) * this.num_nights;

                }

                if (!cost.HasValue)
                {
                    cost = 0;
                }

                return cost.Value;
            }
        }


        [DisplayName("Total Markup")]
        public double TotalGroundHandlingCost
        {
            get
            {

                //(Model.booking_room.Where(e => e.booking_guest != null).Sum(e => e.booking_guest.Sum(p => p.ground_handling_amt)));

                double? cost = null;
                if (this.booking_room != null && this.num_nights.HasValue)
                {
                    cost = (this.booking_room.Where(e => e.booking_guest != null).
                        Sum(e => e.booking_guest.Sum(p => p.ground_handling_amt)));

                }

                if (!cost.HasValue)
                {
                    cost = 0;
                }

                return cost.Value;
            }
        }





        [DisplayName("Total Cost Activity")]
        public double TotalCostActivity
        {
            get
            {
                double? totCostActivity = null;
                if (this.booking_activity != null)
                {
                    totCostActivity = (this.booking_activity.Where(e => !e.is_aftersales).Sum(p => p.total_price));
                }

                if (!totCostActivity.HasValue)
                {
                    totCostActivity = 0;
                }

                return totCostActivity.Value;
            }
        }

        [DisplayName("Total Cost Activity")]
        public double TotalCostActivityWithAfterSales
        {
            get
            {
                double? totCostActivity = null;
                if (this.booking_activity != null)
                {
                    totCostActivity = (this.booking_activity.Sum(p => p.total_price));
                }

                if (!totCostActivity.HasValue)
                {
                    totCostActivity = 0;
                }

                return totCostActivity.Value;
            }
        }
        /*
         * /
         * 
         */





        /**/


        [DisplayName("Total Cost Transfer")]
        public double TotalCostTransfer
        {
            get
            {
                double? totCostTransfer = null;
                if (this.booking_transfer != null)
                {
                    totCostTransfer = (this.booking_transfer.Where(e => !e.is_aftersales).Sum(p => p.total_price));
                }

                if (!totCostTransfer.HasValue)
                {
                    totCostTransfer = 0;
                }

                return totCostTransfer.Value;
            }
        }

        [DisplayName("Total Cost Transfer")]
        public double TotalCostTransferWithAfterSales
        {
            get
            {
                double? totCostTransfer = null;
                if (this.booking_transfer != null)
                {
                    totCostTransfer = (this.booking_transfer.Sum(p => p.total_price));
                }

                if (!totCostTransfer.HasValue)
                {
                    totCostTransfer = 0;
                }

                return totCostTransfer.Value;
            }
        }

        [DisplayName("Total Cost Rental")]
        public double TotalCostRental
        {
            get
            {
                double? totCostRental = null;
                if (this.booking_rental != null)
                {
                    totCostRental = (this.booking_rental.Where(e => !e.is_aftersales).Sum(p => p.total_price * p.num_days));
                }

                if (!totCostRental.HasValue)
                {
                    totCostRental = 0;
                }

                return totCostRental.Value;
            }
        }

        [DisplayName("Total Cost Rental")]
        public double TotalCostRentalWithAfterSales
        {
            get
            {
                double? totCostRental = null;
                if (this.booking_rental != null)
                {
                    totCostRental = (this.booking_rental.Sum(p => p.total_price * p.num_days));
                }

                if (!totCostRental.HasValue)
                {
                    totCostRental = 0;
                }

                return totCostRental.Value;
            }
        }

        [DisplayName("Total Cost Supplements")]
        public double TotalCostSupps
        {
            get
            {
                double? totCostSupps = null;
                if (this.booking_room != null)
                {

                    totCostSupps = this.booking_room.Where(e => e.booking_supplement != null).Sum(e => e.booking_supplement.Sum(f => f.price));
                }

                if (!totCostSupps.HasValue)
                {
                    totCostSupps = 0;
                }

                return totCostSupps.Value;
            }
        }

        public double TotalCompulsoryAmount
        {
            get
            {
                return compulsory_charges_amt.HasValue ? compulsory_charges_amt.Value : 0;
        }
        }

        public double Cancellation_fee
        {
            get
            {
                return cancellation_fee.HasValue ? cancellation_fee.Value : 0;
            }
        }


        [DisplayName("Total Cost")]
        public double TotalCost
        {
            get
            {
                double cost = 0;


                cost = (TotalCostSupps + TotalCostRental + TotalCostTransfer + TotalCostActivity + TotalCostRooms + TotalCompulsoryAmount) -
                    (this.discount_amt.HasValue ? this.discount_amt.Value : 0);

                return cost;
            }
        }

        [DisplayName("Total Cost")]
        public double TotalCostWithAfterSales
        {
            get
            {
                double cost = 0;


                cost = (TotalCostSupps + TotalCostRentalWithAfterSales + TotalCostTransferWithAfterSales + TotalCostActivityWithAfterSales + TotalCompulsoryAmount + TotalCostRooms) -
                    (this.discount_amt.HasValue ? this.discount_amt.Value : 0);

                return cost;
            }
        }


        public double TotalDueAmount
        {
            get
            {
                double _dueAmount = 0;

                _dueAmount = this.TotalCost - (this.TotalPaidAmount + (this.credit_amt.HasValue ? this.credit_amt.Value : 0));

                return _dueAmount;
            }
        }

        [DisplayName("Total Cost Without Discount")]
        public double TotalCostWithoutDiscount
        {
            get
            {
                double cost = 0;

                cost = (TotalCostSupps + TotalCostRental + TotalCostTransfer + TotalCostActivity + TotalCostRooms);

                return cost;
            }
        }

        [DisplayName("Total Cost Without Discount")]
        public double TotalCostWithAfterSalesWithoutDiscount
        {
            get
            {
                double cost = 0;
                cost = (TotalCostSupps + TotalCostRentalWithAfterSales + TotalCostTransferWithAfterSales + TotalCostActivityWithAfterSales + TotalCompulsoryAmount + TotalCostRooms);
                return cost;
            }
        }


        [DisplayName("Total Cost")]
        public String TotalCostWithCurrency
        {
            get
            {
                string currency = string.Empty;




                if (this.market != null && this.market.currency != null)
                {
                    currency = this.market.currency.symbol;
                }

                return string.Concat(currency, String.Format("{0:n}", TotalCost));
            }
        }


        public double TotalPaidAmount
        {
            get
            {
                double? _totalPaid = null;
                if (this != null)
                {
                    if (this.booking_payment != null && this.booking_payment.Count > 0)
                    {
                        _totalPaid = this.booking_payment.Sum(e => e.payment_amt);
                    }

                }

                if (!_totalPaid.HasValue)
                {
                    _totalPaid = 0;
                }

                return _totalPaid.Value;
            }
        }

        public double RemainingAmount
        {
            get
            {
                return this.TotalCostWithAfterSales - this.TotalPaidAmount;
            }

        }

        public IList<bookingTransfer> booking_transfer_no_aftersales
        {
            get
            {
                IList<bookingTransfer> result = new List<bookingTransfer>();

                if (this.booking_transfer != null)
                {
                    result = this.booking_transfer.Where(e => !e.is_aftersales).ToList();
                }

                return result;
            }
        }

        public IList<bookingActivity> booking_activity_no_aftersales
        {
            get
            {
                IList<bookingActivity> result = new List<bookingActivity>();

                if (this.booking_activity != null)
                {
                    result = this.booking_activity.Where(e => !e.is_aftersales).ToList();
                }

                return result;
            }
        }

        public IList<bookingRental> booking_rental_no_aftersales
        {
            get
            {
                IList<bookingRental> result = new List<bookingRental>();

                if (this.booking_rental != null)
                {
                    result = this.booking_rental.Where(e => !e.is_aftersales).ToList();
                }

                return result;
            }
        }

        public DateTime? ProformaExpiryDate
        {
            get
            {
                DateTime? result = null;


                if(this.booking_proforma != null && this.booking_proforma.Count > 0)
                {
                    result = this.booking_proforma.First().due_date;
                }

                return result;
            }
        }


        [DisplayName("Status")]
        public string statusDesc
        {
            get
            {
                if (!status.HasValue)
                {
                    return "None";
                }

                var dsc = (AppEnums.BookingStatus)status ;
                return dsc.ToString().Replace("_"," ");

            }

        }


    }


}