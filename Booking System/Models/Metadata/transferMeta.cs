﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{
    public class transferMeta
    {

        public int transfer_id { get; set; }
        [Required]
        [MaxLength(500)]
        [DisplayName("Name")]
        public string name { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Description")]
        [AllowHtml]       
        public string description { get; set; }

        [DisplayName("Is Active")]
        public bool is_active { get; set; }
    }

    [MetadataType(typeof(transferMeta))]
    public partial class transfer {
        public bool _isSelected { get; set; }
        public bool _isAfterSales { get; set; }
    }
}