﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    public class transferPricingMeta
    {

        [DisplayName("ID")]
        public int transfer_pricing_id { get; set; }

        [DisplayName("Transfer")]
        [Required]
        public int transfer_id { get; set; }

        [DisplayName("Currency")]
        [Required]
        public int currency_id { get; set; }

        [DisplayName("Price Child")]
        [Range(0, double.MaxValue)]
        public Nullable<double> child_price { get; set; }

        [DisplayName("Price Teen")]
        [Range(0, double.MaxValue)]
        public Nullable<double> teen_price { get; set; }

        [DisplayName("Price Adult")]
        [Range(0, double.MaxValue)]
        public Nullable<double> adult_price { get; set; }

        [DisplayName("Active")]
        public bool is_active { get; set; }

    }


    [MetadataType(typeof(transferPricingMeta))]
    public partial class transferPricing
    {
        public bool _isSelected { get; set; }
        public bool _isAfterSales{ get; set; }
        public double? _totalPrice { get; set; }
    }


}