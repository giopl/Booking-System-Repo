﻿using Booking_System.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{

    [MetadataType(typeof(viewBookingMeta))]
    public partial class viewBooking
    {

        public AppEnums.BookingStatus StatusEnum
        {
            get
            {
                return (AppEnums.BookingStatus)status;
            }
        }

        [DisplayName("Status")]
        public string statusDesc
        {
            get
            {
                if (!status.HasValue)
                {
                    return "None";
                }

                var dsc = StatusEnum.ToString();

                return Helpers.Utils.ToTitleCase(dsc.ToString().Replace("_", " "));

            }

        }




        //public string statusDesc
        //{
        //    get
        //    {
        //        if (!status.HasValue)
        //        {
        //            return "None";
        //        }
        //        else
        //        {
        //            switch (status)
        //            {
        //                //case 0: return "New";
        //                //case 1: return "Processing";
        //                //case 2: return "Checked";
        //                //case 3: return "Canceled";
        //                //case 4: return "Awaiting Payment / Confirmation";
        //                //case 5: return "Confirmed";
        //                //case 6: return "Completed";
        //                //case 7: return "Expired";

        //                case 0: return "Draft";
        //                case 1: return "New";
        //                case 2: return "Processing";
        //                case 3: return "Awaiting Confirmation";
        //                case 4: return "Confirmed";
        //                case 5: return "Awaiting Payment";
        //                case 6: return "Completed";
        //                case 7: return "Unvailability";
        //                case 8: return "Cancelled";



        //                default:
        //                    return "None";
        //            }
        //        }

        //    }

        //}

        public double TotalPrice
        {

            get
            {
                return (price.HasValue ? price.Value : 0D) * (num_nights.HasValue ? num_nights.Value : 0);

            }
        }

        public double? TotalBookingPriceWithAfterSales
        {
            get
            {
                return this.price_supplement + this.price_rental + this.price_transfer + this.price_activity + ((this.price * (this.num_nights - this.free_nights)) + this.ground_handling_amt) + this.markup_amt_room;

            }
        }

        public double? TotalBookingPriceWithoutAfterSales
        {
            get
            {
                return this.price_supplement + this.price_rental_no_after_sales + this.price_transfer_no_after_sales + this.price_activity_no_after_sales + ((this.price * (this.num_nights - this.free_nights)) + this.ground_handling_amt) + this.markup_amt_room;
            }
        }

        public bool hasProformaFinalized
        {
            get
            {
                bool result = false;
                if(this.has_proforma_finalized.HasValue)
                {
                    result = this.has_proforma_finalized.Value == 1;
                }

                return result;
            }
        }


        public bool hasVoucherGenerated
        {
            get
            {
                bool result = false;
                if (this.has_voucher_generated.HasValue)
                {
                    result = this.has_voucher_generated.Value == 1;
                }

                return result;
            }
        }



    }

    public class viewBookingMeta
    {
        public Nullable<int> num_rooms { get; set; }
        public string booking_ref { get; set; }
        public string booking_name { get; set; }
        public int provider_id { get; set; }
        public string name { get; set; }
        public Nullable<System.DateTime> checkin_date { get; set; }
        public Nullable<int> num_nights { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public Nullable<int> adults { get; set; }
        public Nullable<int> infants { get; set; }
        public Nullable<int> children { get; set; }
        public Nullable<int> teens { get; set; }
        public Nullable<double> price { get; set; }
        public string symbol { get; set; }
        public int agent_user_id { get; set; }
        public Nullable<int> status { get; set; }
        public int booking_id { get; set; }
        public Nullable<int> num_rental { get; set; }
        public Nullable<double> price_rental { get; set; }
        public Nullable<int> num_activity { get; set; }
        public Nullable<double> price_activity { get; set; }
        public Nullable<int> num_supplement { get; set; }
        public Nullable<double> price_supplement { get; set; }
        public Nullable<int> num_transfer { get; set; }
        public Nullable<double> price_transfer { get; set; }
        public Nullable<System.DateTime> checkout_date { get; set; }
        public Nullable<int> package_id { get; set; }
        public Nullable<double> price_rental_no_after_sales { get; set; }
        public Nullable<double> price_activity_no_after_sales { get; set; }
        public Nullable<double> price_transfer_no_after_sales { get; set; }
        public Nullable<double> discount_amt { get; set; }
        public Nullable<double> ground_handling_amt { get; set; }
        public Nullable<double> compulsory_charge_amt { get; set; }
        public Nullable<double> promotional_discount_amt { get; set; }
        public Nullable<double> price_per_night { get; set; }
        public Nullable<double> markup_amt_room { get; set; }
        public Nullable<int> free_nights { get; set; }


    }
}