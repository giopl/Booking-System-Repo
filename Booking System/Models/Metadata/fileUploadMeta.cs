﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    [MetadataType(typeof(fileUploadMeta))]
    public partial class fileUpload
    {
        public string FullFileName
        {
            get
            {
                string result = string.Empty;
                if (file_guid != null && file_guid.HasValue && !String.IsNullOrWhiteSpace(filetype))
                {
                    result = string.Concat(file_guid.Value, filetype);
                }
                return result;


            }
        }

        public FileSource Source { get; set; }

        public string FileVirtualPath
        {
            get
            {
                string result = string.Empty;
                if(filetype != null)
                {
                    switch (filetype)
                    {
                        case ".pdf":
                            result = Booking_System.Helpers.ConfigurationHelper.GetPdfPath();
                            break;
                        default:
                            return string.Empty;
                    }
                }

                return result;

            }
        }
    }

    public class fileUploadMeta
    {

        [DisplayName("ID")]
        public int file_id { get; set; }

        
        [DisplayName("Provider Id")]
        public int provider_id { get; set; }

        [Required]
        [StringLength(200)]
        [DisplayName("File Name")]
        public int filename { get; set; }

        [DisplayName("Title")]
        [StringLength(100)]
        public int title { get; set; }

        [DisplayName("Description")]
        public int description { get; set; }

        [DisplayName("File Type")]
        [StringLength(50)]
        public int filetype { get; set; }

    }

    public enum FileSource
    {
        Hotel = 0,
        Provider
    }
}