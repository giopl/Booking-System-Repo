﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{


    [MetadataType(typeof(contactMeta))]
    public partial class contact
    {
        [DisplayName("Contact Title")]
        public string TitleFullName
        {
            get
            {
                return string.Concat(salutation, " ", firstname, " ", lastname).TrimStart().TrimEnd();
            }
        }

        [DisplayName("FullName")]
        public string FullName
        {
            get
            {
                return string.Concat( firstname, " ", lastname).TrimStart().TrimEnd();
            }
        }
    }



    


    public class contactMeta
    {
       

        [DisplayName("ID")]
        public int contact_id { get; set; }

        [DisplayName("Salutation")]
        [StringLength(10)]
        public string salutation { get; set; }

        [DisplayName("Firstname")]
        [StringLength(100)]
        [Required]
        public string firstname { get; set; }

        [DisplayName("Surname")]
        [StringLength(300)]
        [Required]
        public string lastname { get; set; }

        [DisplayName("Job Position")]
        [StringLength(100)]
        public string job_title { get; set; }

        [DisplayName("Office num")]
        [RegularExpression(@"\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})?",
         ErrorMessage = "Characters are not allowed.")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Provided office number not valid")]
       
        public string office_phone { get; set; }

        [DisplayName("Mobile")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Provided mobile number not valid")]
        [RegularExpression(@"\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})?", ErrorMessage = "Provided phone number not valid.")]
        public string mobile { get; set; }

        

        [DataType(DataType.EmailAddress)]
        [StringLength(100)]
        [DisplayName("Email Address")]
        public string email { get; set; }

        [DataType(DataType.MultilineText)]
  
        [AllowHtml]
        [DisplayName("Notes")]
        public string notes { get; set; }

        [DisplayName("Is Active")]
        public bool is_active { get; set; }

        public virtual provider provider { get; set; }

        [Required]
        public Nullable<int> provider_id { get; set; }
    }
}