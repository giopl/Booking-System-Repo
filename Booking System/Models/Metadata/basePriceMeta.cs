﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    [MetadataType(typeof(basePriceMeta))]
    public partial class basePrice
    {

    }


    public class basePriceMeta
    {
        [DisplayName("Room Type")]
        [Required]
        public int room_id { get; set; }
        [DisplayName("Market")]
        [Required]
        public int market_id { get; set; }

        [DisplayName("Default Meal Plan")]
        [Required]
        public int meal_plan_id { get; set; }

        [DisplayName("Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Required]
        public System.DateTime start_date { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]
        [Required]
        public System.DateTime end_date { get; set; }

        [DisplayName("Single")]
        public Nullable<double> price_single { get; set; }

        [DisplayName("Twin")]
        public Nullable<double> price_twin { get; set; }

        [DisplayName("Triple")]
        public Nullable<double> price_triple { get; set; }

        [DisplayName("Infant")]
        public Nullable<double> price_infant { get; set; }

        [DisplayName("Child")]
        public Nullable<double> price_child { get; set; }

        [DisplayName("Teen")]
        public Nullable<double> price_teen { get; set; }

        [DisplayName("Quadruple")]
        public Nullable<double> price_quadruple{ get; set; }

        [DisplayName("Honeymoon")]
        public Nullable<double> price_honeymoon { get; set; }

        
        [DisplayName("Child without bed")]
        public Nullable<double> price_without_bed { get; set; }

        [DisplayName("Is Active")]
        public bool is_active { get; set; }

        public virtual market market { get; set; }
        public virtual room room { get; set; }
    }
}