﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{

    [MetadataType(typeof(countryMeta))]
    public partial class country
    {

    }


    public class countryMeta
    {
        [DisplayName("ID")]
        public string country_id { get; set; }

        [DisplayName("Country Code")]
        [StringLength(2)]
        [Required]
        public string country_code { get; set; }

        [DisplayName("Country Name")]
        [StringLength(50)]
        [Required]
        public string name { get; set; }

        [DisplayName("Currency Code")]
        [StringLength(3)]
        [Required]
        public string currency { get; set; }

        [DisplayName("Currency Name")]
        [Required]
        [StringLength(30)]
        public string currency_name { get; set; }

        [DisplayName("Is Independent")]
        [StringLength(30)]
        public string is_independent { get; set; }

        [DisplayName("Language")]
        [StringLength(2)]
        public string language { get; set; }

        [DisplayName("Nationality")]
        [StringLength(100)]
        public string nationality { get; set; }

    }
}