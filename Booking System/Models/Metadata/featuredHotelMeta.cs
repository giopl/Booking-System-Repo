﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{


    [MetadataType(typeof(featuredHotelMeta))]
    public partial class featuredHotel
    {

    }
    public class featuredHotelMeta
    {

        [DisplayName("ID")]      
        public int featured_hotel_id { get; set; }

        [DisplayName("Hotel")]
        [Required]
        public int provider_id { get; set; }


        [DisplayName("Title")]
        [Required]
        public string title { get; set; }

        [DisplayName("Description")]
        [AllowHtml]      
        public string description { get; set; }

        [DisplayName("Section")]
        public string section { get; set; }


        [DisplayName("Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]      
        public Nullable<System.DateTime> from_date { get; set; }


        [DisplayName("End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd-MM-yyyy}")]      
        public Nullable<System.DateTime> to_date { get; set; }

        [DisplayName("Picture")]
        public Nullable<int> picture_id { get; set; }
    }
}