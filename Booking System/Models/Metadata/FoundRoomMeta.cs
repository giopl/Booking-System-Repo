﻿using Booking_System.Helpers;
using Booking_System.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    public class FoundRoomMeta
    {
        public Nullable<long> rn { get; set; }
        public Nullable<int> room_sequence { get; set; }
        public int provider_id { get; set; }
        public string provider_name { get; set; }
        public Nullable<int> base_price_id { get; set; }
        public int market_id { get; set; }
        public int currency_id { get; set; }
        public string currency_code { get; set; }
        public string symbol { get; set; }
        public int room_id { get; set; }
        public string room_name { get; set; }
        public Nullable<int> nights { get; set; }
        public string description { get; set; }
        public Nullable<System.DateTime> from_date { get; set; }
        public Nullable<double> ground_handling_adult { get; set; }
        public Nullable<double> ground_handling_child { get; set; }
        public Nullable<double> ground_handling_teen { get; set; }
        public int includes_ground_handling { get; set; }
        public int package_id { get; set; }
        public string package_name { get; set; }
        public Nullable<System.DateTime> to_date { get; set; }
        public Nullable<double> total_cost_ground_handling { get; set; }
        public Nullable<double> total_price { get; set; }
        public Nullable<double> total_per_adult { get; set; }
        public Nullable<double> total_per_honeymooner { get; set; }
        public Nullable<double> total_per_infant { get; set; }
        public Nullable<double> total_per_child { get; set; }
        public Nullable<double> total_per_teen { get; set; }
        public Nullable<double> charges_infant { get; set; }
        public Nullable<double> charges_child { get; set; }
        public Nullable<double> charges_teen { get; set; }
        public Nullable<double> charges_adult { get; set; }

        
        public Nullable<int> num_adults { get; set; }
        public Nullable<int> num_infants { get; set; }
        public Nullable<int> num_children { get; set; }
        public Nullable<int> num_teens { get; set; }
        public string unavailable_flag { get; set; }
        
        public string unavailable_dates { get; set; }

        public Nullable<int> package_allotment_id { get; set; }
        public Nullable<int> allotments { get; set; }

    }

    [MetadataType(typeof(FoundRoomMeta))]
    public partial class FoundRoom
    {

        public FoundRoom()
        {
            RoomOccupancy = new RoomOccupancy();
            //Promotions = new List<PromotionSetResult>();
            PromotionSets = new List<PromotionSetResult>();

            Promosets = new List<PromosetViewModel>();

            Room = new room();
            Baseprice = new basePrice();
        

            
        }
    
       public string charge_name { get; set; }

        public List<PromosetViewModel> Promosets { get; set; }

      //  public string _UnavailableFlag { get; set; }
        //public string _UnavailableDates { get; set; }

        public bool IsUnavailable
        {
            get
            {
                return (unavailable_flag ?? "N") == "Y";
            }
        }

        public List<DateTime> UnavailableDates
        {
            get
            {
                List<DateTime> dates = new List<DateTime>();
                if (!String.IsNullOrWhiteSpace(unavailable_dates))
                {
                    var dts = unavailable_dates.Split('|');
                    foreach (var dt in dts)
                    {
                        //10/11/2017
                        DateTime d = new DateTime(Convert.ToInt32(dt.Substring(6, 4)),
                            Convert.ToInt32(dt.Substring(3, 2)),
                            Convert.ToInt32(dt.Substring(0, 2)));
                        dates.Add(d);
                    }

                }
                return dates;
            }
        }

        public double LowestPromosetAmount {

            get {
                if (Promosets.Count() > 0)
                    return Promosets.OrderBy(x => x.TotalCostIncludingChargesAndPromotions).First().TotalCostIncludingChargesAndPromotions;
                else
                    return TotalCostIncludingChargesAndPromotions;
            }

        }
            

        public int OccupancyCode
        {

            get
            {
                return (Room.room_id * 10) + Utils.CoalesceInt(room_sequence);

            }
        }


        public double TotalPriceAdult
        {

            get
            {
                if (IsHoneyMoon)
                    return Utils.CoalesceDouble(total_per_honeymooner);

                else
                    return Utils.CoalesceDouble(total_per_adult);

            }
        }

        public bool isSelected { get; set; }

        public room Room { get; set; }
        public basePrice Baseprice { get; set; }

        public int Id { 
            get
            {
                return provider_id * 10 + Utils.CoalesceInt(nights);
            }
        
        }

        public bool IsHoneyMoon { get; set; }

        
        public RoomOccupancy RoomOccupancy { get; set; }


        public IList<PromotionSetResult> Promotions
        {
            get
            {
                HashSet<PromotionSetResult> Promotions = new HashSet<PromotionSetResult>();
                if (Promosets.Count() > 0)
                {
                    var Y = Promosets.OrderBy(x => x.TotalCostIncludingChargesAndPromotions).First().Promotions;
                    foreach (var p in Y)
                    {
                        Promotions.Add(p);
                    }
                }
                return Promotions.ToList();
            }
        }

        public IList<PromotionSetResult> PromotionSets { get; set; }

        public double TotalRoomCost
        {
            get
            {
                if(IsHoneyMoon)
                {
                    return Utils.CoalesceInt(RoomOccupancy.NumAdults) * 1.0 * Utils.CoalesceDouble(total_per_honeymooner);
                }

                return Utils.CoalesceInt(RoomOccupancy.NumAdults) * 1.0 * Utils.CoalesceDouble(total_per_adult) +
                    Utils.CoalesceInt(RoomOccupancy.NumInfants) * 1.0 * Utils.CoalesceDouble(total_per_infant) +
                Utils.CoalesceInt(RoomOccupancy.NumChildren) * 1.0 * Utils.CoalesceDouble(total_per_child) +
                Utils.CoalesceInt(RoomOccupancy.NumTeens) * 1.0 * Utils.CoalesceDouble(total_per_teen);
              
            }
        }        

        public double TotalCompulsoryCharges
        {
            get
            {
                return Utils.CoalesceInt(RoomOccupancy.NumAdults) * 1.0 * Utils.CoalesceDouble(charges_adult) +
                  Utils.CoalesceInt(RoomOccupancy.NumInfants) * 1.0 * Utils.CoalesceDouble(charges_infant) +
              Utils.CoalesceInt(RoomOccupancy.NumChildren) * 1.0 * Utils.CoalesceDouble(charges_child) +
              Utils.CoalesceInt(RoomOccupancy.NumTeens) * 1.0 * Utils.CoalesceDouble(charges_teen);
              
            }
        }

        public double TotalCostIncludingCharges
        {
            get
            {        
                return TotalRoomCost + TotalCompulsoryCharges;
            }
        }



        //obsolete
        public double CumulativeDiscountPercentage
        {
            get
            {
                double totDiscount = 0;
                if (Promotions.Count > 0)
                {
                    totDiscount = Promotions.Sum(x => x.discount);
                }
                return totDiscount;
            }
        }
        public double TotalAdult
        {
            get
            {
                return Utils.CoalesceInt(RoomOccupancy.NumAdults) * 1.0 * Utils.CoalesceDouble(TotalPriceAdult);
            }
        }
        public double TotalInfant
        {
            get { return Utils.CoalesceInt(RoomOccupancy.NumInfants) * 1.0 * Utils.CoalesceDouble(charges_infant); }
        }
        public double TotalChild1
        {
            get
            {
                if (RoomOccupancy.NumChildren > 0)
                    return Utils.CoalesceDouble(total_per_child);
                else
                    return 0.0;
            }
        }
        public double TotalChild2
        {
            get
            {
                if (RoomOccupancy.NumChildren == 2)
                    return Utils.CoalesceDouble(total_per_child);
                else
                    return 0.0;
            }
        }

        public double TotalTeen
        {
            get
            {
                if (RoomOccupancy.NumTeens > 0)
                    return Utils.CoalesceInt(RoomOccupancy.NumTeens) * 1.0 * Utils.CoalesceDouble(total_per_teen);
                else
                    return 0.0;
            }
        }

        public double TotalDiscountAmount
        {
            get
            {
                double totlDiscount = 0;
                foreach (var promotion in Promotions)
                {
                    totlDiscount = totlDiscount + (((TotalRoomCost - FreenightAmount) - totlDiscount) * (promotion.discount / 100));
                }

                return totlDiscount + FreenightAmount;

            }
        }

        public double FreenightAmount
        {
            get
            {
                double freenights_amt = 0D;
                var freenights = Promotions.Sum(x => x.free_nights);
                if (Utils.CoalesceInt(nights) > 0)
                {
                    freenights_amt = TotalRoomCost * (Utils.CoalesceInt(freenights) * 1.0 / Utils.CoalesceInt(nights));
                }
                return freenights_amt;
            }
        }

        public double GroundHandling
        {
           get
           {
               return GroundHandlingAdult + GroundHandlingChild + GroundHandlingTeen;
           }
        }

        public double GroundHandlingAdult
        {
            get
            { 
                return Utils.CoalesceInt(RoomOccupancy.NumAdults) * 1.0 * Utils.CoalesceDouble(ground_handling_adult); 
            }
        }

        public double GroundHandlingChild
        {
            get
            {
                return Utils.CoalesceInt(RoomOccupancy.NumChildren) * 1.0 * Utils.CoalesceDouble(ground_handling_child);
            }
        }
        public double GroundHandlingTeen
        {
            get
            {
                return Utils.CoalesceInt(RoomOccupancy.NumTeens) * 1.0 * Utils.CoalesceDouble(ground_handling_teen);
            }
        }

        public double TotalCostIncludingChargesAndGroundhandling
        {
           get
           {
               return TotalCostIncludingCharges + GroundHandling;
           }
        }

        public double TotalCostIncludingChargesAndPromotions
        {
            get
            {

                double freenights_amt = 0D;
                var freenights = Promotions.Sum(x => x.free_nights);
                if (Utils.CoalesceInt(nights)> 0)
                {
                    freenights_amt = TotalRoomCost * (Utils.CoalesceInt(freenights) * 1.0 / Utils.CoalesceInt(nights));
                }

               // return (TotalCostIncludingCharges - TotalDiscountAmount - freenights_amt) + GroundHandling;
                return (TotalRoomCost - TotalDiscountAmount) + GroundHandling + TotalCompulsoryCharges;

            }
        }


    }


}