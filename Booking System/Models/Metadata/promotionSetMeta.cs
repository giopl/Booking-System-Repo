﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{

    [MetadataType(typeof(promotionSetMeta))]
    public partial class promotionSet
    {

    }

    public class promotionSetMeta
    {

        [DisplayName("ID")]
        public int promotion_set_id { get; set; }

        [Required]
        [DisplayName("promotion Set Name")]
        [StringLength(100)]
        public string name { get; set; }

        [Required]
        [DisplayName("Description")]
        [StringLength(300)]
        [DataType(DataType.MultilineText)]
        public string description { get; set; }

        [DisplayName("Provider")]
        public int provider_id { get; set; }

        [DisplayName("Active")]
        public bool is_active { get; set; }
    }


}