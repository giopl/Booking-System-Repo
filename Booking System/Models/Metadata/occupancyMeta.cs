﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{
    public class occupancyMeta
    {
        public int occupancy_id { get; set; }

        [DisplayName("Name")]
        [Required]
        [StringLength(30)]
        public string name { get; set; }

        [DisplayName("Description")]
        [AllowHtml]
        
        [DataType(DataType.MultilineText)]
        public string description { get; set; }

        [DisplayName("No. Infants")]
        [Range(0, 10, ErrorMessage = "Value for {0} must be between {1} and {2}")]        
        public Nullable<int> infant { get; set; }

        [DisplayName("No. Children With Bed")]
        [Range(0, 10, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        public Nullable<int> child { get; set; }

        [DisplayName("No. Teens")]
        [Range(0, 10, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        public Nullable<int> teen { get; set; }

        [DisplayName("No. Adults")]
        [Range(0, 10, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        public Nullable<int> adult { get; set; }

        [DisplayName("No. Seniors")]
        [Range(0, 10, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        public Nullable<int> senior { get; set; }

        [DisplayName("No. Children Without Bed")]
        [Range(0, 10, ErrorMessage = "Value for {0} must be between {1} and {2}")]
        public Nullable<int> without_bed { get; set; }


    }

    [MetadataType(typeof(occupancyMeta))]
    public partial class occupancy
    {
        //[NotMapped]
        //public string occupancyCode
        //{
        //    get
        //    {
        //        return string.Concat(zeroifnull(infant), zeroifnull(child), zeroifnull(without_bed),
        //            zeroifnull(teen), zeroifnull(adult), zeroifnull(senior));

        //    }
        //}

        [NotMapped]
        public int DisplayOrder
        {
            get
            {
                int order = 0;
                if(adultsOnly)
                {
                    order = 10000;
                }
                if(Both)
                {
                    order = 20000;
                }
                if(ChildrenOnly)
                {
                    order = 30000;
                }

                return order = order + occupancyCode;
            }
        }

        [NotMapped]
        public string SectionHeader
        {
            get
            {
                if (adultsOnly)
                    return "Adults Only";

                if(ChildrenOnly)
                    return "Children Only";

                if (Both)
                    return "Adults and Children";

                return "";

            }
        }
                public bool OccHasValue(int? num)
        {
            return (num.HasValue && num.Value > 0);
        }

        [NotMapped]
        public bool adultsOnly
        {
              get
            {
                return (OccHasValue(adult) && !OccHasValue(infant) && !OccHasValue(child) && !OccHasValue(teen));
            }
        }

        [NotMapped]
        public bool ChildrenOnly
        {
            get
            {
                return (!OccHasValue(adult) && (OccHasValue(infant) || OccHasValue(child) || OccHasValue(teen) ));
            }
        }

        [NotMapped]
        public bool Both
        {
            get
            {
                return (OccHasValue(adult) && (OccHasValue(infant) || OccHasValue(child) || OccHasValue(teen)));
            }
        }




        [NotMapped]
        public int occupancyCode
        {
            get
            {
                return ((adult.HasValue ? adult.Value : 0) * 1000) + (infant.HasValue ? infant.Value : 0) + ((child.HasValue ? child.Value : 0) * 10) + ((teen.HasValue ? teen.Value : 0) * 100);                    
            }
        }

        public string occupancyDesc
        {
            get
            {
                StringBuilder desc = new StringBuilder();
                if(adult.HasValue && adult > 0 )
                {
                    desc.AppendFormat("{0} adult{1},",adult.Value, adult.Value>1?"s":"");
                }

                if (infant.HasValue && infant > 0)
                {
                    desc.AppendFormat("{0} infant{1},", infant.Value, infant.Value > 1 ? "s" : "");

                }
                if (child.HasValue && child> 0)
                {
                    desc.AppendFormat("{0} child{1},", child.Value, child.Value > 1 ? "ren" : "");
                }
                if (teen.HasValue && teen > 0)
                {
                    desc.AppendFormat("{0} teen{1},", teen.Value, teen.Value > 1 ? "s" : "");
                }
                if (senior.HasValue && senior > 0)
                {
                    desc.AppendFormat("{0} senior{1},", senior.Value, senior.Value > 1 ? "s" : "");
                }
                if(desc.Length>0)
                {

                desc.Length--;
                }
                return desc.ToString();
            }
        }

        private string zeroifnull(int? x)
        {
            return x == null ? "0" : x.ToString();
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var t = obj as occupancy;
            if (t == null)
                return false;
            if (occupancyCode == t.occupancyCode)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash += (hash * 43) + occupancyCode.GetHashCode();

            return hash;

        }


    }

}