﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Models
{
    [MetadataType(typeof(activityMeta))]
    public partial class activity
    {
        public string ValidFromDate
        {
            get
            {
                string result = "-";
                if (valid_from.HasValue)
                {
                    result = valid_from.Value.ToString("dd-MM-yyyy");
                }
                return result;

            }
        }

        public string ValidToDate
        {
            get
            {
                string result = "-";
                if (valid_to.HasValue)
                {
                    result = valid_to.Value.ToString("dd-MM-yyyy");
                }
                return result;

            }
        }
    }

    public partial class activityMeta
    {
        [DisplayName("ID")]
        public int activity_id { get; set; }

        //[Required]
        //[DisplayName("Provider Id")]
        //public int provider_id { get; set; }

        [Required]
        [DisplayName("Activity Name")]
        [StringLength(200)]
        public string activity_name { get; set; }

        [Required]
        [DisplayName("Description")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
     
        public string description { get; set; }

        [DisplayName("Cancellation Policy")]
        [DataType(DataType.MultilineText)]
        [AllowHtml]
       
        public string cancellation_policy { get; set; }

        [DisplayName("Conditions")]
        [AllowHtml]
        [DataType(DataType.MultilineText)]
       
        public string conditions { get; set; }

        [DisplayName("Category")]
        [StringLength(50)]
        public string category { get; set; }

        [DisplayName("Duration")]
        [StringLength(50)]
        public string duration { get; set; }

        [DisplayName("Min. Age")]
        [Range(0, 17,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [Required]
        public Nullable<int> min_age { get; set; }

        [DisplayName("Max Child Age")]
        [Range(0, 17,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        [Required]
        public Nullable<int> max_child_age { get; set; }

        
        [DisplayName("Is Active")]
        public bool is_active { get; set; }

        [DisplayName("Valid From")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
               ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> valid_from { get; set; }

        [DisplayName("Valid To")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
               ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> valid_to { get; set; }
    }
}