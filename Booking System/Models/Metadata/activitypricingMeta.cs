﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    [MetadataType(typeof(activityPricingMeta))]
    public partial class activityPricing
    {
     
    }


    public class activityPricingMeta
    {
        [DisplayName("ID")]
        public int activity_pricing_id { get; set; }
        [DisplayName("Activity")]
        [Required]
        public int activity_id { get; set; }

        [DisplayName("Currency")]
        [Required]
        public int currency_id { get; set; }

        [DisplayName("Child Price")]
        [Range(0, double.MaxValue)]
        public Nullable<double> price_child { get; set; }

        [DisplayName("Adult Price")]
        [Range(0, double.MaxValue)]
        public Nullable<double> price_adult { get; set; }
        
        [DisplayName("Child incl SIC")]
        public Nullable<double> price_child_incl_sic_transfer { get; set; }

        [DisplayName("Adult incl SIC")]
        public Nullable<double> price_adult_incl_sic_transfer { get; set; }

        [DisplayName("Child incl Private")]
        public Nullable<double> price_child_incl_private_transfer { get; set; }

        [DisplayName("Adult incl Private")]
        public Nullable<double> price_adult_incl_private_transfer { get; set; }

        
        public virtual activity activity { get; set; }
        public virtual currency currency { get; set; }
    }
}