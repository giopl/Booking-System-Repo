﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    public partial class roomUnavailabilityMeta
    {
       
    }

    [MetadataType(typeof(roomUnavailabilityMeta))]
    public partial class roomUnavailability
    {
        public int numDays
        {
            get
            {
                TimeSpan ts = end_date - start_date;
                return ts.Days+1;
            }
        }

        
    }
}