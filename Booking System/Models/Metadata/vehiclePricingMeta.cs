﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    [MetadataType(typeof(vehiclePricingMeta))]
    public partial class vehiclePricing
    {
     
    }


    public class vehiclePricingMeta
    {
        [DisplayName("ID")]
        public int vehicle_pricing_id { get; set; }

        [DisplayName("Vehicle")]
        [Required]
        public int vehicle_id { get; set; }

        [DisplayName("Currency")]
        [Required]
        public int currency_id { get; set; }

        [DisplayName("½ Day")]
        [Range(0, double.MaxValue)]
        public Nullable<double> half_day_price_with_driver { get; set; }

        [DisplayName("½ Day(driver)")]
        [Range(0, double.MaxValue)]
        public Nullable<double> half_day_price { get; set; }

        [DisplayName("Daily(driver)")]
        [Range(0, double.MaxValue)]
        public Nullable<double> daily_price_with_driver { get; set; }

        [DisplayName("Daily")]
        [Range(0, double.MaxValue)]
        public Nullable<double> daily_price { get; set; }

        [DisplayName("Is Active")]
        public byte is_active { get; set; }

        public virtual vehicle vehicle { get; set; }
        public virtual currency currency { get; set; }
    }
}