﻿using Booking_System.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class ProviderFound
    {
        public ProviderFound()
        {
            FoundRooms = new List<FoundRoom>();
            Hotel = new provider();
            Currency = new currency();
        }


        public IList<FoundCharge> Charges { get; set; }

        [Obsolete]
        public IList<PromotionSetResult> Promotions
        {
            get
            {
                HashSet<PromotionSetResult> Promotions = new HashSet<PromotionSetResult>();
                foreach(var r in FoundRooms)
                {
                    if (r.Promosets.Count() > 0) 
                    { 
                        var Y = r.Promosets.OrderBy(x => x.TotalCostIncludingChargesAndPromotions).First().Promotions;
                        foreach (var p in Y)
                        {
                            Promotions.Add(p);
                        }
                    }
                }
                return Promotions.ToList();
            }
        }

        public provider Hotel { get;set; }
        public currency Currency { get; set; }
        public bool IsHoneymoon { get; set; }

        public int PackageId { get; set; }
        public int Nights {get;set;}

        public int CurrencyId
        {
            get
            {
                if(FoundRooms.Count > 0)
                {
                    return FoundRooms.FirstOrDefault().currency_id;
                }
                return 0;
            }
        }

        public bool IsValid {
            get
            {
                return FoundRooms.Count == TotalRooms;
            }
        
        }
        public double GroundHandling
        {
            get
            {
                return  FoundRooms.Sum(x => x.GroundHandling);;
            }
        }

        public double TotalCostIncludingCharges
        {
            get
            {
                return FoundRooms.Sum(x => x.TotalCostIncludingCharges);
            }
        }

        public double TotalCostIncludingChargesAndGroundhandling
        {
            get
            {
                //return TotalCostIncludingCharges;
                return FoundRooms.Sum(x => x.TotalCostIncludingChargesAndGroundhandling);
            }
        }

        public double TotalCostIncludingChargesAndPromotions
        {
            get
            {
                return FoundRooms.Sum(x => x.TotalCostIncludingChargesAndPromotions);
            }
        }

        public double PerAdult
        {
            get {
                if(IsHoneymoon)
                {

                    return Utils.CoalesceDouble(FoundRooms.Sum(x => x.total_per_honeymooner * 2 + (x.charges_adult * 2)  + (x.ground_handling_adult * 2))) / TotalAdults;
                }
                
                return Utils.CoalesceDouble(FoundRooms.Sum(x => x.total_per_adult * x.RoomOccupancy.NumAdults + (x.charges_adult * x.RoomOccupancy.NumAdults) + (x.ground_handling_adult * x.RoomOccupancy.NumAdults)  ))/TotalAdults;
            }
        }


        public double PerInfant
        {
            get
            {
                
                return Utils.CoalesceDouble(FoundRooms.Sum(x => x.total_per_infant * x.RoomOccupancy.NumInfants+ (x.charges_infant* x.RoomOccupancy.NumInfants))) / TotalInfants;
            }
        }


        public double PerChild
        {
            get
            {
                
                return Utils.CoalesceDouble(FoundRooms.Sum(x => x.total_per_child* x.RoomOccupancy.NumChildren+ (x.charges_child* x.RoomOccupancy.NumChildren) + (x.ground_handling_child * x.RoomOccupancy.NumChildren) )) / TotalChildren;

            }
        }



        public double PerTeen
        {
            get
            {
                return Utils.CoalesceDouble(FoundRooms.Sum(x => x.total_per_teen* x.RoomOccupancy.NumTeens+ (x.charges_teen* x.RoomOccupancy.NumTeens) + (x.ground_handling_teen * x.RoomOccupancy.NumTeens))) / TotalTeens;

            }
        }


        public int TotalAdults
        {
            get
            {
                return FoundRooms.Sum(x => x.RoomOccupancy.NumAdults);
            }
        }

        public int TotalInfants
        {
            get
            {
                return FoundRooms.Sum(x => x.RoomOccupancy.NumInfants);
            }
        }
        public int TotalTeens
        {
            get
            {
                return FoundRooms.Sum(x => x.RoomOccupancy.NumTeens);
            }
        }

        public int TotalChildren
        {
            get
            {
                return FoundRooms.Sum(x => x.RoomOccupancy.NumChildren);
            }
        }

        /// <summary>
        /// number of rooms required to be valid
        /// </summary>
        public int TotalRooms { get; set; }

        public int ProviderId { get; set; }

        public int Id
        {
            get {

            return (ProviderId * 10 ) + Nights;
            }
        }

        public IList<FoundRoom> FoundRooms { get; set; }




        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var t = obj as ProviderFound;
            if (t == null)
                return false;
            if (Id == t.Id)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash += (hash * 7) + Id.GetHashCode();
            

            return hash;

        }

    }


    public class SearchRoomFound
    {
        public int RoomNumber { get; set; }


    }
}