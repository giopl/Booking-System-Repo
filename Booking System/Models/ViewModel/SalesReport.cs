﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class SalesReport
    {
        public string   ProviderName                            {get;set;}
        public DateTime CheckinDate                             {get;set;}
        public DateTime checkout_date                           {get;set;}     
        public int      Adults                                  {get;set;}
        public int      TotalChildrenAndTeens                   {get;set;}             
        public int      bookingRoomCount                        {get;set;}        
        public int      NumNights                               {get;set;} 
        public string   currencyCode                            {get;set;}
        public double   TotalCostRooms                          {get;set;}
        public double   TotalCostSupps                          {get;set;}
        public string   isPackage                               {get;set;}
        public string   TransferType                            {get;set;}    
        public double   TotalCostTransferWithAfterSales         {get;set;}                       
        public double   TotalCostActivityWithAfterSales         {get;set;}                       
        public double   TotalCostRentalWithAfterSales           {get;set;}                     
        public double   TotalCost                               {get;set;}

    }
}