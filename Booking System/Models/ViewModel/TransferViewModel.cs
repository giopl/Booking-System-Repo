﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class TransferViewModel
    {
        public transfer Transfer { get; set; }
        public transferPricing InitialTransferPricing { get; set; }
        public int currency_id { get; set; }
        //public IList<transferPricing> TransferPricings { get; set; }

        public TransferViewModel() {
            Transfer = new transfer();
            InitialTransferPricing = new transferPricing();
            //TransferPricings = new List<transferPricing>();
        }
    }
}