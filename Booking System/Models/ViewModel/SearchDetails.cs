﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class SearchDetails
    {
        public SearchDetails()
        {

        }

        private DateTime _start;

        public DateTime Start
        {
            get
            {
                if(_start == DateTime.MinValue)
                {
                    _start = new DateTime(1753, 1, 1);
                }

                if(_start == DateTime.MaxValue)
                {
                    _start = new DateTime(9999, 12, 31);
                }
                return _start;
            }
            set { _start = value; }
        }


        private DateTime _end;

        public DateTime End
        {
            get
            {
                if (_end == DateTime.MinValue)
                {
                    _end = new DateTime(1753, 1, 1);
                }

                if (_end == DateTime.MaxValue)
                {
                    _end = new DateTime(9999, 12, 31);
                }
                return _end;
            }
            set { _end = value; }
        }

        public int Rooms { get; set; }


        public int HotelId { get; set; }

        public bool IsHotel { get; set; }
        public bool IsPackage { get; set; }
        public bool IsHoneymoon { get; set; }

        public string HoneymoonHotelOrPackage { get; set; }
        public int PackageNights { get; set; }

        public int TotalNights
        {

            get
            {
                TimeSpan ts = End - Start;
                return Convert.ToInt32(ts.TotalDays);
            }
        }

        public int _Rooms
        {
            get
            {
                return Rooms == 0 ? 1 : Rooms;

            }
        }

        public string SearchString { get; set; }
        public string Location { get; set; }
        public string Nationality { get; set; }

        public int[] RoomAdults { get; set; }
        public int[] RoomChildren { get; set; }
        public int[] RoomChildAge1 { get; set; }
        public int[] RoomChildAge2 { get; set; }
        public int[] RoomChildAge3 { get; set; }
        public int[] RoomChildAge4 { get; set; }
        public int[] RoomChildAge5 { get; set; }
        public guestType GuestType { get; set; }


        public int[] RoomChildAgeByNumber(int id = 1)
        {
           
                switch (id)
                {
                    case 1:
                        return RoomChildAge1;
                    case 2:
                        return RoomChildAge2;
                    case 3:
                        return RoomChildAge3;
                    case 4:
                        return RoomChildAge4;
                    case 5:
                        return RoomChildAge5;
                }
                return null;            
        }

        public int TotalGuests
        {
            get
            {
                var total = 0;
                if (RoomAdults != null)
                {
                    foreach (var room in RoomAdults)
                    {
                        total += room;
                    }
                }
                return total;
            }
        }

        public int TotalChildren
        {
            get
            {
                var total = 0;
                if (RoomChildren != null)
                {
                    foreach (var room in RoomChildren)
                    {
                        total += room;
                    }
                }
                return total;
            }
        }

    }



}