﻿using Booking_System.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class RoomsFoundViewModel
    {
        public string BookingRef { get; set; }

        public int bookingId { get; set; }

        public RoomsFoundViewModel() {

            roomAndPrice = new List<RoomAndPrice>();
            
        }




        public IList<RoomAndPrice> roomAndPrice { get; set; }

        public provider hotel { get; set; }

        

        public int numberRoomsRequired { get; set; }
    }


    public class RoomAndPrice
    {
        public RoomAndPrice()
        {
            room = new room();
            baseprice = new basePrice();
            Promotions = new List<PromotionResult>();
        }

        public IList<PromotionResult> Promotions { get; set; }
        public IList<PromotionSetResult> PromotionSets { get; set; }

        public double CumulativeDiscountPercentage
        {
            get
            {
                double totDiscount = 0;
                if (Promotions.Count > 0)
                    totDiscount = Promotions.Sum(x => x.discount);

                return totDiscount;
            }
        }

        public double DiscountAmount
        {
            get
            {

                
                return TotalPriceAllNights * (CumulativeDiscountPercentage / 100);
            }
        }

        public double? PromotionalPrice
        {
            get
            {


                double? freenights_amt = 0D;
                var freenights = Promotions.Sum(x => x.free_nights);
                if (numberNights > 0)
                {
                    freenights_amt = TotalPriceAllNights * (freenights * 1.0 / numberNights);

                }


                return TotalPriceAllNights - DiscountAmount - freenights_amt;
            }
        }



        public int OccupancyCode
        {

            get
            {
                return (room.room_id * 10) + OccupancyNumber;

            }
        }

        public bool isSelected { get; set; }

        public int numberAdults { get; set; }
        public int numberInfants { get; set; }
        public int numberChildren { get; set; }
        public int numberTeens { get; set; }

        public int numberNights { get; set; }


        public double pricePerAdult
        {
            get
            {
                return (TotalPriceAdults / numberAdults) * (100-CumulativeDiscountPercentage)/100.0;
            }
        }

        public double pricePerInfant
        {
            get
            {
                return (TotalPriceInfants / numberInfants) * (100 - CumulativeDiscountPercentage) / 100.0;
            }
        }


        public double pricePerChild
        {
            get
            {
                return (TotalPriceChildren / numberChildren )* (100 - CumulativeDiscountPercentage) / 100.0;
            }
        }

        public double pricePerTeen
        {
            get
            {
                return (TotalPriceTeens / numberTeens) * (100 - CumulativeDiscountPercentage) / 100.0;
            }
        }



        public double TotalPriceAllNights
        {
            get
            {
                return (TotalPrice) + TotalCompulsoryPrice;
            }
        }

        public double TotalPriceAdults { get; set; }
        public double TotalPriceInfants { get; set; }
        public double TotalPriceChildren { get; set; }
        public double TotalPriceTeens { get; set; }
        public double TotalPrice { get; set; }

        public double CompulsoryPriceAdults { get; set; }
        public double CompulsoryPriceInfants { get; set; }
        public double CompulsoryPriceChildren { get; set; }
        public double CompulsoryPriceTeens { get; set; }
        public double TotalCompulsoryPrice { get; set; }




        public int OccupancyNumber { get; set; }

        public room room { get; set; }
        public basePrice baseprice { get; set; }

        

      
    }

}