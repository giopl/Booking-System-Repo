﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class VehicleViewModel
    {


        public int? provider_id { get; set; }
        public int? currency_id { get; set; }

        public VehicleViewModel()
        {
            Vehicle = new vehicle();
            VehiclePricing = new vehiclePricing();
            VehiclePicture = new picture();
            VehiclePricings = new List<vehiclePricing>();
            //TransferPricing = new transferPricing();
            //TransferPricings = new List<transferPricing>();


        }

        public vehicle Vehicle { get; set; }
        public vehiclePricing VehiclePricing { get; set; }

        //public transferPricing TransferPricing { get; set; }

        public IList<vehiclePricing> VehiclePricings { get; set; }
        //public IList<transferPricing> TransferPricings { get; set; }

        public picture VehiclePicture { get; set; }
    }
}