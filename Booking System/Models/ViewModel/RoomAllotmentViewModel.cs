﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class RoomAllotmentViewModel
    {
        public int ProviderId { get; set; }

        [Required]
        [DisplayName("Start Date")]
        public string StartDate { get; set; }

        [Required]
        [DisplayName("End Date")]
        public string EndDate { get; set; }


        [Required]
        [DisplayName("Rooms")]
        public int[] Rooms { get; set; }


        [Required]
        [DisplayName("Number of Allotments")]
        public int NumberAllotments { get; set; }

        [Required]
        [DisplayName("Chosen Dates")]
        public string MultipleDates { get; set; }



    }
}