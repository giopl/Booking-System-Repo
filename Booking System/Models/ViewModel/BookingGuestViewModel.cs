﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class BookingGuestViewModel
    {
        public int booking_id { get; set; }

        
        public string customer_name { get; set; }
        
        public string customer_phone { get; set; }
        
        public string customer_email { get; set; }
        public string booking_file_number { get; set; }
        public bool is_timelimit { get; set; }
        public string[] flight_number { get; set; }
        public string[] flight_date { get; set; }
        public string[] flight_time { get; set; }
        public string[] depflight_number { get; set; }
        public string[] depflight_date { get; set; }
        public string[] depflight_time { get; set; }
        public int num_guests { get; set; }
        public int[] guest_id { get; set; }
        public int[] gender{ get; set; }
        public string remarks { get; set; }


        
        public string[] firstname { get; set; }

        
        public string[] lastname { get; set; }
            public string[] salutation { get; set; }
            public string[] nationality { get; set; }
            public string[] passport_number { get; set; }
            public DateTime[] date_of_birth { get; set; }
            public DateTime[] passport_expiry_date { get; set; }
            
            
    }
}