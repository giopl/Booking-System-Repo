﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models
{
    public class PromotionResultMeta
    {
        public int promotion_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string contracted_offer { get; set; }
        public string cancellation_policy { get; set; }
        public int market_id { get; set; }
        public string market_name { get; set; }
        public int room_id { get; set; }
        public string room_name { get; set; }
        public Nullable<int> min_nights { get; set; }
        public Nullable<int> free_nights { get; set; }
        public double discount { get; set; }
        public bool is_honeymoon { get; set; }
        public bool apply_all_rooms { get; set; }
        public bool apply_all_markets { get; set; }
    }


     [MetadataType(typeof(PromotionResultMeta))]
    public partial class PromotionResult
    {
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var t = obj as PromotionResult;
            if (t == null)
                return false;
            if (promotion_id == t.promotion_id)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash += (hash * 7) + promotion_id.GetHashCode();
            

            return hash;

        }



    }
}