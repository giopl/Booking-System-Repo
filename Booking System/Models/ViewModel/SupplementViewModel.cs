﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class SupplementViewModel
    {
        public int[] mealplan { get; set; }

        public int[] supplementId { get; set; }

    }
}