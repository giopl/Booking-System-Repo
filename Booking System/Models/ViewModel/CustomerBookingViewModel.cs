﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class CustomerBookingViewModel
    {
        
        public CustomerBookingViewModel()
        {
            ViewBooking = new viewBooking();
            Booking = new booking();
        }

        public viewBooking ViewBooking { get; set; }
        public booking Booking { get; set; }


    }
}