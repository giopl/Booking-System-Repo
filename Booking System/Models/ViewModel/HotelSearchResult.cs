﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class HotelSearchResult
    {
        public IList<ProviderFound> Hotels { get; set; }
        public HotelSearchCriteria SearchDetails { get; set; }

        public int NumAdults { get; set; }
        public int NumMinors {get;set;}

        public int NumGuests
        {
            get
            {
                return NumAdults + NumMinors;
            }
        }
       

        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public int Nights
        {
            get
            {
                TimeSpan ts = End - Start;
                return Convert.ToInt32(ts.TotalDays);
            }
        }

        public int NumRooms { get; set; }

        public bool IsPackage { get; set; }
        public bool IsHoneymoon { get; set; }

        public HotelSearchResult()
        {
            Hotels = new List<ProviderFound>();
            SearchDetails = new HotelSearchCriteria();

        }
    }
}