﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class HotelOrPackageViewModel
    {
        public HotelOrPackageViewModel()
        {
            FoundRooms = new List<FoundRoom>();
            Hotel = new provider();
        }

        public IList<FoundRoom> FoundRooms { get; set; }

        public provider Hotel  { get; set; }

        public string BookingRef {get;set;}

        public int NumRooms {get;set;}

        public bool IsPackage { get; set; }
        
    }
}