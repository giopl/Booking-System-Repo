﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Booking_System.Helpers;

namespace Booking_System.Models.ViewModel
{
    public class PromosetViewModel
    {

        public double _TotalAdult;
        public double _TotalChild1;
        public double _TotalChild2;
        public double _TotalTeen;
        
        public double _GroundHandlingChild;
        public double _GroundHandlingTeen;
        public double _GroundHandlingAdult;

        public int NoOfInfant { get; set; }
        public int NoOfChild { get; set; }
        public int NoOfAdult { get; set; }
        public int NoOfTeen { get; set; }
       
        public double TotalInfant { get; set; }

        public double TotalRoomCost{get;set;}
        public double TotalCompulsoryCharges { get; set; }
        public double GroundHandling { get; set; }
        public double TotalTeen 
        {
            get
            {
                if (NoOfTeen > 0)
                {
                    if (Promotions.Count > 0)
                    {
                        //check for flat rate offer
                        var p = Promotions.Where(x => x.promo_categ == "Family Offer" || x.promo_categ == "Flat Rate").FirstOrDefault();
                        if (p != null)
                        {
                            if (NoOfTeen == 1 && p.flat_teen1 != null)
                            {
                                return Utils.CoalesceInt(nights) * Utils.CoalesceDouble(p.flat_teen1) * NoOfTeen;
                            }
                            else
                            {
                                return _TotalTeen;
                            }

                        }
                        else
                        {
                            return _TotalTeen;
                        }
                    }
                    else
                    {
                        return _TotalTeen;
                    }
                }
                else {
                    return 0.0;
                }
                
               
            }
            set
            {
                _TotalTeen = value;
            } 
        
        }
        public double TotalAdult 
        { 
            get 
            {
                if (Promotions.Count > 0)
                {
                    //check for flat rate offer
                    var p = Promotions.Where(x => x.promo_categ == "Family Offer" || x.promo_categ == "Flat Rate").FirstOrDefault();
                    if (p != null)
                    {
                        if (NoOfAdult == 1 && p.flat_single != null) 
                        {
                            return Utils.CoalesceInt(nights) * Utils.CoalesceDouble(p.flat_single) * NoOfAdult;
                        }
                        if (NoOfAdult == 2 && p.flat_twin != null)
                        {
                            return Utils.CoalesceInt(nights) * Utils.CoalesceDouble(p.flat_twin) * NoOfAdult;
                        }
                        if (NoOfAdult == 3 && p.flat_triple != null)
                        {
                            return Utils.CoalesceInt(nights) * Utils.CoalesceDouble(p.flat_triple) * NoOfAdult;
                        }
                        if (NoOfAdult == 4 && p.flat_quadruple != null)
                        {
                            return Utils.CoalesceInt(nights) * Utils.CoalesceDouble(p.flat_quadruple) * NoOfAdult;
                        }
                        return _TotalAdult;
                    }
                }
                return _TotalAdult; 
            
            } 
            set 
            { 
                _TotalAdult = value; 
            } 
        }

        public double TotalChild1 { 
            get 
            {
                if (NoOfChild > 0)
                {
                    if (Promotions.Count > 0)
                    {
                        var p = Promotions.Where(x => x.promo_categ == "Family Offer" || x.promo_categ == "Flat Rate").FirstOrDefault();
                        if (p != null && p.flat_child1 != null)
                        {
                            return Utils.CoalesceInt(nights) * Utils.CoalesceDouble(p.flat_child1);
                        }
                        else
                        {
                            return _TotalChild1;
                        }
                    }
                    else
                    {
                        return _TotalChild1;
                    }     
                }
                else
                {
                    return 0.0;
                }
                           
            } 
            set 
            { 
                _TotalChild1 = value; 
            } 
        }

        public double TotalChild2 {
            get 
            {
                if (NoOfChild > 1)
                {
                    if (Promotions.Count > 0)
                    {
                        var p = Promotions.Where(x => x.promo_categ == "Family Offer" || x.promo_categ == "Flat Rate").FirstOrDefault();
                        if (p != null && p.flat_child2 != null)
                        {
                            return Utils.CoalesceInt(nights) * Utils.CoalesceDouble(p.flat_child2);
                        }
                        else
                        {
                            return _TotalChild2;
                        }
                    }
                    else
                    {
                        return _TotalChild2;
                    }      
                }
                else
                {
                    return 0.0;
                }
                    

                   
            }
            set 
            {
                _TotalChild2 = value; 
            }
        }
        
        public double GroundHandlingChild { get { return _GroundHandlingChild; } set { _GroundHandlingChild = value; } }
        public double GroundHandlingTeen { get { return _GroundHandlingTeen; } set { _GroundHandlingTeen = value; } }
        public double GroundHandlingAdult { get { return _GroundHandlingAdult; } set { _GroundHandlingAdult = value; } } 



        public int nights { get; set; }
        public List<PromotionSetResult> Promotions { get; set; }

        public PromosetViewModel()
        {
            discounts = new List<double>();
            Promotions = new List<PromotionSetResult>();            
        }


        public double TotalAdultWithDiscount
        {
            get
            {
                double totlDiscount = 0;
                var totalCost = TotalAdult;

                double freenights_amt = 0D;
                var freenights = Promotions.Sum(x => x.free_nights);
                if (Utils.CoalesceInt(nights) > 0)
                {
                    freenights_amt = totalCost * (Utils.CoalesceInt(freenights) * 1.0 / Utils.CoalesceInt(nights));
                }

                foreach (var promotion in Promotions)
                {
                    totlDiscount = totlDiscount + (((totalCost - freenights_amt) - totlDiscount) * (promotion.discount / 100));
                }

                return TotalAdult - (totlDiscount + freenights_amt);

            }
        }

        public double TotalChildWithDiscount
        {
            get
            {
                double totlDiscount = 0;
                var totalCost = (TotalChild1 + TotalChild2);

                double freenights_amt = 0D;
                var freenights = Promotions.Sum(x => x.free_nights);
                if (Utils.CoalesceInt(nights) > 0)
                {
                    freenights_amt = totalCost * (Utils.CoalesceInt(freenights) * 1.0 / Utils.CoalesceInt(nights));
                }

                foreach (var promotion in Promotions)
                {
                    totlDiscount = totlDiscount + (((totalCost - freenights_amt) - totlDiscount) * (promotion.discount / 100));
                }

                return  (TotalChild1 + TotalChild2) - ( totlDiscount + freenights_amt);

            }
        }

        public double TotalTeenWithDiscount
        {
            get
            {
                double totlDiscount = 0;
                var totalCost = TotalTeen;

                double freenights_amt = 0D;
                var freenights = Promotions.Sum(x => x.free_nights);
                if (Utils.CoalesceInt(nights) > 0)
                {
                    freenights_amt = totalCost * (Utils.CoalesceInt(freenights) * 1.0 / Utils.CoalesceInt(nights));
                }

                foreach (var promotion in Promotions)
                {
                    totlDiscount = totlDiscount + (((totalCost - freenights_amt) - totlDiscount) * (promotion.discount / 100));
                }

                return TotalTeen - (totlDiscount + freenights_amt);

            }
        }

        public double TotalDiscountAmount
        {
            get
            {
                double totlDiscount = 0;
                var totalCost = TotalAdult + TotalInfant + TotalChild1 + TotalChild2 + TotalTeen;
                foreach (var promotion in Promotions)
                {
                    totlDiscount = totlDiscount + (((totalCost - FreenightAmount) - totlDiscount) * (promotion.discount / 100));
                }

                return totlDiscount + FreenightAmount;

            }
        }

        public double FreenightAmount
        {
            get 
            {
                double freenights_amt = 0D;
                var freenights = Promotions.Sum(x => x.free_nights);
                if (Utils.CoalesceInt(nights) > 0)
                {
                    freenights_amt = TotalRoomCost * (Utils.CoalesceInt(freenights) * 1.0 / Utils.CoalesceInt(nights));
                }
                return freenights_amt;
            }
        }
        //TODO - totalchild1 and teens should be based on if teen / child exists in search.
        public double TotalCostIncludingChargesAndPromotions
        {
            get
            {
                var totalCost = TotalAdult + TotalInfant + TotalChild1 + TotalChild2 + TotalTeen;
                return (totalCost - TotalDiscountAmount) + GroundHandling + TotalCompulsoryCharges;
            }
        }

        public double TotalAdultWithPromotion{

        get{

           // return (TotalCostIncludingChargesAndPromotions - TotalChild1 - TotalChild2 - TotalTeen - TotalInfant - GroundHandlingTeen - GroundHandlingChild)/NoOfAdult;
            return (TotalAdultWithDiscount + GroundHandlingAdult) / NoOfAdult;
        
        }
    }
        public double TotalChildWithPromotion
        {

            get
            {
                if (NoOfChild > 0)
                    return (TotalChildWithDiscount+GroundHandlingChild) / NoOfChild;//  return (TotalCostIncludingChargesAndPromotions - TotalAdult - TotalTeen - TotalInfant - GroundHandlingTeen - GroundHandlingAdult) / NoOfChild;
                else
                    return 0.0;

            }
        }

        public double TotalTeenWithPromotion
        {

            get
            {
                if (NoOfTeen > 0)
                     return (TotalTeenWithDiscount + GroundHandlingTeen) / NoOfTeen; //return (TotalCostIncludingChargesAndPromotions - TotalAdult - TotalChild1 - TotalChild2 - TotalInfant - GroundHandlingChild - GroundHandlingAdult) / NoOfTeen;
                else
                    return 0.0;

            }
        }

        public string name { get; set; }
        public IList<double> discounts { get; set; }
        public int freenights { get; set; }
        public int setid { get; set; }
    }
}