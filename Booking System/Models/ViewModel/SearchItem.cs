﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class SearchItem
    {
        [DisplayName("Booking ID")]
        public Nullable<int> booking_id { get; set; }

        [DisplayName("Market")]
        public Nullable<int> market_id { get; set; }

        [DisplayName("Agent")]
        public Nullable<int> agent_user_id { get; set; }

        [DisplayName("BFN")]
        public string booking_file_number { get; set; }

        [DisplayName("Booking Name")]
        public string booking_name { get; set; }

        
        [DisplayName("Checking Date")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
               ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> checkin_date { get; set; }

        [DisplayName("Checkout Date")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
               ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> checkout_date { get; set; }

        [DisplayName("Number Of Nights")]
        public Nullable<int> num_nights { get; set; }

        [DisplayName("Created Date")]
        public Nullable<System.DateTime> create_date { get; set; }

        [DisplayName("Booking Date")]
        public Nullable<System.DateTime> booking_date { get; set; }

        [DisplayName("Status")]
        public Nullable<int> status { get; set; }


        [DisplayName("Booking Expiry Date")]
        public Nullable<System.DateTime> booking_expiry_date { get; set; }

        [DisplayName("Back Office User")]
        public Nullable<int> backoffice_user_id { get; set; }

        [DisplayName("Booking Reference")]
        public string booking_ref { get; set; }

        [DisplayName("Number Of Adults")]
        public Nullable<int> adults { get; set; }

        [DisplayName("Number Of Infants")]
        public Nullable<int> infants { get; set; }

        [DisplayName("Number Of Children")]
        public Nullable<int> children { get; set; }

        [DisplayName("Number Of Teens")]
        public Nullable<int> teens { get; set; }

        [DisplayName("Number Of Rooms")]
        public Nullable<int> rooms { get; set; }

        [DisplayName("Phone Number")]
        public string phone_number { get; set; }

        [DisplayName("Email")]
        public string email { get; set; }

        [DisplayName("Provider")]
        public Nullable<int> provider_id { get; set; }

        [DisplayName("Discount Amount")]
        public Nullable<double> discount_amt { get; set; }

        [DisplayName("Discount Reason")]

        public string discount_reason { get; set; }

        [DisplayName("Discounted By")]
        public Nullable<int> discounted_by_user_id { get; set; }

        public bool HasSearch { get; set; }

        [DisplayName("Flight Date")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}",
       ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> flight_date { get; set; }
    }
}