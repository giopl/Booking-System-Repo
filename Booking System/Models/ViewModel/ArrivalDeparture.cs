﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class ArrivalDeparture
    {
        
        public string BookingFilenumber { get; set; }
        public string BookingName { get; set; }
        public string AgentName { get; set; }
        public string HotelName { get; set; }   
        public string Market { get; set; }   
        public string CheckinDate { get; set; }
        public string CheckoutDate { get; set; }
        public int Pax { get; set; }
        public string EventDate { get; set; }
        public string EventTime { get; set; }
    }
}