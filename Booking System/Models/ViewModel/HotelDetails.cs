﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class HotelDetails
    {

        public HotelDetails()
        {
            Hotel = new provider();
            Market = new market();
            Room = new room();
            BasePrice = new basePrice();
            Occupancies = new List<SearchRoomOccupancy>();
            Promotions = new List<PromotionResult>();

        }



        /// <summary>
        /// used to ensure same hotel and package nights type not added twice to result list when search package
        /// e.g 6-N hotel A
        /// 4-N hotel A 
        /// </summary>
        public int HotelNightCode
        {
            get
            {
                return Hotel.provider_id * 100 + NumberNights.Value;
            }
        }

        public int PackageId { get; set; }

        public string PackageName { get; set; }

        public provider Hotel { get; set; }
        public market Market { get; set; }
       public room Room { get; set; }

        public int HotelId { get; set; }
        public int? NumberNights { get; set; }

        public double? PriceTotal { get; set; }
        public double? CumulativeDiscountPercentage { 
            get
            {
                double? totDiscount = 0;
                if (Promotions.Count > 0)
                {
                    totDiscount = Promotions.Sum(x => x.discount);


                }

                //free nights
                

                return totDiscount;
            }
        }

        public double? DiscountAmount
        {
            get
            {
                return PriceTotal * (CumulativeDiscountPercentage/100);
            }
        }

        public double? PromotionalPrice { 
            get
            {

                double? freenights_amt = 0D;
                var freenights = Promotions.Sum(x => x.free_nights);
                if (NumberNights.HasValue && NumberNights.Value > 0)
                
                {
                    freenights_amt = PriceTotal * (freenights*1.0 / NumberNights.Value);

                }

                return PriceTotal - DiscountAmount - freenights_amt;
            }
         }
        public double? PriceAdults { get; set; }
        public double? PriceInfants { get; set; }
        public double? PriceChildren { get; set; }
        public double? PriceTeens { get; set; }

        public int Nights { get; set; }
        

        public string Currency { get; set; }
        public string Symbol { get; set; }

        public int NumRoomsFound { get; set; }


        public IList<SearchRoomOccupancy> Occupancies { get; set; }

        public IList<PromotionResult> Promotions { get; set; }

        public basePrice BasePrice { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            var t = obj as HotelDetails;
            if (t == null)
                return false;
            if (HotelId== t.HotelId)
                return true;
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 13;
            hash += (hash * 43) + HotelId.GetHashCode();

            return hash;

        }
        

    }

    public class SearchRoomOccupancy
    {
        public int adult { get; set; }
        public int child { get; set; }
        public int infant { get; set; }
        public int teen { get; set; }

        public double rateAdult { get; set; }
        public double rateChild { get; set; }
        public double rateInfant { get; set; }
        public double rateTeen { get; set; }
    }
}