﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class SearchOccupancy
    {
        public int Adults { get; set; }
        public int RoomNumber { get; set; }

        public List<int> Ages { get; set; }


        public SearchOccupancy()
        {
            Ages = new List<int>();
        }
    }
}