﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class HotelSearchCriteria
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int HotelId { get; set; }
        public int PackageId { get; set; }
        public bool IsHoneymoon { get; set; }

        public bool IsPackage { get; set; }

//        public List<RoomOccupancy> RoomOccupancy { get; set; }
        public List<SearchOccupancy> SearchOccupancies { get; set; }
        public int NumNightsPackage { get; set; }

            public HotelSearchCriteria()
            {
                SearchOccupancies = new List<SearchOccupancy>();

            }

    }
}