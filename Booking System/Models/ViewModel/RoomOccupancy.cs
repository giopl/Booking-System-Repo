﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Models.ViewModel
{
    public class RoomOccupancy
    {

        public RoomOccupancy()
        {
            ChildAges = new List<int>();
            InfantAges = new List<int>();
            TeenAges = new List<int>();
        }
        public RoomOccupancy(int adults, int teens, int children, int infants)
        {

            ChildAges = new List<int>();
            InfantAges = new List<int>();
            TeenAges = new List<int>();

            NumAdults = adults;
            NumChildren = children;
            NumTeens = teens;
            NumInfants = infants;
        }

        public int RoomNumber { get; set; }

        public int NumAdults { get; set; }
        public int NumChildren { get; set; }
        public int NumInfants { get; set; }
        public int NumTeens { get; set; }

        public List<int> ChildAges { get; set; }
        public List<int> InfantAges { get; set; }
        public List<int> TeenAges { get; set; }

        public int OccupancyCode
        {
            get
            {
                return (NumAdults * 1000) + (NumTeens * 100) + (NumChildren * 10) + NumInfants;
            }
        }


    //    public RoomOccupancy(int adults, int teens, int children, int infants)
    //    {
    //        NumAdults = adults;
    //        NumChildren = children;
    //        NumInfants = infants;
    //        NumTeens = teens;
    //        TeenAges = new List<int>();
    //        InfantAges = new List<int>();
    //        ChildrenAges = new List<int>();

    //    }

    //    public RoomOccupancy() {

    //        TeenAges = new List<int>();
    //        InfantAges = new List<int>();
    //        ChildrenAges = new List<int>();
    //    }


    //    public int NumChildren { get; set; }
    //    public int NumAdults{ get; set; }
    //    public int NumTeens { get; set; }
    //    public int NumInfants { get; set; }


        
    //    public List<int> InfantAges { get; set; }
    //    public List<int> ChildrenAges { get; set; }
    //    public List<int> TeenAges { get; set; }

    }
}