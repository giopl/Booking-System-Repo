//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Booking_System.Models
{
    using System;
    
    public partial class FoundCharge
    {
        public int supplement_id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public Nullable<double> price_infant { get; set; }
        public Nullable<double> price_child { get; set; }
        public Nullable<double> price_teen { get; set; }
        public Nullable<double> price_adult { get; set; }
    }
}
