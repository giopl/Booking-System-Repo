//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Booking_System.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class feature
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public feature()
        {
            this.room_feature = new HashSet<roomFeature>();
            this.hotel_feature = new HashSet<hotelFeature>();
            this.vehicle_feature = new HashSet<vehicleFeature>();
        }
    
        public int feature_id { get; set; }
        public string category { get; set; }
        public string description { get; set; }
        public string feature_type { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public bool is_active { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<roomFeature> room_feature { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<hotelFeature> hotel_feature { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<vehicleFeature> vehicle_feature { get; set; }
    }
}
