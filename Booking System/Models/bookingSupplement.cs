//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Booking_System.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class bookingSupplement
    {
        public int booking_supplement_id { get; set; }
        public int booking_id { get; set; }
        public Nullable<int> supplement_id { get; set; }
        public Nullable<int> quantity { get; set; }
        public Nullable<double> price { get; set; }
        public Nullable<int> booking_room_id { get; set; }
        public Nullable<int> infants { get; set; }
        public Nullable<int> children { get; set; }
        public Nullable<int> teens { get; set; }
        public Nullable<int> adults { get; set; }
        public Nullable<double> markup_amt { get; set; }
        public string supplement_name { get; set; }
    
        public virtual bookingRoom booking_room { get; set; }
        public virtual booking booking { get; set; }
    }
}
