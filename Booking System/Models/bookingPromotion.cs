//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Booking_System.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class bookingPromotion
    {
        public int booking_promotion_id { get; set; }
        public int booking_id { get; set; }
        public Nullable<int> booking_room_id { get; set; }
        public Nullable<int> promotion_id { get; set; }
        public string promotion_name { get; set; }
        public string promotion_description { get; set; }
        public Nullable<int> free_nights { get; set; }
        public Nullable<double> discount { get; set; }
    
        public virtual bookingRoom booking_room { get; set; }
        public virtual booking booking { get; set; }
    }
}
