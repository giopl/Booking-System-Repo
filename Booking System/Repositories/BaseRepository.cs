﻿using Booking_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Repositories
{

    /// <summary>
    /// Main class to handle Entity Framework connection and enable dispose of objects 
    /// </summary>
    /// 



  
    public class BaseRepository:IDisposable
    {

        ~BaseRepository()
        {
            Dispose(false);
        }


        protected ABSDBEntities db = new ABSDBEntities(); //initialise Entity framework controller


        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// Clean Entity Framework when everything is completed
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (this.db != null)
            {
                this.db.Dispose();
                this.db = null;
            }
        }
    }
}