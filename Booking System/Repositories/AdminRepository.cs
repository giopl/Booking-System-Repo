﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Booking_System.Models;
using System.Data.Entity;


namespace Booking_System.Repositories
{

    public class AdminRepository:BaseRepository
    {
        ILog log = log4net.LogManager.GetLogger(typeof(AdminRepository));



        #region Person

        /// <summary>
        /// Gert list of all people
        /// </summary>
        /// <returns></returns>
        public List<user> FetchAllUsers()
        {
            try
            {
                return db.users.ToList();
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion

        /*

         #region Person

         /// <summary>
         /// Gert list of all people
         /// </summary>
         /// <returns></returns>
         public List<person> FetchAllPersons()
         {
             try
             {
                 return db.people.ToList();
             }
             catch (Exception)
             {

                 throw;
             }

         }

         /// <summary>
         /// Get a person by id
         /// </summary>
         /// <param name="id"></param>
         /// <returns></returns>
         public person FetchPersonById(int? id)
         {
             try
             {
                 return db.people.Find(id);
             }
             catch (Exception)
             {

                 throw;
             }    
         }
         /// <summary>
         /// Method to create a new person
         /// </summary>
         /// <param name="p"></param>
         public void InsertPerson(person p)
         {
             try
             {
                 db.people.Add(p);
                 db.SaveChanges(); //commit change
             }
             catch (Exception)
             {


                 throw;
             }
         }

         /// <summary>
         /// Method to update a person item in DB
         /// </summary>
         /// <param name="p"></param>
         public void UpdatePerson(person p)
         {
             try
             {
                 db.Entry(p).State = EntityState.Modified;
                 db.SaveChanges();
             }
             catch (Exception)
             {

                 throw;
             }
         }


         /// <summary>
         /// Method to delete an item
         /// </summary>
         /// <param name="p"></param>
         public void DeletePerson(person p)
         {
             try
             {
                 db.people.Remove(p);
                 db.SaveChanges();
             }
             catch (Exception)
             {

                 throw;
             }
         }



         #endregion

     */



    }
}