﻿using Booking_System.Controllers;
using Booking_System.Helpers;
using Booking_System.Models;
using Booking_System.Models.ViewModel;
using Booking_System.Services.Abstract;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Services
{
    public class ReservationService : IReservationService
    {
        private ABSDBEntities db = new ABSDBEntities();
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        public bool SendBookingMail(int bookingid, AppEnums.BookingMailType EmailType,byte[] _attachment = null)
        {
            try
            {
                var _recipient = ConfigurationHelper.BookingAdminEmail();
                
                var _booking = db.bookings.Find(bookingid);
                var _agent = db.users.Find(_booking.agent_user_id);
                var _adminCC = "";
                var _subject = string.Empty;
                var _notes = db.notes.Where(x => x.item_type == "RESERVATIONS" && x.item_id == bookingid).OrderByDescending(o=>o.create_date).ToList();
                var last_note = string.Empty;
                if(_notes.Count>0)
                {
                    last_note = _notes.FirstOrDefault().detail;
                }


                System.Text.StringBuilder _body = new System.Text.StringBuilder();
                EmailHelper emailhelpr = new EmailHelper();
                string _filename = string.Empty;
                switch (EmailType)
                {
                    case AppEnums.BookingMailType.NEW_TIMELIMIT_ADMIN:
                        _subject = String.Format("Booking ID #{0}: Time Limit", _booking.booking_id);
                        _body = _body.Append("Dear Sales/Admin <br/><br/>New <i>Time Limit</i> Booking generated on System. Please update the time limit.");
                        break;
                    case AppEnums.BookingMailType.NEW_CONFIRMED_ADMIN:
                        _subject = String.Format("Booking ID #{0}: Confirmed Booking Request", _booking.booking_id);
                        _body = _body.Append("Dear Sales/Admin <br/><br/>New <i>Confirmed</i> Booking request generated on the System.");
                        break;
                    case AppEnums.BookingMailType.AWAITING_CONFIRMATION_AGENT:
                        _recipient = _agent.email;
                        _subject = String.Format("Booking ID #{0}: Time Limit {1}", _booking.booking_id, _booking.timelimit_date.Value.ToString("dd/MMM/yyyy") );
                        _body = _body.AppendFormat("Dear Agent {0} <br/><br/><i>Time Limit</i>: {1} has been updated successfully. Please confirm the booking before due date.", _booking.agent_name, _booking.timelimit_date.Value.ToString("dd/MMM/yyyy"));
                        _adminCC = ConfigurationHelper.BookingAdminEmail();
                        break;
                    case AppEnums.BookingMailType.CLIENT_CONFIRMED_ADMIN:
                        _subject = String.Format("Booking ID #{0}: Confirmation", _booking.booking_id);
                        _body = _body.Append("Dear Sales/Admin <br/><br/>Agent has confirmed the booking.");
                        break;
                    case AppEnums.BookingMailType.DISCOUNT_AGENT:
                        _recipient = _agent.email;
                        _subject = String.Format("Booking ID #{0}: Discount", _booking.booking_id);
                        _body = _body.AppendFormat("Dear Agent {0} <br/><br/>Booking is updated with discount.", _booking.agent_name);

                        break;
                    case AppEnums.BookingMailType.DISCUSSION_AGENT:
                        _recipient = _agent.email;
                        _subject = String.Format("Booking ID #{0}: Note from Admin", _booking.booking_id);
                        _body = _body.AppendFormat("Dear Agent {0} <br/><br/>Booking note added by admin:<br/>{1}.", _booking.agent_name,last_note);


                        break;
                    case AppEnums.BookingMailType.DISCUSSION_ADMIN:
                        _subject = String.Format("Booking ID #{0}: Note from Agent", _booking.booking_id);
                        _body = _body.AppendFormat("Dear Sales/Admin <br/><br/>Booking note added by Agent {0}:<br/>{1}", _booking.agent_name, last_note);


                        break;

                    case AppEnums.BookingMailType.EXTENSION_REQUEST_ADMIN:
                        _subject = String.Format("Booking ID #{0}: Extension Requested for Time Limit", _booking.booking_id);
                        _body = _body.AppendFormat("Dear Sales/Admin <br/><br/>Extension requested by Agent {0}:<br/>", _booking.agent_name);

                        break;
                    case AppEnums.BookingMailType.CANCEL_REQUEST_ADMIN:
                        _subject = String.Format("Booking ID #{0}: Cancellation Requested for Time Limit", _booking.booking_id);
                        _body = _body.AppendFormat("Dear Sales/Admin <br/><br/>Cancellation requested by Agent {0}:<br/>", _booking.agent_name);
                        break;
                    case AppEnums.BookingMailType.PROFORMA_GENERATED_AGENT:
                        _recipient = _agent.email;
                        _adminCC = ConfigurationHelper.BookingAdminEmail();
                        _subject = String.Format("Booking ID #{0}: ProForma Invoice Generated", _booking.booking_id);
                        _body = _body.AppendFormat("Dear Agent {0} <br/><br/>ProForma Invoice Generated:<br/> Please make your payment to confirm the booking", _booking.agent_name);
                        _filename = String.Format("proforma_{0}", _booking.booking_id);
                        break;
                    case AppEnums.BookingMailType.VOUCHER_GENERATED_AGENT:
                        _recipient = _agent.email;
                        _adminCC = ConfigurationHelper.BookingAdminEmail();
                        _subject = String.Format("Booking ID #{0}: Voucher Generated", _booking.booking_id);
                        _body = _body.AppendFormat("Dear Agent {0} <br/><br/>Voucher Generated:<br/>", _booking.agent_name);
                        _filename = String.Format("voucher_{0}", _booking.booking_id);
                        break;

                    case AppEnums.BookingMailType.BOOKING_UPDATED_ADMIN:
                        _subject = String.Format("Booking ID #{0}: Details Updated", _booking.booking_id);
                        _body = _body.AppendFormat("Dear Sales/Admin <br/><br/>Booking details updated by Agent {0}:<br/>", _booking.agent_name);
                        break;

                   
                    default:
                        break;
                }

                _body.AppendFormat(@"<br/> <h4>Booking Details</h4> 
                <ul>
                <li>Booking Id: {0} </li>
                <li>BFN: {1} </li>
                <li>booking Name: {2} </li>
                <li>Hotel Name: {8} </li>
                <li>Checkin Date: {3} </li>
                <li>Checkout Date: {4} </li>
                <li>No. of Nights: {5} </li>
                <li>No. of Rooms: {6} </li>
                <li>No. of Guests: {7} </li>
                  </ul>"
                , _booking.booking_id,_booking.booking_file_number,_booking.booking_name , 
                _booking.checkin_date.Value.ToString("dd MMM yyyy"), _booking.checkout_date.Value.ToString("dd MMM yyyy"),
                _booking.num_nights, _booking.rooms.Value,_booking._totalGuests,_booking.provider_name);

                _body.Append(@"<h3>Room Sharing Details:</h3>");
                foreach (var room in _booking.booking_room)
                {
                    _body.AppendFormat(@"Room Type: <b>{0}</b>", room.room_name);
                    
                    //foreach (var guest in room.booking_guest)
                    //{
                    //    _body.AppendFormat(@"<li>{0} {1} {2} {3} {4} y.o - Arrival:{5} {6} Departure:{7} {8}</li> ", guest.salutation, guest.firstname, guest.lastname, guest.gender, guest.age, guest.arrival_flight_no, guest.arrival_datetime,guest.departure_flight_no, guest.departure_datetime );
                    //}
                    //_body.Append(@"</ul>");

                    _body.Append(@"<table style='border-collapse:collapse;padding:2px;border:1px solid #cccccc'>");
                    _body.Append(@"<tr  style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>
                            <th style='padding:2px; border:1px solid #dddddd; border-collapse:collapse'>Title</th>
                            <th style='padding:2px; border:1px solid #dddddd; border-collapse:collapse'>Firstname</th>
                            <th style='padding:2px; border:1px solid #dddddd; border-collapse:collapse'>Lastname</th>
                            <th style='padding:2px; border:1px solid #dddddd; border-collapse:collapse'>Gender</th>
                            <th style='padding:2px; border:1px solid #dddddd; border-collapse:collapse'>Age</th>
                            <th style='padding:2px; border:1px solid #dddddd; border-collapse:collapse'>Arrival flight no.</th>
                            <th style='padding:2px; border:1px solid #dddddd; border-collapse:collapse'>Arrival Date/Time</th>
                            <th style='padding:2px; border:1px solid #dddddd; border-collapse:collapse'>Departure flight no.</th>
                            <th style='padding:2px; border:1px solid #dddddd; border-collapse:collapse'>Departure Date/Time</th>
                        </tr>");
                    foreach (var guest in room.booking_guest)
                    {
                        _body.AppendFormat(@"
                        <tr style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>
                            <td style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>{0}</td>
                            <td style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>{1}</td>
                            <td style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>{2}</td>
                            <td style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>{3}</td>
                            <td style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>{4}</td>
                            <td style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>{5}</td>
                            <td style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>{6}</td>
                            <td style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>{7}</td>
                            <td style='padding:2px; border:1px solid #dddddd ;border-collapse:collapse'>{8}</td>
                        </tr>
                        ", guest.salutation, guest.firstname, guest.lastname, guest.gender==1?"M":"F" ,guest.age, guest.arrival_flight_no, guest.arrival_datetime, guest.departure_flight_no, guest.departure_datetime);

                    }
                        _body.Append(@"</table><br/><br/>");
                    
                }
                
               


                _body.Append("<br/><br/>");
                _body.Append("<br/>Regards,<br/> TCI Notification Service");
                _body.Append("<br/>(<i>Automated Email</i>)");

                if (_attachment == null)
                    return emailhelpr.SendEmail(_recipient, _subject, _body.ToString(),_adminCC);
                else
                    return emailhelpr.SendEmail(_recipient, _subject, _body.ToString(), _filename, _attachment, MediaTypeNames.Application.Pdf,_adminCC);

            }
            catch (Exception e)
            {

                throw;
            }
        }

        


        public List<GetSupplementsResult> GetReservationSupplements(int id, booking booking)
        {
            //List<viewSupplement> suppsforhotel = db.viewSupplements.Where(p => p.provider_id == booking.provider_id).ToList();

            List<GetSupplementsResult> suppsforhotel = db.GetSupplements(booking.checkin_date, booking.checkout_date,
               false, booking.provider_id, booking.market_id, 0, null, booking.adults, booking.infants, booking.children, booking.teens, null).ToList<GetSupplementsResult>();


            List<GetSupplementsResult> supps = new List<GetSupplementsResult>();

            //List<bookingSupplement> bookedSupplements = db.bookingSupplements.Where(x => x.booking_id == booking.booking_id);

            List<bookingSupplement> bookingsupps = db.bookingSupplements.Where(x => x.booking_id == id).ToList();

            var provider_id = booking.provider_id;

            var rooms = db.bookingRooms.Where(x => x.booking_id == booking.booking_id).ToList();

            var room_num = 1;

            if (rooms.Count > 0)
            {
                foreach (var room in rooms)
                {
                    //room.meal_plan_id

                    if (suppsforhotel != null)
                    {

                        foreach (var suppA in suppsforhotel)
                        {

                            if (suppA.valid_all_rooms == true || suppA.room_id == room.room_id)
                            {

                                var supp = Utils.Clone<GetSupplementsResult>(suppA);


                                supp._adults = room.adults;
                                supp._teens = room.teens;
                                supp._children = room.children;
                                supp._infants = room.infants;
                                supp._room_number = room_num;
                                supp.booking_room_id = room.booking_room_id;
                                supp._OccupancyIcons = Utils.OccupancyIcons(room.adults, room.infants, room.children, room.teens);
                                supp._room_name = room.room_name;

                                double? price = 0;
                                if (supp.PricingType == AppEnums.PricingType.PER_ITEM)
                                {
                                    price = supp.price_per_item;
                                }
                                else if (supp.PricingType == AppEnums.PricingType.PER_NIGHT)
                                {
                                    price = supp.price_per_item * booking.num_nights;
                                }
                                else if (supp.PricingType == AppEnums.PricingType.PER_PERSON)
                                {
                                    price = Utils.price_per_pax(supp.price_adult, room.adults, supp.price_infant, room.infants, supp.price_child, room.children, supp.price_teen, room.teens);
                                }
                                else if (supp.PricingType == AppEnums.PricingType.PER_PERSON_PER_NIGHT)
                                {
                                    price = Utils.price_per_pax(supp.price_adult, room.adults, supp.price_infant, room.infants, supp.price_child, room.children, supp.price_teen, room.teens) * booking.num_nights;
                                }
                                else if (supp.PricingType == AppEnums.PricingType.PER_ROOM)
                                {
                                    price = supp.price_per_item;
                                }
                                else if (supp.PricingType == AppEnums.PricingType.PER_ROOM_PER_NIGHT)
                                {
                                    price = supp.price_per_item * booking.num_nights;
                                }

                                supp.supp_price = price;

                                if (bookingsupps.Exists(s => s.supplement_id == supp.supplement_id && s.booking_room_id == supp.booking_room_id))
                                {
                                    supp.IsSelected = true;
                                }
                                if (supp.IsMealPlan)
                                {

                                    if (supp.meal_plan_id > room.meal_plan_id)
                                    {
                                        supps.Add(supp);
                                    }
                                }
                                else
                                {
                                    supps.Add(supp);
                                }
                            }
                        }

                    }
                    room_num++;
                }
            }

            return supps;
        }

        public void SaveReservationSupplements(int booking_id, string[] mealplan, string[] supplement, bool[] chosen)
        {
            db.bookingSupplements.Where(b => b.booking_id == booking_id).Delete();

            var allSupplements = db.supplements.ToList();

            bool hasChanges = false;

            List<bookingSupplement> addedSupplements = new List<bookingSupplement>();

            int i = 0;
            if (mealplan != null)
            {


                foreach (var m in mealplan)
                {
                    //fisrt part is supp id,second part booking id, third part price
                    string[] mp = m.Split('#');
                    int suppId = Convert.ToInt32(mp[0]);
                    int roomId = Convert.ToInt32(mp[1]);
                    int _price = Convert.ToInt32(mp[2]);

                    if (suppId > 0)
                    {
                        string supName = string.Empty;
                        var sup = allSupplements.Where(e => e.supplement_id == suppId).FirstOrDefault();
                        if (sup != null)
                        {
                            supName = sup.name;

                            bookingSupplement bs = new bookingSupplement
                            {
                                booking_id = booking_id,
                                supplement_id = suppId,
                                price = _price,
                                booking_room_id = roomId,
                                supplement_name = supName,
                                markup_amt = _price * UserSession.Current.PercMarkup
                            };

                            addedSupplements.Add(bs);
                            db.bookingSupplements.Add(bs);
                            //            db.SaveChanges();
                            hasChanges = true;
                        }
                    }
                    i++;
                }
            }

            if (supplement != null)
            {
                var j = 0;
                foreach (var supp in supplement)
                {
                    string[] mp = supp.Split('#');
                    int suppId = Convert.ToInt32(mp[0]);
                    int roomId = Convert.ToInt32(mp[1]);
                    int _price = Convert.ToInt32(mp[2]);

                    if (Convert.ToBoolean(chosen[j]))
                    {
                        string supName = string.Empty;
                        var sup = allSupplements.Where(e => e.supplement_id == suppId).FirstOrDefault();
                        if (sup != null)
                        {
                            supName = sup.name;


                            bookingSupplement bs = new bookingSupplement
                            {
                                booking_id = booking_id,
                                supplement_id = suppId,
                                price = _price,
                                booking_room_id = roomId,
                                supplement_name = supName,
                                markup_amt = _price * UserSession.Current.PercMarkup
                            };
                            db.bookingSupplements.Add(bs);
                            //db.SaveChanges();

                            addedSupplements.Add(bs);
                            hasChanges = true;
                        }
                        //db.SaveChanges();
                    }
                    j++;
                }
            }

            if (hasChanges)
            {
                //foreach (var child in addedSupplements)
                //{
                //    db.bookingSupplements.Attach(child);
                //    db.Entry(child).State = EntityState.Added;

                //}


                db.SaveChanges();
            }
        }


        public List<viewActivity> GetReservationActivities(int id, booking booking)
        {
            var bookingActivity = db.bookingActivities.Where(x => x.booking_id == id).ToList();

            int currency_id = 0;
            market m = db.markets.Find(booking.market_id);
            if (m != null)
            {
                currency_id = m.currency_id;
            }


            var vActivity = db.viewActivities.Where(c => c.currency_id == currency_id).ToList();

            foreach (var ba in bookingActivity)
            {
                var activites = vActivity.Where(x => x.activity_id == ba.activity_id).ToList();
                if (activites.Count == 1)
                {
                    var act = activites.FirstOrDefault();
                    act._adult = ba.adults.HasValue ? ba.adults.Value : 0;
                    act._child = ba.children.HasValue ? ba.children.Value : 0;
                    act._after_sales_price_adult = ba.price_per_adult;
                    act._after_sales_price_child = ba.price_per_child;
                    act.transfer_type = ba.transfer_type.HasValue ? ba.transfer_type.Value : 0;

                    act._total_price = ba.total_price.HasValue ? ba.total_price.Value : 0;

                    act._isSelected = true;
                    if (!ba.is_aftersales)
                    {
                        act._SoldInFrontEnd = true;
                    }
                    else
                    {
                        act._SoldInFrontEnd = false;
                    }
                }
            }

            return vActivity;
        }



        public void SaveReservationActivities(int booking_id, int[] activity_id, int[] transfer_type, int[] child, int[] adult, int[] activity_pricing_id, bool isAfterSales, double[] afterSalesPriceAdult, double[] afterSalesPriceChild)
        {

            try
            {

                //get all existing activities
                var bookingActivities = db.bookingActivities.Where(x => x.booking_id == booking_id);

                int i = 0;
                bool hasChanges = false;

                if (activity_id != null && activity_id.Length > 0)
                {
                    //Loop thru all activities

                    foreach (var activity in activity_id)
                    {

                        int numOfAdults = 0;
                        int numOfChildren = 0;
                        int transferType = 0;

                        if (adult != null && adult.Length >= i)
                        {
                            numOfAdults = adult[i];
                        }

                        if (child != null && child.Length >= i)
                        {
                            numOfChildren = child[i];
                        }

                        if (transfer_type != null && transfer_type.Length >= i)
                        {
                            transferType = transfer_type[i];
                        }

                        double? priceAdult = 0;
                        double? priceChild = 0;

                        //In aftersales, there should be a value for the aftersales price.
                        if (isAfterSales)
                        {
                            if (afterSalesPriceAdult != null && afterSalesPriceAdult.Length >= i)
                            {
                                priceAdult = afterSalesPriceAdult[i];
                            }

                            if (afterSalesPriceChild != null && afterSalesPriceChild.Length >= i)
                            {
                                priceChild = afterSalesPriceChild[i];
                            }
                        }
                        else
                        {

                            //If in normal flow -> check the price from the activity pricing
                            if (activity_pricing_id != null && activity_pricing_id.Length >= i && (numOfAdults > 0 || numOfChildren > 0))
                            {
                                var api = activity_pricing_id[i];
                                var activity_pricing = db.activityPricings.Where(x => x.activity_pricing_id == api).ToList();
                                if (activity_pricing.Count() == 1)
                                {

                                    priceAdult = transfer_type[i] == 0 ? activity_pricing[0].price_adult :
                                    (transfer_type[i] == 1 ? activity_pricing[0].price_adult_incl_sic_transfer : activity_pricing[0].price_adult_incl_private_transfer);

                                    priceChild = transfer_type[i] == 0 ? activity_pricing[0].price_child :
                                     (transfer_type[i] == 1 ? activity_pricing[0].price_child_incl_sic_transfer : activity_pricing[0].price_child_incl_private_transfer);

                                }
                            }
                        }

                        double? total_price = (numOfAdults * priceAdult) + (numOfChildren * priceChild);

                        var act = bookingActivities.Where(e => e.activity_id == activity);
                        //if the activity is found
                        if (act != null && act.Count() > 0)
                        {
                            var chosenActivity = act.First();

                            int existingSelectedAdults = chosenActivity.adults.HasValue ? chosenActivity.adults.Value : 0;
                            int existingSelectedChildren = chosenActivity.children.HasValue ? chosenActivity.children.Value : 0;

                            int? existingTransferPlan = chosenActivity.transfer_type;

                            double? existingAdultPrice = chosenActivity.price_per_adult;
                            double? existingChildrenPrice = chosenActivity.price_per_child;

                            //if number of adults / child or transfer type has changed, therefore the booking should be updated.
                            //else no update.
                            //if prices are different and aftersales - update the values.
                            if ((existingSelectedAdults != numOfAdults
                                || existingSelectedChildren != numOfChildren
                                || existingTransferPlan != transferType)
                                || (isAfterSales && (existingAdultPrice != priceAdult
                                || existingChildrenPrice != priceChild)))
                            {
                                //if the number has become to 0, delete the activity
                                if (numOfAdults == 0 && numOfChildren == 0)
                                {
                                    db.Entry(chosenActivity).State = EntityState.Deleted;
                                    hasChanges = true;
                                }
                                else
                                {
                                    chosenActivity.adults = numOfAdults;
                                    chosenActivity.children = numOfChildren;
                                    chosenActivity.transfer_type = transferType;
                                    if (numOfAdults > 0)
                                    {
                                        chosenActivity.price_per_adult = priceAdult;
                                    }
                                    else
                                    {
                                        chosenActivity.price_per_adult = null;
                                    }

                                    if (numOfChildren > 0)
                                    {
                                        chosenActivity.price_per_child = priceChild;
                                    }
                                    else
                                    {
                                        chosenActivity.price_per_child = null;
                                    }

                                    //chosenActivity.markup_amt = total_price * UserSession.Current.PercMarkup;
                                    chosenActivity.total_price = total_price;

                                    if (isAfterSales)
                                    {
                                        chosenActivity.is_aftersales = true;
                                        chosenActivity.aftersales_admin = Helpers.UserSession.Current.UserId;
                                    }

                                    db.Entry(chosenActivity).State = EntityState.Modified;
                                    hasChanges = true;

                                }
                            }
                        }
                        else
                        {
                            //add the booking.
                            if (numOfAdults > 0 || numOfChildren > 0)
                            {
                                bookingActivity ba = new bookingActivity();

                                var _activity = db.activities.Find(activity);

                                ba.booking_id = booking_id;
                                ba.activity_id = activity;
                                ba.activity_name = _activity.activity_name;
                                ba.adults = numOfAdults;
                                ba.children = numOfChildren;
                                ba.transfer_type = transferType;
                                if (numOfAdults > 0)
                                {
                                    ba.price_per_adult = priceAdult;
                                }

                                if (numOfChildren > 0)
                                {
                                    ba.price_per_child = priceChild;
                                }

                                ba.markup_amt = total_price * UserSession.Current.PercMarkup;
                                ba.total_price = total_price;

                                if (isAfterSales)
                                {
                                    ba.is_aftersales = true;
                                    ba.aftersales_admin = Helpers.UserSession.Current.UserId;
                                }
                                db.bookingActivities.Add(ba);
                                hasChanges = true;
                            }
                        }

                        i++;
                    }
                }

                if (hasChanges)
                {
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }


        public IList<vehicle> GetReservationRentals(int id, booking booking)
        {
            var existingRentals = db.bookingRentals.Where(x => x.booking_id == id).ToList();

            int currency_id = 0;
            market m = db.markets.Find(booking.market_id);
            if (m != null)
            {
                currency_id = m.currency_id;
            }


            var vehiclepricings = db.vehiclePricings.Where(x => x.currency_id == currency_id);

            IList<vehicle> vehiclesToDisplay = new List<vehicle>();

            foreach (var p in vehiclepricings)
            {
                var vehicle = db.vehicles.Find(p.vehicle_id);
                if (existingRentals.Exists(s => s.vehicle_id == vehicle.vehicle_id))
                {
                    var rental = existingRentals.Where(r => r.vehicle_id == vehicle.vehicle_id).FirstOrDefault();
                    vehicle._isSelected = true;
                    vehicle._rentalDays = rental.num_days;
                    vehicle.includes_driver = rental.has_driver;
                }
                vehiclesToDisplay.Add(vehicle);

            }

            return vehiclesToDisplay;
        }

        public void SaveReservationRentals(int booking_id, double[] price, double[] pricewithdriver, int[] days, int[] vehicle_id, bool isAfterSales, bool[] hasDriver)
        {
            var existingRentals = db.bookingRentals.Where(x => x.booking_id == booking_id).ToList();
            var j = 0;

            foreach (var vehicle in vehicle_id)
            {
                //If there are rentals for the vehicle
                if (existingRentals.Exists(r => r.vehicle_id == vehicle))
                {
                    //If number of days is 0, delete the booking.
                    if (days[j] == 0)
                    {
                        db.bookingRentals.Where(x => x.booking_id == booking_id && x.vehicle_id == vehicle).Delete();
                    }
                    else
                    {
                        //Else update it.
                        var rental = db.bookingRentals.Where(x => x.booking_id == booking_id && x.vehicle_id == vehicle);
                        if (rental.Count() > 0)
                        {
                            var rnt = rental.FirstOrDefault();

                            if (hasDriver[j])
                            {
                                rnt.total_price = pricewithdriver[j];
                                rnt.has_driver = true;
                            }
                            rnt.num_days = days[j];
                            if (isAfterSales)
                            {
                                rnt.is_aftersales = true;
                                rnt.aftersales_admin = Helpers.UserSession.Current.UserId;
                            }

                            db.Entry(rnt).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                }
                else
                {
                    //If there are no rental and the number of days > 0, create a rental.
                    if (days[j] > 0)
                    {

                        bookingRental rental = new bookingRental();
                        rental.booking_id = booking_id;
                        rental.num_days = days[j];
                        rental.vehicle_id = vehicle_id[j];
                        if (hasDriver[j])
                        {
                            rental.total_price = pricewithdriver[j];
                            rental.has_driver = true;
                        }
                        else
                        {
                            rental.total_price = price[j];

                        }
                        //rental.markup_amt = price[j] * Helpers.UserSession.Current.PercMarkup;

                        if (isAfterSales)
                        {
                            rental.is_aftersales = true;
                            rental.aftersales_admin = Helpers.UserSession.Current.UserId;
                        }

                        db.bookingRentals.Add(rental);
                        db.SaveChanges();

                    }
                }
                j++;
            }
        }



        public IList<transferPricing> GetReservationTransfers(int id, booking booking)
        {
            var bookingTransfers = db.bookingTransfers.Where(x => x.booking_id == id).ToList();

            // var allTransfers = db.transfers.Where(x => x.is_active);
            int currency_id = 0;
            market m = db.markets.Find(booking.market_id);
            if (m != null)
            {
                currency_id = m.currency_id;
            }

            var transferpricings = db.transferPricings.Include(x => x.transfer).Where(x => x.currency_id == currency_id && x.is_active && x.is_selling_price);

            foreach (var p in transferpricings)
            {
                if (bookingTransfers.Any(b => b.transfer_pricing_id == p.transfer_pricing_id))
                {
                    p._isSelected = true;
                    var bTransfer = bookingTransfers.Where(x => x.transfer_id == p.transfer_id).FirstOrDefault();
                    p._isAfterSales = bTransfer.is_aftersales;
                    p._totalPrice = bTransfer.total_price;
                    //p._isAfterSales = bookingTransfers.Where(x => x.transfer_pricing_id == p.transfer_pricing_id).ToList();
                }

            }
            //bookingTransfers.Any(b=>b.transfer Tr p.transfer_pricing_id == )
            //List1.Any(l1 => List2.Any(l2 => l1.Key == l2.Key && l1.Value == l2.Value));

            //if ()

            //    var bTransfer = bookingTransfers.Where(x => x.transfer_id == p.transfer_id).FirstOrDefault();
            //    if (bTransfer != null)
            //    {
            //        p._isSelected = true;
            //        if(bTransfer.is_aftersales)
            //        {
            //            p._isAfterSales = true;
            //        }
            //    }
            //}

            //return allTransfers.ToList();
            return transferpricings.ToList();
        }



        public bool SaveReservationTransfers(int booking_id, int? transfer_id, double? totalPriceTransfer, double? totalPriceCoach, bool isAfterSales)
        {
            try
            {
                bool result = false;

                //Search the booking
                var booking = db.bookings.Find(booking_id);
                refurbishbooking(booking);

                if (booking == null)
                {
                    return false;
                }


                if (isAfterSales)
                {
                    //if aftersales , delete all transfers that are aftersales only.
                    db.bookingTransfers.Where(x => (x.booking_id == booking_id && x.is_aftersales) && x.transfer_id != transfer_id).Delete();
                }
                else
                {
                    //delete transfer bookings which are not related to those passed in params.
                    db.bookingTransfers.Where(x => x.booking_id == booking_id && x.transfer_id != transfer_id).Delete();
                }

                bookingTransfer bookingTransfer = null;
                int? transferpricingid = null;
                if (transfer_id.HasValue && transfer_id.Value > 0)
                {
                    //check if there is an existing booking.
                    var existingBooking = db.bookingTransfers.Where(x => x.booking_id == booking_id && x.transfer_id == transfer_id).FirstOrDefault();
                    //var selectedtransfer = db.transfers.Find(transfer);

                    double? totalprice = null;

                    int currency_id = 0;
                    market m = db.markets.Find(booking.market_id);
                    if (m != null)
                    {
                        currency_id = m.currency_id;
                    }


                    var pricing = db.transferPricings.Where(x => x.transfer_id == transfer_id && x.currency_id == currency_id && x.is_selling_price).FirstOrDefault();
                    totalprice = (pricing.adult_price * booking.adults) + (pricing.teen_price * booking.teens) + (pricing.child_price * booking.children);
                    transferpricingid = pricing.transfer_pricing_id;

                    if (totalPriceTransfer.HasValue && isAfterSales)
                    {
                        totalprice = totalPriceTransfer.Value;
                    }

                    if (existingBooking == null)
                    {
                        bookingTransfer = new bookingTransfer();

                        bookingTransfer.booking_id = booking_id;
                        bookingTransfer.transfer_id = transfer_id.Value;
                        bookingTransfer.children = booking.children;

                        bookingTransfer.transfer_pricing_id = transferpricingid;
                        bookingTransfer.adults = booking.adults;
                        bookingTransfer.total_price = totalprice;
                        bookingTransfer.transfer_name = pricing.transfer.name;
                        if (isAfterSales)
                        {
                            bookingTransfer.is_aftersales = true;
                            bookingTransfer.aftersales_admin = Helpers.UserSession.Current.UserId;
                        }

                        db.bookingTransfers.Add(bookingTransfer);
                    }
                    else
                    {
                        existingBooking.children = booking.children;
                        existingBooking.teens = booking.teens;
                        existingBooking.adults = booking.adults;
                        if (isAfterSales)
                        {
                            existingBooking.total_price = totalprice;
                            existingBooking.is_aftersales = true;
                            existingBooking.aftersales_admin = Helpers.UserSession.Current.UserId;
                        }

                        db.Entry(existingBooking).State = EntityState.Modified;
                    }

                    result = db.SaveChanges() > 0;
                }





                return result;
            }
            catch (Exception e)
            {
                throw;
            }


        }


        private void refurbishbooking(booking booking)
        {
            try
            {


                var m = db.markets.Find(booking.market_id);
                var p = db.providers.Find(booking.provider_id);
                var pa = db.packages.Find(booking.package_id);

                if (m != null)
                {
                    booking.market = m;

                }

                if (p != null)
                {

                    booking.provider = p;

                }

                if (pa != null)
                {
                    booking.package = pa;

                }


            }
            catch (Exception e)
            {

                throw;
            }


        }

        public bool SaveGuestParam(BookingGuestViewModel guestparam, bool UseAgentCredit, bool isUpdate, bool updatebyadmin = false)
        {

            try
            {
                bool result = false;
                var booking = db.bookings.Find(guestparam.booking_id);
                if (booking == null)
                {
                    return false;
                }

                refurbishbooking(booking);

                for (var i = 0; i < guestparam.num_guests; i++)
                {

                    var guest = db.bookingGuests.Find(guestparam.guest_id[i]);
                    DateTime flightDate = new DateTime();
                    if (!String.IsNullOrWhiteSpace(guestparam.flight_date[i]))
                    {
                        DateTime.TryParse(guestparam.flight_date[i], out flightDate);
                    }

                    var ts = guestparam.flight_time[i].Replace(':', ' ').Split(' ');
                    int hour = 0;
                    int min = 0;
                    if (ts.Length == 3)
                    {
                        if (ts[2] == "PM")
                        {
                            hour = Convert.ToInt32(ts[0]) + 12;
                        }
                        else
                        {
                            hour = Convert.ToInt32(ts[0]) == 12 ? 0 : Convert.ToInt32(ts[0]);
                        }

                        min = Convert.ToInt32(ts[1]);
                    } else if (ts.Length == 2)
                    {                        
                        hour = Convert.ToInt32(ts[0]);                        
                        min = Convert.ToInt32(ts[1]);
                    }

                    TimeSpan time = new TimeSpan(hour, min, 0);
                    DateTime ArrivalDate = flightDate + time;

                    // departure flight
                    DateTime dflightDate = new DateTime();
                    if (!String.IsNullOrWhiteSpace(guestparam.depflight_date[i]))
                    {
                        DateTime.TryParse(guestparam.depflight_date[i], out dflightDate);
                    }

                    var dts = guestparam.depflight_time[i].Replace(':', ' ').Split(' ');
                    int dhour = 0;
                    int dmin = 0;
                    if (dts.Length == 3)
                    {
                        if (dts[2] == "PM")
                        {
                           dhour = Convert.ToInt32(dts[0]) + 12;
                        }
                        else
                        {
                            dhour = Convert.ToInt32(dts[0]) == 12 ? 0 : Convert.ToInt32(dts[0]);
                        }

                        dmin = Convert.ToInt32(dts[1]);
                    }
                    else if (dts.Length == 2)
                    {
                        dhour = Convert.ToInt32(dts[0]);
                        dmin = Convert.ToInt32(dts[1]);
                    }

                    TimeSpan dtime = new TimeSpan(dhour, dmin, 0);
                    DateTime DepartureDate = dflightDate + dtime;


                    if (guest != null)
                    {

                        guest.firstname = guestparam.firstname[i];
                        guest.lastname = guestparam.lastname[i];
                        if (guestparam.date_of_birth[i] > DateTime.MinValue)
                        {
                            guest.date_of_birth = guestparam.date_of_birth[i];

                        }
                        guest.salutation = guestparam.salutation[i];
                        guest.passport_number = guestparam.passport_number[i];
                        guest.nationality = guestparam.nationality[i];
                        guest.gender = guestparam.gender[i];
                        if (guestparam.passport_expiry_date[i] > DateTime.MinValue)
                        {
                            guest.passport_expiry_date = guestparam.passport_expiry_date[i];

                        }

                        guest.arrival_flight_no = guestparam.flight_number[i];
                        guest.departure_flight_no = guestparam.depflight_number[i];

                        if (ArrivalDate.Date > DateTime.MinValue)
                        {
                            guest.arrival_datetime = ArrivalDate;
                        }
                        if (DepartureDate.Date > DateTime.MinValue)
                        {
                            guest.departure_datetime = DepartureDate;
                        }

                        db.Entry(guest).State = EntityState.Modified;
                        //result = db.SaveChanges() > 0;
                    }


                }
                
                booking.booking_name = guestparam.customer_name;
                booking.phone_number = guestparam.customer_phone;
                booking.booking_file_number = guestparam.booking_file_number;
                booking.email = guestparam.customer_email;
                booking.booking_name = guestparam.customer_name;
                if (!isUpdate == true) 
                {
                    booking.is_timelimit = guestparam.is_timelimit;
                    booking.status = (int)AppEnums.BookingStatus.NEW;
                }

                
                booking.markup_amt = booking.TotalMarkup;
                booking.paid_amt = 0;
                booking.booking_amt = (booking.TotalCost - booking.TotalMarkup);
                booking.remarks = guestparam.remarks;

                //if (UseAgentCredit)
                //{
                //    //if the agent uses credit - set booking credit to 0
                //    if(Helpers.UserSession.Current.CreditLimit > 0)
                //    {
                //        booking.credit_amt = 0;
                //    }
                //}

                db.Entry(booking).State = EntityState.Modified;

                //if (UseAgentCredit)
                //{
                //    var agent = db.users.Find(Helpers.UserSession.Current.UserId);
                //    if (agent != null)
                //    {
                //        var creditRemaining = agent.credit_remaining.HasValue ? agent.credit_remaining.Value : 0;

                //        if (creditRemaining > 0)
                //        {
                //            booking.credit_amt = creditRemaining >= booking.booking_amt ? booking.booking_amt : creditRemaining;

                //            //decrease the credit limit of the agent.
                //            agent.credit_used = (agent.credit_used.HasValue ? agent.credit_used : 0) + booking.credit_amt.Value;

                //            UserSession.Current.RemainingCredit = (double)agent.credit_remaining;

                //            db.Entry(agent).State = EntityState.Modified;
                //        }
                //    }
                //}


                result = db.SaveChanges() > 0;
                if (isUpdate) 
                {
                    if (!updatebyadmin) { 
                        SendBookingMail(booking.booking_id, AppEnums.BookingMailType.BOOKING_UPDATED_ADMIN);
                    }
                }
                else
                {
                    SendBookingMail(booking.booking_id, booking.is_timelimit ? AppEnums.BookingMailType.NEW_TIMELIMIT_ADMIN : AppEnums.BookingMailType.NEW_CONFIRMED_ADMIN);
                }
              

                return result;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    log.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);


                    foreach (var ve in eve.ValidationErrors)
                    {
                        log.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                        //Console.WriteLine
                    }
                }
                throw;
            }

            catch (Exception)
            {


                throw;
            }


        }

        public bool SaveReservation(int? booking_id, int? status, DateTime? timelimitdate = null,float CancellationFEE=0)
        {
            try
            {
                var booking = db.bookings.Find(booking_id);
                AppEnums.BookingStatus _status = (AppEnums.BookingStatus)status.Value;

                /// save the booking as is
                if (_status == AppEnums.BookingStatus.DRAFT
                    || _status == AppEnums.BookingStatus.NEW
                    || _status == AppEnums.BookingStatus.DECLINED
                    || _status == AppEnums.BookingStatus.REQUEST_FOR_EXTENSION
                    )
                {
                    booking.status = status;
                }

                if (_status == AppEnums.BookingStatus.CANCELED)
                {
                    //ResetBookingFlow(booking.booking_id);
                    booking.status = (int)AppEnums.BookingStatus.CANCELED;
                }

                // delete all existing doc and save the booking
                if (_status == AppEnums.BookingStatus.PROCESSING)
                {
                    ResetBookingFlow(booking.booking_id);
                    booking.status = (int)AppEnums.BookingStatus.PROCESSING;
                }

                if (_status == AppEnums.BookingStatus.PROCESSING_NEW_TIMELIMIT)
                {
                    //ResetBookingFlow(booking.booking_id);
                    booking.status = (int)AppEnums.BookingStatus.PROCESSING_NEW_TIMELIMIT;
                    booking.is_new_timelimit = true;
                }

                if (_status == AppEnums.BookingStatus.CANCELED_WITH_CHARGES)
                {
                    ResetBookingFlow(booking.booking_id);
                    booking.status = (int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES;
                    booking.cancellation_fee = CancellationFEE;
                }

                //status is equivalent to HOTEL_CONFIRMED
                if (_status == AppEnums.BookingStatus.AWAITING_CONFIRMATION)
                {

                    if (booking.is_timelimit)
                    {
                        if (timelimitdate.HasValue && timelimitdate.Value < DateTime.Now)
                        {
                            //booking.status = (int)AppEnums.BookingStatus.EXPIRED;
                        }
                        else
                        {
                            booking.status = (int)AppEnums.BookingStatus.AWAITING_CONFIRMATION;
                            booking.timelimit_date = timelimitdate.Value;
                        }

                    }
                    else
                    {
                        //TO CHECK
                        if (booking.is_credit)
                        {
                            booking.status = (int)AppEnums.BookingStatus.COMPLETED;
                        }
                        else
                        {
                            booking.status = (int)AppEnums.BookingStatus.AWAITING_PAYMENT;
                        }
                    }
                }

                if (_status == AppEnums.BookingStatus.CONFIRMED)
                {
                    if (booking.is_credit)
                    {
                        UpdateAgentCredit(booking.booking_id);
                        booking.status = (int)AppEnums.BookingStatus.CONFIRMED;
                    }
                    else
                    {
                        booking.status = (int)AppEnums.BookingStatus.CONFIRMED;
                        //SaveProformaInfo(booking.booking_id, booking.checkin_date, string.Empty);
                    }
                }
                
                if (_status == AppEnums.BookingStatus.COMPLETED)
                {
                    booking.status = (int)AppEnums.BookingStatus.COMPLETED;
                }

                if (_status == AppEnums.BookingStatus.AWAITING_PAYMENT)
                {
                    booking.status = (int)AppEnums.BookingStatus.AWAITING_PAYMENT;
                }

                if (_status == AppEnums.BookingStatus.CANCEL_REQUEST)
                {
                    booking.status = (int)AppEnums.BookingStatus.CANCEL_REQUEST;
                }

                db.Entry(booking).State = EntityState.Modified;
                db.SaveChanges();



                //sending email
                if(booking.StatusEnum == AppEnums.BookingStatus.NEW)
                {
                    if(booking.is_timelimit)
                    {
                        SendBookingMail(booking.booking_id, AppEnums.BookingMailType.NEW_TIMELIMIT_ADMIN);
                    } else
                    {
                        SendBookingMail(booking.booking_id, AppEnums.BookingMailType.NEW_CONFIRMED_ADMIN);
                    }
                }

                if(booking.StatusEnum == AppEnums.BookingStatus.AWAITING_CONFIRMATION)
                {
                    SendBookingMail(booking.booking_id, AppEnums.BookingMailType.AWAITING_CONFIRMATION_AGENT);
                }

                if (booking.StatusEnum == AppEnums.BookingStatus.CONFIRMED && Booking_System.Helpers.UserSession.Current.UserRole == UserRoles.Agent)
                {
                    SendBookingMail(booking.booking_id, AppEnums.BookingMailType.CLIENT_CONFIRMED_ADMIN);
                }

                if (booking.StatusEnum == AppEnums.BookingStatus.REQUEST_FOR_EXTENSION)
                {
                    SendBookingMail(booking.booking_id, AppEnums.BookingMailType.EXTENSION_REQUEST_ADMIN);
                }

                if (booking.StatusEnum == AppEnums.BookingStatus.CANCEL_REQUEST)
                {
                    SendBookingMail(booking.booking_id, AppEnums.BookingMailType.CANCEL_REQUEST_ADMIN);
                }


                return true;
            }
            catch (Exception e)
            {

                throw;
            }
        }



        /// <summary>
        /// removes any existing voucher / proforma for booking
        /// </summary>
        /// <param name="booking_id"></param>
        private void ResetBookingFlow(int booking_id)
        {
            db.bookingVouchers.Where(x => x.booking_id == booking_id).Delete();
            db.bookingProformas.Where(x => x.booking_id == booking_id).Delete();
            db.bookingPayments.Where(x => x.booking_id == booking_id).Delete();

            var booking = db.bookings.Find(booking_id);
            /// return credit if agent was debited
            if (booking.is_credit)
                UpdateAgentCredit(booking_id);

        }



        /// <summary>
        /// Method to generate voucher information and save in DB
        /// </summary>
        /// <param name="bookingid"></param>
        public bookingVoucher SaveVoucherInfo(int bookingid)
        {
            try
            {
                var vouchers = db.bookingVouchers.Where(x => x.booking_id == bookingid).ToList();
                var vc = new bookingVoucher();
                bool hasExistingVoucher = false;
                if (vouchers != null && vouchers.Count > 0)
                {
                    vc = vouchers.FirstOrDefault();
                    hasExistingVoucher = true;
                }

                var book = db.bookings.Find(bookingid);
                refurbishbooking(book);
                var market = db.markets.Where(x => x.market_id == book.market_id).Include(x => x.currency).FirstOrDefault();
                var users = db.users.ToList();
                var mealplans = db.mealPlans;
                var agent = users.Where(x => x.user_id == book.agent_user_id).First();
                var admin = users.Where(x => x.user_id == book.backoffice_user_id).FirstOrDefault();
                var bookrooms = db.bookingRooms.Where(x => x.booking_id == bookingid).ToList();
                var pckg = book.package_id.HasValue && book.package_id.Value != 0 ? db.packages.Find(book.package_id) : new package { name = "", description = "" };
                //group rooms and count
                var countrooms =
                  from br in bookrooms
                  group br by br.room_name into groupedrooms

                  select new
                  {
                      room_name = groupedrooms.Key,
                      roomcount = groupedrooms.Count()
                  };

                var roomtypes = "";

                foreach (var rt in countrooms.ToList())
                {
                    if (roomtypes == "")
                    {
                        roomtypes = String.Concat(bookrooms.Count().ToString(), (bookrooms.Count() < 1) ? " Room - " : " Rooms - ", rt.roomcount.ToString(), " ", rt.room_name);
                    }
                    else
                    {
                        roomtypes = String.Concat(roomtypes, " and ", rt.roomcount.ToString(), " ", rt.room_name);
                    }

                }

                //1 inf, 2chi, 3tee, 4adu, 5senio
                int infants = book.booking_guest.Count(x => x.guest_type == 1);
                int children = book.booking_guest.Count(x => x.guest_type == 2);
                int teens = book.booking_guest.Count(x => x.guest_type == 3);
                int adults = book.booking_guest.Count(x => x.guest_type == 4);
                int seniors = book.booking_guest.Count(x => x.guest_type == 5);

                StringBuilder oSB = new StringBuilder();
                if(adults>0)
                {
                    oSB.AppendFormat("{0} adult{1},", adults, adults>1?"s":"");
                }
                if (infants > 0)
                {
                    oSB.AppendFormat("{0} infant{1},", infants, infants> 1 ? "s" : "");
                }
                if (children > 0)
                {
                    oSB.AppendFormat("{0} child{1},", children, children> 1 ? "ren" : "");
                }
                if (teens > 0)
                {
                    oSB.AppendFormat("{0} teen{1},", teens > 1 ? "s" : "");
                }
                if (seniors > 0)
                {
                    oSB.AppendFormat("{0} senior{1},", seniors > 1 ? "s" : "");
                }

                if (oSB.Length > 1)
                    oSB.Length--;

                vc.occupation_summary = oSB.ToString();


                var services = "";

                if (book.booking_supplement.Count > 0)
                {
                    services = "Supplements";
                    services = string.Concat(services, "<br/>", "<ul>");
                    foreach (var item in book.booking_supplement)
                    {
                        services = string.Concat(services, "<li>", item.supplement_name, "</li>");
                    }
                    services = string.Concat(services, "</ul>");
                }

                if (book.booking_transfer.Count > 0)
                {
                    services = string.Concat(services, "Transfers");
                    services = string.Concat(services, "<br/>", "<ul>");
                    foreach (var item in book.booking_transfer)
                    {
                        services = string.Concat(services, "<li>", item.transfer_name, "</li>");
                    }
                    services = string.Concat(services, "</ul>");
                }

                if (book.booking_activity.Count > 0)
                {
                    services = string.Concat(services, "Activities");
                    services = string.Concat(services, "<br/>", "<ul>");
                    foreach (var item in book.booking_activity)
                    {
                        services = string.Concat(services, "<li>", item.activity_name, "</li>");
                    }
                    services = string.Concat(services, "</ul>");

                }

                if (book.booking_rental.Count > 0)
                {
                    services = string.Concat(services, "Rental");
                    services = string.Concat(services, "<br/>", "<ul>");
                    foreach (var item in book.booking_rental)
                    {
                        services = string.Concat(services, "<li>", item.vehicle_model, "</li>");
                    }
                    services = string.Concat(services, "</ul>");

                }

                var contacts = "";
                var contactstring = "<span>{0}</span><br/>";
                if (book.provider.IsHotel)
                {

                    contacts = book.provider.street1 + "<br/>";
                    if (!String.IsNullOrEmpty(book.provider.street2))
                    {
                        contacts = string.Concat(contacts, String.Format(contactstring, book.provider.street2));
                    }
                    if (!String.IsNullOrEmpty(book.provider.town))
                    {
                        contacts = string.Concat(contacts, String.Format(contactstring, book.provider.town));
                    }
                    if (!String.IsNullOrEmpty(book.provider.phone1))
                    {
                        contacts = string.Concat(contacts, "phone: ", String.Format(contactstring, book.provider.phone1));
                    }
                    if (!String.IsNullOrEmpty(book.provider.fax))
                    {
                        contacts = string.Concat(contacts, "fax: ", String.Format(contactstring, book.provider.fax));
                    }
                }


                vc.booking_id = book.booking_id;
                vc.booking_file_number = book.booking_file_number;
                vc.check_in = book.checkin_date.Value.ToString("dd-MM-yyyy");
                vc.check_out = book.checkout_date.Value.ToString("dd-MM-yyyy");
                vc.guest_names = book.booking_name;
                vc.hotel_name = book.provider_name;
                vc.num_guest = book.booking_guest.Count();
                vc.num_nights = book.num_nights;
                vc.remarks = book.remarks;
                vc.room_types = roomtypes;
                vc.flight_details = "";
                vc.package_name = pckg.name;
                vc.package_description = pckg.description;
                vc.services_details = services;
                vc.hotel_address = contacts;
                vc.cancellation_policy = book.provider.cancellation_policy;
                
                if (hasExistingVoucher)
                {
                    db.Entry(vc).State = EntityState.Modified;
                }
                else
                {
                    db.bookingVouchers.Add(vc);
                }
                //db.bookingVouchers.Add(vc);
                db.SaveChanges();

                return vc;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// Method to generate Proforma information and save in DB
        /// </summary>
        /// <param name="bookingid"></param>
        public bookingProforma SaveProformaInfo(int bookingid, DateTime? expiryDate, string comments, double proformaHandlingFee, double proformaIBFee)
        {
            try
            {
                //build the proforma model 
                // note:
                // For all fields containing multiple items e.g Passenger Details, each field will be separated by | 
                bool hasExistingProforma = false;
                bookingProforma bp = null;
                var existingProforma = db.bookingProformas.Where(x => x.booking_id == bookingid);


                if (existingProforma != null && existingProforma.Count() > 0)
                {
                    hasExistingProforma = true;
                    bp = existingProforma.FirstOrDefault();
                }
                else
                {
                    bp = new bookingProforma();
                }

                //get all required data to build proforma
                var book = db.bookings.Find(bookingid);
                var market = db.markets.Where(x => x.market_id == book.market_id).Include(x => x.currency).FirstOrDefault();
                var users = db.users.ToList();
                var mealplans = db.mealPlans;
                var agent = users.Where(x => x.user_id == book.agent_user_id).FirstOrDefault();
                var admin = users.Where(x => x.user_id == book.backoffice_user_id).FirstOrDefault();
                var currentuser = Booking_System.Helpers.UserSession.Current.Fullname;
                var pckg = book.package_id.HasValue && book.package_id.Value != 0 ? db.packages.Find(book.package_id) : new package { name = "", description = "" };
                //get a list of all rooms with all bookings 
                var bookrooms = from Item1 in book.booking_room   //get info on booking 
                                join Item2 in mealplans
                               on Item1.meal_plan_id equals Item2.meal_plan_id // 
                                select new
                                {
                                    Item1.meal_plan_id,
                                    Item1.room_name,
                                    Item1.room_number,
                                    meal_plan_name = Item2.name,
                                    meal_plan_desc = Item2.description
                                };



                //initialise prices and descriptions
                string Salesdesc = "";
                string Salescateg = "";
                bp.nett_price_accomodation = 0;
                bp.nett_price_activity = 0;
                bp.nett_price_rental = 0;
                bp.nett_price_supplement = 0;
                bp.nett_price_transfer = 0;


                //SECTION TO BUILD THE SALES CATEGORY AND DESC

                //Sales desc for Hotel
                var distinctmealplans = bookrooms.Select(x => new mealPlan { name = x.meal_plan_name, description = x.meal_plan_desc }).Distinct(); 
                
                // Hotelname
                // room1 
                // room2

                string package = "";
                if (book.isPackage) { package = pckg.name + "<br/>"; }
                string hoteldesc = book.provider_name+"<br/>";
                string room="";
                foreach(var x in book.booking_room)
                {
                    room = String.Concat(room, String.Format("{0} - {1}<br/>", x.room_name, mealplans.Where(y => y.meal_plan_id == x.meal_plan_id).FirstOrDefault().name));
                }
                
                //foreach (var mp in distinctmealplans)
                //{
                //    if (Salesdesc == "")
                //    {
                //        Salesdesc = String.Concat(mp.name, " (", mp.description, ")");
                //    }
                //    else
                //    {
                //        Salesdesc = String.Concat(Salesdesc, "|", mp.name, " (", mp.description, ")");
                //    }
                //}
                Salesdesc = String.Concat(package,hoteldesc, room);
                if (!String.IsNullOrEmpty(Salesdesc))
                {
                    bp.nett_price_accomodation = book.TotalCostRooms;
                    if(book.isPackage){
                    Salescateg = "Accomodation and Ground handling";
                    }
                    else
                    {
                        Salescateg = "Accomodation";
                    }
                }

                //Sales desc for supplements
                if (book.booking_supplement.Count() > 0)
                {
                    if (!String.IsNullOrEmpty(Salescateg))
                    {
                        Salescateg = String.Concat(Salescateg, "~~", "Supplements"); //use a delimiter of ~~ to separate different sales category
                        Salesdesc = String.Concat(Salesdesc, "~~"); //use a delimiter of ~~ to separate different sales category}
                    }
                    else
                    {
                        Salescateg = String.Concat(Salescateg, "Supplements"); //use a delimiter of ~~ to separate different sales category
                    }
                    bp.nett_price_supplement = book.TotalCostSupps;
                }
                foreach (var sp in book.booking_supplement)
                {
                    if (Salesdesc == "")
                    {
                        Salesdesc = String.Concat(sp.supplement_name, " (", sp.quantity.HasValue ? sp.quantity.Value.ToString() : "", ")");
                    }
                    else
                    {
                        Salesdesc = String.Concat(Salesdesc, "|", sp.supplement_name, " (", sp.quantity.HasValue ? sp.quantity.Value.ToString() : "", ")");
                    }
                }

                //Sales desc for activities
                if (book.booking_activity.Count() > 0)
                {

                    if (!String.IsNullOrEmpty(Salescateg))
                    {
                        Salescateg = String.Concat(Salescateg, "~~", "Activities"); //use a delimiter of ~~ to separate different sales category
                        Salesdesc = String.Concat(Salesdesc, "~~"); //use a delimiter of ~~ to separate different sales category
                    }
                    else
                    {
                        Salescateg = String.Concat(Salescateg, "Activities"); //use a delimiter of ~~ to separate different sales category
                    }
                    bp.nett_price_activity = book.TotalCostActivity;
                }
                foreach (var sa in book.booking_activity)
                {
                    if (Salesdesc == "")
                    {
                        Salesdesc = String.Concat(sa.activity_name);
                    }
                    else
                    {
                        Salesdesc = String.Concat(Salesdesc, "|", sa.activity_name);
                    }
                }
                //Sales desc for promotions
                if (book.booking_promotion.Count() > 0)
                {

                    if (!String.IsNullOrEmpty(Salescateg))
                    {
                        Salescateg = String.Concat(Salescateg, "~~", "Promotions"); //use a delimiter of ~~ to separate different sales category
                        Salesdesc = String.Concat(Salesdesc, "~~"); //use a delimiter of ~~ to separate different sales category
                    }
                    else
                    {
                        Salescateg = String.Concat(Salescateg, "Promotions"); //use a delimiter of ~~ to separate different sales category
                    }

                }
                foreach (var sa in book.booking_promotion)
                {
                    if (Salesdesc == "")
                    {
                        Salesdesc = String.Concat(sa.promotion_name);
                    }
                    else
                    {
                        Salesdesc = String.Concat(Salesdesc, "|", sa.promotion_name);
                    }
                }

                //Sales desc for transfer
                if (book.booking_transfer.Count() > 0)
                {
                    if (!String.IsNullOrEmpty(Salescateg))
                    {
                        Salescateg = String.Concat(Salescateg, "~~", "Transfer"); //use a delimiter of ~~ to separate different sales category
                        Salesdesc = String.Concat(Salesdesc, "~~"); //use a delimiter of ~~ to separate different sales category
                    }
                    else
                    {
                        Salescateg = String.Concat(Salescateg, "Transfer"); //use a delimiter of ~~ to separate different sales category
                    }

                    bp.nett_price_transfer = book.TotalCostTransfer;
                }
                foreach (var sa in book.booking_transfer)
                {
                    if (Salesdesc == "")
                    {
                        Salesdesc = String.Concat(sa.transfer_name);
                    }
                    else
                    {
                        Salesdesc = String.Concat(Salesdesc, "|", sa.transfer_name);
                    }
                }


                //Sales desc for Rental
                if (book.booking_rental.Count() > 0)
                {
                    if (!String.IsNullOrEmpty(Salescateg))
                    {
                        Salescateg = String.Concat(Salescateg, "~~", "Rental"); //use a delimiter of ~~ to separate different sales category
                        Salesdesc = String.Concat(Salesdesc, "~~"); //use a delimiter of ~~ to separate different sales category
                    }
                    else
                    {
                        Salescateg = String.Concat(Salescateg, "Rental"); //use a delimiter of ~~ to separate different sales category
                    }

                    bp.nett_price_rental = book.TotalCostRental;
                }
                foreach (var sa in book.booking_rental)
                {
                    if (Salesdesc == "")
                    {
                        Salesdesc = String.Concat(!String.IsNullOrEmpty(sa.vehicle_model) ? sa.vehicle_model : "Generic Vehicle", " from ", sa.provider_name);
                    }
                    else
                    {
                        Salesdesc = String.Concat(Salesdesc, "|", !String.IsNullOrEmpty(sa.vehicle_model) ? sa.vehicle_model : "Generic Vehicle", " from ", sa.provider_name);
                    }
                }


                string bookingRooms = "";
                foreach (var br in bookrooms)
                {
                    if (bookingRooms == "")
                    {
                        bookingRooms = String.Concat(br.room_number, " ", br.room_name);
                    }
                    else
                    {
                        bookingRooms = String.Concat(bookingRooms, "|", br.room_number, " ", br.room_name);
                    }
                }

                bp.booking_id = book.booking_id;
                if (market != null && market.currency != null)
                {
                    bp.currency = market.currency.currency_code;
                }

                bp.agent_details = String.Concat(agent.FullName, "|", agent.job_title, "|", agent.mobile);
                bp.reference = agent.username;
                bp.due_date = expiryDate.HasValue ? expiryDate.Value : book.booking_expiry_date;
                bp.checked_by = admin != null ? admin.FullName : "";
                bp.nett_price = book.TotalCost;
                bp.sales_category = Salescateg;
                bp.sales_description = Salesdesc;
                bp.handling_fees = proformaHandlingFee;
                bp.bankcharges = proformaIBFee;
                bp.remarks = agent.FullName;
                bp.booking_details = String.Concat(book.booking_name, "|", book.HotelName, "|", bookingRooms, "|", book.checkin_date.Value.ToString("dd MMM yyyy"), "|", book.checkout_date.Value.ToString("dd MMM yyyy"));
                bp.issued_by = currentuser;// admin != null ? admin.FullName : "";
                bp.approved_by = admin != null ? admin.FullName : "";
                bp.issue_date = DateTime.Now;

                if (hasExistingProforma)
                {
                    db.Entry(bp).State = EntityState.Modified;
                }
                else
                {
                    db.bookingProformas.Add(bp);
                }
                db.SaveChanges();

                return bp;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public void UpdateAgentCredit(int booking_id)
        {
            try
            {
                var user = db.users.Find(UserSession.Current.UserId);
                var booking = db.bookings.Find(booking_id);

                if (user.is_credit_based)
                {

                    if (booking.StatusEnum == AppEnums.BookingStatus.CONFIRMED)
                    {
                        booking.credit_amt = booking.TotalCost;
                        user.credit_used = Utils.CoalesceDouble(user.credit_used) + booking.TotalCost;
                        UserSession.Current.RemainingCredit = user.credit_remaining.Value;
                        db.Entry(booking).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    if (booking.StatusEnum != AppEnums.BookingStatus.CONFIRMED)
                    {
                        if (booking.credit_amt > 0)
                        {

                            var creditToDebit = booking.credit_amt;
                            booking.credit_amt = 0;
                            user.credit_used = Utils.CoalesceDouble(user.credit_used) - creditToDebit;
                            UserSession.Current.RemainingCredit = user.credit_remaining.Value;
                            db.Entry(booking).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                    }



                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();

                }

            }
            catch (Exception)
            {

                throw;
            }
        }


    }
}