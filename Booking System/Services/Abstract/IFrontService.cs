﻿using Booking_System.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking_System.Services.Abstract
{
    interface IFrontService
    {
        //RoomOccupancy GetOccupancy(int? adults, int? children, int[] ages, int? hotelId, int sequence);
      //  IList<RoomOccupancy> GetOccupancies(SearchDetails search);
        //IList<RoomOccupancy> GetOccupanciesForHotel(List<RoomOccupancy> occupancies, int hotelId);

        IList<SearchOccupancy> GetSearchOccupancies(SearchDetails search);

        RoomOccupancy GetRoomOccupancy(SearchOccupancy searchOccupancy, int hotelId);

        IList<RoomOccupancy> GetRoomOccupancies(List<SearchOccupancy> searchOccupanies, int hotelId);

    }
}
