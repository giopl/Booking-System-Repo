﻿using Booking_System.Helpers;
using Booking_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Booking_System.Services.Abstract
{
    interface IAdminService
    {
        LoginState VerifyUser(LoginState loginState);

        Models.user GetUser(LoginState loginState);

        bool ChangePassword(LoginState loginState);

        bool DeleteResourceFromDisk(picture img);

        bool ResetPassword(string userName);

        bool SaveUploadedFileToDisk(HttpPostedFileBase mainFile, fileUpload file);

        bool DeleteUploadedFileFromDisk(int fileId);
    }
}
