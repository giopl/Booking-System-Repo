﻿using Booking_System.Models;
using Booking_System.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Services.Abstract
{
    interface IReservationService
    {

        bookingVoucher SaveVoucherInfo(int bookingid);

        bookingProforma SaveProformaInfo(int bookingid, DateTime? expiryDate, string comments, double proformaHandlingFee, double proformaIBFee);

        List<GetSupplementsResult> GetReservationSupplements(int id, booking booking);

        void SaveReservationSupplements(int booking_id, string[] mealplan, string[] supplement, bool[] chosen);

        List<viewActivity> GetReservationActivities(int id, booking booking);

        void SaveReservationActivities(int booking_id, int[] activity_id, int[] transfer_type, int[] child, int[] adult, int[] activity_pricing_id,bool isAfterSales, double[] afterSalesPriceAdult, double[] afterSalesPriceChild);

        IList<vehicle> GetReservationRentals(int id, booking booking);

        void SaveReservationRentals(int booking_id, double[] price, double[] pricewithdriver, int[] days, int[] vehicle_id, bool isAfterSales, bool[] hasDriver);

        IList<transferPricing> GetReservationTransfers(int id, booking booking);

        bool SaveReservationTransfers(int booking_id, int? transfer_id, double? totalPriceTransfer, double? totalPriceCoach, bool isAfterSales);

        bool SaveGuestParam(BookingGuestViewModel guestparam, bool UseAgentCredit, bool isUpdate, bool updatebyadmin = false);

        bool SaveReservation(int? booking_id, int? status, DateTime? timelimitdate = null, float CancellationFEE = 0);

        bool SendBookingMail(int bookingid, Helpers.AppEnums.BookingMailType EmailType, byte[] _attachment = null);
    }
}