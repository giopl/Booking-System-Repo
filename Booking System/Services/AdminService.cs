﻿using Booking_System.Helpers;
using Booking_System.Models;
using Booking_System.Repositories;
using Booking_System.Services.Abstract;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using EntityFramework.Extensions;

namespace Booking_System.Services
{
    public class AdminService : CoreService, IAdminService
    {
        ILog log = log4net.LogManager.GetLogger(typeof(AdminService));
        private ABSDBEntities db = new ABSDBEntities();



        #region Person


        /// <summary>
        /// Returns all persons in db
        /// </summary>
        /// <returns></returns>
        public List<user> GetAllPersons()
        {
            try
            {
                AdminRepository adm = new AdminRepository();
                return adm.FetchAllUsers();
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        /*
                #region Person


                /// <summary>
                /// Returns all persons in db
                /// </summary>
                /// <returns></returns>
                public List<person> GetAllPersons()
                {
                    try
                    {
                        AdminRepository adm = new AdminRepository();
                        return adm.FetchAllPersons();
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }


                /// <summary>
                /// Get a person object by fetching by id
                /// </summary>
                /// <param name="id"></param>
                /// <returns></returns>
                public person GetPersonById(int? id)
                {
                    try
                    {
                        AdminRepository adm = new AdminRepository();
                        return adm.FetchPersonById(id);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

                /// <summary>
                /// Method to create a person
                /// </summary>
                /// <param name="p"></param>
                public void CreatePerson(person p)
                {
                    try
                    {
                        AdminRepository adm = new AdminRepository();
                        adm.InsertPerson(p);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }



                /// <summary>
                /// Method to update a person
                /// </summary>
                /// <param name="p"></param>
                public void EditPerson(person p)
                {
                    try
                    {
                        AdminRepository adm = new AdminRepository();
                        adm.UpdatePerson(p);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

                /// <summary>
                /// Method to delete a person item
                /// </summary>
                /// <param name="p"></param>
                public void DeletePerson(person p)
                {
                    try
                    {
                        AdminRepository adm = new AdminRepository();
                        adm.DeletePerson(p);
                    }
                    catch (Exception)
                    {

                        throw;
                    }
                }

                #endregion

            */



        public LoginState VerifyUser(LoginState loginState)
        {

            try
            {
                log.InfoFormat("ValidateUser - userId: {0}", loginState.Username);

                var result = LoginStateEnum.Unauthorized_Access;

                //check if either username or password is missing
                if (string.IsNullOrWhiteSpace(loginState.Username) || string.IsNullOrWhiteSpace(loginState.Password))
                {
                    result = LoginStateEnum.Username_Or_Password_Missing;
                    return new LoginState { LoginStateEnum = result };
                }

                //check against user access table
                var persons = GetAllPersons();
                if (persons != null && persons.Count() > 0)
                {

                    var currentUsers = persons.Where(e => e.username.ToUpper() == loginState.Username.ToUpper());

                    if (currentUsers != null && currentUsers.Count() == 1)
                    {

                        var currentUser = currentUsers.First();
                        if (!(currentUser.is_active))
                        {
                            result = LoginStateEnum.User_Access_Disabled;
                            //result = LoginState.LoginStateEnum.User_Access_Disabled;
                            SaveAccessLog(new AccessLog { Username = loginState.Username, Operation = "LOGIN-FAILURE", Details = string.Format("Access Disabled for {0}", loginState.Username), Type = "USER" });
                            return new LoginState { LoginStateEnum = result };
                        }

                        bool EnablePassword = ConfigurationHelper.GetIsPasswordEnabled();

                        if (EnablePassword)
                        {
                        
                            if (currentUser.password != Helpers.Utils.base64Encode(loginState.Password))
                            {
                                SaveAccessLog(new AccessLog { Username = loginState.Username, Operation = "FAILED LOGIN", Details = "AUTHENTICATION FAILED" });
                                result = LoginStateEnum.Authentication_Failed;
                                return new LoginState { LoginStateEnum = result };
                            }
                            else
                            {
                                UserSession.Current.Username = currentUser.username;
                                UserSession.Current.Fullname = Utils.ToTitleCase(currentUser.firstname + " " + currentUser.lastname);
                                UserSession.Current.UserId = currentUser.user_id;
                                UserSession.Current.FixedMarkup = currentUser.markup_fixed.HasValue ? currentUser.markup_fixed.Value : 0;
                                UserSession.Current.PercMarkup = currentUser.markup_percentage.HasValue ? currentUser.markup_percentage.Value / 100.0 : 0;
                                UserSession.Current.CreditLimit = currentUser.credit_limit.HasValue ? currentUser.credit_limit.Value : 0;
                                UserSession.Current.RemainingCredit = currentUser.credit_remaining.HasValue ? currentUser.credit_remaining.Value : 0;
                                UserSession.Current.IsUserCreditBased = currentUser.is_credit_based;

                                UserSession.Current.UserRole = (UserRoles) currentUser.role;

                                UserSession.Current.IsValid = true;
                                if (currentUser.user_picture != null && currentUser.user_picture.Count > 0)
                                {
                                    UserSession.Current.UserImage = currentUser.user_picture.First().picture.FullFileName;
                                }

                                SaveAccessLog(new AccessLog("LOGIN", loginState.Browser, "USER"));

                                result = LoginStateEnum.Login_Successful;
                                return new LoginState { LoginStateEnum = result, role = UserSession.Current.UserRole };
                            }
                        }
                        //if authentication is not enabled, perform a check against web.config password
                        else
                        {
                            //Retrieve test password from web.config
                            var TestPassword = ConfigurationHelper.GetTestPassword();
                            if (loginState.Password == TestPassword)
                            {
                                UserSession.Current.Username = loginState.Username.ToUpper();
                                UserSession.Current.Fullname = currentUser.firstname + " " + currentUser.lastname;

                                UserSession.Current.UserRole = UserRoles.Administrator;
                                UserSession.Current.IsValid = true;

                                result = LoginStateEnum.Login_Successful;
                                SaveAccessLog(new AccessLog("LOGIN-TEST", loginState.Browser, "USER"));
                                return new LoginState { LoginStateEnum = result, role = UserRoles.Administrator };

                            }
                            else
                            {
                                SaveAccessLog(new AccessLog { Username = loginState.Username, Operation = "FAILED LOGIN", Details = "AUTHENTICATION FAILED" });
                                result = LoginStateEnum.Authentication_Failed;
                                return new LoginState { LoginStateEnum = result };
                            }
                        }
                    }
                    else
                    {

                        result = LoginStateEnum.Unauthorized_Access;
                        SaveAccessLog(new AccessLog { Username = loginState.Username, Operation = "LOGIN-FAILURE", Details = string.Format("Unauthorized Access for {0}", loginState.Username), Type = "USER" });
                        return new LoginState { LoginStateEnum = result };
                    }
                }

                return new LoginState { LoginStateEnum = result };

            }
            catch (Exception e)
            {
                log.ErrorFormat("CheckUser for userId: {0} - error {1} ", loginState.Username, e.ToString());
                throw;

            }
        }

        public user GetUser(LoginState loginState)
        {

            if (string.IsNullOrWhiteSpace(loginState.Username))
            {
                loginState.Username = string.Empty;
            }

            if (string.IsNullOrWhiteSpace(loginState.Email))
            {
                loginState.Email = string.Empty;
            }

            //if both are empty string, return null. cannot search for a user.
            if (String.IsNullOrWhiteSpace(loginState.Username) && String.IsNullOrWhiteSpace(loginState.Email))
            {
                return null;
            }

            var persons = GetAllPersons();

            var user = persons.Where(e => e.is_active && (e.username.ToUpper() == loginState.Username.ToUpper() || e.email.ToUpper() == loginState.Email.ToUpper()));

            if (user == null || user.Count() == 0)
            {
                return null;
            }

            return user.First();
        }

        public bool ChangePassword(LoginState loginState)
        {
            bool result = false;

            var users = db.users.Where(e => e.username == loginState.Username);
            if (users != null && users.Count() > 0)
            {
                var usr = users.First();

                if (usr.password == loginState.OldPassword)
                {
                    if ((loginState.NewPassword == loginState.ConfirmPassword) && (Helpers.Utils.base64Encode(loginState.NewPassword) != usr.password))
                    {
                        usr.password = Helpers.Utils.base64Encode(loginState.NewPassword);
                        usr.temp_password = null;
                        usr.temp_password_expiry = null;

                        db.Entry(usr).State = EntityState.Modified;
                        result = db.SaveChanges() > 0;

                        if (Helpers.ConfigurationHelper.SendEmail())
                        {
                            if (!String.IsNullOrWhiteSpace(usr.email))
                            {
                                EmailHelper emailHelper = new EmailHelper();
                                string mail = string.Format(@"<div style = 'background: #fff none repeat scroll 0 0; border: 1px solid #dadfe2; border-radius: 4px; margin: 40px auto;max-width: 400px;padding: 1px 20px 20px;'><h1>{0}</h1><p>Your password has been successfully changed.</p>
<p>This is an autogenerated email and needs no reply.</p>
</div>

", usr.FullName);

                                emailHelper.SendEmail(usr.email, "TCI Password Changed.", mail);
                            }
                        }
                    }
                }
            }

            return result;
        }

        public bool ResetPassword(string userName)
        {

            bool result = false;

            var users = db.users.Where(e => e.username == userName);
            if (users != null && users.Count() > 0)
            {
                var usr = users.First();

                var newPassword = Guid.NewGuid().ToString();

                var validityDate = DateTime.Now.AddDays(1).Date;
                if(String.IsNullOrWhiteSpace(usr.password))
                {
                    validityDate = DateTime.Now.AddDays(7).Date;
                }

                usr.temp_password = Helpers.Utils.base64Encode(newPassword);
                usr.temp_password_expiry = validityDate;

                db.Entry(usr).State = EntityState.Modified;
                result = db.SaveChanges() > 0;

                String strUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;
                if (!strUrl.EndsWith(@"/"))
                {
                    strUrl = string.Concat(strUrl, @"/");
                }

                string resetPasswordLink = string.Concat(strUrl, "Login/ConfirmResetPassword/", Utils.base64Encode(usr.username), "?reference=", usr.temp_password);

                if (Helpers.ConfigurationHelper.SendEmail())
                {
                    if (!String.IsNullOrWhiteSpace(usr.email))
                    {
                        EmailHelper emailHelper = new EmailHelper();
                        string mail = string.Empty;
                        string subject = string.Empty;

                        if (!String.IsNullOrWhiteSpace(usr.password)) { 
                            mail = string.Format(@"<div style = 'background: #fff none repeat scroll 0 0; border: 1px solid #dadfe2; border-radius: 4px; margin: 40px auto;max-width: 400px;padding: 1px 20px 20px;'><h1>{0}</h1><p>Someone requested that the password for your TCI account be reset.</p>
<a href='{1}' style=' background-color: #ff5c1a;background-image: none;border: 1px solid #ff5c1a;color: #fff;padding: 13px 10px;'>Reset My Password</a>
<div style = 'height:50px;'></div>
<p>This link is valid until {2}</p>
<p>If you did not request this, you can ignore this email or let us know. Your password won't change until you create another password.</p>
</div>

", usr.FullName, resetPasswordLink, validityDate.ToShortDateString());

                            subject = "TCI Password Reset.";
                        }
                        else
                        {
                            mail = string.Format(@"<div style = 'background: #fff none repeat scroll 0 0; border: 1px solid #dadfe2; border-radius: 4px; margin: 40px auto;max-width: 400px;padding: 1px 20px 20px;'><h1>{0}</h1><p>Your account has been successfully created on TCI.</p>
<p>To access your account, please create a password.</p>
<a href='{1}' style=' background-color: #ff5c1a;background-image: none;border: 1px solid #ff5c1a;color: #fff;padding: 13px 10px;'>Create My Password</a>
<div style = 'height:50px;'></div>
<p>This link is valid until {2}</p>
</div>

", usr.FullName, resetPasswordLink, validityDate.ToShortDateString());

                            subject = "TCI Account Created.";

                        }
                        emailHelper.SendEmail(usr.email, subject, mail);
                    }
                }
            }

            return result;
        }

        public bool SaveResourceToDisk(HttpPostedFileBase mainFile, picture img)
        {
            try
            {
                bool saved = false;

                string imageExt = string.Empty;
                string imageName = string.Empty;
                string svrImagePath = string.Empty;
                string origFileName = string.Empty;
                string id = Guid.NewGuid().ToString();

                bool hasResourceFile = mainFile != null && mainFile.ContentLength > 0;

                if (hasResourceFile)
                {
                    imageExt = System.IO.Path.GetExtension(mainFile.FileName);
                    origFileName = mainFile.FileName;
                    imageName = String.Concat(id, imageExt);

                    bool isValidItem = Helpers.ConfigurationHelper.AuthorizedImagesExt().Contains(imageExt.ToLower());
                    int sizeKb = Convert.ToInt32(Math.Round(Convert.ToDecimal(mainFile.ContentLength / 1024), 0));

                    int maxAllowedSize = 0;

                    switch (img.imgType)
                    {
                        case ImagesType.User:
                            maxAllowedSize = Helpers.ConfigurationHelper.MaxUploadSizeUser();
                            svrImagePath = DirectoryHelper.GetPath(Helpers.ConfigurationHelper.GetUserImgPath());
                            break;

                        case ImagesType.Provider:
                        case ImagesType.Hotel:

                            maxAllowedSize = Helpers.ConfigurationHelper.MaxUploadSizeProvider();
                            svrImagePath = DirectoryHelper.GetPath(Helpers.ConfigurationHelper.GetProviderImgPath());
                            break;
                        case ImagesType.Room:
                            maxAllowedSize = Helpers.ConfigurationHelper.MaxUploadSizeRoom();
                            svrImagePath = DirectoryHelper.GetPath(Helpers.ConfigurationHelper.GetRoomImgPath());
                            break;

                        case ImagesType.Activity:
                            maxAllowedSize = Helpers.ConfigurationHelper.MaxUploadSizeActivity();
                            svrImagePath = DirectoryHelper.GetPath(Helpers.ConfigurationHelper.GetActivityImgPath());
                            break;

                        case ImagesType.Vehicle:
                            maxAllowedSize = Helpers.ConfigurationHelper.MaxUploadSizeVehicle();
                            svrImagePath = DirectoryHelper.GetPath(Helpers.ConfigurationHelper.GetVehicleImgPath());
                            break;
                        default:
                            return false;
                    }

                    string path = System.IO.Path.Combine(svrImagePath, imageName);


                    //if imagetype is 
                    if(img.imgType == ImagesType.User)
                    {
                        var imgToDel = db.userPictures.Where(x => x.user_id == img.SectionId);
                        if(imgToDel.Count() >0)
                        {
                            foreach(var imgx in imgToDel)
                            {
                                DeleteResourceFromDisk(new picture { picture_id = imgx.picture_id, imgType = ImagesType.User });
                               
                            }
                        }
                        db.userPictures.Where(x => x.user_id == img.SectionId).Delete();

                        if (imgToDel.Count() > 0)
                        {
                            foreach (var imgx in imgToDel)
                            {
                                db.pictures.Where(x => x.picture_id == imgx.picture_id).Delete();

                            }
                        }

                    }


                    if (isValidItem && (sizeKb <= maxAllowedSize))
                    {
                        mainFile.SaveAs(path);

                        img.picture_guid = Guid.Parse(id);
                        img.file_name = origFileName;

                        db.pictures.Add(img);
                        db.SaveChanges();

                        int imgId = img.picture_id;


                        if (imgId > 0)
                        {
                            switch (img.imgType)
                            {
                                case ImagesType.User:
                                    userPicture userPic = new userPicture();
                                    userPic.picture_id = imgId;
                                    userPic.user_id = img.SectionId;
                                    db.userPictures.Add(userPic);
                                    db.SaveChanges();

                                    break;
                                case ImagesType.Provider:
                                case ImagesType.Hotel:

                                    providerPicture pic = new providerPicture();
                                    pic.picture_id = imgId;
                                    pic.provider_id = img.SectionId;
                                    pic.display_order = img.display_order;
                                    pic.section = img.Section.ToString();
                                    db.providerPictures.Add(pic);
                                    db.SaveChanges();

                                    break;
                                case ImagesType.Room:

                                    roomPicture roomPic = new roomPicture();
                                    roomPic.picture_id = imgId;
                                    roomPic.room_id = img.SectionId;
                                    roomPic.section = img.Section.ToString();
                                    db.roomPictures.Add(roomPic);
                                    db.SaveChanges();

                                    break;
                                case ImagesType.Vehicle:

                                    vehiclePicture vehiclePic = new vehiclePicture();
                                    vehiclePic.picture_id = imgId;
                                    vehiclePic.vehicle_id = img.SectionId;
                                    vehiclePic.section = img.Section.ToString();
                                    db.vehiclePictures.Add(vehiclePic);
                                    db.SaveChanges();

                                    break;
                                case ImagesType.Activity:
                                    activityPicture activityPic = new activityPicture();
                                    activityPic.picture_id = imgId;
                                    activityPic.activity_id = img.SectionId;
                                    activityPic.section = img.Section.ToString();
                                    db.activityPictures.Add(activityPic);
                                    db.SaveChanges();
                                    break;
                                default:
                                    break;
                            }


                            // clear cache here
                            //Helpers.CacheHelper.RemoveObjectFromCache(Helpers.CacheHelperKeys.CK_ALL_PAGES);
                        }

                    }
                }
                return saved;
            }
            catch (Exception e)
            {


                throw;
            }
        }

        public bool DeleteResourceFromDisk(picture img)
        {
            bool result = false;
            string path = string.Empty;
            switch (img.imgType)
            {
                case ImagesType.User:
                    path = Helpers.DirectoryHelper.GetPath(Helpers.ConfigurationHelper.GetUserImgPath());
                    break;
                case ImagesType.Provider:
                    path = Helpers.DirectoryHelper.GetPath(Helpers.ConfigurationHelper.GetProviderImgPath());
                    break;
                case ImagesType.Room:
                    path = Helpers.DirectoryHelper.GetPath(Helpers.ConfigurationHelper.GetRoomImgPath());
                    break;
                case ImagesType.Vehicle:
                    path = Helpers.DirectoryHelper.GetPath(Helpers.ConfigurationHelper.GetVehicleImgPath());
                    break;
                case ImagesType.Activity:
                    break;
                default:
                    return false;
            }
            result = Helpers.DirectoryHelper.DeleteFile(path, img.FullFileName) == "File Deleted";

            return result;
        }


        public bool SaveUploadedFileToDisk(HttpPostedFileBase mainFile, fileUpload file)
        {
            try
            {
                bool saved = false;

                string uploadFileExt = string.Empty;
                string uploadedFileName = string.Empty;
                string origFileName = string.Empty;
                string id = Guid.NewGuid().ToString();

                bool hasResourceFile = mainFile != null && mainFile.ContentLength > 0;

                if (hasResourceFile)
                {
                    uploadFileExt = System.IO.Path.GetExtension(mainFile.FileName).ToLower();
                    origFileName = mainFile.FileName.ToLower();
                    uploadedFileName = String.Concat(id, uploadFileExt);

                    bool isValidItem = Helpers.ConfigurationHelper.AuthorizedFilesExt().Contains(uploadFileExt);
                    int sizeKb = Convert.ToInt32(Math.Round(Convert.ToDecimal(mainFile.ContentLength / 1024), 0));

                    int maxAllowedSize = Helpers.ConfigurationHelper.MaxUploadSizeUploadedFile();

                    if (isValidItem && (sizeKb <= maxAllowedSize))
                    {
                        file.filename = origFileName;
                        file.filetype = uploadFileExt;
                        file.file_guid = Guid.Parse(id);
                        db.fileUploads.Add(file);
                        saved = db.SaveChanges() > 0;
                        if (saved)
                        {

                            string svrUploadedFilePath = DirectoryHelper.GetPath(file.FileVirtualPath);
                            string path = System.IO.Path.Combine(svrUploadedFilePath, uploadedFileName);
                            mainFile.SaveAs(path);
                        }
                    }
                }
                return saved;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public bool DeleteUploadedFileFromDisk(int fileId)
        {
            bool result = false;

            var file = db.fileUploads.Find(fileId);
            if (file != null)
            {
                string path = DirectoryHelper.GetPath(file.FileVirtualPath);

                result = Helpers.DirectoryHelper.DeleteFile(path, file.FullFileName) == "File Deleted";
                if (result)
                {
                    db.fileUploads.Remove(file);
                    result = db.SaveChanges() > 0;
                }
            }
            return result;
        }
    }
}