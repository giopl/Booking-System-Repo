﻿using Booking_System.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Booking_System.Models.ViewModel;  
using Booking_System.Models;
using System.Text;

namespace Booking_System.Services
{
    public class FrontService : IFrontService
    {
        private ABSDBEntities db = new ABSDBEntities();

        //public IList<RoomOccupancy> GetOccupanciesForHotel(List<RoomOccupancy> occupancies, int hotelId)
        //{
        //    try
        //    {
        //        IList<RoomOccupancy> rooms = new List<RoomOccupancy>();

        //        foreach (var occ in occupancies)
        //        {

        //            List<int> agelist = new List<int>();
        //            agelist.AddRange(occ.InfantAges);
        //            agelist.AddRange(occ.ChildrenAges);
        //            agelist.AddRange(occ.TeenAges);

        //            int[] ages = agelist.ToArray();
                    
        //            var occupancy = GetOccupancy(occ.NumAdults, occ.NumChildren + occ.NumInfants + occ.NumTeens
        //                , ages, hotelId, occ.RoomNumber);
        //            rooms.Add(occupancy);     
                    
        //            }
        //        return rooms;
            
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //}


        public IList<SearchOccupancy> GetSearchOccupancies(SearchDetails search)
        {
            try
            {

                IList<SearchOccupancy> result = new List<SearchOccupancy>();
                for (int i = 0; i < search._Rooms; i++)
                {

                    SearchOccupancy so = new SearchOccupancy();

                    if(search.RoomAdults[i]+search.RoomChildren[i] >0)
                    {

                        so.Adults = search.RoomAdults[i];
                        so.RoomNumber = i + 1;
                        

                        for(int j=0;j < search.RoomChildren[i]; j++ )
                        {
                            if(j==0) { so.Ages.Add(search.RoomChildAge1[i]);}
                            if (j == 1) { so.Ages.Add(search.RoomChildAge2[i]); }
                            if (j == 2) { so.Ages.Add(search.RoomChildAge3[i]); }
                            if (j == 3) { so.Ages.Add(search.RoomChildAge4[i]); }
                            if (j == 4) { so.Ages.Add(search.RoomChildAge5[i]); }

                        }

                    }

                    result.Add(so);
                }

                return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        
        }

        //public IList<RoomOccupancy> GetOccupancies(SearchDetails search)
        //{
        //    try
        //    {
        //        IList<RoomOccupancy> rooms = new List<RoomOccupancy>();

        //        for (int i = 0; i < search._Rooms; i++)
        //        {
        //            if(search.RoomAdults[i]+search.RoomChildren[i] >0)
        //            {
        //            int[] ages = new int[] { search.RoomChildAge1[i], search.RoomChildAge2[i],
        //            search.RoomChildAge3[i],search.RoomChildAge4[i],search.RoomChildAge5[i] };
        //            var occupancy = GetOccupancy(search.RoomAdults[i], search.RoomChildren[i], ages, null, i + 1);
        //            rooms.Add(occupancy);

        //            }
        //        }
        //        return rooms;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //}




        public RoomOccupancy GetRoomOccupancy(SearchOccupancy searchOccupancy, int hotelId)
        {
            try
            {
                RoomOccupancy ro = new RoomOccupancy();

                int infant_max_age = 2;
                int child_min_age = 3;
                int child_max_age = 12;

                ro.RoomNumber = searchOccupancy.RoomNumber;

                    var guesttype = db.providers.Find(hotelId).guest_type.ToList();
                    if (guesttype.Count() == 1)
                    {
                        var _g = guesttype.FirstOrDefault();
                        infant_max_age = _g.infant_max_age.HasValue?_g.infant_max_age.Value: 0;

                        child_min_age = _g.child_min_age.HasValue?_g.child_min_age.Value:0;
                        
                        child_max_age = _g.child_max_age.HasValue ?_g.child_max_age.Value:17;
                    }




                ro.NumAdults = searchOccupancy.Adults;
                

                if (searchOccupancy.Ages.Count > 0)
                {
                    foreach (var age in searchOccupancy.Ages)
                    {
                        if (age <= infant_max_age)
                        {
                            ro.NumInfants++;
                            ro.InfantAges.Add(age);
                        }
                        else if (age >= child_min_age && age <= child_max_age)
                        {
                            ro.NumChildren++;
                            ro.ChildAges.Add(age);
                        }
                        else if (age > child_max_age && age <18)
                        {
                            ro.NumTeens++;
                            ro.TeenAges.Add(age);

                        }



                    }

                }
                return ro;


            }
            catch (Exception e)
            {
                throw;
            }
        }


        public IList<RoomOccupancy> GetRoomOccupancies(List<SearchOccupancy> searchOccupanies, int hotelId)
        {
            try
            {
                IList<RoomOccupancy> list = new List<RoomOccupancy>();
                foreach (var so in searchOccupanies)
                {
                    var room = GetRoomOccupancy(so, hotelId);
                    list.Add(room);
                }
                return list;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
    }
}