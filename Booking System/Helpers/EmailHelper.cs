﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;

namespace Booking_System.Helpers
{
    public class EmailHelper
    {
        public bool SendEmail(string email, string subject, string body,string cc=null)
        {
            log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            /*
             * http://www.codeproject.com/Tips/520998/Send-Email-from-Yahoo-GMail-Hotmail-Csharp
             * Yahoo! 	smtp.mail.yahoo.com
             * GMail 	smtp.gmail.com
             * Hotmail 	smtp.live.com
             */

            var uselocal = ConfigurationHelper.UseLocalEmail();

            
                try
                {



                    string smtpAddress = ConfigurationHelper.SmtpServer();
                    int portNumber = ConfigurationHelper.SmtpServerPort();
                    bool enableSSL = false;

                    string emailFrom = ConfigurationHelper.WebEmail();
                    string password = ConfigurationHelper.WebEmailPwd();
                    string emailTo = email;

                    using (MailMessage mail = new MailMessage())
                    {
                        mail.From = new MailAddress(emailFrom);
                        mail.To.Add(emailTo);
                        if (!String.IsNullOrEmpty(cc))
                        {
                            mail.CC.Add(cc);
                        }
                        mail.Subject = subject;
                        mail.Body = body;
                        mail.IsBodyHtml = true;
                        // Can set to false, if you are sending pure text.


                        

                    if(uselocal)
                    {
                        using (SmtpClient smtp = new SmtpClient("localhost", portNumber))
                        {
                                smtp.Send(mail);
                        }

                    } else
                    {
                        using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                        {
                            smtp.Credentials = new NetworkCredential(emailFrom, password);
                            smtp.EnableSsl = enableSSL;
                            if (ConfigurationHelper.SendEmail())
                            {
                                smtp.Send(mail);

                            }
                        }
                    }
                        


                    }
                }
                catch (Exception e)
                {
                    log.ErrorFormat("[EmailHelper ]could not send email with error {0}", e.ToString());
                    return false;
                }

            


  

            return true;
        }





        public bool SendEmail(string email, string subject, string body,string AttachmentName,byte[] AttachmentContent,string mediatype,string cc=null)
        {

            /*
             * http://www.codeproject.com/Tips/520998/Send-Email-from-Yahoo-GMail-Hotmail-Csharp
             * Yahoo! 	smtp.mail.yahoo.com
             * GMail 	smtp.gmail.com
             * Hotmail 	smtp.live.com
             */

            string smtpAddress = ConfigurationHelper.SmtpServer();
            int portNumber = ConfigurationHelper.SmtpServerPort(); 
            bool enableSSL = false;

            string emailFrom = ConfigurationHelper.WebEmail();
            string password = ConfigurationHelper.WebEmailPwd();
            string emailTo = email;
            bool mailSent = false;
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom);
                    mail.To.Add(emailTo);
                    if (!String.IsNullOrEmpty(cc))
                    {
                        mail.CC.Add(cc);
                    }
                    mail.Subject = subject;
                    mail.Body = body;
                    mail.IsBodyHtml = true;

                    mail.Attachments.Add(new Attachment(new MemoryStream(AttachmentContent), AttachmentName, mediatype));
                    // Can set to false, if you are sending pure text.

                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(emailFrom, password);
                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);
                        mailSent = true;

                    }
                }
            }
            catch (Exception ex)
            {
                mailSent = false;
            }

            return mailSent;
        }
    }
}