﻿using Booking_System.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Helpers
{


    public class UserSession
    {

        private UserSession()
        {
            //generics
            IsValid = false;
            Username = string.Empty;
            Fullname = string.Empty;
            ReadOnlyMode = false;
            UserId = 0;
            //SearchDetails CurrentSearchDetails = new SearchDetails();
            PercMarkup = 0d;
            FixedMarkup = 0d;
            RemainingCredit = 0d;
            CreditLimit = 0d;
            BookingId = 0;

            //string SearchOccupancies = string.Empty;
            //DateTime SearchStartDate = DateTime.MinValue;
            //DateTime SearchEndDate = DateTime.MinValue;

        }



        public static UserSession Current
        {
            get
            {
                try
                {
                    UserSession session = (UserSession)HttpContext.Current.Session["__MySession__"];
                    if (session == null)
                    {
                        session = new UserSession();
                        HttpContext.Current.Session["__MySession__"] = session;
                    }
                    return session;
                }
                catch (Exception)
                {

                    throw;
                }

            }
        }

        //public bool IsTeamLeader { get; set; }
        public bool IsValid { get; set; }
        public bool ReadOnlyMode { get; set; }
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Fullname { get; set; }
        public Models.UserRoles UserRole { get; set; }
        public string UserImage { get; set; }
        public int BookingId { get; set; }

        //public string SearchOccupancies { get; set; }
        //public DateTime SearchStartDate { get; set; }
        //public DateTime SearchEndDate { get; set; }
        //public int SearchHotelId { get; set; }        
        //public List<RoomOccupancy> occupancies { get; set; }

        public string HotelSearchCriteriaXML { get; set; }
        public SearchDetails SearchDetails { get; set; }

        public SearchItem SearchItem { get; set; } 

        public double PercMarkup { get; set; }

        public double FixedMarkup { get; set; }

        public double CreditLimit { get; set; }


        public double RemainingCredit { get; set; }

        public bool IsUserCreditBased { get; set; }

        //public SearchDetails CurrentSearchDetails { get; set; }


        /// <summary>
        /// Clears the current session.
        /// </summary>
        public void ClearSession()
        {
            IsValid = false; 
            HttpContext.Current.Session["__MySession__"] = null;
            HttpContext.Current.Session.Clear();
            HttpContext.Current.Session.Abandon();
        }



    }



}