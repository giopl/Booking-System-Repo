﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Booking_System.Helpers
{
    public class LoginState
    {
        public bool IsValid { get; set; }

        public  LoginStateEnum LoginStateEnum { get; set; }

        public string Browser { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public bool IsFront { get; set; }

        public Models.UserRoles role { get; set; }

        /*This part is used for reset password / change password*/
        public string Email { get; set; }

        public string OldPassword { get; set; }

        public string NewPassword { get; set; }

        public string ConfirmPassword { get; set; }


        public string GetErrorDescription()
        {
            return LoginStateEnum.ToString().Replace('_', ' ');
        }
    }


    public  enum LoginStateEnum
    {

        Not_Set = 0,

        [Description("Login Successful")]
        Login_Successful,

        [Description("Either Username or Password or both are missing!")]
        Username_Or_Password_Missing,

        [Description("You are not Authorized to access this site!")]
        Unauthorized_Access,

        [Description("Your account is disabled. Please contact your admin!")]
        User_Access_Disabled,

        [Description("Your account is locked out. Please contact your admin!")]
        User_Account_Locked_Out,

        [Description("Authentication Failed, please re-try.")]
        Authentication_Failed,

        [Description("System Error, Please contact your admin.")]
        Application_Error,

        [Description("Error while changing the password, please try again")]
        Password_Changed_Error,

        [Description("Your session expired after a period of inactivity, please re-login.")]
        Session_Expired,

        [Description("Password Resetted, please check your email.")]
        Password_Reset,

        [Description("Password Changed, please check your email.")]
        Password_Changed,

        [Description("Use of password is now Mandatory. Ensure you have a valid user to login and PasswordEnabled is set to True in Web.Config")]
        Use_of_password_mandatory
    }


}