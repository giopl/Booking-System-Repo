﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Helpers
{
    public class SelectItems
    {

        public static List<SelectListItem> GetTitleListItem(string selected = "")
        {
            List<SelectListItem> titleListItem = new List<SelectListItem>();
            titleListItem.Add(new SelectListItem() { Text = "Ms", Value = "Ms", Selected = selected == "Ms" });
            titleListItem.Add(new SelectListItem() { Text = "Mrs", Value = "Mrs", Selected = selected == "Mrs" });
            titleListItem.Add(new SelectListItem() { Text = "Mr", Value = "Mr", Selected = selected == "Mr" });
            titleListItem.Add(new SelectListItem() { Text = "Dr", Value = "Dr", Selected = selected == "Dr" });
            titleListItem.Add(new SelectListItem() { Text = "Prof", Value = "Prof", Selected = selected == "Prof" });

            return titleListItem;
        }

        public static List<SelectListItem> GetFeaturedSectionListItem(string selected = "")
        {
            List<SelectListItem> featuredsectionListItem = new List<SelectListItem>();
            featuredsectionListItem.Add(new SelectListItem() { Text = "1", Value = "1", Selected = selected == "1" });
            featuredsectionListItem.Add(new SelectListItem() { Text = "2", Value = "2", Selected = selected == "2" });
            featuredsectionListItem.Add(new SelectListItem() { Text = "3", Value = "3", Selected = selected == "3" });
            featuredsectionListItem.Add(new SelectListItem() { Text = "4", Value = "4", Selected = selected == "4" });
            return featuredsectionListItem;
        }

        public static List<SelectListItem> GetActivityCategory(string selected = "")
        {
            List<SelectListItem> titleListItem = new List<SelectListItem>();

            titleListItem.Add(new SelectListItem() { Text = "Tour", Value = "tour", Selected = selected == "tour" });
            titleListItem.Add(new SelectListItem() { Text = "Water", Value = "water", Selected = selected == "water" });
            titleListItem.Add(new SelectListItem() { Text = "Cruising", Value = "cruising", Selected = selected == "cruising" });
            titleListItem.Add(new SelectListItem() { Text = "Adventure", Value = "adventure", Selected = selected == "adventure" });
            titleListItem.Add(new SelectListItem() { Text = "Sport", Value = "sport", Selected = selected == "sport" });
            titleListItem.Add(new SelectListItem() { Text = "Land", Value = "land", Selected = selected == "land" });
            titleListItem.Add(new SelectListItem() { Text = "Other", Value = "other", Selected = selected == "other" });

            return titleListItem;
        }




        public static List<SelectListItem> GetFuelTypeListItem(string selected = "")
        {
            List<SelectListItem> titleListItem = new List<SelectListItem>();
            titleListItem.Add(new SelectListItem() { Text = "Gas", Value = ((int)AppEnums.FuelType.GAS).ToString(), Selected = selected == ((int)AppEnums.FuelType.GAS).ToString() });
            titleListItem.Add(new SelectListItem() { Text = "Diesel", Value = ((int)AppEnums.FuelType.DIESEL).ToString(), Selected = selected == ((int)AppEnums.FuelType.DIESEL).ToString() });
            titleListItem.Add(new SelectListItem() { Text = "Hybrid", Value = ((int)AppEnums.FuelType.HYBRID).ToString(), Selected = selected == ((int)AppEnums.FuelType.HYBRID).ToString() });
            titleListItem.Add(new SelectListItem() { Text = "Electric", Value = ((int)AppEnums.FuelType.ELECTRIC).ToString(), Selected = selected == ((int)AppEnums.FuelType.ELECTRIC).ToString() });


            return titleListItem;
        }





        public static List<SelectListItem> GetTransferType(string selected = "")
        {
            List<SelectListItem> titleListItem = new List<SelectListItem>();
            titleListItem.Add(new SelectListItem() { Text = "Private Car", Value = "Private", Selected = selected == "Private" });
            titleListItem.Add(new SelectListItem() { Text = "Coach", Value = "Coach", Selected = selected == "Coach" });
            titleListItem.Add(new SelectListItem() { Text = "Bus", Value = "Bus", Selected = selected == "Bus" });

            return titleListItem;
        }



        public static List<SelectListItem> GetVehicleType(string selected = "")
        {
            List<SelectListItem> titleListItem = new List<SelectListItem>();
            titleListItem.Add(new SelectListItem() { Text = "Economy", Value = "Economy", Selected = selected == "Economy" });
            titleListItem.Add(new SelectListItem() { Text = "Compact", Value = "Compact", Selected = selected == "Compact" });
            titleListItem.Add(new SelectListItem() { Text = "Family", Value = "Family", Selected = selected == "Family" });
            titleListItem.Add(new SelectListItem() { Text = "Mid-Size", Value = "Mid-Size", Selected = selected == "Mid-Size" });
            titleListItem.Add(new SelectListItem() { Text = "Crossover", Value = "Crossover", Selected = selected == "Crossover" });
            titleListItem.Add(new SelectListItem() { Text = "Minivan", Value = "Minivan", Selected = selected == "Minivan" });
            titleListItem.Add(new SelectListItem() { Text = "Executive", Value = "Executive", Selected = selected == "Executive" });
            titleListItem.Add(new SelectListItem() { Text = "Luxury", Value = "Luxury", Selected = selected == "Luxury" });
            titleListItem.Add(new SelectListItem() { Text = "Sports", Value = "Sports", Selected = selected == "Sports" });


            titleListItem.Add(new SelectListItem() { Text = "Sedan", Value = "Sedan", Selected = selected == "Sedan" });
            titleListItem.Add(new SelectListItem() { Text = "Hatchback", Value = "Hatchback", Selected = selected == "Hatchback" });
            titleListItem.Add(new SelectListItem() { Text = "SUV", Value = "SUV", Selected = selected == "SUV" });
            titleListItem.Add(new SelectListItem() { Text = "Coach", Value = "Coach", Selected = selected == "Coach" });
            titleListItem.Add(new SelectListItem() { Text = "Bus", Value = "Bus", Selected = selected == "Bus" });

            return titleListItem;
        }


        public static List<SelectListItem> GetLocationListItem(string selected = "")
        {
            List<SelectListItem> ListItem = new List<SelectListItem>();

            ListItem.Add(new SelectListItem() { Text = "North", Value = "North", Selected = selected == "North" });
            ListItem.Add(new SelectListItem() { Text = "West", Value = "West", Selected = selected == "West" });
            ListItem.Add(new SelectListItem() { Text = "East", Value = "East", Selected = selected == "East" });
            ListItem.Add(new SelectListItem() { Text = "South", Value = "South", Selected = selected == "South" });
            ListItem.Add(new SelectListItem() { Text = "Port Louis Region", Value = "Port Louis", Selected = selected == "Port Louis" });
            ListItem.Add(new SelectListItem() { Text = "Centre", Value = "Centre", Selected = selected == "Centre" });

            return ListItem;
        }


        public static List<SelectListItem> GetHotelFeatureCategoryListItem(string selected = "")
        {
            List<SelectListItem> ListItem = new List<SelectListItem>();

            ListItem.Add(new SelectListItem() { Text = "Amenity", Value = "FACILITY", Selected = selected == "FACILITY" });
            ListItem.Add(new SelectListItem() { Text = "Location", Value = "LOCATION", Selected = selected == "LOCATION" });
            ListItem.Add(new SelectListItem() { Text = "Entertainment", Value = "ENTERTAINMENT", Selected = selected == "ENTERTAINMENT" });
            ListItem.Add(new SelectListItem() { Text = "Sport", Value = "SPORT", Selected = selected == "SPORT" });

            return ListItem;
        }


        public static List<SelectListItem> GetRoomFeatureCategoryListItem(string selected = "")
        {
            List<SelectListItem> ListItem = new List<SelectListItem>();

            ListItem.Add(new SelectListItem() { Text = "Amenity", Value = "FACILITY", Selected = selected == "FACILITY" });
            ListItem.Add(new SelectListItem() { Text = "Location", Value = "LOCATION", Selected = selected == "LOCATION" });
            ListItem.Add(new SelectListItem() { Text = "Entertainment", Value = "ENTERTAINMENT", Selected = selected == "ENTERTAINMENT" });
            ListItem.Add(new SelectListItem() { Text = "Other", Value = "OTHER", Selected = selected == "OTHER" });


            return ListItem;
        }


        public static List<SelectListItem> GetVehicleFeatureCategoryListItem(string selected = "")
        {
            List<SelectListItem> ListItem = new List<SelectListItem>();

            ListItem.Add(new SelectListItem() { Text = "Comfort", Value = "COMFORT", Selected = selected == "COMFORT" });
            ListItem.Add(new SelectListItem() { Text = "Performance", Value = "PERFORMANCE", Selected = selected == "PERFORMANCE" });
            ListItem.Add(new SelectListItem() { Text = "Other", Value = "OTHER", Selected = selected == "OTHER" });


            return ListItem;
        }



        public static List<SelectListItem> GetGenderListItem(int? selected)
        {
            List<SelectListItem> ListItem = new List<SelectListItem>();

            ListItem.Add(new SelectListItem() { Text = "Male", Value = "1", Selected = selected == 1 });
            ListItem.Add(new SelectListItem() { Text = "Female", Value = "2", Selected = selected == 2 });

            return ListItem;
        }

        public static List<SelectListItem> GetEnumSelectList(Type enumValue, string selected = "")
        {
            Array values = Enum.GetValues(enumValue);
            List<SelectListItem> ListItem = new List<SelectListItem>();

            foreach (var i in values)
            {
                ListItem.Add(new SelectListItem
                {
                    Text = Enum.GetName(enumValue, i),
                    Value = ((int)i).ToString(),
                    Selected = Enum.GetName(enumValue, i).ToString() == selected
                });
            }

            return ListItem;
        }

        public static List<SelectListItem> GetBookingStatusListItem(int? selected, bool addNewStatus = false)
        {
            List<SelectListItem> ListItem = new List<SelectListItem>();
            if (addNewStatus)
            {
                ListItem.Add(new SelectListItem() { Text = "New", Value = "0", Selected = selected == 0 });
            }

            ListItem.Add(new SelectListItem() { Text = "Processing", Value = "1", Selected = selected == 1 });
            //ListItem.Add(new SelectListItem() { Text = "Checked", Value = "2", Selected = selected == 2 });
            ListItem.Add(new SelectListItem() { Text = "Canceled", Value = "3", Selected = selected == 3 });
            ListItem.Add(new SelectListItem() { Text = "Awaiting Payment / Confirmation", Value = "4", Selected = selected == 4 });
            ListItem.Add(new SelectListItem() { Text = "Confirmed Request", Value = "5", Selected = selected == 5 });
            ListItem.Add(new SelectListItem() { Text = "Confirmed", Value = "6", Selected = selected == 6 });
            ListItem.Add(new SelectListItem() { Text = "Expired", Value = "7", Selected = selected == 7 });

            return ListItem;
        }

        public static List<SelectListItem> GetBookingPaymentMethodsListItem(int? selected)
        {
            List<SelectListItem> ListItem = new List<SelectListItem>();

            //ListItem.Add(new SelectListItem() { Text = "New", Value = "0", Selected = selected == 0 });
            ListItem.Add(new SelectListItem() { Text = "Cash", Value = "1", Selected = selected == 1 });
            ListItem.Add(new SelectListItem() { Text = "Cheque", Value = "2", Selected = selected == 2 });
            ListItem.Add(new SelectListItem() { Text = "Transfer", Value = "3", Selected = selected == 3 });

            return ListItem;
        }

        public static List<SelectListItem> GetUserList(int? selected, List<Booking_System.Models.user> users)
        {
            List<SelectListItem> ListItem = new List<SelectListItem>();

            if (users != null)
            {
                foreach (var item in users)
                {
                    ListItem.Add(new SelectListItem() { Text = item.FullName, Value = item.user_id.ToString(), Selected = selected == item.user_id });
                }
            }


            return ListItem;
        }

        public static List<SelectListItem> GetBookingNextStatusByStatus(int? selected, bool bookingCreditBased, bool isTimelimit)
        {
            List<SelectListItem> ListItem = new List<SelectListItem>();
            #region Common
            switch (selected.Value)
            {

                case (int)AppEnums.BookingStatus.DRAFT:
                    ListItem.Add(new SelectListItem() { Text = "Draft", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString(), Selected = true, Disabled = true });
                    break;
                case (int)AppEnums.BookingStatus.NEW:
                    ListItem.Add(new SelectListItem() { Text = "New", Value = ((int)AppEnums.BookingStatus.NEW).ToString(), Selected = true, Disabled = true });
                    ListItem.Add(new SelectListItem() { Text = "Processing", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString(), Selected = selected == (int)AppEnums.BookingStatus.PROCESSING });
                    break;
                case (int)AppEnums.BookingStatus.PROCESSING:
                    ListItem.Add(new SelectListItem() { Text = "Processing", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString(), Selected = true, Disabled = true });
                    //ListItem.Add(new SelectListItem() { Text = "Hotel Timelimit", Value = ((int)AppEnums.BookingStatus.AWAITING_CONFIRMATION).ToString() });
                    ListItem.Add(new SelectListItem() { Text = "Hotel Unavailable", Value = ((int)AppEnums.BookingStatus.DECLINED).ToString() });
                    break;
                case (int)AppEnums.BookingStatus.PROCESSING_NEW_TIMELIMIT:
                    ListItem.Add(new SelectListItem() { Text = "Processing", Value = ((int)AppEnums.BookingStatus.PROCESSING_NEW_TIMELIMIT).ToString(), Selected = true, Disabled = true });
                    //ListItem.Add(new SelectListItem() { Text = "Hotel New Timelimit", Value = ((int)AppEnums.BookingStatus.AWAITING_CONFIRMATION).ToString() });
                    ListItem.Add(new SelectListItem() { Text = "Hotel Unavailable", Value = ((int)AppEnums.BookingStatus.DECLINED).ToString() });
                    break;
                case
                    (int)AppEnums.BookingStatus.DECLINED:
                    ListItem.Add(new SelectListItem() { Text = "Unavailable", Value = ((int)AppEnums.BookingStatus.DECLINED).ToString(), Selected = true, Disabled = true });
                    ListItem.Add(new SelectListItem() { Text = "Cancel", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                    ListItem.Add(new SelectListItem() { Text = "Re-Process", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                    break;                    
                case (int)AppEnums.BookingStatus.COMPLETED:
                    ListItem.Add(new SelectListItem() { Text = "Confirmed", Value = ((int)AppEnums.BookingStatus.COMPLETED).ToString(), Selected = true, Disabled = true });
                    ListItem.Add(new SelectListItem() { Text = "Cancel Booking", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                    ListItem.Add(new SelectListItem() { Text = "Cancel Booking with FEES", Value = ((int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES).ToString() });
                    ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                    break;
                case (int)AppEnums.BookingStatus.CANCELED:
                    ListItem.Add(new SelectListItem() { Text = "Canceled", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString(), Selected = true, Disabled = true });
                    ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                    break;
                case (int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES:
                    ListItem.Add(new SelectListItem() { Text = "Cancel Booking with FEES", Value = ((int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES).ToString(), Selected = true, Disabled = true });
                    ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                    break;
                case (int)AppEnums.BookingStatus.CANCEL_REQUEST:
                    ListItem.Add(new SelectListItem() { Text = "Cancel Request", Value = ((int)AppEnums.BookingStatus.CANCEL_REQUEST).ToString(), Selected = true, Disabled = true });
                    ListItem.Add(new SelectListItem() { Text = "Cancel Booking", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                    ListItem.Add(new SelectListItem() { Text = "Cancel Booking with FEES", Value = ((int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES).ToString() });
                    ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                    break;

            }
                    #endregion

            //0  DRAFT
            //1   NEW
            //2   PROCESSING
            //3   DECLINED
            //4   AWAITING CONFIRMATION
            //5   EXPIRED
            //6   REQUEST FOR EXTENSION
            //7   CONFIRMED
            //8   AWAITING PAYMENT
            //9   COMPLETED
            //10  CANCELED


            #region "Credit and Timelimit"
            if (bookingCreditBased && isTimelimit)
            {
                switch (selected.Value)
                {

                    //DRAFT
                    //NEW
                    //PROCESSING
                    //DECLINED
                    //AWAITING CONFIRMATION
                    //EXPIRED
                    //REQUEST FOR EXTENSION
                    //CONFIRMED
                    //COMPLETED
                    //CANCELED
                    case (int)AppEnums.BookingStatus.PROCESSING:
                        ListItem.Add(new SelectListItem() { Text = "Hotel Timelimit", Value = ((int)AppEnums.BookingStatus.AWAITING_CONFIRMATION).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.PROCESSING_NEW_TIMELIMIT:
                        ListItem.Add(new SelectListItem() { Text = "Hotel New Timelimit", Value = ((int)AppEnums.BookingStatus.AWAITING_CONFIRMATION).ToString() });
                        break;
                    case
                    (int)AppEnums.BookingStatus.EXPIRED:
                        ListItem.Add(new SelectListItem() { Text = "Expired", Value = ((int)AppEnums.BookingStatus.EXPIRED).ToString(), Selected = true, Disabled = true });
                        ListItem.Add(new SelectListItem() { Text = "Cancel", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Re-Process", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.AWAITING_CONFIRMATION:
                        ListItem.Add(new SelectListItem() { Text = "Awaiting Confirmation", Value = ((int)AppEnums.BookingStatus.AWAITING_CONFIRMATION).ToString(), Selected = true, });
                        ListItem.Add(new SelectListItem() { Text = "Expired", Value = ((int)AppEnums.BookingStatus.EXPIRED).ToString(), Selected = selected == (int)AppEnums.BookingStatus.EXPIRED });
                        ListItem.Add(new SelectListItem() { Text = "Re-Process", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.REQUEST_FOR_EXTENSION:
                        ListItem.Add(new SelectListItem() { Text = "Extension Requested", Value = ((int)AppEnums.BookingStatus.REQUEST_FOR_EXTENSION).ToString(), Selected = true, Disabled = true });
                        ListItem.Add(new SelectListItem() { Text = "Processing", Value = ((int)AppEnums.BookingStatus.PROCESSING_NEW_TIMELIMIT).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Hotel Unavailable", Value = ((int)AppEnums.BookingStatus.DECLINED).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.CONFIRMED:
                        ListItem.Add(new SelectListItem() { Text = "Confirmed Request", Value = ((int)AppEnums.BookingStatus.CONFIRMED).ToString(), Selected = true, Disabled = true });
                        ListItem.Add(new SelectListItem() { Text = "Completed", Value = ((int)AppEnums.BookingStatus.COMPLETED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking with FEES", Value = ((int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                        break;
                    default:
                        break;
                }

            }
            #endregion

            // credit & confirmed
            #region "Credit and Confirmed"

            if (bookingCreditBased && !isTimelimit)
            {
                //DRAFT
                 //   NEW
                 //   PROCESSING
                 //   DECLINED
                 //   COMPLETED
                 //   CANCELED
                switch (selected.Value)
                {
                    case (int)AppEnums.BookingStatus.PROCESSING:
                        //ListItem.Add(new SelectListItem() { Text = "Hotel Confirmed", Value = ((int)AppEnums.BookingStatus.CONFIRMED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Confirmed", Value = ((int)AppEnums.BookingStatus.COMPLETED).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.CONFIRMED:
                        ListItem.Add(new SelectListItem() { Text = "Confirmed Request", Value = ((int)AppEnums.BookingStatus.CONFIRMED).ToString(), Selected = true, Disabled = true });
                        ListItem.Add(new SelectListItem() { Text = "Confirmed", Value = ((int)AppEnums.BookingStatus.COMPLETED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking with FEES", Value = ((int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                        break;
                }
                    
                

            }

            #endregion

            #region "Cash and Timelimit"
            //non-credit && timelimit
            //DRAFT
            //NEW
            //PROCESSING
            //DECLINED
            //AWAITING CONFIRMATION
            //EXPIRED
            //REQUEST FOR EXTENSION
            //CONFIRMED
            //AWAITING PAYMENT
            //COMPLETED
            //CANCELED

            if (!bookingCreditBased && isTimelimit)
            {
                switch (selected.Value)
                {
                    case (int)AppEnums.BookingStatus.PROCESSING:
                        ListItem.Add(new SelectListItem() { Text = "Hotel Timelimit", Value = ((int)AppEnums.BookingStatus.AWAITING_CONFIRMATION).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.PROCESSING_NEW_TIMELIMIT:
                        ListItem.Add(new SelectListItem() { Text = "Hotel New Timelimit", Value = ((int)AppEnums.BookingStatus.AWAITING_CONFIRMATION).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.AWAITING_CONFIRMATION:
                        ListItem.Add(new SelectListItem() { Text = "Awaiting Confirmation", Value = ((int)AppEnums.BookingStatus.AWAITING_CONFIRMATION).ToString(), Selected = true, });
                        ListItem.Add(new SelectListItem() { Text = "Expired", Value = ((int)AppEnums.BookingStatus.EXPIRED).ToString(), Selected = selected == (int)AppEnums.BookingStatus.EXPIRED });
                        ListItem.Add(new SelectListItem() { Text = "Re-Process", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.REQUEST_FOR_EXTENSION:
                        ListItem.Add(new SelectListItem() { Text = "Request Extension", Value = ((int)AppEnums.BookingStatus.REQUEST_FOR_EXTENSION).ToString(), Selected = true, Disabled = true });
                        ListItem.Add(new SelectListItem() { Text = "Processing", Value = ((int)AppEnums.BookingStatus.PROCESSING_NEW_TIMELIMIT).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Hotel Unavailable", Value = ((int)AppEnums.BookingStatus.DECLINED).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.CONFIRMED:
                        ListItem.Add(new SelectListItem() { Text = "Confirmed Request", Value = ((int)AppEnums.BookingStatus.CONFIRMED).ToString(), Selected = true, Disabled = true });
                        ListItem.Add(new SelectListItem() { Text = "Awaiting Payment", Value = ((int)AppEnums.BookingStatus.AWAITING_PAYMENT).ToString()});
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking with FEES", Value = ((int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.AWAITING_PAYMENT:
                        ListItem.Add(new SelectListItem() { Text = "Awaiting Payment", Value = ((int)AppEnums.BookingStatus.AWAITING_PAYMENT).ToString(), Selected = true, Disabled = true });
                        ListItem.Add(new SelectListItem() { Text = "Confirmed", Value = ((int)AppEnums.BookingStatus.COMPLETED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking with FEES", Value = ((int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                        break;
                    
                    default:
                       
                        break;
                }

            }

            #endregion
            //cash based & confirmed

            #region "Cash and Confirmed"
            //DRAFT
            //NEW
            //PROCESSING
            //DECLINED
            //AWAITING PAYMENT
            //COMPLETED
            //CANCELED


            if (!bookingCreditBased && !isTimelimit)
            {
                switch (selected.Value)
                {
                    case (int)AppEnums.BookingStatus.PROCESSING:
                        //ListItem.Add(new SelectListItem() { Text = "Hotel Confirmed", Value = ((int)AppEnums.BookingStatus.CONFIRMED).ToString() });
                         ListItem.Add(new SelectListItem() { Text = "Awaiting Payment", Value = ((int)AppEnums.BookingStatus.AWAITING_PAYMENT).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking with FEES", Value = ((int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.CONFIRMED:
                        ListItem.Add(new SelectListItem() { Text = "Confirmed", Value = ((int)AppEnums.BookingStatus.CONFIRMED).ToString(), Selected = true, Disabled = true });
                        ListItem.Add(new SelectListItem() { Text = "Awaiting Payment", Value = ((int)AppEnums.BookingStatus.AWAITING_PAYMENT).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking with FEES", Value = ((int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                        break;
                    case (int)AppEnums.BookingStatus.AWAITING_PAYMENT:
                        ListItem.Add(new SelectListItem() { Text = "Awaiting Payment", Value = ((int)AppEnums.BookingStatus.AWAITING_PAYMENT).ToString(), Selected = true, Disabled = true });
                        ListItem.Add(new SelectListItem() { Text = "Confirmed", Value = ((int)AppEnums.BookingStatus.COMPLETED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking", Value = ((int)AppEnums.BookingStatus.CANCELED).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Cancel Booking with FEES", Value = ((int)AppEnums.BookingStatus.CANCELED_WITH_CHARGES).ToString() });
                        ListItem.Add(new SelectListItem() { Text = "Re-Process Booking", Value = ((int)AppEnums.BookingStatus.PROCESSING).ToString() });
                        break;
                    default:
                        break;
                }

            }

            #endregion

            return ListItem;
        }

    }

}