﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace Booking_System.Helpers
{
    public static class HtmlExtensions
    {
        public static HtmlString NavigationLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName)
        {
            string contextAction = (string)htmlHelper.ViewContext.RouteData.Values["action"];
            string contextController = (string)htmlHelper.ViewContext.RouteData.Values["controller"];

            bool isCurrent =
                string.Equals(contextAction, actionName, StringComparison.CurrentCultureIgnoreCase) &&
                string.Equals(contextController, controllerName, StringComparison.CurrentCultureIgnoreCase);
            var html = new StringBuilder("<li");
            if (isCurrent)
            {
                html.Append(" class=\"active\"");
            }
            html.Append(">")
               .Append(htmlHelper.ActionLink(linkText, actionName, controllerName))
               .Append("</li>");

            return MvcHtmlString.Create(html.ToString());
        }
        
    }
}