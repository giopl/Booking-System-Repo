﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Booking_System.Helpers
{
    public static class AppEnums
    {

        #region cacheEnums
        public enum CacheExpiration
        {
            Midnight = 0,
            Fix = 1,
            Sliding = 2,
            In_2_Hours = 3

        }

        #endregion


        //public enum BookingStatus
        //{
        //    DRAFT = 0,
        //    PROCESSING = 1,
        //    AWAITING_PAYMENT,
        //    CONFIRMED,
        //    EXPIRED = 7,
        //    CANCELLED_OFFICE = 8,
        //    CANCELLED_AGENT = 9

        //}

        public enum BookingMailType
        {
            NEW_TIMELIMIT_ADMIN = 1,
            NEW_CONFIRMED_ADMIN,
            AWAITING_CONFIRMATION_AGENT,
            CLIENT_CONFIRMED_ADMIN,
            DISCOUNT_AGENT,
            DISCUSSION_AGENT,
            DISCUSSION_ADMIN,
            EXTENSION_REQUEST_ADMIN,
            PROFORMA_GENERATED_AGENT,
            VOUCHER_GENERATED_AGENT,
            BOOKING_UPDATED_ADMIN,
            CANCEL_REQUEST_ADMIN
            

        }

        public enum BookingStatus
        {

            DRAFT = 0,
            NEW,
            PROCESSING,
            PROCESSING_NEW_TIMELIMIT,
            DECLINED,
            AWAITING_CONFIRMATION,
            EXPIRED,
            REQUEST_FOR_EXTENSION,
            CONFIRMED,
            AWAITING_PAYMENT,
            COMPLETED,
            CANCEL_REQUEST,
            CANCELED,
            CANCELED_WITH_CHARGES


            //DRAFT = 0,
            //NEW = 1,
            //PROCESSING = 2,            
            //AWAITING_CONFIRMATION = 3,
            //CONFIRMED = 4,
            //AWAITING_PAYMENT = 5,
            ////PROFORMA_GENERATED,
            ////VOUCHER_GENERATED,
            //COMPLETED ,
            //UNVAILABILITY ,
            //CANCELED ,
            //EXPIRED ,
            //REQUEST_TIME_LIMIT_DATE_CHANGE
            ////PROFORMA_SENT
        }

        public enum BookingType
        {
            NO_ACCOMMODATION = 0,
            HOTEL= 1,
            PACKAGE,
            HONEYMOON
        }
        public enum Event
            {
                LOGIN_SUCCESS =0,
                LOGIN_FAILED,
                CREATE,
                UPDATE,
                DELETE,
                UPLOAD
        }
        public enum HotelTab
        {
            DETAILS = 0,
            ROOMS,
            CONTACTS,
            //MARKETS,
            //GUESTS_AGE,
            FEATURES,
            GALLERY,
            SUPPLEMENTS,
            CHARGES,
            PACKAGES,
            FILEUPLOAD,
            PROMOTIONS,
            PROMOTION_SETS,
            BASE_PRICE,
            ROOM_ALLOTMENT


        }

        public enum DateFormatEnum
        {
            YYYY_MM_DD,
            DD_MM_YYYY,
            MM_DD_YYYY
        }

        public enum RoomTab
        {
            
            GALLERY,
            OCCUPANCIES,
            FEATURES,
            BASE_PRICE,
            UNAVAILABILITY

        }


        public enum UserTab
        {
            GALLERY,
            PASSWORD,
            ROLE,
            CREDIT
        }

        public enum VehicleTab
        {
            GALLERY,
            PRICING
        }

        public enum ProviderTab
        {
            GALLERY,
            VEHICLES,
            TRANSFER,
            ACTIVITIES,
            CONTACTS,
            FILEUPLOAD
        }

        public enum ActivityTab
        {
            GALLERY,
            PRICING
        }

        public enum UserPreference
        {
            GALLERY,
            USERPREF,
            CHANGEPASSWORD
        }

        public enum MealPlan
        {
            NO_PLAN = 0,
            BED_AND_BREAKFAST,
            HALF_BOARD,
            FULL_BOARD,
            ALL_INCLUSIVE
                
        }

        public enum FuelType
        {
            GAS = 1,
            DIESEL,
            HYBRID,
            ELECTRIC
            

        }



        public enum GuestType
        {
            INFANT = 1,
            CHILD = 2,
            TEEN = 3,
            ADULT = 4,
            SENIOR = 5
                

        }

        public enum PricingType
        {
            PER_PERSON = 1,
            PER_PERSON_PER_NIGHT,
            PER_ROOM,
            PER_ROOM_PER_NIGHT,
            PER_ITEM,
            PER_NIGHT
        }


        public enum FeaturedSectionEnum
        {
            LAST_MINUTE_DEAL = 1,
            FEATURED_HOTEL_1,
            FEATURED_HOTEL_2,
                FEATURED_HOTEL_3,
                FEATURED_HOTEL_4
            
        }



        public static string DescEnum(string text, bool toTitleCase)
        {

            System.Globalization.TextInfo myTI = new System.Globalization.CultureInfo("en-US", false).TextInfo;

            if (String.IsNullOrEmpty(text))
                return string.Empty;

            if (toTitleCase)
            {
                return myTI.ToTitleCase(text.Trim().ToLower()).Replace('_', ' ');
            }
            else
            {
                return text.Replace('_', ' ');
            }
        }

        
    }


}