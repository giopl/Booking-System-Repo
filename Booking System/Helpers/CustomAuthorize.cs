﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Helpers
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public class CustomAuthorize : AuthorizeAttribute
    {
        private string[] UserProfilesRequired { get; set; }

        public CustomAuthorize(params object[] userProfilesRequired)
        {
            if (userProfilesRequired.Any(p => p.GetType().BaseType != typeof(Enum)))
                throw new ArgumentException("userProfilesRequired");

            this.UserProfilesRequired = userProfilesRequired.Select(p => Enum.GetName(p.GetType(), p)).ToArray();
        }

        public override void OnAuthorization(AuthorizationContext context)
        {
            //Check authorization only if user is valid.
            //On Authorization is done only if the user is valid.
            //If the user is not valid, the base controller will check the session.
            if (Helpers.UserSession.Current.IsValid)
            {
                bool authorized = false;

                foreach (var role in this.UserProfilesRequired)
                {
                    if (Helpers.UserSession.Current.UserRole.ToString() == role)
                    {
                        authorized = true;
                        break;
                    }
                }

                if (!authorized)
                {
                    var url = new UrlHelper(context.RequestContext);
                    string logonUrl = url.Action("Error", "Error", new { Id = 403 });
                    context.Result = new RedirectResult(logonUrl);

                    return;
                }
            }

        }
    }
}