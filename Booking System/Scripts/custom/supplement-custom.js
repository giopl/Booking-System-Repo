﻿
/////Supplement Section///////////////////////////////



//check initial state 
if ($(".allrooms").prop("checked")) { //if per room is checked

    $(".roomtypes").hide(); //show room types 
}
else {
    $(".roomtypes").show(); //show room types 
}


//also monitor live changes 
$('.allrooms').change(function () {
    if (this.checked) {
        $(".roomtypes").hide();
    }
    else {
        $(".roomtypes").show();
    }

});


////////////end of supplement section/////////////////