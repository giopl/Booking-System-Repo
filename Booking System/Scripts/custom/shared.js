﻿


$(document).ready(function () {
    //override for dates validation
    $.validator.methods.date = function (value, element) {
        return this.optional(element) || parseDate(value, "dd-MM-yyyy") !== null;
    };


    //for rich text

    bkLib.onDomLoaded(function () {

        var textareas = document.getElementsByClassName("richtext");
        for (var i = 0; i < textareas.length; i++) {
            var myNicEditor = new nicEditor();
            myNicEditor.panelInstance(textareas[i]);

        }
    });
    
    //bkLib.onDomLoaded(nicEditors.allTextAreas); //on init
    $(window).resize(function (e) {
     
        bkLib.onDomLoaded(function () {

            var textareas = document.getElementsByClassName("richtext");
            for (var i = 0; i < textareas.length; i++) {
                var myNicEditor = new nicEditor();
                myNicEditor.panelInstance(textareas[i]);

            }
        });
    });

    $('select').not('.noselect2').select2();

    //$('.datepicker').datepicker({
    //    format: 'dd-mmm-yyyy'
    //});
    $.fn.datepicker.defaults.format = 'dd-mm-yyyy';
    $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        orientation: "auto"
    });

    $('.datepickerdown').datepicker({
        format: 'dd-mm-yyyy',
        orientation: "bottom"
    });

    //disable all submit buttons when form is submitted
    $('form').on( "submit", function () {
        // On submit disable all submit button
        if ($(this).valid()) {
            $('*[type=submit]  *:not(.nodisable)', this).attr('disabled', 'disabled');
        };
    });




});

function getVirtualDir() {
    var path = $('#svrpath').val();
    return path;
}
