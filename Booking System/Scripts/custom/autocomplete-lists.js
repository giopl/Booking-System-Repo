﻿$(document).ready(function () {

    countries = [
                 "Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antigua and Barbuda",
                 "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh",
                 "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia",
                 "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burma",
                 "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Republic", "Chad",
                 "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic", "Congo, Republic of the",
                 "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti",
                 "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador",
                 "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon",
                 "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guatemala", "Guinea",
                 "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India",
                 "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan",
                 "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos",
                 "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg",
                 "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands",
                 "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Mongolia", "Morocco", "Monaco",
                 "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger",
                 "Nigeria", "Norway", "Oman", "Pakistan", "Panama", "Papua New Guinea", "Paraguay", "Peru",
                 "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino",
                 "Sao Tome", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone",
                 "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain",
                 "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan",
                 "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey",
                 "Turkmenistan", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States",
                 "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe"
    ];

    towns = ['Port Louis', 'Ebene', 'Beau Bassin', 'Rose Hill', 'Curepipe', 'Quatre Bornes', 'Vacoas', 'Phoenix', 'Belle Etoile', 'Albion', 'Amaury', 'Amitie-Gokhoola', 'Arsenal', 'Baie du Cap', 'Baie du Tombeau', 'Bambous', 'Bambous Virieux', 'Bananes', 'Beau Vallon', 'Bel Air Riviere Seche', 'Bel Ombre', 'Belle Vue Maurel', 'Benares', 'Bois Cheri', 'Bois des Amourettes', 'Bon Accueil', 'Bramsthan', 'Brisee Verdiere', 'Britannia', 'Calebasses', 'Camp Carol', 'Camp de Masque', 'Camp de Masque Pave', 'Camp Diable', 'Camp Ithier', 'Camp Thorel', 'Cap Malheureux', 'Cascavelle', 'Case Noyale', 'Centre de Flacq', 'Chamarel', 'Chamouny', 'Chemin Grenier', 'Clemencia', 'Cluny', 'Congomah', 'Coromandel', 'Cottage', 'Creve Coeur', 'D\'epinay', 'Dagotiere', 'Dubreuil', 'ecroignard', 'Esperance', 'Esperance Trebuchet', 'Flic en Flac', 'Fond du Sac', 'Goodlands', 'Grand Baie', 'Grand Bel Air', 'Grand Bois', 'Grand Gaube', 'Grand Sable', 'Grande Retraite', 'Grande Riviere Noire', 'Grande Riviere Sud Est', 'Gros Cailloux', 'L\'Avenir', 'L\'Escalier', 'La Flora', 'La Gaulette', 'La Laura Malenga', 'Lalmatie', 'Laventure', 'Le Hochet', 'Le Morne', 'The Vale', 'Mahebourg', 'Mapou', 'Mare Chicose', 'Mare d\'Albert', 'Mare La Chaux', 'Mare Tabac', 'Medine Camp de Masque', 'Melrose', 'Midlands', 'Moka', 'Montagne Blanche', 'Montagne Longue', 'Morcellement Saint Andre', 'New Grove', 'Notre Dame', 'Nouvelle Decouverte', 'Nouvelle France', 'Olivia', 'Pailles', 'Pamplemousses', 'Petit Bel Air', 'Petit Raffray', 'Petite Riviere', 'Piton', 'Plaine des Papayes', 'Plaine des Roches', 'Plaine Magnien', 'Pointe aux Piments', 'Poste de Flacq', 'Poudre d\'Or', 'Poudre d\'Or Hamlet', 'Providence', 'Quartier Militaire', 'Quatre Cocos', 'Quatre Soeurs', 'Queen Victoria', 'Richelieu', 'Ripailles', 'Riviere des Anguilles', 'Riviere des Creoles', 'Riviere du Poste', 'Riviere du Rempart', 'Roche Terre', 'Roches Noires', 'Rose Belle', 'Saint Aubin', 'Saint Hubert', 'Saint Julien Village', 'Saint Julien d\'Hotman', 'Saint Pierre', 'Sebastopol', 'Seizieme Mille', 'Souillac', 'Surinam', 'Tamarin', 'Terre Rouge', 'Triolet', 'Trois Boutiques', 'Trou aux Biches', 'Trou d\'Eau Douce', 'Tyack', 'Union Park', 'Verdun', 'Vieux Grand Port', 'Ville Bague']


    $(".country-autocomplete").autocomplete({
        source: [countries]
    });



    $(".town-autocomplete").autocomplete({
        source: [towns]
    });







});
