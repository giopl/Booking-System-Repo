﻿
$(document).ready(function () {
    $('.phonenum')
            .intlTelInput({
                utilsScript: $('#svrpath').val() + 'Scripts/plugins/utils.js',
                autoPlaceholder: true,
                nationalMode: true,
                preferredCountries: ['mu'],
                numberType: "FIXED_LINE_OR_MOBILE"
            });
    
   


    $.validator.addMethod('phonenum', function () {
        if ($('.phonenum').intlTelInput("isValidNumber")) {
            
            return true;
        } else
        {
            if ($('.phonenum').val().trim() != "")
            {
              
                return false;
            }
            else
            {
                
                return true;
            }
           
        }
    }, "Number is not valid");
    jQuery.validator.classRuleSettings.phonenum = { phonenum: true };

});
