﻿


///////////////////Home page js////////////////////////

// parse a date in yyyy-mm-dd format
function parseFullDate(dateinput) {
    var datepart = dateinput.split('-');
    // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
    return new Date(datepart[2], datepart[1] - 1, datepart[1]); // Note: months are 0-based
}

$(document).ready(function () {

    $.fn.datepicker.defaults.format = 'dd-mm-yyyy';


    //if a date is already set when page is loaded, trigger the validation manually
    if ($('.from_date').val() != "") {
        var startDate = parseFullDate($('.from_date').val());

        var endDate = parseFullDate($('.to_date').val());
        $(".from_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            startDate: '0d'
        });
        $(".to_date").datepicker({
            format: 'dd-mm-yyyy',
            autoclose: true,
            startDate: '0d'
        });

        $('.to_date').datepicker('setStartDate', startDate);
        $('.from_date').datepicker('setEndDate', endDate);
    }

    //otherwise initialise the datepickers
    $(".from_date").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        startDate: '0d'
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('.to_date').datepicker('setStartDate', startDate);
    }).on('clearDate', function (selected) {
        $('.to_date').datepicker('setStartDate', null);
    });

    $(".to_date").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
    }).on('changeDate', function (selected) {
        var endDate = new Date(selected.date.valueOf());
        $('.from_date').datepicker('setEndDate', endDate);
    }).on('clearDate', function (selected) {
        $('.from_date').datepicker('setEndDate', null);
    });

    //$(function () {
    //    $(document).scrollTop($("#top").offset().top);
    //});


    $(".package_start").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        startDate: '0d'
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
    });



    $(".honeymoon_start").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        startDate: '0d'
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
    });




//for hotel
    $('body').on('click', '.room', function () {
        var roomnum = $(this).data('num');


        $("div[class^='row showroom']").hide();

        for (var i = 2; i <= roomnum; i++) {

            //alert($('.showroom-' + i));
            $('.showroom-' + i).show();
        }
    });



    $('body').on('change', '.showchild', function () {

        var rn = $(this).data('roomnum');

        var v = $(this).val();

        $("div[class^='showchild_" + rn + "']").hide();

        for (var i = 1; i <= v; i++) {

            $('.showchild_' + rn + '_' + i).show();
        }

    });
    //$('.childage').niceselect();


    //Package

    $('body').on('click', '.roomPkg', function () {
        var roomnum = $(this).data('num');
     

        $("div[class^='row showroomPkg']").hide();

        for (var i = 2; i <= roomnum; i++) {

            //alert($('.showroom-' + i));
            $('.showroomPkg-' + i).show();
        }
    });



    $('body').on('change', '.showchildPkg', function () {

        var rn = $(this).data('roomnumpkg');
                var v = $(this).val();

        $("div[class^='showchildPkg_" + rn + "']").hide();

        for (var i = 1; i <= v; i++) {

            $('.showchildPkg_' + rn + '_' + i).show();
        }

    });




});
function limitAdult(input) {
    //if (input.value < 0) input.value = 0;
    //if (input.value > 5) input.value = 5;

    var charCode = input.keyCode;
    //alert(charCode);
    //Non-numeric character range
    if (charCode < 48 || charCode > 53)
        return false;

}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}



////End of Home Page JS///////////////