﻿

///////////////Search Results////////////
$(document).ready(function () {
    //assign the text value of the attribute on which sorting is made to span
    //if logic is apply to anything else than a span, the .text needs to be replaced by html or val()
    $("a[data-sort]").on("click", function () {
       
        $("span.sorttype").text(" by " + this.text);
    });


    /*Script to dynamically paginate results*/
    var PagedList = new List('result-list', {
        valueNames: ['booking-item',
         { attr: 'data-star-rating', name: 'booking-item-rating' },
          { attr: 'data-region', name: 'booking-item-region' },
            { attr: 'data-price', name: 'booking-item-price' }],
        page: 5,
        plugins: [ListPagination({})]
    });


    //generic method for Filter trigger
    function TriggerFilter() {

        var fromPrice = $('#price-slider').prop("value").split(';')[0];
        var toPrice = $('#price-slider').prop("value").split(';')[1];


        ///logic to check values of checkbox  (for star rating)
        var checkstars = new Array();
        $("input.i-check-s").each(function () {
            if ($(this).iCheck('update')[0].checked) {
                checkstars.push(this.value);
            }

        });

        ///logic to check values of checkbox  (for region)
        var checkregion = new Array();
        $("input.i-check-r").each(function () {
            if ($(this).iCheck('update')[0].checked) {
                checkregion.push(this.value);
            }

        });
        //filter based on the values passed 
        PagedList.filter(function (item) {
            if (
                    $.inArray(item.values()['booking-item-rating'], checkstars) != -1 && $.inArray(item.values()['booking-item-region'], checkregion) != -1
                       &&
                    (Number(item.values()['booking-item-price']) >= fromPrice && Number(item.values()['booking-item-price']) <= toPrice)
                ) {
               
                return true;
            } else {
                
                return false;
            }
        });      
    }

    TriggerFilter();


    //monitor change to span storing values  since monitroing original input does not work...pfff
    $(".irs-from , .irs-to").on("DOMSubtreeModified", function () {

        TriggerFilter();

    });



    ///logic for special checkbox when a checkbox is toggled 
    $('input.i-check').on('ifToggled', function (event) {

        TriggerFilter();

        return false;
    });

});

///////end of Search Results//////////