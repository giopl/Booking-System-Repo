﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using Booking_System.Models.ViewModel;
using EntityFramework.Extensions;

namespace Booking_System.Controllers
{
    public class TransfersController : AdminController
    {
        private ABSDBEntities db = new ABSDBEntities();

        // GET: Transfers
        public ActionResult ListTransfers()
        {
            return View(db.transfers.ToList());
        }

        // GET: Transfers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transfer transfer = db.transfers.Find(id);
            if (transfer == null)
            {
                return HttpNotFound();
            }
            return View(transfer);
        }

        // GET: Transfers/Create
        public ActionResult CreateTransfer()
        {
            ViewBag.currency_id = new SelectList(db.currencies.Where(x=>x.is_active), "currency_id", "currency_code");
            return View();
        }

        // POST: Transfers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTransfer(TransferViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.Transfer.is_active = true;
                db.transfers.Add(model.Transfer);
                db.SaveChanges();
                SaveAudit(Helpers.AppEnums.Event.CREATE, "Transfer", model.Transfer.transfer_id);

                model.InitialTransferPricing.is_active = true;
                model.InitialTransferPricing.is_selling_price= true;
                model.InitialTransferPricing.transfer_id = model.Transfer.transfer_id;
                model.InitialTransferPricing.currency_id = model.currency_id;

                db.transferPricings.Add(model.InitialTransferPricing);
                db.SaveChanges();
                SaveAudit(Helpers.AppEnums.Event.CREATE, "TransferPricing", model.InitialTransferPricing.transfer_id);


                return RedirectToAction("ListTransfers");
            }
            ViewBag.currency_id = new SelectList(db.currencies.Where(x=>x.is_active), "currency_id", "currency_code");
            return View(model);
        }

        // GET: Transfers/Edit/5
        public ActionResult EditTransfer(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transfer transfer = db.transfers.Find(id);
            if (transfer == null)
            {
                return HttpNotFound();
            }

            var allcurrencies = db.currencies.Where(x => x.is_active);
            List<currency> currencies = new List<currency>();
            List<transferPricing> transferpricings = db.transferPricings.Where(x => x.transfer_id == id).ToList();
            foreach (var curr in allcurrencies)
            {
                if (!transferpricings.Exists(s => s.currency_id == curr.currency_id))
                {
                    currencies.Add(curr);
                }                
            }
            ViewBag.currency_id = new SelectList(currencies, "currency_id", "currency_code");

            return View(transfer);
        }

        // POST: Transfers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTransfer([Bind(Include = "transfer_id,name,description,category,is_active,is_upgrade")] transfer transfer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transfer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListTransfers");
            }
            return View(transfer);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTransferPricing([Bind(Include = "transfer_id,transfer_pricing_id,child_price,teen_price,adult_price,currency_id")] transferPricing transferPricing)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transferPricing).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("EditTransfer", new { id = transferPricing.transfer_id});
            }
            return View(transferPricing);
        }

        // GET: Transfers/Delete/5
        public ActionResult DeleteTransferPricing(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transferPricing transferPricing = db.transferPricings.Find(id);
            
            var transfer_id = transferPricing.transfer_id;
            if (transferPricing == null)
            {
                return HttpNotFound();
            }
            db.transferPricings.Remove(transferPricing);
            db.SaveChanges();

            return RedirectToAction("EditTransfer", new { id = transfer_id});
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTransferPricing([Bind(Include = "transfer_pricing_id,transfer_id,currency_id,child_price,teen_price,adult_price,is_active,provider_id,is_selling_price")] transferPricing transferPricing)
        {
            if (ModelState.IsValid)
            {
                transferPricing.is_active = true;
                db.transferPricings.Add(transferPricing);
                db.SaveChanges();
                return RedirectToAction("EditTransfer", new { id = transferPricing.transfer_id});
            }

            ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "currency_code", transferPricing.currency_id);
            ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", transferPricing.provider_id);
            ViewBag.transfer_id = new SelectList(db.transfers, "transfer_id", "name", transferPricing.transfer_id);
            return View(transferPricing);
        }

        // GET: Transfers/Delete/5
        public ActionResult DeleteTransfer(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transfer transfer = db.transfers.Find(id);
            if (transfer == null)
            {
                return HttpNotFound();
            }
            return View(transfer);
        }

        // POST: Transfers/Delete/5
        [HttpPost, ActionName("DeleteTransfer")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteTransferConfirmed(int id)
        {
            transfer transfer = db.transfers.Find(id);

            db.transferPricings.Where(x => x.transfer_id == id).Delete();
            db.transfers.Remove(transfer);
            db.SaveChanges();
            return RedirectToAction("ListTransfers");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
