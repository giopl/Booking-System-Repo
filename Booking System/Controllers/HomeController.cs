﻿using Booking_System.Helpers;
using Booking_System.Models;
using Booking_System.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Controllers
{
    public class HomeController : BaseController
    {

         log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ActionResult Index()
        {
            log.Info("Hello World");
            //return View();
            return RedirectToAction("Menu");
        }




        public ActionResult Error()
        {

            //var errLog = ViewBag.ErrorLog;
            var errLog = TempData["errorLog"] as ErrorLog;

            log.Debug("[ShowError]");


            return View(errLog);
        }


        public ActionResult Menu()
        {
            return View();
        }

        public ActionResult Logout()
        {
            UserSession.Current.ClearSession();
           return RedirectToAction("Index", "Login");

        }
    }
}