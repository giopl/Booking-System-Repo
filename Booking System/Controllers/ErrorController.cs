﻿using Booking_System.Helpers;
using Booking_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Controllers
{
    [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator)]
    public class ErrorController : Controller
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ABSDBEntities db = new ABSDBEntities();

        // GET: Error

        public ActionResult Index()
        {

            return View(db.errorLogs.OrderByDescending(x=>x.error_date_time).Take(5));
        }


        public ActionResult Details(int? id)
        {
            errorLog errlog;
            if(id.HasValue)
            {

            errlog = db.errorLogs.Find(id);
            }
            else
            {
                errlog = db.errorLogs.OrderByDescending(x=>x.error_date_time).Take(1).FirstOrDefault();
            }


            return View(errlog);

        }






        public ActionResult DBValidationError()
        {
            var dbException = TempData["dbUpdateException"];

            ViewBag.DBException = dbException;
            return View();
        }



        public ActionResult GeneralError()
        {
            var gException = TempData["Exception"];

            ViewBag.Exception = gException;
            return View();
        }


        public ActionResult Error(string id = "")
        {
            ViewBag.ErrorCode = id;

            if (id == "401")
                return RedirectToAction("Index", "Login");


            return View();
        }
    }
}