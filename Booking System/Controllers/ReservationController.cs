﻿using Booking_System.Helpers;
using Booking_System.Models;
using Booking_System.Models.ViewModel;
using Booking_System.Services;
using Booking_System.Services.Abstract;
using EntityFramework.Extensions;
using Rotativa.Core.Options;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Controllers
{
    [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Planning, UserRoles.Sales)]
    public class ReservationController : BaseController
    {

        private ABSDBEntities db = new ABSDBEntities();
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: Reservation
        public ActionResult ViewReservations()
        {
            var bookings = db.bookings.ToList();

            var filteredLst = GetFilteredBookings(bookings);

            bool filterApplied = false;
            if (bookings.Count!= filteredLst.Count)
            {
                filterApplied = true;
            }
            ViewBag.FilterApplied = filterApplied;

            ViewBag.Users = db.users.ToList();

            return View(filteredLst);
        }

        private List<booking> GetFilteredBookings(List<booking> bk)
        {
            try
            {
                List<booking> filteredList = bk;

                var searchCriteria = Helpers.UserSession.Current.SearchItem;
                if (searchCriteria != null && bk != null)
                {
                    if (!String.IsNullOrWhiteSpace(searchCriteria.booking_ref))
                    {
                        filteredList = filteredList.Where(e => e.booking_ref == searchCriteria.booking_ref).ToList();
                    }

                    if (!String.IsNullOrWhiteSpace(searchCriteria.booking_name))
                    {
                        filteredList = filteredList.Where(e => e.booking_name == searchCriteria.booking_name).ToList();
                    }


                    if (searchCriteria.booking_id.HasValue)
                    {
                        filteredList = filteredList.Where(e => e.booking_id == searchCriteria.booking_id).ToList();
                    }


                    if (searchCriteria.checkin_date.HasValue)
                    {
                        filteredList = filteredList.Where(e => e.checkin_date.HasValue && e.checkin_date.Value == searchCriteria.checkin_date).ToList();
                    }

                    if (searchCriteria.checkout_date.HasValue)
                    {
                        filteredList = filteredList.Where(e => e.checkout_date.HasValue && e.checkout_date.Value == searchCriteria.checkout_date).ToList();
                    }

                    if (searchCriteria.flight_date.HasValue)
                    {
                        filteredList = filteredList.Where(e => e.booking_guest != null && e.booking_guest.Count(f => f.arrival_datetime.HasValue && f.arrival_datetime.Value.Date == searchCriteria.flight_date.Value.Date) > 0).ToList();
                    }

                    if (searchCriteria.num_nights.HasValue)
                    {
                        filteredList = filteredList.Where(e => e.num_nights.HasValue && e.num_nights.Value == searchCriteria.num_nights).ToList();
                    }

                    if (searchCriteria.status.HasValue)
                    {
                        filteredList = filteredList.Where(e => e.status.HasValue && e.status.Value == searchCriteria.status).ToList();
                    }

                    if (searchCriteria.agent_user_id > 0)
                    {
                        filteredList = filteredList.Where(e => e.agent_user_id == searchCriteria.agent_user_id).ToList();
                    }

                    if (searchCriteria.backoffice_user_id.HasValue)
                    {
                        filteredList = filteredList.Where(e => e.backoffice_user_id.HasValue && e.backoffice_user_id.Value == searchCriteria.backoffice_user_id).ToList();
                    }

                    if (!String.IsNullOrWhiteSpace(searchCriteria.booking_file_number))
                    {
                        filteredList = filteredList.Where(e => e.booking_file_number == searchCriteria.booking_file_number).ToList();
                    }

                    //if (searchCriteria.discounted_by_user_id.HasValue)
                    //{
                    //    filteredList = filteredList.Where(e => e.discounted_by_user_id.HasValue && e.discounted_by_user_id.Value == searchCriteria.discounted_by_user_id).ToList();
                    //}

                }

                return filteredList;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public string BookingSummaryHtml(int bookingid)
        {
            try
            {
                var booking = db.bookings.Find(bookingid);
                return RenderRazorViewToString(this.ControllerContext, "_BookingSummaryBox", booking);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult SetSearchFilters(SearchItem SearchItem)
        {
            UserSession.Current.SearchItem = SearchItem;

            return RedirectToAction("ViewReservations");
        }

        public ActionResult ClearSearchFilters()
        {
            UserSession.Current.SearchItem = null;

            return RedirectToAction("ViewReservations");
        }


        /// <summary>
        /// Generates either a proforma or voucher
        /// </summary>
        /// <param name="id">booking id</param>
        /// <param name="doctype">P=proforma (default), V=voucher</param>
        /// <returns></returns>
        public ActionResult GenerateDocument(int id, string doctype = "P", DateTime? proformaExpiryDate = null, string comments = "", double proformaHandlingFee = 0, double proformaIBFee = 0)
        {
            try
            {
                IReservationService reservationService = new ReservationService();

                if(doctype=="P")
                {
                    reservationService.SaveProformaInfo(id, proformaExpiryDate, comments, proformaHandlingFee, proformaIBFee);
                    var proforma = db.bookingProformas.Where(e => e.booking_id == id);

                    var actionPDF = new Rotativa.MVC.ViewAsPdf("GenerateProforma", proforma.First());//some route values)
                    actionPDF.RotativaOptions.PageSize = Size.A4;
                    actionPDF.RotativaOptions.PageOrientation = Orientation.Portrait;

                    byte[] applicationPDFData = actionPDF.BuildPdf(ControllerContext);

                    reservationService.SendBookingMail(id, AppEnums.BookingMailType.PROFORMA_GENERATED_AGENT,applicationPDFData);
                }
                else
                {
                    reservationService.SaveVoucherInfo(id);
                    var voucher = db.bookingVouchers.Where(e => e.booking_id == id);
                    //var hotelid = voucher.Any()?voucher.FirstOrDefault().
                    
                    var actionPDF = new Rotativa.MVC.ViewAsPdf("GenerateVoucher", voucher.First());//some route values)
                    actionPDF.RotativaOptions.PageSize = Size.A4;
                    actionPDF.RotativaOptions.PageOrientation = Orientation.Portrait;

                    byte[] applicationPDFData = actionPDF.BuildPdf(ControllerContext);

                    reservationService.SendBookingMail(id, AppEnums.BookingMailType.VOUCHER_GENERATED_AGENT,applicationPDFData);
                }

                return RedirectToAction("ReservationDetails", new { id = id });

            }
            catch (Exception)
            {

                throw;
            }

        }


        // GET: Reservation/Details/5
        public ActionResult ReservationDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);
            refurbishbooking(booking);

            if (booking == null)
            {
                return HttpNotFound();
            }

            var notes = db.notes.Where(e => e.item_type == "RESERVATIONS" && e.item_id == booking.booking_id);

            ViewBag.Notes = notes;

            return View(booking);
        }

        public ActionResult ReservationDetails2(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);

            refurbishbooking(booking);


            if (booking == null)
            {
                return HttpNotFound();
            }

            var notes = db.notes.Where(e => e.item_type == "RESERVATIONS" && e.item_id == booking.booking_id);

            ViewBag.Notes = notes;

            return View(booking);
        }


        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditSupplement(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);

            if (booking == null)
            {
                return HttpNotFound();
            }
            refurbishbooking(booking);

            IReservationService res = new ReservationService();
            List<GetSupplementsResult> supps = res.GetReservationSupplements(id.Value, booking);

            ViewBag.Booking = booking;
            return View(supps);

        }

        [HttpPost]
        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditSupplement(int booking_id, string[] mealplan, string[] supplement, bool[] chosen)
        {
            try
            {
                IReservationService res = new ReservationService();
                res.SaveReservationSupplements(booking_id, mealplan, supplement, chosen);

                return RedirectToAction("ReservationDetails", new { id = booking_id });
            }
            catch (Exception e)
            {

                throw;
            }
        }

        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditActivities(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);
            if (booking == null)
            {
                return HttpNotFound();
            }

            refurbishbooking(booking);

            var vBooking = db.viewBookings.Where(x => x.booking_ref == booking.booking_ref).FirstOrDefault();
            if (vBooking != null)
            {
                ViewBag.VBooking = vBooking;
            }

            ViewBag.BookingRef = booking.booking_ref;
            ViewBag.Booking = booking;

            IReservationService res = new ReservationService();
            List<viewActivity> vActivity = res.GetReservationActivities(id.Value, booking);

            return View(vActivity);

        }

        [HttpPost]
        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditActivities(int booking_id, int[] activity_id, int[] transfer_type, int[] child, int[] adult, int[] activity_pricing_id)
        {
            try
            {

                IReservationService res = new ReservationService();
                res.SaveReservationActivities(booking_id, activity_id, transfer_type, child, adult, activity_pricing_id, false, null, null);


                return RedirectToAction("ReservationDetails", new { id = booking_id });

            }
            catch (Exception e)
            {

                throw;
            }
        }

        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditRental(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);

            if (booking == null)
            {
                return HttpNotFound();
            }
            refurbishbooking(booking);

            var vBooking = db.viewBookings.Where(x => x.booking_ref == booking.booking_ref).FirstOrDefault();
            if (vBooking != null)
            {
                ViewBag.VBooking = vBooking;
            }

            IReservationService res = new ReservationService();
            IList<vehicle> vehiclesToDisplay = res.GetReservationRentals(id.Value, booking);

            ViewBag.Booking = booking;

            return View(vehiclesToDisplay);
        }

        [HttpPost]
        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditRental(int booking_id, double[] price, double[] pricewithdriver, int[] days, int[] vehicle_id, bool[] hasDriver)
        {
            try
            {
                IReservationService res = new ReservationService();
                res.SaveReservationRentals(booking_id, price, pricewithdriver, days, vehicle_id, false, hasDriver);

                return RedirectToAction("ReservationDetails", new { id = booking_id });

            }
            catch (Exception e)
            {

                throw;
            }

        }

        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditTransfer(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);

            if (booking == null)
            {
                return HttpNotFound();
            }

            refurbishbooking(booking);

            IReservationService res = new ReservationService();
            IList<transferPricing> transfers = res.GetReservationTransfers(id.Value, booking);

            int currency_id = 0;
            market m = db.markets.Find(booking.market_id);
            if (m != null)
            {
                currency_id = m.currency_id;
            }



            var pricings = db.transferPricings.Where(x => x.currency_id == currency_id).ToList();

            ViewBag.TransferPricings = pricings;

            ViewBag.Booking = booking;

            return View(transfers);
        }

        [HttpPost]
        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditTransfer(int booking_id, int transfer)
        {
            try
            {

                IReservationService res = new ReservationService();
                res.SaveReservationTransfers(booking_id, transfer, null, null, false);

                return RedirectToAction("ReservationDetails", new { id = booking_id });

            }
            catch (Exception)
            {

                throw;
            }
        }

        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditBookingDetails(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(id);
                if (booking == null)
                {
                    return HttpNotFound();
                }
                refurbishbooking(booking);
                var booking_guests = db.bookingGuests.Where(x => x.booking_id == id);

                ViewBag.Booking = booking;
                return View(booking_guests);

            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditBookingDetails(BookingGuestViewModel guestparam)

        {
            try
            {

                IReservationService res = new ReservationService();
                res.SaveGuestParam(guestparam, false,true,true);

                return RedirectToAction("ReservationDetails", new { id = guestparam.booking_id });

            }
            catch (Exception e)
            {

                throw;
            }
        }

        [HttpPost]
        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator)]
        public ActionResult AddDiscountToBooking(int? booking_id, double? discount_amt, string discount_reason)
        {
            try
            {
                if (booking_id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(booking_id);
                if (booking == null)
                {
                    return HttpNotFound();
                }

                if (discount_amt.HasValue)
                {
                    double oldDiscount = booking.discount_amt.HasValue ? booking.discount_amt.Value : 0;

                    double differenceInDiscount = discount_amt.Value - oldDiscount;
                    //booking.discount= discount_amt.Value;
                    booking.discount_amt = discount_amt.Value;
                    booking.discount_reason = discount_reason;
                    booking.discounted_by_user_id = Helpers.UserSession.Current.UserId;

                    db.Entry(booking).State = EntityState.Modified;

                    //Refund the agent if the booking was done on credit
                    if (booking.credit_amt.HasValue)
                    {
                        booking.credit_amt = booking.credit_amt.Value - differenceInDiscount;

                        //refund the agent
                        var agent = db.users.Find(booking.agent_user_id);
                        if (agent != null)
                        {
                            if (agent.credit_used.HasValue)
                            {
                                double agentCredit = agent.credit_used.Value - differenceInDiscount;
                                if (agentCredit < 0)
                                {
                                    agentCredit = 0;
                                }

                                agent.credit_used = agentCredit;
                                db.Entry(agent).State = EntityState.Modified;
                            }
                        }
                    }

                    IReservationService reservationService = new ReservationService();
                    reservationService.SendBookingMail(booking_id.Value, AppEnums.BookingMailType.DISCOUNT_AGENT);

                    db.SaveChanges();
                }

                return RedirectToAction("ReservationDetails", new { id = booking_id });

            }
            catch (Exception)
            {

                throw;
            }
        }

       

        [HttpPost]
        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales, UserRoles.Agent)]
        public ActionResult UpdateReservationStatus(int? booking_id, int? status, DateTime? timelimitdate = null, float CancellationFEE=0)
        {
            try
            {


                if (booking_id == null || status == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(booking_id);
                if (booking == null)
                {
                    return HttpNotFound();
                }

                if(booking.is_timelimit && timelimitdate == null && booking.StatusEnum == AppEnums.BookingStatus.AWAITING_CONFIRMATION)
                {
                    ViewBag.ValidationError = "Please Select a time limit Date";
                    return RedirectToAction("ReservationDetails", new { id = booking_id });
                }
                //call method to save reservation details
                IReservationService reservationService = new ReservationService();
                reservationService.SaveReservation(booking_id, status, timelimitdate, CancellationFEE);

                return RedirectToAction("ReservationDetails", new { id = booking_id });

                
            }

            catch (Exception e)
            {

                throw;
            }
        }




        [HttpPost]
        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator)]
        public ActionResult AddReservationPayment(int? booking_id, double? payment, string details, int? paymentMethod, string paymentDate)
        {
            try
            {
                if (booking_id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(booking_id);
                if (booking == null)
                {
                    return HttpNotFound();
                }

                if (payment.HasValue)
                {
                    DateTime dt;

                    if (!DateTime.TryParse(paymentDate, out dt))
                    {
                        dt = DateTime.Now;
                    }

                    bookingPayment paymentReceived = new bookingPayment();
                    paymentReceived.booking_id = booking_id.Value;
                    paymentReceived.payment_amt = payment;
                    paymentReceived.payment_details = details;
                    paymentReceived.payment_date = dt;
                    paymentReceived.received_by = Helpers.UserSession.Current.UserId;
                    paymentReceived.payment_method = paymentMethod;

                    db.bookingPayments.Add(paymentReceived);

                    booking.paid_amt = (booking.paid_amt.HasValue ? booking.paid_amt.Value : 0) + payment.Value;

                    //Refund the agent if the booking was done on credit
                    //if (booking.credit_amt.HasValue)
                    //{
                    //    if (booking.credit_amt.Value > 0 && payment.Value > 0)
                    //    {
                    //        booking.credit_amt = booking.credit_amt.Value - payment.Value;

                    //        //refund the agent
                    //        var agent = db.users.Find(booking.agent_user_id);
                    //        if (agent != null)
                    //        {
                    //            if (agent.credit_used.HasValue && agent.credit_used.Value > 0)
                    //            {
                    //                agent.credit_used = agent.credit_used.Value - payment.Value;
                    //                db.Entry(agent).State = EntityState.Modified;
                    //            }
                    //        }
                    //    }
                    //}

                    //if(booking.TotalDueAmount <= 0 )
                    //{
                    //    booking.status = (int)AppEnums.BookingStatus.CONFIRMED;

                    //    db.bookingVouchers.Where(x => x.booking_id == booking_id).Delete();

                    //    IReservationService reservationService = new ReservationService();

                    //    //var voucher = reservationService.SaveVoucherInfo(booking.booking_id);


                    //} else
                    //{

                    //}

                    db.Entry(booking).State = EntityState.Modified;

                    db.SaveChanges();


                }

                return RedirectToAction("ReservationDetails", new { id = booking_id });

            }

            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost]
        [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult AddReservationNote(int? booking_id, string note, string sender="admin")
        {
            try
            {
                if (booking_id == null || string.IsNullOrWhiteSpace(note))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(booking_id);
                if (booking == null)
                {
                    return HttpNotFound();
                }

                Booking_System.Models.note notes = new Models.note();
                notes.user_id = Helpers.UserSession.Current.UserId;
                notes.item_id = booking_id.Value;
                notes.item_type = "RESERVATIONS";
                notes.detail = note;
                notes.create_date = DateTime.Now;

                db.notes.Add(notes);
                db.SaveChanges();


                IReservationService reservationService = new ReservationService();
                //reservationService.SendBookingMail(booking_id.Value, Helpers.UserSession.Current.UserRole == UserRoles.Agent ?AppEnums.BookingMailType.DISCUSSION_AGENT: AppEnums.BookingMailType.DISCUSSION_ADMIN);

                reservationService.SendBookingMail(booking_id.Value, sender=="admin" ? AppEnums.BookingMailType.DISCUSSION_AGENT : AppEnums.BookingMailType.DISCUSSION_ADMIN);
                    



                return RedirectToAction("ReservationDetails", new { id = booking_id });

            }

            catch (Exception)
            {

                throw;
            }
        }



        #region Receipt and vouchers

  
        public ActionResult DownloadVoucher(int bookingid)
        {
            try
            {
                var book = db.bookings.Find(bookingid);
                bool isCorrectAgent = book != null && book.agent_user_id == UserSession.Current.UserId;
                if (UserSession.Current.UserRole != UserRoles.Agent || isCorrectAgent)
                {
                    var vouchers = db.bookingVouchers.Where(v => v.booking_id == bookingid).ToList();

                    if (vouchers.Count() > 1)
                    {
                        var voucher = vouchers.FirstOrDefault();
                        return new Rotativa.MVC.ViewAsPdf("GenerateVoucher", voucher);
                    }

                    return View();
                }
                else
                {
                    return RedirectToAction("Error", "Error", new { Id = 403 });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        //public ActionResult GenerateVoucher(int id)
        //{
        //    try
        //    {
                

        //        var booking = db.bookings.Find(id);
        //        db.bookingVouchers.Where(b => b.booking_id == id).Delete();
        //        //booking.status = (int)AppEnums.BookingStatus.VOUCHER_GENERATED;
        //        db.Entry(booking).State = EntityState.Modified;
        //        db.SaveChanges();
                
        //        var voucher = SaveVoucherInfo(id);
               
        //        return new Rotativa.MVC.ViewAsPdf("GenerateVoucher", voucher);

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}


        public ActionResult DownloadProforma(int? bookingid)
        {
            try
            {
               
                if (bookingid == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var book = db.bookings.Find(bookingid);
                bool isCorrectAgent = book != null && book.agent_user_id == UserSession.Current.UserId;
                if (UserSession.Current.UserRole != UserRoles.Agent || isCorrectAgent)
                {
                    var proformas = db.bookingProformas.Where(x => x.booking_id == bookingid);

                    if (proformas.Count() > 0)
                    {
                        var proforma = proformas.OrderByDescending(x => x.issue_date).FirstOrDefault();
                        return new Rotativa.MVC.ViewAsPdf("GenerateProforma", proforma);
                    }
                    return View();
                }
                else
                {
                    return RedirectToAction("Error", "Error", new { Id = 403 });
                }
                
            }
            catch (Exception)
            {

                throw;
            }
        }

        //public ActionResult GenerateProforma(int? id, DateTime? expiryDate, string comments)
        //{
        //    try
        //    {
        //        if (id == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }

        //        var booking = db.bookings.Find(id);
        //        if (booking == null)
        //        {
        //            return HttpNotFound();
        //        }

        //        if(!expiryDate.HasValue)
        //        {
        //            return RedirectToAction("ReservationDetails", new { id = id.Value });
        //        }


        //        var book = SaveProformaInfo(booking.booking_id, expiryDate, comments);
        //        if(booking.is_credit)
        //        {
        //            //booking.status = (int)AppEnums.BookingStatus.PROFORMA_GENERATED;
        //        } else
        //        {
        //            booking.status = (int)AppEnums.BookingStatus.AWAITING_PAYMENT;
        //        }

        //        db.Entry(booking).State = EntityState.Modified;
        //        db.SaveChanges();

        //        //return new Rotativa.MVC.ViewAsPdf("GenerateProforma", book);
        //            return RedirectToAction("ReservationDetails", new { id = id.Value });

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}


        //public ActionResult GenerateProFormaAndVoucher(int?id)
        //{
            
        //        try
        //        {
        //            if (id == null)
        //            {
        //                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //            }

        //            var booking = db.bookings.Find(id);

        //            if (booking == null)
        //            {
        //                return HttpNotFound();
        //            }

        //        //delete all existing proforma 
        //        IReservationService reservationService = new ReservationService();


        //        var book = db.bookings.Find(id);
        //        bool isCorrectAgent = book != null && book.agent_user_id == UserSession.Current.UserId;
        //        if (UserSession.Current.UserRole != UserRoles.Agent || isCorrectAgent)
        //        {

        //            db.bookingProformas.Where(x => x.booking_id == id.Value).Delete();
        //            var proforma = reservationService.SaveProformaInfo(booking.booking_id, booking.checkin_date, string.Empty);


        //            //delete all existing vouchers
        //            db.bookingVouchers.Where(x => x.booking_id == id.Value).Delete();
        //            //create new voucher
        //            var voucher = reservationService.SaveVoucherInfo(id.Value);



        //            booking.status = (int)AppEnums.BookingStatus.COMPLETED;
        //            db.Entry(booking).State = EntityState.Modified;
        //            db.SaveChanges();

        //            return RedirectToAction("ReservationDetails", new { id = id.Value });
        //        }
        //        else
        //        {
        //            return RedirectToAction("Error", "Error", new { Id = 403 });
        //        }

        //        }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        /// <summary>
        /// Method to send Voucher to agent by Name
        /// </summary>
        /// <param name="agentemail"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult SendVoucher(int? id,bool resend = false)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var book = db.bookings.Find(id);

                if (book == null)
                {
                    return HttpNotFound();
                }

                bool isCorrectAgent = book != null && book.agent_user_id == UserSession.Current.UserId;
                if (UserSession.Current.UserRole != UserRoles.Agent || isCorrectAgent)
                {
                    var voucher = book.booking_voucher.FirstOrDefault();
                    if (voucher != null)
                    {
                        var actionPDF = new Rotativa.MVC.ViewAsPdf("GenerateVoucher", voucher);//some route values)
                        actionPDF.RotativaOptions.PageSize = Size.A4;
                        actionPDF.RotativaOptions.PageOrientation = Orientation.Portrait;

                        byte[] applicationPDFData = actionPDF.BuildPdf(ControllerContext);
                        string FileName = book.booking_id.ToString();
                        string Subject = String.Concat(book.booking_id, "-", book.booking_name);
                        if (!string.IsNullOrEmpty(book.booking_file_number))
                        {
                            Subject = String.Concat(Subject, book.booking_file_number);
                        }

                        book.agent = db.users.Find(book.agent_user_id);

                        if (book.agent != null)
                        {
                            string booktext = String.Concat("Dear ", book.agent.FullName, ",", "<br/><br/>", "Please find attached voucher for ", FileName, "<br/><br/>");
                            EmailHelper eh = new EmailHelper();
                            eh.SendEmail(book.agent.email, Subject, booktext, FileName, applicationPDFData, MediaTypeNames.Application.Pdf);
                        }
                        if (!resend)
                        {
                            voucher.finalized = true;
                            db.Entry(voucher).State = EntityState.Modified;

                            //Set booking to completed
                            book.status = 6;
                            db.Entry(book).State = EntityState.Modified;

                            db.SaveChanges();
                        }
                    }
                    return RedirectToAction("ReservationDetails", new { id = id });
                }
                else
                {
                    return RedirectToAction("Error", "Error", new { Id = 403 });
                }

            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Method to send proforma to agent by mail
        /// </summary>     
        /// <param name="bookingid"></param>

        /// <returns></returns>
        public ActionResult SendProforma(int? id,bool resend =false)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var book = db.bookings.Find(id);

                if (book == null)
                {
                    return HttpNotFound();
                }
              
                bool isCorrectAgent = book != null && book.agent_user_id == UserSession.Current.UserId;
                if (UserSession.Current.UserRole != UserRoles.Agent || isCorrectAgent)
                {
                    var proforma = book.booking_proforma.FirstOrDefault();
                    if (proforma != null)
                    {
                        var actionPDF = new Rotativa.MVC.ViewAsPdf("GenerateProforma", proforma);//some route values)
                        actionPDF.RotativaOptions.PageSize = Size.A4;
                        actionPDF.RotativaOptions.PageOrientation = Orientation.Portrait;
                        byte[] applicationPDFData = actionPDF.BuildPdf(ControllerContext);
                        string FileName = book.booking_file_number;
                        string Subject = String.Concat(book.booking_file_number, "-", book.booking_name);
                        book.agent = db.users.Find(book.agent_user_id);
                        if (book.agent != null)
                        {
                            string booktext = String.Concat("Dear ", book.agent.FullName, ",", "<br/><br/>", "Please find attached proforma for ", FileName, "<br/><br/>");
                            EmailHelper eh = new EmailHelper();
                            eh.SendEmail(book.agent.email, Subject, booktext, FileName, applicationPDFData, MediaTypeNames.Application.Pdf);
                        }
                        if (!resend)
                        {
                            proforma.finalized = true;
                            db.Entry(proforma).State = EntityState.Modified;

                            if (book.booking_id > 0)
                            {
                                if (book.TotalDueAmount <= 0)
                                {
                                    //Confirmed
                                    book.status = 5;
                                }
                                else
                                {
                                    //awaiting payment
                                    book.status = 4;
                                }

                                db.Entry(book).State = EntityState.Modified;
                            }

                            db.SaveChanges();
                        }

                    }
                    return RedirectToAction("ReservationDetails", new { id = id });
                }
                else
                {
                    return RedirectToAction("Error", "Error", new { Id = 403 });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult ViewProforma(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var book = db.bookings.Find(id);
                bool isCorrectAgent = book != null && book.agent_user_id == UserSession.Current.UserId;
                if (UserSession.Current.UserRole != UserRoles.Agent || isCorrectAgent)
                {
                    var proforma = db.bookingProformas.Where(e => e.booking_id == id);

                    if (proforma == null || proforma.Count() == 0)
                    {
                        return HttpNotFound();
                    }

                    return new Rotativa.MVC.ViewAsPdf("~/Views/Reservation/GenerateProforma.cshtml", proforma.First());
                }
                else
                {
                    return RedirectToAction("Error", "Error", new { Id = 403 });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult ViewVoucher(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                var book = db.bookings.Find(id);
                bool isCorrectAgent = book != null && book.agent_user_id == UserSession.Current.UserId;
                if (UserSession.Current.UserRole != UserRoles.Agent || isCorrectAgent)
                {
                    var voucher = db.bookingVouchers.Where(e => e.booking_id == id);

                    if (voucher == null || voucher.Count() == 0)
                    {
                        return HttpNotFound();
                    }

                    //return new Rotativa.MVC.ViewAsPdf("~/Views/Reservation/GenerateVoucher.cshtml", voucher.First());
                    return new Rotativa.MVC.ViewAsPdf("~/Views/Reservation/GenerateVoucher.cshtml", voucher.First());
                }
                else
                {
                    return RedirectToAction("Error", "Error", new { Id = 403 });
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion
    }
}
