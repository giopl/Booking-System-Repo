﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using Booking_System.Helpers;
using System.Data.Entity.Infrastructure;

namespace Booking_System.Controllers
{
    public class RoomsController : AdminController
    {
        private ABSDBEntities db = new ABSDBEntities();

        #region Room
        // GET: Rooms
        public ActionResult ListRooms()
        {
            
            return View(db.rooms.ToList());
        }

        // GET: Rooms/Details/5
        public ActionResult RoomDetails(int? id, AppEnums.RoomTab tab = AppEnums.RoomTab.OCCUPANCIES)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            room room = db.rooms.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }


            var roompoccupancies = room.occupancies;
            var occupancies = db.occupancies.ToList();
            System.Collections.Generic.List<occupancy> remainingOccupancies = new List<occupancy>();

            foreach (var ro in roompoccupancies)
            {
                if (occupancies.Contains(ro))
                {
                    occupancies.Remove(ro);

                }

            }

            ViewBag.Occupancies = occupancies;

            var availableFeatures = db.roomFeatures.Where(h => h.room_id == id).ToList();
            var systemfeatures = db.features.Where(x => x.feature_type == "R").ToList();

            foreach (var f in availableFeatures)
            {
                feature feature = new feature { feature_id = f.feature_id };
                if (systemfeatures.Contains(feature))
                {
                    systemfeatures.Remove(feature);
                }
            }


            //ViewBag.RoomFeatures = new SelectList(systemfeatures, "feature_id", "name");
            ViewBag.RoomFeatures = systemfeatures;

            ViewBag.Room = room;



            ViewBag.tab = tab;

            return View(room);
        }


        /// <summary>
        /// room 
        /// </summary>
        /// <param name="id">room Id</param>
        /// <returns></returns>
        public ActionResult RoomOccupancy(int id)
        {

            try
            {
                var room = db.rooms.Find(id);

                var roompoccupancies = room.occupancies;
                var occupancies = db.occupancies.ToList();
                System.Collections.Generic.List<occupancy> remainingOccupancies = new List<occupancy>();

                foreach (var ro in roompoccupancies)
                {
                    if (occupancies.Contains(ro))
                    {
                        occupancies.Remove(ro);

                    }

                }

                ViewBag.Occupancies = occupancies;


                return View(room);


            }
            catch (Exception)
            {
                
                throw;
            }
        }

        // GET: Rooms/Create
        public ActionResult CreateRoom(int? id)
        {

            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ViewBag.Hotel = db.providers.Find(id);
            ViewBag.Occupancies = db.occupancies.ToList();

            return View();
        }

        // POST: Rooms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateRoom([Bind(Include = "room_id,provider_id,name,description,min_adult,max_adult,room_size")] room room, bool[] occupancies)
        {
            if (ModelState.IsValid)
            {
                room.is_active = true;
                db.rooms.Add(room);
                db.SaveChanges();


                //add occupancies
                List<occupancy> _occupancies = db.occupancies.ToList();
                var _occupanciesOrdered = _occupancies.OrderBy(x => x.occupancyCode);

                int i = 0;
                foreach(var occ in _occupanciesOrdered)
                {
                    if(occupancies[i])
                    {
                        room.occupancies.Add(occ);
                    }
                    i++;
                }
                
                db.Entry(room).State = EntityState.Modified;
                db.SaveChanges();

                

                SaveAudit(AppEnums.Event.CREATE, "Room",room.room_id );
                return RedirectToAction("HotelDetails", "Hotels", new { id = room.provider_id, tab = AppEnums.HotelTab.ROOMS });
              

            }

            
            ViewBag.Hotel = db.providers.Where(x => x.provider_id == room.provider_id);
            return View(room);
        }

        // GET: Rooms/Edit/5
        public ActionResult EditRoom(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            room room = db.rooms.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }

            var provider_id = room.provider.provider_id;
            ViewBag.Hotel = db.providers.Find(provider_id);


            return View(room);
        }

        // POST: Rooms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRoom([Bind(Include = "room_id,provider_id,name,description,min_adult,max_adult,room_size,is_active")] room room)
        {
            if (ModelState.IsValid)
            {
                db.Entry(room).State = EntityState.Modified;
                db.SaveChanges();
                SaveAudit(AppEnums.Event.UPDATE, "Room", room.room_id);

                return RedirectToAction("HotelDetails","Hotels", new { id = room.provider_id, tab = AppEnums.HotelTab.ROOMS});
            }
            ViewBag.Hotel = db.providers.Find(room.provider_id);

            return View(room);


            
        }

        // GET: Rooms/Delete/5
        public ActionResult DeleteRoom(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            room room = db.rooms.Find(id);
            if (room == null)
            {
                return HttpNotFound();
            }
            ViewBag.Hotel = db.providers.Find(room.provider_id);
            return View(room);
        }

        // POST: Rooms/Delete/5
        [HttpPost, ActionName("DeleteRoom")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteRoomConfirmed(int id)
        {
            room room = db.rooms.Find(id);
            var hotel_id = room.provider_id;


          
                db.rooms.Remove(room);
                db.SaveChanges();
            SaveAudit(AppEnums.Event.DELETE, "Room", room.room_id);

            return RedirectToAction("HotelDetails", "Hotels", new { id = hotel_id, tab = AppEnums.HotelTab.DETAILS });

        }
        #endregion

        #region Occupancies 

        [HttpPost]
        public ActionResult AddRoomOccupancy(int? roomId, int[] occupancies)
        {

            if (occupancies.Count() > 0)
            {
                var room = db.rooms.Find(roomId);

                foreach (var item in occupancies)
                {
                    if (item > 0)
                    {
                        var occ = db.occupancies.Find(item);
                        room.occupancies.Add(occ);
                    }
                }

                //var occupancy = db.occupancies.Find(occupancyId);
                //room.occupancies.Add(occupancy);

                db.Entry(room).State = EntityState.Modified;
                db.SaveChanges();


            }

            return RedirectToAction("RoomDetails", "Rooms", new { id = roomId, tab = AppEnums.RoomTab.OCCUPANCIES });
        }


        public ActionResult DeleteRoomOccupancy(int? roomId, int? occupancyId)
        {

            if (occupancyId > 0)
            {
                var room = db.rooms.Find(roomId);
                var occupancy = db.occupancies.Find(occupancyId);
                room.occupancies.Remove(occupancy);

                db.Entry(room).State = EntityState.Modified;
                db.SaveChanges();


            }

            return RedirectToAction("RoomDetails", "Rooms", new { id = roomId, tab = AppEnums.RoomTab.OCCUPANCIES });
        }


        #endregion

        #region "Room Features"

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddRoomFeature([Bind(Include = "feature_id,room_id,quantity")] roomFeature roomFeature)
        {
            if (ModelState.IsValid)
            {
                db.roomFeatures.Add(roomFeature);
                db.SaveChanges();
               
                return RedirectToAction("RoomDetails", "Rooms", new { id = roomFeature.room_id, tab = AppEnums.RoomTab.FEATURES });
            }

            return RedirectToAction("RoomDetails", "Rooms", new { id = roomFeature.room_id, tab = AppEnums.RoomTab.FEATURES });
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRoomFeature([Bind(Include = "feature_id,room_id,quantity")] roomFeature roomFeature)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roomFeature).State = EntityState.Modified;
                db.SaveChanges();
             
                return RedirectToAction("Details", "Rooms", new { id = roomFeature.room_id, tab = AppEnums.RoomTab.FEATURES });

            }
            ViewBag.feature_id = new SelectList(db.features, "feature_id", "name", roomFeature.feature_id);
            ViewBag.room_id = new SelectList(db.rooms, "room_id", "name", roomFeature.room_id);
            return RedirectToAction("RoomDetails",new { id=roomFeature, tab = AppEnums.RoomTab.FEATURES });
        }
        
        public ActionResult DeleteRoomFeature(int id, int roomId)
        {
            roomFeature roomFeature = db.roomFeatures.Where(x => x.feature_id == id && x.room_id == roomId).FirstOrDefault();
            db.roomFeatures.Remove(roomFeature);
            db.SaveChanges();
            return RedirectToAction("RoomDetails", "Rooms", new { id = roomId, tab = AppEnums.RoomTab.FEATURES });

        }

        #endregion

        public ActionResult AddRoomUnavailability([Bind(Include = "room_id,start_date,end_date")] roomUnavailability roomUnavailability)
        {

            if(ModelState.IsValid & roomUnavailability.end_date >= roomUnavailability.start_date)
            {
                db.roomUnavailabilities.Add(roomUnavailability);
                db.SaveChanges();
            }
            

            return RedirectToAction("RoomDetails", "Rooms", new { id = roomUnavailability.room_id, tab = AppEnums.RoomTab.UNAVAILABILITY });
        }


        public ActionResult DeleteRoomUnavailability(int? id, int roomId)
        {

            if(id.HasValue && id.Value>0)
            {
                var roomUnavailability = db.roomUnavailabilities.Find(id);
                
                if(roomUnavailability != null)
                {

                    db.roomUnavailabilities.Remove(roomUnavailability);
                    db.SaveChanges();

                }

            }

            return RedirectToAction("RoomDetails", "Rooms", new { id = roomId, tab = AppEnums.RoomTab.UNAVAILABILITY });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
