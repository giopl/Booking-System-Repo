﻿using Booking_System.Models;
using Booking_System.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Booking_System.Services.Abstract;
using Booking_System.Services;
using Booking_System.Helpers;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Data;
using EntityFramework.Extensions;
using System.Net;
using Rotativa.Core.Options;

namespace Booking_System.Controllers
{
    [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Agent, UserRoles.Sales)]
    public class FrontController : BaseController
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ABSDBEntities db = new ABSDBEntities();
        private IFrontService frontService = new FrontService();
        private IReservationService reservationService = new ReservationService();

        private const String FINDPACKAGE = "FindPackage";
        private const String FINDROOM = "FindRoom";
        private const String GETPACKAGES = "GetPackages";
        private const String GETPROMOTIONSWITHSETS = "GetPromotionsWithSets";
        private const String GETPROMOTIONS = "GetPromotions";
        private const String GETCHARGES = "GetCharges";
        private const String FINDCHARGENAME = "FindChargeName";



        // GET: Front
        public ActionResult Index()
        {
            return RedirectToAction("Home");
        }
        public ActionResult Aboutus()
        {
            return View("Aboutus");
        }

        public ActionResult Privacy()
        {
            return View("privacy");
        }

        public ActionResult Contactus()
        {
            return View("contactus");
        }

        public SelectList GetHotels(SearchDetails searchDet)
        {

            return new SelectList((from hotel in db.providers.Where(x => x.is_active && x.provider_type == "H").OrderBy(x => x.name.Trim()).ToList()
                                   select new
                                   {
                                       provider_id = hotel.provider_id,
                                       Description = String.Format("{0} - {1} ({2})", hotel.name, hotel.town, hotel.location)
                                   }),
                                    "provider_id",
                                    "Description",
                                    searchDet.Location);
        }

        /// <summary>
        /// 1. landing page 
        /// </summary>
        /// <returns></returns>
        public ActionResult Home(int Resume = 0)
        {
            try
            {
                var searchDet = new SearchDetails(); //initialise a search o bject 
                List<featuredHotel> featuredHotelList = db.featuredHotels.ToList();

                ViewBag.FeaturedList = featuredHotelList;
                {
                    //if resume is set to true and a record exists in session
                    if (Resume == 1 && UserSession.Current.SearchDetails != null)
                    {
                        searchDet = UserSession.Current.SearchDetails;
                    }
                    ViewBag.location = GetHotels(searchDet);
                    return View(searchDet);
                }
            }
            catch (Exception)
            {

                throw;
            }

        }

        #region userpreferences 



        public ActionResult UserSettings()
        {

            var usr = db.users.Find(Helpers.UserSession.Current.UserId);

            return View(usr);
        }

        [HttpPost]
        public ActionResult ChangePassword(user usr)
        {
            IAdminService loginService = new AdminService();
            if (!String.IsNullOrWhiteSpace(usr.OldPassword) && !String.IsNullOrWhiteSpace(usr.NewPassword)
                && !String.IsNullOrWhiteSpace(usr.ConfirmedPassword))
            {
                LoginState changePassword = new LoginState();
                changePassword.Username = Helpers.UserSession.Current.Username;
                changePassword.OldPassword = Utils.base64Encode(usr.OldPassword);
                changePassword.NewPassword = usr.NewPassword;
                changePassword.ConfirmPassword = usr.ConfirmedPassword;

                changePassword.LoginStateEnum = LoginStateEnum.Password_Changed;
                bool result = loginService.ChangePassword(changePassword);
                if (result)
                {
                    Helpers.UserSession.Current.IsValid = false;
                    return RedirectToAction("Index", "Login", new { loginState = changePassword.LoginStateEnum });
                }
            }

            return View(usr);
        }


        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateUserInformation([Bind(Include = "firstname,lastname,officephone,mobile,email")] user user)
        {
            try
            {

                var usr = db.users.Find(Helpers.UserSession.Current.UserId);

                ModelState["Salutation"].Errors.Clear();
                //ModelState["date_of_birth"].Errors.Clear();
                ModelState["gender"].Errors.Clear();
                //ModelState["nationality"].Errors.Clear();

                if (user != null)
                {
                    if (String.IsNullOrWhiteSpace(user.email))
                    {
                        ModelState.AddModelError("email", "Email is required.");
                    }
                    else
                    {
                        var count = db.users.Count(e => e.email == user.email && e.user_id != usr.user_id);
                        if (count > 0)
                        {
                            ModelState.AddModelError("email", "This email is already in use by another account.");
                        }
                    }
                }

                if (ModelState.IsValid)
                {
                    usr.firstname = user.firstname;
                    usr.lastname = user.lastname;
                    usr.officephone = user.officephone;
                    usr.mobile = user.mobile;
                    usr.email = user.email;

                    db.Entry(usr).State = EntityState.Modified;

                    db.SaveChanges();


                    Helpers.UserSession.Current.Fullname = usr.FullName;
                    return RedirectToAction("UserBookings");
                }

                return View("UserSettings", usr);
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }


            catch (Exception e)
            {
                log.ErrorFormat("controller error {0} ", e.ToString());

                throw;
            }
        }


        [CustomAuthorize(UserRoles.Agent, UserRoles.Sales, UserRoles.Administrator, UserRoles.SuperAdministrator)]
        public ActionResult FrontEndUpdateReservationStatus(int? booking_id, int? status)
        {
            try
            {
                if (booking_id == null || status == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(booking_id);
                if (booking == null)
                {
                    return HttpNotFound();
                }



                //public ActionResult UpdateReservationStatus(int? booking_id, int? status, DateTime? timelimitdate = null, bool agentupdate = false)


                //call method to save reservation details
                IReservationService reservationService = new ReservationService();
                reservationService.SaveReservation(booking_id, status, null);

                return RedirectToAction("UserBookings");

            }
            catch (Exception)
            {

                throw;
            }


        }




        public ActionResult UserBookings()
        {
            var today = DateTime.Now.Date;
            var bookings = db.viewBookings.Where(x => x.agent_user_id == UserSession.Current.UserId
            && !(x.checkout_date < today));

            return View(bookings);


        }

        public ActionResult UserBookingsHistory()
        {
            var today = DateTime.Now.Date;
            //take all bookings for the user BUT with a checkout in the past.
            var bookings = db.viewBookings.Where(x => x.agent_user_id == UserSession.Current.UserId
            && (x.checkout_date < today || x.status == 7 || x.status == 8 || x.status == 9 || x.status == 3));

            return View(bookings);
        }


        #endregion

        public ActionResult CancelBooking(int? id)
        {
            try
            {
                var booking = db.bookings.Find(id);
                refurbishbooking(booking);

                if (id == null || booking == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var guid = booking.booking_ref;
                var deleteMe = booking.status == 0;
                if (deleteMe)
                {
                    db.bookingActivities.Where(x => x.booking_id == id).Delete();

                    db.bookingGuests.Where(x => x.booking_id == id).Delete();
                    db.bookingRentals.Where(x => x.booking_id == id).Delete();
                    db.bookingSupplements.Where(x => x.booking_id == id).Delete();
                    db.bookingPromotions.Where(x => x.booking_id == id).Delete();
                    db.bookingRooms.Where(x => x.booking_id == id).Delete();

                    db.bookingTransfers.Where(x => x.booking_id == id).Delete();
                    db.bookings.Where(x => x.booking_id == id).Delete();
                    db.bookingSearches.Where(x => x.booking_ref == guid).Delete();

                    SaveAudit(AppEnums.Event.DELETE, "Booking", id.Value);
                }
                else
                {
                    booking.status = (int)AppEnums.BookingStatus.CANCELED;
                    var agent = db.users.Find(booking.agent_user_id);
                    if (agent != null)
                    {
                        if (booking.credit_amt.HasValue && booking.credit_amt.Value > 0)
                        {
                            //booking.credit_amt = creditRemaining >= booking.booking_amt ? booking.booking_amt : creditRemaining;
                            //decrease the credit limit of the agent.

                            agent.credit_used = agent.credit_used - booking.credit_amt;

                            if (Helpers.UserSession.Current.UserId == booking.agent_user_id)
                            {
                                UserSession.Current.RemainingCredit = (double)agent.credit_remaining;
                            }

                            db.Entry(agent).State = EntityState.Modified;
                        }
                    }

                    db.Entry(booking).State = EntityState.Modified;
                    db.SaveChanges();
                }

                return RedirectToAction("UserBookings");

            }
            catch (Exception e)
            {

                throw;
            }

        }

        [CustomAuthorize(UserRoles.Agent, UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditBookingDetails(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(id);
                if (booking == null)
                {
                    return HttpNotFound();
                }
                refurbishbooking(booking);
                var booking_guests = db.bookingGuests.Where(x => x.booking_id == id);

                ViewBag.Booking = booking;
                return View(booking_guests);

            }
            catch (Exception)
            {

                throw;
            }
        }

        [CustomAuthorize(UserRoles.Agent, UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult ConfirmBookingDetails(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(id);
                if (booking == null)
                {
                    return HttpNotFound();
                }
                refurbishbooking(booking);
                var booking_guests = db.bookingGuests.Where(x => x.booking_id == id);

                ViewBag.Booking = booking;
                return View(booking_guests);

            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        [CustomAuthorize(UserRoles.Agent, UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult ConfirmBookingDetails(BookingGuestViewModel guestparam)
        {
            try
            {

                IReservationService res = new ReservationService();
                var booking = db.bookings.Find(guestparam.booking_id);

                if (booking == null)
                {
                    return HttpNotFound();
                }

                //Confirmed booking
                booking.status = 8;

                var agent = db.users.Find(Helpers.UserSession.Current.UserId);
                //The agent should be credit based so as to confirm
                if (agent != null && agent.is_credit_based)
                {
                    var creditRemaining = agent.credit_remaining.HasValue ? agent.credit_remaining.Value : 0;
                    var bookingAmt = booking.booking_amt.HasValue ? booking.booking_amt : 0;

                    if (creditRemaining >= bookingAmt)
                    {
                        booking.credit_amt = bookingAmt;

                        //decrease the credit limit of the agent.
                        agent.credit_used = (agent.credit_used.HasValue ? agent.credit_used : 0) + bookingAmt;

                        UserSession.Current.RemainingCredit = (double)agent.credit_remaining;

                        db.Entry(agent).State = EntityState.Modified;
                    }
                }

                db.Entry(booking).State = EntityState.Modified;

                db.SaveChanges();

                res.SaveGuestParam(guestparam, false, true, true);
                res.SendBookingMail(booking.booking_id, AppEnums.BookingMailType.CLIENT_CONFIRMED_ADMIN);

                return RedirectToAction("userbookings", new { id = guestparam.booking_id });

            }
            catch (Exception e)
            {

                throw;
            }
        }

        [HttpPost]
        [CustomAuthorize(UserRoles.Agent, UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
        public ActionResult EditBookingDetails(BookingGuestViewModel guestparam)

        {
            try
            {

                IReservationService res = new ReservationService();
                res.SaveGuestParam(guestparam, false, true);

                return RedirectToAction("BookingSummary", new { id = guestparam.booking_id });

            }
            catch (Exception e)
            {

                throw;
            }
        }


        #region search and booking 


        public ActionResult ListActivities()
        {
            try
            {
                List<activity> activities = db.activities.Where(x => x.is_active).ToList();
                return View(activities);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        public ActionResult ListHotels(string id = "")
        {
            try
            {
                List<provider> hotels;
                if (!String.IsNullOrEmpty(id)) {
                    hotels = db.providers.Where(x => x.is_active && x.provider_type == "H" && (x.location == id)).ToList();
                }
                else
                {
                    hotels = db.providers.Where(x => x.is_active && x.provider_type == "H").ToList();
                }
                var searchDet = new SearchDetails();
                ViewBag.SearchForm = searchDet;
                ViewBag.location = GetHotels(searchDet);
                return View(hotels);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        public ActionResult ListHotelPromotions(string id = "")
        {
            try
            {
                List<provider> hotels;
                if (!String.IsNullOrEmpty(id))
                {
                    hotels = db.providers.Where(x => x.is_active && x.provider_type == "H" && x.promotions.Count > 0 && (x.location == id)).ToList();
                }
                else
                {
                    hotels = db.providers.Where(x => x.is_active && x.provider_type == "H" && x.promotions.Count > 0).ToList();
                }
                var searchDet = new SearchDetails();
                ViewBag.SearchForm = searchDet;
                ViewBag.location = GetHotels(searchDet);
                return View(hotels);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpGet]
        public ActionResult ActivityDetails(int id)
        {
            int a = id;
            try
            {
                //var activities = db.activities.Where(x => x.is_active && x.activity_id == activityid);
                activity activity = db.activities.Where(x => x.activity_id == id && x.is_active).FirstOrDefault();
                return View(activity);
            }
            catch (Exception e)
            {

                throw;
            }
        }


        public ActionResult Search(SearchDetails search = null)
        {
            try
            {
                HotelSearchCriteria hotelSearchCriteria = new HotelSearchCriteria();
                IList<SearchOccupancy> Occupancies = new List<SearchOccupancy>();


                //section filters by chosen hotel if hotel is provided
                int hotelId = 0;
                if (!string.IsNullOrWhiteSpace(search.Location))
                {
                    //string[] searchsplit = search.Location.Split('-');
                    //if (searchsplit.Count() > 0)
                    //{
                    //    var hotelname = searchsplit[0].Trim();
                    //    var hotel = db.providers.Where(x => x.name == hotelname && x.provider_type == "H" && x.is_active).FirstOrDefault();
                    //    if (hotel != null)
                    //    {
                    //        hotelId = hotel.provider_id;
                    //    }
                    //}
                    var x = Int32.TryParse(search.Location, out hotelId);
                }


                if (search == null || search._Rooms == 0)
                {
                    var xmlCriteria = UserSession.Current.HotelSearchCriteriaXML;


                    if (xmlCriteria == null)
                    {
                        return RedirectToAction("Home");
                    }

                    hotelSearchCriteria = Utils.DeserializeXml<HotelSearchCriteria>(xmlCriteria);
                    Occupancies = hotelSearchCriteria.SearchOccupancies;
                    search.Start = hotelSearchCriteria.FromDate;
                    search.End = hotelSearchCriteria.ToDate;
                    ViewBag.NumRoomsOrdered = Occupancies.Count;

                    hotelSearchCriteria.IsPackage = search.IsPackage;

                    hotelSearchCriteria.NumNightsPackage = search.IsPackage ? search.PackageNights : 0;

                    hotelSearchCriteria.HotelId = hotelId;
                    hotelSearchCriteria.IsHoneymoon = search.IsHoneymoon;
                }
                else
                {
                    Occupancies = frontService.GetSearchOccupancies(search);
                    ViewBag.NumRoomsOrdered = Occupancies.Count;
                    hotelSearchCriteria.FromDate = search.Start;
                    hotelSearchCriteria.ToDate = search.End;
                    hotelSearchCriteria.SearchOccupancies = Occupancies.ToList();
                    hotelSearchCriteria.IsPackage = search.IsPackage;

                    hotelSearchCriteria.NumNightsPackage = search.IsPackage ? search.PackageNights : 0;

                    hotelSearchCriteria.HotelId = hotelId;
                    hotelSearchCriteria.IsHoneymoon = search.IsHoneymoon;

                    UserSession.Current.SearchDetails = search; //add another searchdetails since it is easier to resend it to controller
                    UserSession.Current.HotelSearchCriteriaXML = Utils.SerializeToXml<HotelSearchCriteria>(hotelSearchCriteria);
                }

                return RedirectToAction("Result");

            }
            catch (Exception e)
            {

                throw;
            }
        }


        /// <summary>
        /// 1. search for results from all available hotels that have rooms, market and correct date
        /// </summary>
        /// <param name="search"></param>
        /// <returns></returns>

        //public ActionResult Search0(SearchDetails search = null)
        //{
        //    try
        //    {

        //        HotelSearchCriteria hotelSearchCriteria = new HotelSearchCriteria();
        //        IList<RoomOccupancy> Occupancies = new List<RoomOccupancy>();


        //        //section filters by chosen hotel if hotel is provided
        //        int hotelId = 0;
        //        if (!string.IsNullOrWhiteSpace(search.Location))
        //        {
        //            string[] searchsplit = search.Location.Split('-');
        //            if (searchsplit.Count() > 0)
        //            {
        //                var hotelname = searchsplit[0].Trim();
        //                var hotel = db.providers.Where(x => x.name == hotelname && x.provider_type == "H" && x.is_active).FirstOrDefault();
        //                if (hotel != null)
        //                {
        //                    hotelId = hotel.provider_id;
        //                }
        //            }
        //        }

        //        if(search.End < search.Start)
        //        {
        //            if(search.HoneymoonHotelOrPackage != "package")
        //            {
        //            search.End = search.Start.AddDays(4);

        //            }
        //        }

        //        if(search.IsHoneymoon)
        //        {
        //            search.IsPackage = search.HoneymoonHotelOrPackage == "package";
        //            search.IsHotel = search.HoneymoonHotelOrPackage == "hotel";

        //        }

        //        if (search == null || search._Rooms == 0)
        //        {
        //            var xmlCriteria = UserSession.Current.HotelSearchCriteriaXML;


        //            if (xmlCriteria == null)
        //            {
        //                return RedirectToAction("Home");
        //            }

        //            hotelSearchCriteria = Utils.DeserializeXml<HotelSearchCriteria>(xmlCriteria);
        //            Occupancies = hotelSearchCriteria.SearchOccupancies;
        //            search.Start = hotelSearchCriteria.FromDate;
        //            search.End = hotelSearchCriteria.ToDate;
        //            ViewBag.NumRoomsOrdered = Occupancies.Count;

        //        }
        //        else
        //        {
        //            Occupancies = frontService.GetOccupancies(search);
        //            ViewBag.NumRoomsOrdered = Occupancies.Count;
        //            hotelSearchCriteria.FromDate = search.Start;
        //            hotelSearchCriteria.ToDate = search.End;
        //            hotelSearchCriteria.RoomOccupancy = Occupancies.ToList();

        //            hotelSearchCriteria.NumNightsPackage = search.IsPackage ? search.PackageNights : 0;

        //            hotelSearchCriteria.HotelId = hotelId;
        //            hotelSearchCriteria.IsHoneymoon = search.IsHoneymoon;

        //            UserSession.Current.SearchDetails = search; //add another searchdetails since it is easier to resend it to controller
        //            UserSession.Current.HotelSearchCriteriaXML = Utils.SerializeToXml<HotelSearchCriteria>(hotelSearchCriteria);
        //        }


        //        if(search.IsHoneymoon && search.IsHotel)
        //        {
        //            return RedirectToAction("HoneymoonResult");
        //        }

        //        if (search.IsHoneymoon && search.IsPackage)
        //        {
        //            return RedirectToAction("HoneymoonPackageResult");
        //        }



        //        if (search.IsPackage)
        //        {

        //            return RedirectToAction("PackageResult");

        //        }




        //        if (search.IsHotel)
        //        {


        //            return RedirectToAction("HotelResult");

        //        }

        //        //search criteria is invalid return to home
        //        return RedirectToAction("Home");


        //    }
        //    catch (Exception e)
        //    {
        //        log.ErrorFormat("Error in Searching for hotel {0}", e.ToString());
        //        throw;
        //    }
        //}


        public ActionResult HotelDetails(int id)
        {
            int a = id;
            try
            {
                //var activities = db.activities.Where(x => x.is_active && x.activity_id == activityid);
                provider provider = db.providers.Where(x => x.provider_id == id && x.is_active && x.provider_type == "H").FirstOrDefault();
                var searchDet = new SearchDetails();
                if (UserSession.Current.SearchDetails != null)
                {
                    searchDet = UserSession.Current.SearchDetails;
                }
                searchDet.Location = provider.provider_id.ToString();

                ViewBag.SearchForm = searchDet;
                ViewBag.location = GetHotels(searchDet);
                return View(provider);
            }
            catch (Exception e)
            {

                throw;
            }
        }


        public ActionResult Result()
        {
            try
            {

                HotelSearchCriteria hsc = Utils.DeserializeXml<HotelSearchCriteria>(UserSession.Current.HotelSearchCriteriaXML);

                if (hsc == null)
                {
                    return RedirectToAction("Home");
                }

                ViewBag.NumRoomsOrdered = hsc.SearchOccupancies.Count();

                var hotelId = hsc.HotelId;
                //var Occupancies = hsc.SearchOccupancies;
                List<FoundRoom> resultlist = new List<FoundRoom>();

                IFrontService FrontService = new FrontService();
                //add result to a ResultObject per provider
                HashSet<ProviderFound> SearchResult = new HashSet<ProviderFound>();

                int occNum = 1;
                foreach (var occupancy in hsc.SearchOccupancies)
                {
                    resultlist = new List<FoundRoom>();
                    using (var ctx = new ABSDBEntities())
                    {

                        int zero = 0;
                        if (hsc.IsPackage)
                        {
                            var occupancyResult = db.Database.SqlQuery<FoundRoom>(string.Format(@"exec {0} @start,@adults,@age1,@age2,@age3,@age4,@age5,@nights,@currency_id,@hotel_id,@room_num,@package_id,@is_honeymoon,@onlyBestPrice", FINDPACKAGE)
                             , new object[]
                              {
                                 new SqlParameter("start", hsc.FromDate),
                                 new SqlParameter("adults", occupancy.Adults),
                             new SqlParameter("age1", occupancy.Ages.Count>0?occupancy.Ages[0]:-1),
                            new SqlParameter("age2", occupancy.Ages.Count>1?occupancy.Ages[1]:-1),
                            new SqlParameter("age3", occupancy.Ages.Count>2?occupancy.Ages[2]:-1),
                            new SqlParameter("age4", occupancy.Ages.Count>3?occupancy.Ages[3]:-1),
                            new SqlParameter("age5", occupancy.Ages.Count>4?occupancy.Ages[4]:-1),

                                 new SqlParameter("nights", hsc.NumNightsPackage),
                                 new SqlParameter("currency_id",zero),
                                 new SqlParameter("hotel_id",hotelId),
                                 new SqlParameter("room_num",occNum),
                                 new SqlParameter("package_id", hsc.PackageId),
                                 new SqlParameter("is_honeymoon",hsc.IsHoneymoon),
                                 new SqlParameter("onlyBestPrice",zero)
                            }
                                             ).ToList<FoundRoom>();
                            resultlist.AddRange(occupancyResult);

                        }
                        else
                        {
                            var occupancyResult = db.Database.SqlQuery<FoundRoom>(string.Format(@"exec {0} @start, @end, @adults,@age1,@age2,@age3,@age4,@age5, @occNum, @ishoneymoon,@providerId,@currencyId,@onlyBestPrice", FINDROOM)
                                                , new object[]
                              {
                                 new SqlParameter("start", hsc.FromDate),
                                 new SqlParameter("end", hsc.ToDate),
                                 new SqlParameter("adults", occupancy.Adults),
                                 new SqlParameter("age1", occupancy.Ages.Count>0?occupancy.Ages[0]:-1),
                                 new SqlParameter("age2", occupancy.Ages.Count>1?occupancy.Ages[1]:-1),
                                 new SqlParameter("age3", occupancy.Ages.Count>2?occupancy.Ages[2]:-1),
                                 new SqlParameter("age4", occupancy.Ages.Count>3?occupancy.Ages[3]:-1),
                                 new SqlParameter("age5", occupancy.Ages.Count>4?occupancy.Ages[4]:-1),
                                 new SqlParameter("occNum",occNum),
                                 new SqlParameter("ishoneymoon", hsc.IsHoneymoon),
                                 new SqlParameter("providerId",hotelId),
                                 new SqlParameter("currencyId",zero),
                                 new SqlParameter("onlyBestPrice",zero)
                            }
                                              ).ToList<FoundRoom>();
                            resultlist.AddRange(occupancyResult);

                        }


                        //Found room for each occupancies

                        List<FoundRoom> tempFoundroom = new List<FoundRoom>();
                        foreach (var r in resultlist.OrderBy(x => x.Id))
                        {
                            room room = db.rooms.Find(r.room_id);
                            var roomOccupancies = room.occupancies.ToList();
                            r.RoomOccupancy = frontService.GetRoomOccupancy(hsc.SearchOccupancies.Where(x => x.RoomNumber == r.room_sequence).FirstOrDefault(), r.provider_id);

                            if (roomOccupancies.Exists(x => x.occupancyCode == r.RoomOccupancy.OccupancyCode))
                            {
                                r.IsHoneyMoon = hsc.IsHoneymoon;

                                var psets = GetPromotionSets(r.from_date ?? DateTime.Now, r.to_date ?? DateTime.Now, r.provider_id, r.room_id, r.market_id, hsc.IsHoneymoon);

                                TimeSpan numnights = (r.to_date ?? DateTime.Now).Subtract(r.from_date ?? DateTime.Now);
                                int _nights = (int)numnights.TotalDays;

                                var max = psets.Max(x => x.promotion_set_id);
                                var min = psets.Min(x => x.promotion_set_id);

                                for (int i = min ?? 0; i <= max; i++)
                                {
                                    var _psets = psets.Where(x => x.promotion_set_id == i);

                                    if (_psets.Count() > 0)
                                    {
                                        PromosetViewModel pvm = new PromosetViewModel();
                                        foreach (var _p in _psets)
                                        {
                                            pvm.Promotions.Add(_p);
                                        }
                                        pvm.setid = i;
                                        pvm.nights = Utils.CoalesceInt(r.nights);
                                        pvm.TotalRoomCost = r.TotalRoomCost;
                                        //For Family offer
                                        pvm.TotalAdult = r.TotalAdult;
                                        pvm.TotalChild1 = r.TotalChild1;
                                        pvm.TotalChild2 = r.TotalChild2;
                                        pvm.TotalTeen = r.TotalTeen;
                                        pvm.TotalInfant = r.TotalInfant;
                                        pvm.NoOfChild = Utils.CoalesceInt(r.num_children);
                                        pvm.NoOfAdult = Utils.CoalesceInt(r.num_adults);
                                        pvm.NoOfTeen = Utils.CoalesceInt(r.num_teens);
                                        pvm.GroundHandlingChild = r.GroundHandlingChild;
                                        pvm.GroundHandlingTeen = r.GroundHandlingTeen;
                                        pvm.GroundHandlingAdult = r.GroundHandlingAdult;
                                        //
                                        pvm.TotalCompulsoryCharges = r.TotalCompulsoryCharges;
                                        pvm.GroundHandling = r.GroundHandling;
                                        r.Promosets.Add(pvm);
                                    }
                                }
                                tempFoundroom.Add(r);
                            }
                        }
                        //Find lowest rooms.
                        if (tempFoundroom.Count > 0)
                        {
                            FoundRoom lowest = new FoundRoom();

                            var rProviders = tempFoundroom.GroupBy(x => x.provider_id).Select(grp => grp.First()).ToList();
                            foreach (var r in rProviders)
                            {
                                lowest = tempFoundroom.Where(x => x.provider_id == r.provider_id).OrderBy(x => x.LowestPromosetAmount).First();

                                if (SearchResult.Any(x => x.ProviderId == lowest.provider_id))
                                {
                                    var _providerFound = SearchResult.Where(x => x.ProviderId == lowest.provider_id).FirstOrDefault();
                                    _providerFound.FoundRooms.Add(lowest);
                                }
                                else
                                {
                                    var providerFound = new ProviderFound();
                                    providerFound.ProviderId = lowest.provider_id;
                                    providerFound.TotalRooms = hsc.SearchOccupancies.Count();
                                    providerFound.IsHoneymoon = hsc.IsHoneymoon;
                                    providerFound.Currency = db.currencies.Find(lowest.currency_id);
                                    providerFound.PackageId = lowest.package_id;

                                    providerFound.Hotel = db.providers.Find(lowest.provider_id);
                                    providerFound.FoundRooms.Add(lowest);
                                    providerFound.Nights = Utils.CoalesceInt(lowest.nights);
                                    SearchResult.Add(providerFound);
                                }
                            }

                        }

                    }
                    occNum++;
                }

                HotelSearchResult result = new HotelSearchResult
                {
                    Hotels = SearchResult.ToList(),
                    NumAdults = hsc.SearchOccupancies.Sum(x => x.Adults),
                    NumMinors = hsc.SearchOccupancies.Sum(x => x.Ages.Count()),
                    Start = hsc.FromDate,
                    End = hsc.ToDate,
                    SearchDetails = hsc,
                    IsPackage = hsc.IsPackage
                };
                return View(result);

            }
            catch (Exception e)
            {

                throw;
            }
        }



        private List<PromotionSetResult> GetPromotionSets(DateTime startDate, DateTime endDate, int providerId, int roomId = 0, int marketId = 0, bool isHoneymoon = false)
        {
            try
            {
                var result = new List<PromotionSetResult>();
                using (var ctx = new ABSDBEntities())
                {

                    //Get student name of string type
                    result = db.Database.SqlQuery<PromotionSetResult>(String.Format(@"exec {0} @start, @end, @provider_id,@room_id,@market_id,@is_honeymoon", GETPROMOTIONSWITHSETS)
                          , new object[]
                              {
                                 new SqlParameter("start", startDate),
                                 new SqlParameter("end", endDate),
                                 new SqlParameter("provider_id", providerId),
                                 new SqlParameter("room_id", roomId),
                                 new SqlParameter("market_id", marketId),
                                 new SqlParameter("is_honeymoon",isHoneymoon)
                              }
                        ).ToList<PromotionSetResult>();

                }

                return result;
            }
            catch (Exception e)
            {

                throw;
            }
        }



        private List<PromotionResult> GetPromotions(DateTime startDate, DateTime endDate, int providerId, int roomId = 0, int marketId = 0, bool isHoneymoon = false)
        {
            try
            {
                var result = new List<PromotionResult>();
                using (var ctx = new ABSDBEntities())
                {

                    //Get student name of string type
                    result = db.Database.SqlQuery<PromotionResult>(string.Format(@"exec {0} @start, @end, @provider_id,@room_id,@market_id,@is_honeymoon", GETPROMOTIONS)
                          , new object[]
                              {
                                 new SqlParameter("start", startDate),
                                 new SqlParameter("end", endDate),
                                 new SqlParameter("provider_id", providerId),
                                 new SqlParameter("room_id", roomId),
                                 new SqlParameter("market_id", marketId),
                                 new SqlParameter("is_honeymoon",isHoneymoon)
                              }
                        ).ToList<PromotionResult>();

                }

                return result;
            }
            catch (Exception e)
            {

                throw;
            }
        }




        private List<FoundCharge> GetCharges(DateTime startDate, DateTime endDate, int providerId, int marketId = 0, bool isHoneymoon = false)
        {
            try
            {
                var result = new List<FoundCharge>();
                using (var ctx = new ABSDBEntities())
                {

                    //Get student name of string type
                    result = db.Database.SqlQuery<FoundCharge>(String.Format(@"exec {0} @start, @end, @provider_id,@market_id", GETCHARGES)
                          , new object[]
                              {
                                 new SqlParameter("start", startDate),
                                 new SqlParameter("end", endDate),
                                 new SqlParameter("provider_id", providerId),
                                 new SqlParameter("market_id", marketId),
                              }
                        ).ToList<FoundCharge>();

                }

                return result;
            }
            catch (Exception e)
            {

                throw;
            }
        }


        public ActionResult HotelOrPackage(int? id, int? nights, int? currency_id, bool? isPackage, string bookingref = "")
        {
            try
            {

                booking bkg = new booking();

                package _package = new package();
                var packageId = 0;
                var providerId = 0;
                if (id.HasValue)
                {

                    if (Utils.CoalesceBool(isPackage))
                    {
                        packageId = id.Value;

                        _package = db.packages.Find(id);

                        providerId = _package.provider_id;
                        ViewBag.PackageId = id.Value;
                        bkg.package = _package;
                    }
                    else
                    {
                        providerId = id.Value;

                    }
                }

                if (!string.IsNullOrWhiteSpace(bookingref))
                {
                    var bsearch = db.bookingSearches.Where(x => x.booking_ref.Equals(bookingref)).ToList();


                    if (bsearch.Count == 1)
                    {
                        var bookingSearch = bsearch.FirstOrDefault();
                        var searchXml = bookingSearch.search_details;
                        UserSession.Current.HotelSearchCriteriaXML = searchXml;
                        providerId = Utils.CoalesceInt(bookingSearch.hotel_id);

                        isPackage = bookingSearch.package_id > 0;

                        _package = db.packages.Find(bookingSearch.package_id);

                        if (packageId == 0 && bookingSearch.package_id > 0)
                        {
                            packageId = _package.package_id;
                        }
                    }
                }

                if (db.bookings.Any(x => x.booking_ref == bookingref))
                {
                    bkg = db.bookings.Where(x => x.booking_ref == bookingref).FirstOrDefault();
                    if (bkg != null)
                    {
                        refurbishbooking(bkg);
                    }
                }


                ViewBag.Booking = bkg;

                if (Utils.CoalesceBool(isPackage) && string.IsNullOrWhiteSpace(bookingref))
                {

                }

                var hotel = db.providers.Find(providerId);

                var searchCritXml = UserSession.Current.HotelSearchCriteriaXML;
                HotelSearchCriteria hsc = Utils.DeserializeXml<HotelSearchCriteria>(searchCritXml);


                HotelOrPackageViewModel model = new HotelOrPackageViewModel();
                model.Hotel = hotel;
                model.BookingRef = bookingref;
                model.NumRooms = hsc.SearchOccupancies.Count;
                model.IsPackage = isPackage ?? false;

                List<FoundRoom> resultlist = new List<FoundRoom>();



                // this time recreate search occupancy based on hotel GuestAges
                var occupancies = frontService.GetRoomOccupancies(hsc.SearchOccupancies, hotel.provider_id).ToList();

                //replace occupancies in hotelsearchcriteria
                // hsc.SearchOccupancies= occupancies;
                hsc.HotelId = providerId;
                hsc.PackageId = packageId;

                int zero = 0;


                int occNum = 1;
                foreach (var occupancy in hsc.SearchOccupancies)
                {
                    using (var ctx = new ABSDBEntities())
                    {

                        /*
                             [dbo].[FindRoom] @start = '2016-07-01',@end = '2016-07-03',@adults = 2,@occNum = 0,@occupancyType = 0,@providerId = 0,@currencyId = 0,@onlyBestPrice = 0
                         */
                        //Get student name of string type
                        var occType = 0;
                        if (hsc.IsHoneymoon)
                        {
                            occType = 2;
                        }
                        else if (occupancy.Ages.Count > 0)
                        {
                            occType = 1;
                        }


                        /*
                         EXEC	[dbo].[FindPackage]
		                    @start = '2016-07-01',
		                    @adults = 2,
		                    @age1 = 4,@age2 = NULL,@age3 = NULL,@age4 = NULL,@age5 = NULL,
		                    @nights = 0,
		                    @currency_id = 0,@hotel_id = 0,@room_num = 1,@package_id = 100,
		                    @is_honeymoon = 0,@onlyBestPrice = 0
                        */




                        if (hsc.IsPackage)
                        {

                            var occupancyResult = db.Database.SqlQuery<FoundRoom>(string.Format(@"exec {0} @start,@adults,@age1,@age2,@age3,@age4,@age5,@nights,@currency_id,@hotel_id,@room_num,@package_id,@is_honeymoon,@onlyBestPrice", FINDPACKAGE)
                                               , new object[]
                              {
                                 new SqlParameter("start", hsc.FromDate),
                                 new SqlParameter("adults", occupancy.Adults),
                                  new SqlParameter("age1", occupancy.Ages.Count>0?occupancy.Ages[0]:-1),
                                 new SqlParameter("age2", occupancy.Ages.Count>1?occupancy.Ages[1]:-1),
                                 new SqlParameter("age3", occupancy.Ages.Count>2?occupancy.Ages[2]:-1),
                                 new SqlParameter("age4", occupancy.Ages.Count>3?occupancy.Ages[3]:-1),
                                 new SqlParameter("age5", occupancy.Ages.Count>4?occupancy.Ages[4]:-1),

                                 new SqlParameter("nights", nights==null?zero:nights),
                                 new SqlParameter("currency_id",currency_id==null?zero:currency_id),
                                 new SqlParameter("hotel_id",hsc.HotelId),
                                 new SqlParameter("room_num",occNum),
                                 new SqlParameter("package_id", packageId),
                                 new SqlParameter("is_honeymoon",hsc.IsHoneymoon),
                                 new SqlParameter("onlyBestPrice",zero)
                            }
                                             ).ToList<FoundRoom>();
                            resultlist.AddRange(occupancyResult);

                        }

                        /*
                     EXEC	 [dbo].[FindRoom]
                        @start = '2016-06-01',@end = '2016-06-05',
                        @adults = 2,@age1 = -1,@age2 = -1,@age3 = -1,@age4 = -1,@age5 = -1,
                        @occNum = 1,@ishoneymoon = 0,
                        @providerId = 1143,@currencyId = 0,@onlyBestPrice = 0

                     */
                        else
                        {
                            var occupancyResult = db.Database.SqlQuery<FoundRoom>(string.Format(@"exec {0} @start, @end, @adults,@age1,@age2,@age3,@age4,@age5, @occNum, @ishoneymoon,@providerId,@currencyId,@onlyBestPrice", FINDROOM)
                                                , new object[]
                              {
                                 new SqlParameter("start", hsc.FromDate),
                                 new SqlParameter("end", hsc.ToDate),
                                 new SqlParameter("adults", occupancy.Adults),
                                 new SqlParameter("age1", occupancy.Ages.Count>0?occupancy.Ages[0]:-1),
                                 new SqlParameter("age2", occupancy.Ages.Count>1?occupancy.Ages[1]:-1),
                                 new SqlParameter("age3", occupancy.Ages.Count>2?occupancy.Ages[2]:-1),
                                 new SqlParameter("age4", occupancy.Ages.Count>3?occupancy.Ages[3]:-1),
                                 new SqlParameter("age5", occupancy.Ages.Count>4?occupancy.Ages[4]:-1),

                                 new SqlParameter("occNum",occNum),
                                 new SqlParameter("ishoneymoon", hsc.IsHoneymoon?1:0),
                                 new SqlParameter("providerId",providerId),
                                 new SqlParameter("currencyId",currency_id==null?zero:currency_id),
                                 new SqlParameter("onlyBestPrice",zero)
                            }
                                              ).ToList<FoundRoom>();
                            resultlist.AddRange(occupancyResult);

                        }


                    }
                    occNum++;
                }



                //serialize search criteria and add to session
                UserSession.Current.HotelSearchCriteriaXML = Utils.SerializeToXml<HotelSearchCriteria>(hsc);
                //var selectedrooms = db.bookingGuests.Where(x => x.booking_ref == bookingref).Select(x => new {  x.room_id, x.room_number }).Distinct();
                var selectedrooms = db.bookingRooms.Where(x => x.booking_ref == bookingref).ToList();

                List<int> existingOccupancies = new List<int>();
                foreach (var x in selectedrooms)
                {
                    int occupancyid = (x.room_id * 10) + x.room_number;
                    existingOccupancies.Add(occupancyid);

                }

                //TODO
                int minusone = -1;

                //charges

                var ChargeNameResult = db.Database.SqlQuery<FoundChargeName>(string.Format(@"exec {0} @booking_id, @start, @end, @provider_id, @currency_id", FINDCHARGENAME)
                                               , new object[]
                             {
                                 new SqlParameter("booking_id", minusone),
                                 new SqlParameter("start", hsc.FromDate),
                                 new SqlParameter("end", (hsc.IsPackage?hsc.FromDate.AddDays(hsc.NumNightsPackage):hsc.ToDate)),
                                 new SqlParameter("provider_id",providerId),
                                 new SqlParameter("currency_id",currency_id==null?zero:currency_id)
                           }
                                             ).ToList<FoundChargeName>();

                var chargeName = string.Empty;
                if (ChargeNameResult.Count > 1)
                {
                    int prev = 0;
                    StringBuilder sb = new StringBuilder();
                    foreach (var cnr in ChargeNameResult)
                    {
                        if (cnr.supplement_id != prev)
                        {
                            sb.Append(cnr.name + " ");

                        }
                        prev = cnr.supplement_id;
                    }
                    sb.Length--;
                    chargeName = sb.ToString();

                } else if (ChargeNameResult.Count == 1)
                {
                    chargeName = ChargeNameResult.FirstOrDefault().name;
                }


                foreach (var r in resultlist)
                {

                    room room = db.rooms.Find(r.room_id);
                    var roomOccupancies = room.occupancies.ToList();
                    r.RoomOccupancy = frontService.GetRoomOccupancy(hsc.SearchOccupancies.Where(x => x.RoomNumber == r.room_sequence).FirstOrDefault(), r.provider_id);

                    r.charge_name = chargeName;


                    if (roomOccupancies.Exists(x => x.occupancyCode == r.RoomOccupancy.OccupancyCode))
                    {


                        r.Room = room;
                        r.IsHoneyMoon = hsc.IsHoneymoon;
                        //r.Promotions = GetPromotions(r.from_date ?? DateTime.Now, r.to_date ?? DateTime.Now, r.provider_id, r.room_id, r.market_id, hsc.IsHoneymoon);
                        var psets = GetPromotionSets(r.from_date ?? DateTime.Now, r.to_date ?? DateTime.Now, r.provider_id, r.room_id, r.market_id, hsc.IsHoneymoon);

                        TimeSpan numnights = (r.to_date ?? DateTime.Now).Subtract(r.from_date ?? DateTime.Now);
                        int _nights = (int)numnights.TotalDays;

                        var max = psets.Max(x => x.promotion_set_id);
                        var min = psets.Min(x => x.promotion_set_id);
                        //NEED to RE LOOK the LOGIC
                        for (int i = min ?? 0; i <= max; i++)
                        {
                            var _psets = psets.Where(x => x.promotion_set_id == i);

                            if (_psets.Count() > 0)
                            {
                                PromosetViewModel pvm = new PromosetViewModel();
                                foreach (var _p in _psets)
                                {
                                    pvm.Promotions.Add(_p);
                                }
                                pvm.setid = i;
                                pvm.nights = Utils.CoalesceInt(r.nights);
                                pvm.TotalRoomCost = r.TotalRoomCost;
                                //For family offer
                                pvm.TotalAdult = r.TotalAdult;
                                pvm.TotalChild1 = r.TotalChild1;
                                pvm.TotalChild2 = r.TotalChild2;
                                pvm.TotalTeen = r.TotalTeen;
                                pvm.TotalInfant = r.TotalInfant;
                                pvm.NoOfChild = Utils.CoalesceInt(r.num_children);
                                pvm.NoOfAdult = Utils.CoalesceInt(r.num_adults);
                                pvm.NoOfTeen = Utils.CoalesceInt(r.num_teens);
                                pvm.GroundHandlingChild = r.GroundHandlingChild;
                                pvm.GroundHandlingTeen = r.GroundHandlingTeen;
                                pvm.GroundHandlingAdult = r.GroundHandlingAdult;
                                //
                                pvm.TotalCompulsoryCharges = r.TotalCompulsoryCharges;
                                pvm.GroundHandling = r.GroundHandling;
                                r.Promosets.Add(pvm);
                            }
                        }



                        var baseprice = db.basePrices.Find(r.base_price_id);
                        r.Baseprice = baseprice;

                        if (existingOccupancies.Contains(r.OccupancyCode))
                        {
                            r.isSelected = true;
                        }

                        model.FoundRooms.Add(r);
                    }
                }


                ViewBag.SearchDet = hsc;

                var searchDet = new SearchDetails();
                if (UserSession.Current.SearchDetails != null)
                {
                    searchDet = UserSession.Current.SearchDetails;
                }
                searchDet.Location = hotel.provider_id.ToString();

                ViewBag.SearchForm = searchDet;
                ViewBag.location = GetHotels(searchDet);
                return View(model);
            }
            catch (Exception e)
            {

                throw;
            }
        }

        ///// <summary>
        ///// 2. Display selected hotel with available room combinations
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="bookingref"></param>
        ///// <returns></returns>
        //public ActionResult Hotel(int? id = null, int? nights = null, int? currency = null, string bookingref = "")
        //{
        //    try
        //    {
        //        booking bkg = new booking();
        //        var isPackage = false;
        //        package _package = new package();
        //        if (!string.IsNullOrWhiteSpace(bookingref))
        //        {
        //            var bsearch = db.bookingSearches.Where(x => x.booking_ref.Equals(bookingref)).ToList();


        //            if (bsearch.Count == 1)
        //            {
        //                var bookingSearch = bsearch.FirstOrDefault();
        //                var searchXml = bookingSearch.search_details;
        //                UserSession.Current.HotelSearchCriteriaXML = searchXml;
        //                id = bookingSearch.hotel_id;

        //                isPackage = bookingSearch.package_id > 0;

        //                _package = db.packages.Find(bookingSearch.package_id);

        //                if (id == 0 && bookingSearch.package_id > 0)
        //                {                            
        //                    id = _package.provider_id;
        //                }
        //            }
        //        }

        //        if (db.bookings.Any(x => x.booking_ref == bookingref))
        //        {
        //            bkg = db.bookings.Where(x => x.booking_ref == bookingref).FirstOrDefault();
        //            if(bkg!= null)
        //            {
        //                refurbishbooking(bkg);
        //            }
        //        }


        //        ViewBag.Booking = bkg;

        //        var hotel = db.providers.Find(id);

        //        var searchCritXml = UserSession.Current.HotelSearchCriteriaXML;
        //        HotelSearchCriteria hsc = Utils.DeserializeXml<HotelSearchCriteria>(searchCritXml);

        //        RoomsFoundViewModel model = new RoomsFoundViewModel();
        //        model.hotel = hotel;
        //        model.BookingRef = bookingref;
        //        model.numberRoomsRequired = hsc.SearchOccupancies.Count;


        //        List<FoundRoom> Finalresultlist = new List<FoundRoom>();



        //        // this time recreate search occupancy based on hotel GuestAges
        //        var occupancies = frontService.GetRoomOccupancies(hsc.SearchOccupancies, hotel.provider_id).ToList();

        //        //replace occupancies in hotelsearchcriteria
        //       // hsc.SearchOccupancies= occupancies;
        //        hsc.HotelId = id.Value;
        //        /*
        //        EXEC [dbo].[FindRoom]
        //          @start = '2016-06-01',@end = '2016-06-05',
        //          @adults = 2,
        //          @age1 = NULL,@age2 = NULL,@age3 = NULL,@age4 = NULL,@age5 = NULL,
        //          @occNum = 1,
        //          @ishoneymoon = 0,
        //          @providerId = 1143,
        //          @currencyId = 0, @onlyBestPrice = 0

        //        */
        //        int zero = 0;
        //        foreach (var occupancy in hsc.SearchOccupancies)
        //        {
        //            using (var ctx = new ABSDBEntities())
        //            {


        //                //Get student name of string type
        //                List<FoundRoom> resultlist = db.Database.SqlQuery<FoundRoom>(@"exec FindRooms @start, @end, @adults, @age1, @age2,@age3,@age4,@occNum,@ishoneymoon,@providerId,@currencyId,@onlyBestPrice "
        //                    , new object[]
        //                    {
        //                         new SqlParameter("start", hsc.FromDate),
        //                         new SqlParameter("end", isPackage ? hsc.FromDate.AddDays(_package.number_nights.HasValue?_package.number_nights.Value:0)  :hsc.ToDate),
        //                         new SqlParameter("adults", occupancy.Adults),
        //                   new SqlParameter("age1", occupancy.Ages.Count>0?occupancy.Ages[0]:-1),
        //                         new SqlParameter("age2", occupancy.Ages.Count>1?occupancy.Ages[1]:-1),
        //                         new SqlParameter("age3", occupancy.Ages.Count>2?occupancy.Ages[2]:-1),
        //                         new SqlParameter("age4", occupancy.Ages.Count>3?occupancy.Ages[3]:-1),
        //                         new SqlParameter("age5", occupancy.Ages.Count>4?occupancy.Ages[4]:-1),
        //                         new SqlParameter("occNum", occupancy.RoomNumber),
        //                         new SqlParameter("ishoneymoon", hsc.IsHoneymoon?1:zero),
        //                         new SqlParameter("providerId", id),
        //                         new SqlParameter("currencyId", currency),
        //                         new SqlParameter("onlyBestPrice", zero)
        //                    }
        //                    ).ToList<FoundRoom>();

        //                Finalresultlist.AddRange(resultlist);
        //            }

        //        }

        //        //serialize search criteria and add to session
        //        UserSession.Current.HotelSearchCriteriaXML = Utils.SerializeToXml<HotelSearchCriteria>(hsc);
        //        //var selectedrooms = db.bookingGuests.Where(x => x.booking_ref == bookingref).Select(x => new {  x.room_id, x.room_number }).Distinct();
        //        var selectedrooms = db.bookingRooms.Where(x => x.booking_ref == bookingref).ToList();

        //        List<int> existingOccupancies = new List<int>();
        //        foreach (var x in selectedrooms)
        //        {
        //            int occupancyid = (x.room_id * 10) + x.room_number;
        //            existingOccupancies.Add(occupancyid);

        //        }


        //        foreach (var res in Finalresultlist)
        //        {

        //            var baseprice = db.basePrices.Find(res.base_price_id);

        //            var promotions = GetPromotions(hsc.FromDate,
        //                isPackage ? hsc.FromDate.AddDays(_package.number_nights.HasValue?_package.number_nights.Value:0):hsc.ToDate
        //                ,id.HasValue?id.Value:0, res.room_id,baseprice.market_id,hsc.IsHoneymoon );


        //            var promotionSets = GetPromotionSets(hsc.FromDate,
        //                isPackage ? hsc.FromDate.AddDays(_package.number_nights.HasValue ? _package.number_nights.Value : 0) : hsc.ToDate
        //                , id.HasValue ? id.Value : 0, res.room_id, baseprice.market_id, hsc.IsHoneymoon);


        //            var room = db.rooms.Find(res.room_id);


        //            var rnp = new RoomAndPrice();

        //            rnp.Promotions = promotions;
        //            rnp.PromotionSets = promotionSets;
        //            TimeSpan span = hsc.ToDate - hsc.FromDate;

        //            rnp.numberNights = span.Days;
        //            rnp.baseprice = baseprice;
        //            rnp.room = room;
        //            rnp.OccupancyNumber = res.room_sequence.Value;
        //            rnp.numberAdults = res.num_adults.Value;
        //            rnp.numberInfants = res.num_infants.Value;
        //            rnp.numberChildren = res.num_children.Value;
        //            rnp.numberTeens = res.num_teens.Value;

        //            if(hsc.IsHoneymoon)
        //            {
        //                rnp.TotalPriceAdults = res.total_per_honeymooner.Value;
        //                rnp.TotalPrice = res.total_per_honeymooner.Value * 2;

        //            }
        //            else
        //            {
        //            rnp.TotalPriceAdults = res.total_per_adult.Value;
        //            rnp.TotalPrice = res.total_per_adult.Value * res.num_adults.Value;

        //            }
        //            rnp.TotalPriceInfants = res.total_per_infant.Value * res.num_infants.Value;
        //            rnp.TotalPriceChildren = res.total_per_child.Value * res.num_children.Value;
        //            rnp.TotalPriceTeens = res.total_per_teen.Value * res.num_teens.Value;

        //            if (existingOccupancies.Contains(rnp.OccupancyCode))
        //            {
        //                rnp.isSelected = true;
        //            }

        //            model.roomAndPrice.Add(rnp);

        //        }


        //        return View(model);
        //    }
        //    catch (Exception e)
        //    {

        //        throw;
        //    }
        //}


        public ActionResult Package(int? id, string bookingref = "")
        {
            try
            {
                booking bkg = new booking();

                if (!string.IsNullOrWhiteSpace(bookingref))
                {
                    var bsearch = db.bookingSearches.Where(x => x.booking_ref.Equals(bookingref)).ToList();


                    if (bsearch.Count == 1)
                    {
                        var bookingSearch = bsearch.FirstOrDefault();
                        var searchXml = bookingSearch.search_details;
                        UserSession.Current.HotelSearchCriteriaXML = searchXml;
                        id = bookingSearch.package_id;


                    }
                }

                if (db.bookings.Any(x => x.booking_ref == bookingref))
                {
                    bkg = db.bookings.Where(x => x.booking_ref == bookingref).FirstOrDefault();
                }


                var package = db.packages.Find(id);

                bkg.package = package;
                ViewBag.Booking = bkg;

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var provider_id = package.provider_id;
                var hotel = db.providers.Find(provider_id);


                var searchCritXml = UserSession.Current.HotelSearchCriteriaXML;
                HotelSearchCriteria hsc = Utils.DeserializeXml<HotelSearchCriteria>(searchCritXml);

                RoomsFoundViewModel model = new RoomsFoundViewModel();
                model.hotel = hotel;
                model.BookingRef = bookingref;
                model.numberRoomsRequired = hsc.SearchOccupancies.Count;

                int hotelid = hotel.provider_id;

                List<FoundRoom> Finalresultlist = new List<FoundRoom>();



                // this time recreate search occupancy based on hotel GuestAges
                var occupancies = frontService.GetRoomOccupancies(hsc.SearchOccupancies, hotel.provider_id).ToList();

                //replace occupancies in hotelsearchcriteria
                //hsc.SearchOccupancies= occupancies;

                //represents package
                hsc.PackageId = id.Value;

                var start = hsc.FromDate;
                var end = start.AddDays(hsc.NumNightsPackage);
                var currency_id = 0;
                var nights = 0;


                /*
                 EXEC	[dbo].[FindPackage]
		            @start = '2016-07-01',
		            @adults = 2,
		            @age1 = NULL,@age2 = NULL,@age3 = NULL,@age4 = NULL,@age5 = NULL,
		            @nights = 0,
		            @currency_id = 0,@hotel_id = 0,@room_num = 1,@package_id = 100,
		            @is_honeymoon = 0,@onlyBestPrice = 0
                */

                int zero = 0;
                foreach (var occupancy in hsc.SearchOccupancies)
                {
                    using (var ctx = new ABSDBEntities())
                    {


                        List<FoundRoom> aList = db.Database.SqlQuery<FoundRoom>(string.Format(@"exec {0} @start,@adults,@age1,@age2,@age3,@age4,@age5,@nights,@currency_id,@hotel_id,@room_num,@package_id,@is_honeymoon,@onlyBestPrice", GETPACKAGES)
                                , new object[]
                                {
                                 new SqlParameter("start", start),
                                 new SqlParameter("adults", occupancy.Adults),
                     new SqlParameter("age1", occupancy.Ages.Count>0?occupancy.Ages[0]:-1),
new SqlParameter("age2", occupancy.Ages.Count>1?occupancy.Ages[1]:-1),
new SqlParameter("age3", occupancy.Ages.Count>2?occupancy.Ages[2]:-1),
new SqlParameter("age4", occupancy.Ages.Count>3?occupancy.Ages[3]:-1),
new SqlParameter("age5", occupancy.Ages.Count>4?occupancy.Ages[4]:-1),
                                 new SqlParameter("nights",nights),
                                 new SqlParameter("currency_id", currency_id),
                                 new SqlParameter("hotel_id", hotelid),
                                 new SqlParameter("room_num", occupancy.RoomNumber),
                                 new SqlParameter("package_id", id),
                                 new SqlParameter("is_honeymoon", hsc.IsHoneymoon?1:zero),
                                 new SqlParameter("onlyBestPrice", zero),
                                }
                            ).ToList<FoundRoom>();

                        if (aList.Count > 0)
                        {
                            foreach (var l in aList)
                            {
                                l.room_sequence = occupancy.RoomNumber;
                                l.num_adults = occupancy.Adults;
                                //need to find replaceent for below
                                //l.num_infants = occupancy.NumInfants;
                                //l.num_children= occupancy.NumChildren;
                                //l.num_teens = occupancy.NumTeens;
                            }
                        }

                        Finalresultlist.AddRange(aList);
                    }

                }

                //serialize search criteria and add to session
                UserSession.Current.HotelSearchCriteriaXML = Utils.SerializeToXml<HotelSearchCriteria>(hsc);
                //var selectedrooms = db.bookingGuests.Where(x => x.booking_ref == bookingref).Select(x => new {  x.room_id, x.room_number }).Distinct();
                var selectedrooms = db.bookingRooms.Where(x => x.booking_ref == bookingref).ToList();

                List<int> existingOccupancies = new List<int>();
                foreach (var x in selectedrooms)
                {
                    int occupancyid = (x.room_id * 10) + x.room_number;
                    existingOccupancies.Add(occupancyid);

                }


                foreach (var res in Finalresultlist)
                {
                    var promotions = GetPromotions(start, start.AddDays(package.number_nights.Value), hotelid, res.room_id, res.market_id, hsc.IsHoneymoon);

                    var room = db.rooms.Find(res.room_id);
                    var baseprice = db.basePrices.Find(res.base_price_id);
                    var rnp = new RoomAndPrice();

                    TimeSpan span = hsc.ToDate - hsc.FromDate;

                    rnp.numberNights = res.nights.Value;
                    rnp.baseprice = baseprice;
                    rnp.room = room;
                    rnp.OccupancyNumber = res.room_sequence.Value;
                    rnp.numberAdults = res.num_adults.Value;
                    rnp.numberInfants = res.num_infants.Value;
                    rnp.numberChildren = res.num_children.Value;
                    rnp.numberTeens = res.num_teens.Value;

                    rnp.Promotions = promotions;

                    if (hsc.IsHoneymoon)
                    {

                        rnp.TotalPriceAdults = res.total_per_honeymooner.Value + res.ground_handling_adult.Value;
                        rnp.TotalPrice = ((res.total_per_honeymooner.Value * 2) + res.total_cost_ground_handling.Value);

                    }
                    else
                    {

                        rnp.TotalPriceAdults = res.total_per_adult.Value + res.ground_handling_adult.Value;

                        rnp.TotalPrice = (res.total_per_adult.Value + res.total_cost_ground_handling.Value) * res.num_adults.Value;

                    }
                    rnp.TotalPriceInfants = res.total_per_infant.Value * res.num_infants.Value;
                    rnp.TotalPriceChildren = (res.total_per_child.Value + res.ground_handling_child.Value) * res.num_children.Value;
                    rnp.TotalPriceTeens = (res.total_per_teen.Value + res.ground_handling_teen.Value) * res.num_teens.Value;

                    if (existingOccupancies.Contains(rnp.OccupancyCode))
                    {
                        rnp.isSelected = true;
                    }

                    model.roomAndPrice.Add(rnp);

                }
                ViewBag.PackageId = id.Value;

                return View(model);
            }
            catch (Exception e)
            {

                throw;
            }
        }




        /// <summary>
        /// 3. create booking in database 
        /// </summary>
        /// <param name="baseprice"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateBooking(int[] baseprice, int[] roomid, int[] allotment, AppEnums.BookingType bookingtype, double[] totalcharges, string[] chargesname, string submitaction, string bookingref = "", int package_id = 0, int[] promoset = null)
        {

            try
            {
                var session = UserSession.Current;
                var isnewbooking = string.IsNullOrWhiteSpace(bookingref);
                var bookingid = 0;

                //retrieve loaded search criterion from session
                var searchCrit = Utils.DeserializeXml<HotelSearchCriteria>(session.HotelSearchCriteriaXML);

                var _hotel_id_for_package = 0;
                package _package = new package();
                if (package_id > 0)
                {

                    _package = db.packages.Find(package_id);
                    _hotel_id_for_package = _package.provider_id;
                }

                //check if booking exists or if new booking (if new create a booking search)
                if (String.IsNullOrWhiteSpace(bookingref))
                {
                    var bookingSearch = new bookingSearch();
                    bookingSearch.search_details = session.HotelSearchCriteriaXML;
                    bookingSearch.hotel_id = searchCrit.HotelId;
                    bookingSearch.booking_ref = Guid.NewGuid().ToString("N");
                    bookingSearch.timestamp = DateTime.Now;
                    if (package_id > 0)
                    {
                        bookingSearch.package_id = package_id;
                        bookingSearch.hotel_id = _hotel_id_for_package;
                    }

                    db.bookingSearches.Add(bookingSearch);
                    db.SaveChanges();

                    bookingref = bookingSearch.booking_ref;
                }

                var bookings = db.bookings.Where(r => r.booking_ref == bookingref).ToList();

                booking b = new booking();
                if (bookings.Count == 1)
                {
                    b = bookings.FirstOrDefault();
                    if (b.isPackage) {
                        _package = db.packages.Find(b.package_id);
                        _hotel_id_for_package = _package.provider_id;
                    }
                }

                int[] chosen_baseprices = new int[100];

                var ii = 0;
                foreach (var _baseprice in baseprice)
                {
                    if (_baseprice > 0)
                    {
                        chosen_baseprices[ii] = _baseprice;
                        ii++;
                    }
                }

                var existingrooms = db.bookingRooms.Where(x => x.booking_id == b.booking_id).ToList();
                bool nochange = false;
                var iii = 0;
                if (existingrooms.Count > 0)
                {
                    foreach (var existingroom in existingrooms.OrderBy(r => r.room_number))
                    {
                        if (existingroom.base_price_id != chosen_baseprices[iii])
                        {
                            nochange = false;
                        }
                        iii++;
                    }

                }


                //compulsorycharges
                double TotalCompulsoryCharges = 0;
                StringBuilder chargesNameSB = new StringBuilder();
                string prevCName = string.Empty;
                for (int i = 0; i < baseprice.Count(); i++)
                {
                    if (baseprice[i] > 0)
                    {
                        TotalCompulsoryCharges += totalcharges[i];

                        if (!string.IsNullOrWhiteSpace(chargesname[i]))
                        {
                            if (chargesname[i] != prevCName)
                            {
                                chargesNameSB.Append(chargesname[i] + " ");
                            }
                            prevCName = chargesname[i];
                        }
                    }
                }


                //retrieve market 
                var market_id = 0;
                var marketName = "";
                for (int i = 0; i < baseprice.Count(); i++)
                {
                    if (baseprice[i] > 0)
                    {
                        var bp = db.basePrices.Find(baseprice[i]);
                        market_id = bp.market.market_id;
                        marketName = bp.market.market_name;


                        break;
                    }
                }

                var mkt = db.markets.Find(market_id);


                // if new booking create a new booking 
                if (isnewbooking)
                {

                    b.booking_type = (int)bookingtype;

                    b.booking_name = string.Concat("New Booking-", DateTime.Now.ToString("yyyyMMddHHmmss"));
                    b.agent_user_id = session.UserId;
                    b.agent_name = session.Fullname;
                    b.checkin_date = searchCrit.FromDate;
                    b.checkout_date = bookingtype == AppEnums.BookingType.PACKAGE ? searchCrit.FromDate.AddDays(_package.number_nights.HasValue ? _package.number_nights.Value : 0) : searchCrit.ToDate;
                    b.num_nights = bookingtype == AppEnums.BookingType.PACKAGE ? _package.number_nights : Utils.NumOfNightsBetweenDates(searchCrit.FromDate, searchCrit.ToDate);
                    b.create_date = DateTime.Now;
                    b.booking_date = DateTime.Now;
                    b.booking_ref = bookingref;
                    b.status = 0;
                    b.market_id = market_id;
                    b.market_name = marketName;
                    b.provider_id = searchCrit.HotelId;
                    var provname = db.providers.Where(x => x.provider_id == searchCrit.HotelId).First().name;
                    b.HotelName = provname == null ? "" : provname;
                    b.provider_name = provname == null ? "" : provname;
                    if (mkt != null)
                    {
                        b.currency_code = mkt.currency.currency_code;
                        b.symbol = mkt.currency.symbol;
                    }


                    if (bookingtype == AppEnums.BookingType.PACKAGE)
                    {
                        b.package_id = package_id;
                        b.provider_id = _hotel_id_for_package;
                    }

                    b.is_credit = session.IsUserCreditBased;

                    b.compulsory_charges_amt = TotalCompulsoryCharges;
                    b.charge_name = chargesNameSB.ToString();
                    db.bookings.Add(b);
                    db.SaveChanges();

                    //re-save the booking name using the booking id
                    b.booking_name = "#" + b.booking_id;
                    db.Entry(b).State = EntityState.Modified;
                    db.SaveChanges();

                }
                // else retrieve the bookingref of existing booking
                else
                {
                    b = db.bookings.Where(x => x.booking_ref == bookingref).FirstOrDefault();

                    //reset free_nights
                    b.free_nights = 0;

                }

                //assign booking id whether new or existing
                bookingid = b.booking_id;
                UserSession.Current.BookingId = bookingid;


                int adults = 0, teens = 0, infants = 0, children = 0, rooms = 0;



                //if booking already exists, delete all entries from booking_guest and re-create below
                if (!isnewbooking)
                {
                    db.bookingGuests.Where(x => x.booking_ref == b.booking_ref).Delete();

                    db.bookingSupplements.Where(x => x.booking_id == bookingid).Delete();
                    //using entity framework extensions
                    db.bookingPromotions.Where(x => x.booking_id == bookingid).Delete();
                    db.bookingRooms.Where(x => x.booking_id == bookingid).Delete();
                }


                //create entries in booking_guest table
                for (int i = 0; i < baseprice.Count(); i++)
                {
                    if (baseprice[i] > 0)
                    {
                        int bpid = baseprice[i];
                        var BasePrice = db.basePrices.Where(x => x.base_price_id == bpid).Include(x => x.room).FirstOrDefault();
                        var prices = new List<viewPrice>();



                        var promotionsets = GetPromotionSets(
                            searchCrit.FromDate,
                            (bookingtype == AppEnums.BookingType.PACKAGE ? searchCrit.FromDate.AddDays(_package.number_nights.HasValue ? _package.number_nights.Value : 0) : searchCrit.ToDate),
                            bookingtype == AppEnums.BookingType.PACKAGE ? _hotel_id_for_package : searchCrit.HotelId,
                            roomid[i],
                            market_id,
                            searchCrit.IsHoneymoon);

                        List<PromotionSetResult> promotions = new List<PromotionSetResult>();

                        if (promoset != null)
                        {
                            promotions = promotionsets.Where(x => x.promotion_set_id == promoset[i]).ToList();
                        }


                        if (promotions != null && promotions.Count() > 0)
                        {
                            var freenights = promotions.Where(x => x.free_nights > 0).ToList();
                            if (freenights.Count > 0)
                            {
                                var fn = freenights.FirstOrDefault().free_nights;
                                var bk = db.bookings.Find(b.booking_id);
                                bk.free_nights = fn;

                                db.Entry(bk).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                        // var promotional_discount_percentage = 0.0;



                        //var adults =   UserSession.Current.CurrentSearchDetails.RoomAdults[count];
                        IList<SearchOccupancy> occupancies = searchCrit.SearchOccupancies;

                        if (roomid[i] > 0)
                        {
                            var r = roomid[i];

                            var end_date = searchCrit.ToDate;

                            if (searchCrit.ToDate < searchCrit.FromDate)
                            {
                                if (searchCrit.PackageId > 0)
                                {
                                    var package = db.packages.Find(searchCrit.PackageId);
                                    end_date = searchCrit.FromDate.AddDays(package.number_nights.Value);
                                }
                            }

                            prices = db.viewPrices.Where(x => x.room_id == r && x.fulldate >= searchCrit.FromDate && x.fulldate < end_date).ToList();
                        }


                        var mealplan = BasePrice.meal_plan_id;

                        //guest type = 1: adult, 2: infant, 3: child, 4: teen , 5: senior
                        rooms++;

                        var occupancy = occupancies.Where(x => x.RoomNumber == rooms).FirstOrDefault();

                        var roomOccupancy = frontService.GetRoomOccupancy(occupancy, Utils.CoalesceInt(b.provider_id));

                        int? _allotment = null;
                        if (allotment != null)
                        {
                            _allotment = allotment[i];
                        }


                        bookingRoom booking_room = new bookingRoom
                        {
                            booking = b,
                            room_id = BasePrice.room_id,
                            base_price_id = BasePrice.base_price_id,
                            meal_plan_id = BasePrice.meal_plan_id,
                            room_number = rooms,
                            room_name = BasePrice.room.name,
                            booking_ref = bookingref,
                            adults = occupancy.Adults,
                            infants = roomOccupancy.NumInfants,
                            children = roomOccupancy.NumChildren,
                            teens = roomOccupancy.NumTeens,
                            allotments = _allotment

                        };

                        db.bookingRooms.Add(booking_room);
                        db.SaveChanges();

                        if (promotions != null && promotions.Count() > 0)
                        {

                            // promotional_discount_percentage = promotions.Sum(x => x.discount) / 100.0;
                            List<bookingPromotion> bookprom =
                              promotions.Select(data => new bookingPromotion
                              {
                                  booking = b,
                                  booking_room_id = booking_room.booking_room_id,
                                  free_nights = data.free_nights,
                                  discount = data.discount,
                                  promotion_id = data.promotion_id,
                                  promotion_name = data.name,
                                  promotion_description = data.description

                              }).ToList();
                            db.bookingPromotions.AddRange(bookprom);
                            db.SaveChanges();
                        }

                        //RECALCULATE
                        //var adult_price_total = 0;

                        var adult_price_total = roomOccupancy.NumAdults == 1 ? prices.Sum(x => x.price_single) :
                                (roomOccupancy.NumAdults == 2 && prices.Sum(x => x.price_twin) > 0 ? prices.Sum(x => x.price_twin) :
                                 (roomOccupancy.NumAdults == 3 && prices.Sum(x => x.price_triple) > 0 ? prices.Sum(x => x.price_triple) :
                                 roomOccupancy.NumAdults == 4 && prices.Sum(x => x.price_quadruple) > 0 ? prices.Sum(x => x.price_quadruple) :
                                   prices.Sum(x => x.price_single))
                                   );

                        if (searchCrit.IsHoneymoon)
                        {
                            //RECALCUALTE
                            adult_price_total = prices.Sum(x => x.price_honeymoon) > 0 ? prices.Sum(x => x.price_honeymoon) : adult_price_total;
                        }

                        var adult_price_per_night = (adult_price_total / prices.Count);
                        var teen_price_per_night = prices.Sum(x => x.price_teen) / prices.Count;
                        var child_price_per_night = prices.Sum(x => x.price_child) / prices.Count;
                        var infant_price_per_night = prices.Sum(x => x.price_infant) / prices.Count;

                        //check for flat_rate offer.
                        adult_price_per_night = GetAdultOfferRate(adult_price_per_night, roomOccupancy.NumAdults, promotions.ToList());
                        teen_price_per_night = GetTeenOfferRate(teen_price_per_night, promotions.ToList());

                        // Find and set if child1 and child2 inn family offer
                        var child1_price_per_night = GetChild1OfferRate(child_price_per_night, promotions.ToList());
                        var child2_price_per_night = GetChild2OfferRate(child_price_per_night, promotions.ToList());

                        for (int j = 0; j < occupancy.Adults; j++)
                        {
                            var _bp = occupancy.Adults == 1 ? BasePrice.price_single :
                                (occupancy.Adults == 2 && BasePrice.price_twin > 0 ? BasePrice.price_twin :
                                 (occupancy.Adults == 3 && BasePrice.price_triple > 0 ? BasePrice.price_triple :
                                   BasePrice.price_single)
                                   );

                            adults++;
                            bookingGuest bg = new bookingGuest
                            {
                                booking_room_id = booking_room.booking_room_id,
                                price_per_night = adult_price_per_night,
                                markup_amt = adult_price_per_night * (session.PercMarkup),
                                guest_type = (int)AppEnums.GuestType.ADULT,
                                booking_ref = bookingref,
                                booking_id = b.booking_id,
                                promotional_discount_amt = GetDiscountAmount(adult_price_per_night, promotions.ToList())
                            };

                            if (bookingtype == AppEnums.BookingType.PACKAGE)
                            {
                                bg.ground_handling_amt = _package.price_ground_handling_adult;
                            }

                            db.bookingGuests.Add(bg);
                            db.SaveChanges();

                        }

                        for (int j = 0; j < roomOccupancy.NumInfants; j++)
                        {
                            infants++;
                            bookingGuest bg = new bookingGuest
                            {
                                booking_room_id = booking_room.booking_room_id,
                                price_per_night = infant_price_per_night,
                                markup_amt = infant_price_per_night * (session.PercMarkup),
                                guest_type = (int)AppEnums.GuestType.INFANT,
                                age = roomOccupancy.InfantAges[j],
                                booking_ref = bookingref,
                                booking_id = b.booking_id,
                                promotional_discount_amt = GetDiscountAmount(infant_price_per_night, promotions.ToList())
                            };

                            db.bookingGuests.Add(bg);
                            db.SaveChanges();

                        }

                        //Children max 2
                        for (int j = 0; j < roomOccupancy.NumChildren; j++)
                        {
                            children++;
                            bookingGuest bg = new bookingGuest
                            {
                                booking_room_id = booking_room.booking_room_id,
                                price_per_night = (j == 0) ? child1_price_per_night : child2_price_per_night,      // SET child1 or child2 price.
                                markup_amt = ((j == 0) ? child1_price_per_night : child2_price_per_night) * (session.PercMarkup),
                                guest_type = (int)AppEnums.GuestType.CHILD,
                                age = roomOccupancy.ChildAges[j],
                                booking_ref = bookingref,
                                booking_id = b.booking_id,
                                promotional_discount_amt = GetDiscountAmount((j == 0) ? child1_price_per_night : child2_price_per_night, promotions.ToList())

                            };

                            if (bookingtype == AppEnums.BookingType.PACKAGE)
                            {
                                bg.ground_handling_amt = _package.price_ground_handling_child;
                            }


                            db.bookingGuests.Add(bg);
                            db.SaveChanges();

                        }

                        for (int j = 0; j < roomOccupancy.NumTeens; j++)
                        {
                            teens++;
                            bookingGuest bg = new bookingGuest
                            {
                                booking_room_id = booking_room.booking_room_id,
                                price_per_night = teen_price_per_night,
                                markup_amt = teen_price_per_night * (session.PercMarkup),
                                guest_type = (int)AppEnums.GuestType.TEEN,
                                age = roomOccupancy.TeenAges[j],
                                booking_ref = bookingref,
                                booking_id = b.booking_id,
                                promotional_discount_amt = GetDiscountAmount(teen_price_per_night, promotions.ToList())
                            };

                            if (bookingtype == AppEnums.BookingType.PACKAGE)
                            {
                                bg.ground_handling_amt = _package.price_ground_handling_teen;
                            }

                            db.bookingGuests.Add(bg);
                            db.SaveChanges();
                        }

                    }
                }

                //update table bookings with occupancy and room                 
                b.rooms = rooms;
                b.adults = adults;
                b.infants = infants;
                b.children = children;
                b.teens = teens;

                db.Entry(b).State = EntityState.Modified;
                db.SaveChanges();

                if (submitaction == "continue")
                {
                    return RedirectToAction("Supplements", new { id = b.booking_id });

                }
                return RedirectToAction("ConfirmBooking", new { id = b.booking_id });
            }
            catch (Exception e)
            {

                throw;
            }
        }

        private double GetDiscountAmount(double? Cost, List<PromotionSetResult> Promotions)
        {

            // double totlDiscount = 0;
            var cost = Cost ?? 0.0;

            List<double> _promDiscOnly = new List<double>();
            foreach (var promotion in Promotions)
            {
                if (promotion.discount > 0)
                {
                    _promDiscOnly.Add(promotion.discount);
                }
                //totlDiscount = totlDiscount + ((cost - totlDiscount) * (promotion.discount / 100));
            }
            return cost - Utils.AmtLessMultipleDisc(cost, _promDiscOnly);

            //return totlDiscount;

        }
        private double GetAdultOfferRate(double? Cost, int NumAdults, List<PromotionSetResult> Promotions)
        {

            // double totlDiscount = 0;
            var cost = Cost ?? 0.0;

            if (Promotions.Count > 0)
            {
                var p = Promotions.Where(x => x.promo_categ == "Family Offer" || x.promo_categ == "Flat Rate").FirstOrDefault();
                if (p != null)
                {
                    if (NumAdults == 1 && p.flat_single != null)
                    {
                        return Utils.CoalesceDouble(p.flat_single);
                    }
                    if (NumAdults == 2 && p.flat_twin != null)
                    {
                        return Utils.CoalesceDouble(p.flat_twin);
                    }
                    if (NumAdults == 3 && p.flat_triple != null)
                    {
                        return Utils.CoalesceDouble(p.flat_triple);
                    }
                    if (NumAdults == 3 && p.flat_quadruple != null)
                    {
                        return Utils.CoalesceDouble(p.flat_quadruple);
                    }
                    return cost;
                }
            }
            return cost;

            //return totlDiscount;

        }

        private double GetChild1OfferRate(double? Cost, List<PromotionSetResult> Promotions)
        {

            // double totlDiscount = 0;
            var cost = Cost ?? 0.0;

            if (Promotions.Count > 0)
            {
                var p = Promotions.Where(x => x.promo_categ == "Family Offer" || x.promo_categ == "Flat Rate").FirstOrDefault();
                if (p != null && p.flat_child1 != null)
                {
                    return Utils.CoalesceDouble(p.flat_child1);
                }
            }
            return cost;

            //return totlDiscount;

        }
        private double GetTeenOfferRate(double? Cost, List<PromotionSetResult> Promotions)
        {

            // double totlDiscount = 0;
            var cost = Cost ?? 0.0;

            if (Promotions.Count > 0)
            {
                var p = Promotions.Where(x => x.promo_categ == "Family Offer" || x.promo_categ == "Flat Rate").FirstOrDefault();
                if (p != null && p.flat_teen1 != null)
                {
                    return Utils.CoalesceDouble(p.flat_teen1);
                }
            }
            return cost;

            //return totlDiscount;

        }
        private double GetChild2OfferRate(double? Cost, List<PromotionSetResult> Promotions)
        {

            //double totlDiscount = 0;
            var cost = Cost ?? 0.0;

            if (Promotions.Count > 0)
            {
                var p = Promotions.Where(x => x.promo_categ == "Family Offer" || x.promo_categ == "Flat Rate").FirstOrDefault();
                if (p != null && p.flat_child2 != null)
                {
                    return Utils.CoalesceDouble(p.flat_child2);
                }
            }
            return cost;

            //return totlDiscount;
        }



        public ActionResult Supplements(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);
            refurbishbooking(booking);

            if (booking == null)
            {
                return HttpNotFound();
            }


            IReservationService res = new ReservationService();
            List<GetSupplementsResult> supps = res.GetReservationSupplements(id.Value, booking);

            ViewBag.Booking = booking;
            return View(supps);

        }

        [HttpPost]
        public ActionResult AddSupplement(int booking_id, string[] mealplan, string[] supplement, bool[] chosen, string submitaction)
        {
            try
            {

                IReservationService res = new ReservationService();
                res.SaveReservationSupplements(booking_id, mealplan, supplement, chosen);


                if (submitaction == "continue")
                {
                    return RedirectToAction("transfer", new { id = booking_id });
                }
                else
                {
                    return RedirectToAction("ConfirmBooking", new { id = booking_id });
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }


        public ActionResult ConfirmBooking(int? id)
        {
            try
            {

                var booking = db.bookings.Find(id);
                refurbishbooking(booking);
                var booking_guests = db.bookingGuests.Where(x => x.booking_id == id);

                ViewBag.Booking = booking;
                return View(booking_guests);

            }
            catch (Exception)
            {

                throw;
            }
        }



        [HttpPost]
        //public ActionResult SaveConfirmBooking(int booking_id, int num_guests, int[] guest_id, 
        //    string customer_name, string customer_email, string customer_phone,
        //    int[] gender, string[] firstname, string[] lastname, DateTime[] date_of_birth, string[] salutation,
        //    string[] nationality,string[] passport_number, DateTime[] passport_expiry_date)
        public ActionResult SaveConfirmBooking(BookingGuestViewModel guestparam)
        {
            try
            {
                if(guestparam == null || guestparam.booking_id == 0)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(guestparam.booking_id);
                if (booking == null)
                {
                    return HttpNotFound();
                }

                //refurbishbooking(booking);


                //GL : room allotment develpment, check if rooms were alloted for 

                var rooms = booking.booking_room;
               // bool allotmentSuccess = false;

                //check if package and verify each room for available allotmente
                if(booking.isPackage)
                {
                    //verify allotments for each room
                    bool allotmentSuccess = true;


                    //grouping similar rooms to check total amount against rooms lefe
                    var groupedRoomList = rooms
                        .GroupBy(u => u.room_id)
                        .Select(grp => grp.ToList())
                        .ToList();

                    
                    foreach (var roomGrp in groupedRoomList)
                    {
                        var cnt = roomGrp.Count();
                        var _roomId = roomGrp.FirstOrDefault().room_id;
                        if(!db.roomAllotments.Where(p => p.room_id == _roomId
                            && p.allotment_date == booking.checkin_date 
                            && p.allotment_amount >= cnt).Any())
                        {
                            allotmentSuccess = false;
                        }
                    }

                    //if all rooms in package have allotments
                    if(allotmentSuccess)
                    {
                        foreach(var room in rooms)
                        {
                            VerifyAndDecrementAllotment(room.room_id, booking.package_id.Value, booking.checkin_date.Value);
                            var broom = db.bookingRooms.Find(room.booking_room_id);
                            broom.allotment_confirmed = true;
                            db.Entry(broom).State = EntityState.Modified;
                            var result = db.SaveChanges();
                        }

                        //set booking as alloted
                        booking.is_allotted = true;
                        booking.remarks = guestparam.remarks;
                        db.Entry(booking).State = EntityState.Modified;
                        var ret = db.SaveChanges();

                        //fixing bug: no remarks for allotted booking
                        booking.remarks = guestparam.remarks;

                        //Generate Voucher
                        reservationService.SaveVoucherInfo(booking.booking_id);
                        var voucher = db.bookingVouchers.Where(e => e.booking_id == booking.booking_id);

                        var actionPDF = new Rotativa.MVC.ViewAsPdf("~/Views/Reservation/GenerateVoucher.cshtml", voucher.First());

                        actionPDF.RotativaOptions.PageSize = Size.A4;
                        actionPDF.RotativaOptions.PageOrientation = Orientation.Portrait;

                        byte[] applicationPDFData = actionPDF.BuildPdf(ControllerContext);

                        //send email to info agent
                        reservationService.SendBookingMail(booking.booking_id, AppEnums.BookingMailType.VOUCHER_GENERATED_AGENT, applicationPDFData);
                        

                    }
                    

                }

                
                IReservationService res = new ReservationService();
                res.SaveGuestParam(guestparam, true,false);

                //ViewBag.Booking = booking;
                return RedirectToAction("BookingSummary", new { id = booking.booking_id });

            }
            catch (Exception e)
            {

                throw;
            }
        }


        private bool VerifyAndDecrementAllotment(int roomId, int packageId, DateTime ArrivalDate)
        {
            bool success = false;
            if (db.roomAllotments.Where(p => p.room_id == roomId && p.allotment_date == ArrivalDate).Any())
            {
                var allotment = db.roomAllotments.Where(p => p.room_id == roomId && p.allotment_date == ArrivalDate).FirstOrDefault();

                if (allotment.allotment_amount > 0)
                {
                    allotment.allotment_amount--;

                    db.Entry(allotment).State = EntityState.Modified;
                    var ret = db.SaveChanges();

                    
                    success = ret > 0;
                    

                }
            }
            return success;
        }
        

        public ActionResult Transfer(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);
            refurbishbooking(booking);


            if (booking == null)
            {
                return HttpNotFound();
            }

            IReservationService res = new ReservationService();

            IList<transferPricing> transfers = res.GetReservationTransfers(id.Value, booking);

            market m = db.markets.Find(booking.market_id);
            if (m != null)
            {
                booking.market = m;
            }
            else {

                booking.market  = new market { currency = new currency { currency_id = 0 } };
                
            }

            var pricings = db.transferPricings.Where(x => x.currency_id == booking.market.currency_id && x.is_active && x.is_selling_price).ToList();

            ViewBag.TransferPricings = pricings;
            ViewBag.Booking = booking;

            return View(transfers);
        }

        public ActionResult AddTransfer(int booking_id, int transfer_id, string submitaction)
        {
            try
            {

                if (transfer_id > 0 )
                {

                    IReservationService res = new ReservationService();
                    res.SaveReservationTransfers(booking_id, transfer_id, null, null, false);
                }

                if (submitaction == "continue")
                {
                    return RedirectToAction("Activities", new { id = booking_id });

                }
                return RedirectToAction("ConfirmBooking", new { id = booking_id });

            }
            catch (Exception e)
            {

                throw;
            }
        }


        public ActionResult Activities(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);
            refurbishbooking(booking);


            if (booking == null)
            {
                return HttpNotFound();
            }

            var vBooking = db.viewBookings.Where(x => x.booking_ref == booking.booking_ref).FirstOrDefault();
            if (vBooking != null)
            {
                ViewBag.VBooking = vBooking;
            }

            ViewBag.BookingRef = booking.booking_ref;
            ViewBag.Booking = booking;

            IReservationService res = new ReservationService();
            List<viewActivity> vActivity = res.GetReservationActivities(id.Value, booking);

            return View(vActivity);

        }

        [HttpPost]
        public ActionResult AddActivity(int booking_id, int[] activity_id, int[] transfer_type, int[] child, int[] adult, int[] activity_pricing_id, string submitaction)            
        {
            try
            {

                IReservationService res = new ReservationService();
                res.SaveReservationActivities(booking_id, activity_id, transfer_type, child, adult, activity_pricing_id, false, null, null);


                if (submitaction == "continue")
                {
                    return RedirectToAction("Rental", new { id = booking_id });

                }
                return RedirectToAction("ConfirmBooking", new { id = booking_id });

            }
            catch (Exception e)
            {

                throw;
            }
        }


        public ActionResult Rental(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);
            refurbishbooking(booking);

            if (booking == null)
            {
                return HttpNotFound();
            }


            var vBooking = db.viewBookings.Where(x => x.booking_ref == booking.booking_ref).FirstOrDefault();
            if (vBooking != null)
            {
                ViewBag.VBooking = vBooking;
            }

            IReservationService res = new ReservationService();
            IList<vehicle> vehiclesToDisplay = res.GetReservationRentals(id.Value, booking);


            ViewBag.Booking = booking;

            return View(vehiclesToDisplay);
        }




        public ActionResult AddRental(int booking_id, double[] price, double[] pricewithdriver, int[] days, int[] vehicle_id, bool[] hasDriver, string submitaction)
        {
            try
            {

                IReservationService res = new ReservationService();
                res.SaveReservationRentals(booking_id, price, pricewithdriver, days, vehicle_id, false, hasDriver);


                return RedirectToAction("ConfirmBooking", new { id = booking_id });

            }
            catch (Exception e)
            {

                throw;
            }

        }

        public ActionResult BookingSummary(int? id)
        {
            try
            {

                var booking = db.bookings.Find(id);
                refurbishbooking(booking);

                if (id == null || booking == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var notes = db.notes.Where(e => e.item_type == "RESERVATIONS" && e.item_id == booking.booking_id);

                ViewBag.Notes = notes;

                return View(booking);

            }
            catch (Exception e)
            {

                throw;
            }

        }

        public ContentResult FindPlace(string q)
        {
            ContentResult result = new ContentResult();

            var towns = db.providers.Where(x => x.name.Contains(q) && x.provider_type == "H" && x.is_active).Distinct().ToList();
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            foreach (var t in towns)
            {
                sb.AppendFormat(@"""{0} - {1} ({2})"",", t.name, t.town, t.location);
            }

            sb.Length--;
            sb.Append("]");

            result.Content = sb.ToString();
            return result;

        }

        public ContentResult FindNationality(string q)
        {
            ContentResult result = new ContentResult();

            var countries = db.countries.Where(x => x.name.Contains(q)).Distinct().ToList();
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            foreach (var c in countries)
            {
                sb.AppendFormat(@"""{0} - {1}"",", c.country_code, c.name);
            }

            sb.Length--;
            sb.Append("]");

            result.Content = sb.ToString();
            return result;

        }

        [HttpPost]
        public ActionResult AddReservationNote(int? booking_id, string note)
        {
            try
            {
                if (booking_id == null || string.IsNullOrWhiteSpace(note))
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(booking_id);
                refurbishbooking(booking);

                if (booking == null)
                {
                    return HttpNotFound();
                }

                Booking_System.Models.note notes = new Models.note();
                notes.user_id = Helpers.UserSession.Current.UserId;
                notes.item_id = booking_id.Value;
                notes.item_type = "RESERVATIONS";
                notes.detail = note;
                notes.create_date = DateTime.Now;

                db.notes.Add(notes);
                db.SaveChanges();

                IReservationService reservationService = new ReservationService();
                reservationService.SendBookingMail(booking_id.Value, AppEnums.BookingMailType.DISCUSSION_ADMIN);

                return RedirectToAction("BookingSummary", new { id = booking_id });

            }

            catch (Exception)
            {

                throw;
            }
        }


        public ActionResult ViewProforma(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var proforma = db.bookingProformas.Where(e => e.booking_id == id);

                if (proforma == null || proforma.Count() == 0)
                {
                    return HttpNotFound();
                }

                return new Rotativa.MVC.ViewAsPdf("~/Views/Reservation/GenerateProforma.cshtml", proforma.First());

            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult ViewVoucher(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var voucher = db.bookingVouchers.Where(e => e.booking_id == id);

                if (voucher == null || voucher.Count() == 0)
                {
                    return HttpNotFound();
                }

                return new Rotativa.MVC.ViewAsPdf("~/Views/Reservation/GenerateVoucher.cshtml", voucher.First());

            }
            catch (Exception)
            {

                throw;
            }
        }


        public ActionResult ApproveProforma(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var booking = db.bookings.Find(id);

                if (booking == null)
                {
                    return HttpNotFound();
                }

                //Confirmed booking
                booking.status = 5;

                var agent = db.users.Find(Helpers.UserSession.Current.UserId);
                //The agent should be credit based so as to confirm
                if (agent != null && agent.is_credit_based)
                {
                    var creditRemaining = agent.credit_remaining.HasValue ? agent.credit_remaining.Value : 0;
                    var bookingAmt = booking.booking_amt.HasValue ? booking.booking_amt : 0;

                    if (creditRemaining >= bookingAmt)
                    {
                        booking.credit_amt = bookingAmt;

                        //decrease the credit limit of the agent.
                        agent.credit_used = (agent.credit_used.HasValue ? agent.credit_used : 0) + bookingAmt;

                        UserSession.Current.RemainingCredit = (double)agent.credit_remaining;

                        db.Entry(agent).State = EntityState.Modified;
                    }
                }

                db.Entry(booking).State = EntityState.Modified;

                db.SaveChanges();

                IReservationService reservationService = new ReservationService();
                reservationService.SendBookingMail(booking.booking_id, AppEnums.BookingMailType.CLIENT_CONFIRMED_ADMIN);

                return RedirectToAction("BookingSummary", new { id = id });

            }
            catch (Exception)
            {

                throw;
            }
        }

        //public ActionResult CancelProforma(int? id)
        //{
        //    try
        //    {
        //        if (id == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }

        //        var booking = db.bookings.Find(id);

        //        if (booking == null)
        //        {
        //            return HttpNotFound();
        //        }

        //        //Cancel the booking
        //        booking.status = 3;

        //        var agent = db.users.Find(Helpers.UserSession.Current.UserId);
        //        if (agent != null)
        //        {
        //            if (booking.credit_amt.HasValue && booking.credit_amt.Value > 0)
        //            {
        //                agent.credit_used = agent.credit_used - booking.booking_amt;
        //                UserSession.Current.RemainingCredit = (double)agent.credit_remaining;
        //                db.Entry(agent).State = EntityState.Modified;
        //            }
        //        }

        //        db.Entry(booking).State = EntityState.Modified;

        //        db.SaveChanges();

        //        return RedirectToAction("BookingSummary", new { id = id });

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        #endregion



    }
}
