﻿using Booking_System.Helpers;
using Booking_System.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Controllers
{
    [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator)]
    public class AdminController : BaseController
    {

    }
}