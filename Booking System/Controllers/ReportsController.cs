﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using OfficeOpenXml;
using Booking_System.Models.ViewModel;
using static Booking_System.Helpers.AppEnums;

namespace Booking_System.Controllers
{
    public class ReportsController : BaseController
    {
        private ABSDBEntities db = new ABSDBEntities();



      
        public ActionResult Arrivals()
        {
            try
            {
                //DateTime start = startdate.HasValue ? startdate.Value : DateTime.Now;
                //DateTime end = enddate.HasValue ? enddate.Value : DateTime.Now.AddDays(7);
                ViewBag.Range = "";
                //var book = db.bookings.Where(x => x.checkin_date >= start && x.checkin_date <= end);
                var book = new List<booking>();
                return View(book);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult Arrivals(DateTime? startdate, DateTime? enddate)
        {
            try
            {
                DateTime start = startdate.HasValue ? startdate.Value : DateTime.Now;
                DateTime end = enddate.HasValue ? enddate.Value : DateTime.Now.AddDays(7);
                ViewBag.start = start;
                ViewBag.end = end;
                ViewBag.Range = String.Concat(" from ", start.ToString("dd-MM-yyyy"), " to ", end.ToString("dd-MM-yyyy"));
                var book = db.bookings.Where(x => x.checkin_date >= start && x.checkin_date <= end  && x.status == (int)BookingStatus.COMPLETED);
                return View(book);
            }
            catch (Exception)
            {

                throw;
            }
        }

        
        public FileResult ArrivalsExport(DateTime? startdate, DateTime? enddate)
        {
            try
            {
                DateTime start = startdate.HasValue ? startdate.Value : DateTime.Now;
                DateTime end = enddate.HasValue ? enddate.Value : DateTime.Now.AddDays(7);
                ViewBag.start = start;
                ViewBag.end = end;
                ViewBag.Range = String.Concat(" from ", start.ToString("dd-MM-yyyy"), " to ", end.ToString("dd-MM-yyyy"));
                var book = db.bookings.Where(x => x.checkin_date >= start && x.checkin_date <= end && x.status == (int)BookingStatus.COMPLETED);
                List<ArrivalDeparture> lst = new List<ArrivalDeparture>();
                foreach(var bk in book)
                {
                    ArrivalDeparture ad = new ArrivalDeparture();
                    ad.BookingFilenumber = bk.booking_file_number;                                
                    ad.BookingName = bk.booking_name;
                    ad.AgentName = bk.agent_name;                   
                    ad.HotelName = bk.provider_name;
                    ad.Market = bk.market_name;
                    ad.CheckinDate = bk.checkin_date.HasValue? bk.checkin_date.Value.ToString("dd-MM-yyyy"):"";
                    ad.CheckoutDate = bk.checkout_date.HasValue ? bk.checkout_date.Value.ToString("dd-MM-yyyy") : "";
                    ad.Pax = bk.booking_guest.Count();
                    ad.EventDate = (bk.booking_guest.Min(x => x.arrival_datetime).HasValue ? bk.booking_guest.Min(x => x.arrival_datetime).Value.ToString("dd-MM-yyyy") : "N/A");
                    ad.EventTime = (bk.booking_guest.Min(x => x.arrival_datetime).HasValue ? bk.booking_guest.Min(x => x.arrival_datetime).Value.ToString("HH:mm") : "N/A");
                    lst.Add(ad);
                }

                using (ExcelPackage package = new ExcelPackage())
                {
                    var ws = package.Workbook.Worksheets.Add("Arrivals");
                    //true generates headers
                    ws.Cells["A1"].Value = "Booking File Number";
                    
                    ws.Cells["B1"].Value = "Booking Name";
                    ws.Cells["C1"].Value = "Agent Name";
                    ws.Cells["D1"].Value = "Hotel Name ";
                    ws.Cells["E1"].Value = "Market Name ";
                    ws.Cells["F1"].Value = "Check-in Date";
                    ws.Cells["G1"].Value = "Check-out Date ";
                    ws.Cells["H1"].Value = "No of pax";
                    ws.Cells["I1"].Value = "Arrival Date";
                    ws.Cells["J1"].Value = "Arrival Time";
                    ws.Cells["A2"].LoadFromCollection(lst);

                    int totalRows = ws.Dimension.End.Row;
                    int totalCols = ws.Dimension.End.Column;
                    var headerCells = ws.Cells[1, 1, 1, totalCols];
                    var headerFont = headerCells.Style.Font;
                    headerFont.Bold = true;
                    //headerFont.Italic = true;
                    ws.Cells.AutoFitColumns();

                    var stream = new System.IO.MemoryStream();
                    package.SaveAs(stream);

                    //string fileName = "Arrivals.xls+x";
                    string fileName = string.Format("Arrivals {0}-{1}.xlsx", start.ToString("ddMMMyyyy"), end.ToString("ddMMMyyyy"));
                
                    string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    stream.Position = 0;
                    return File(stream, contentType, fileName);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        public ActionResult Departures()
        {
            try
            {
                //DateTime start = startdate.HasValue ? startdate.Value : DateTime.Now;
                //DateTime end = enddate.HasValue ? enddate.Value : DateTime.Now.AddDays(7);
                ViewBag.Range = "";
                //var book = db.bookings.Where(x => x.checkin_date >= start && x.checkin_date <= end);
                var book = new List<booking>();
                return View(book);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public ActionResult Departures(DateTime? startdate, DateTime? enddate)
        {
            try
            {
                DateTime start = startdate.HasValue ? startdate.Value : DateTime.Now;
                DateTime end = enddate.HasValue ? enddate.Value : DateTime.Now.AddDays(7);
                ViewBag.start = start;
                ViewBag.end = end;
                ViewBag.Range = String.Concat(" from ", start.ToString("dd-MM-yyyy"), " to ", end.ToString("dd-MM-yyyy"));
                var book = db.bookings.Where(x => x.checkout_date >= start && x.checkout_date <= end && x.status == (int)BookingStatus.COMPLETED);
                return View(book);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public FileResult DeparturesExport(DateTime? startdate, DateTime? enddate)
        {
            try
            {
                DateTime start = startdate.HasValue ? startdate.Value : DateTime.Now;
                DateTime end = enddate.HasValue ? enddate.Value : DateTime.Now.AddDays(7);
                ViewBag.start = start;
                ViewBag.end = end;
                ViewBag.Range = String.Concat(" from ", start.ToString("dd-MM-yyyy"), " to ", end.ToString("dd-MM-yyyy"));
                var book = db.bookings.Where(x => x.checkout_date >= start && x.checkout_date <= end && x.status == (int)BookingStatus.COMPLETED);
                List<ArrivalDeparture> lst = new List<ArrivalDeparture>();
                foreach (var bk in book)
                {
                    ArrivalDeparture ad = new ArrivalDeparture();
                    ad.BookingFilenumber = bk.booking_file_number;
                    ad.BookingName = bk.booking_name;
                    ad.AgentName = bk.agent_name;
                    ad.HotelName = bk.provider_name;
                    ad.Market = bk.market_name;
                    ad.CheckinDate = bk.checkin_date.HasValue ? bk.checkin_date.Value.ToString("dd-MM-yyyy") : "";
                    ad.CheckoutDate = bk.checkout_date.HasValue ? bk.checkout_date.Value.ToString("dd-MM-yyyy") : "";
                    ad.Pax = bk.booking_guest.Count();
                    ad.EventDate = (bk.booking_guest.Min(x => x.departure_datetime).HasValue ? bk.booking_guest.Min(x => x.departure_datetime).Value.ToString("dd-MM-yyyy") : "N/A");
                    ad.EventTime = (bk.booking_guest.Min(x => x.departure_datetime).HasValue ? bk.booking_guest.Min(x => x.departure_datetime).Value.ToString("HH:mm") : "N/A");
                    lst.Add(ad);
                }

                using (ExcelPackage package = new ExcelPackage())
                {
                    var ws = package.Workbook.Worksheets.Add("Departures");
                    //true generates headers
                    ws.Cells["A1"].Value = "Booking File Number";
                    ws.Cells["B1"].Value = "Booking Name";
                    ws.Cells["C1"].Value = "Agent Name";
                    ws.Cells["D1"].Value = "Hotel Name ";
                    ws.Cells["E1"].Value = "Market Name ";
                    ws.Cells["F1"].Value = "Check-in Date";
                    ws.Cells["G1"].Value = "Check-out Date ";
                    ws.Cells["H1"].Value = "No of pax";
                    ws.Cells["I1"].Value = "Departure Date";
                    ws.Cells["J1"].Value = "Departure Time";
                    ws.Cells["A2"].LoadFromCollection(lst);


                    int totalRows = ws.Dimension.End.Row;
                    int totalCols = ws.Dimension.End.Column;
                    var headerCells = ws.Cells[1, 1, 1, totalCols];
                    var headerFont = headerCells.Style.Font;
                    headerFont.Bold = true;
                    //headerFont.Italic = true;
                    ws.Cells.AutoFitColumns();


                    var stream = new System.IO.MemoryStream();
                    package.SaveAs(stream);



                   // string fileName = "Departures.xlsx";
                    string fileName = string.Format("Departures {0}-{1}.xlsx",start.ToString("ddMMMyyyy"),end.ToString("ddMMMyyyy"));
                    string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    stream.Position = 0;
                    return File(stream, contentType, fileName);
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActionResult SalesReport()
        {
            try
            {
                var book = new List<booking>();
                return View(book);
            }
            catch (Exception)
            {

                throw;
            }
           
        }
        [HttpPost]
        public ActionResult SalesReport(DateTime? startdate, DateTime? enddate)
        {
            try
            {
                DateTime start = startdate.HasValue ? startdate.Value : DateTime.Now;
                DateTime end = enddate.HasValue ? enddate.Value : DateTime.Now.AddDays(7);
                ViewBag.start = start.ToString("dd-MM-yyyy");
                ViewBag.end = end.ToString("dd-MM-yyyy");
                ViewBag.Range = String.Concat(" from ", start.ToString("dd-MM-yyyy"), " to ", end.ToString("dd-MM-yyyy"));
                var book = db.bookings.Where(x => x.booking_date >= start && x.booking_date <= end && x.status == (int)BookingStatus.COMPLETED);
                return View(book);
              
            }
            catch (Exception)
            {

                throw;
            }

        }

       
        public FileResult SalesReportExport(DateTime? startdate, DateTime? enddate)
        {
            try
            {
                DateTime start = startdate.HasValue ? startdate.Value : DateTime.Now;
                DateTime end = enddate.HasValue ? enddate.Value : DateTime.Now.AddDays(7);
                ViewBag.start = start;
                ViewBag.end = end;
                ViewBag.Range = String.Concat(" from ", start.ToString("dd-MM-yyyy"), " to ", end.ToString("dd-MM-yyyy"));
                var book = db.bookings.Where(x => x.booking_date >= start && x.booking_date <= end && x.status == (int)BookingStatus.COMPLETED);
                List<SalesReport> srl = new List<SalesReport>();
                foreach(var bk in book)
                {
                    SalesReport sr = new SalesReport();
                    sr.ProviderName = bk.provider_name;
                    sr.CheckinDate = bk.checkin_date.Value;
                    sr.checkout_date = bk.checkout_date.Value;
                    sr.Adults = bk.adults.HasValue?bk.adults.Value:0;
                    sr.TotalChildrenAndTeens = bk._totalChildrenAndTeens;
                    sr.bookingRoomCount = bk.booking_room.Count();
                    sr.NumNights = bk.num_nights.HasValue? bk.num_nights.Value:0;
                    sr.currencyCode = bk.currency.currency_code;
                    sr.TotalCostRooms = bk.TotalCostRooms;
                    sr.TotalCostSupps = bk.TotalCostSupps;
                    sr.isPackage = bk.isPackage ? "Y" : "N";
                    sr.TransferType = "Data Not Available";
                    sr.TotalCostTransferWithAfterSales = bk.TotalCostTransferWithAfterSales;
                    sr.TotalCostActivityWithAfterSales = bk.TotalCostActivityWithAfterSales;
                    sr.TotalCostRentalWithAfterSales = bk.TotalCostRentalWithAfterSales;
                    sr.TotalCost = bk.TotalCost;
                    srl.Add(sr);
                }
                using (ExcelPackage package = new ExcelPackage())
                {
                    var ws = package.Workbook.Worksheets.Add("SalesReport");
                    //true generates headers
                    ws.Cells["A1"].Value = "Provider Name";
                    ws.Cells["B1"].Value = "Check-in Date";
                    ws.Cells["C1"].Value = "Check-out Date ";
                    ws.Cells["D1"].Value = "Number Of Adults";
                    ws.Cells["E1"].Value = "Children";
                    ws.Cells["F1"].Value = "Number of Rooms";
                    ws.Cells["G1"].Value = "Number of Nights";                   
                    ws.Cells["H1"].Value = "Currency";
                    ws.Cells["I1"].Value = "Selling Price Hotel";
                    ws.Cells["J1"].Value = "Supplement Sales";
                    ws.Cells["K1"].Value = "Is Package";
                    ws.Cells["L1"].Value = "Transfer Type";
                    ws.Cells["M1"].Value = "Total Transport Sales";
                    ws.Cells["N1"].Value = "Total Activity Sales";
                    ws.Cells["O1"].Value = "Total Car rental Sales";
                    ws.Cells["P1"].Value = "Total Sales";
                    ws.Cells["A2"].LoadFromCollection(srl);


                    int totalRows = ws.Dimension.End.Row;
                    int totalCols = ws.Dimension.End.Column;
                    var headerCells = ws.Cells[1, 1, 1, totalCols];
                    var headerFont = headerCells.Style.Font;
                    headerFont.Bold = true;
                    //headerFont.Italic = true;
                    ws.Cells.AutoFitColumns();



                    var stream = new System.IO.MemoryStream();
                    package.SaveAs(stream);

                    //string fileName = "SalesReport.xlsx";
                    string fileName = string.Format("SalesReport {0}-{1}.xlsx", start.ToString("ddMMMyyyy"), end.ToString("ddMMMyyyy"));

                    string contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    stream.Position = 0;
                    return File(stream, contentType, fileName);
                }


                

            }
            catch (Exception)
            {

                throw;
            }

        }



        public ActionResult RateSheet()
        {
            try
            {
                var RateSheet = db.rateSheets.OrderBy(x => new { x.provider_name, x.room_name, x.start_date, x.price_type,x.package_name });
                    //var hotels = RateSheet.Select(x => x.provider_id).Distinct();

                    ////initialise excel 
                    //ExcelPackage p = new ExcelPackage();
                    ////add a sheet name
                    //p.Workbook.Worksheets.Add("Rates");
                    //ExcelWorksheet ws = p.Workbook.Worksheets[1];
                    //int currentRow = 1;

                    //ws.Name = "Rates"; //Setting Sheet's name

                    //foreach(var hotel in hotels)
                    //{
                    //    //first get a package count for the Hotel
                    //    int packageCnt = RateSheet.Where(x => x.provider_id == hotel && x.price_type == "PC").Select(x => x.package_id).Distinct().Count();
                    //    String HotelName = RateSheet.Where(x => x.provider_id == hotel).Select(x => x.provider_name).Distinct().First();


                    //}
                    return View(RateSheet);

                
            }
            catch (Exception)
            {

                throw;
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
