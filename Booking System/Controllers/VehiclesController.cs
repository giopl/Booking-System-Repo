﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using Booking_System.Helpers;
using System.Data.Entity.Infrastructure;
using Booking_System.Models.ViewModel;
using EntityFramework.Extensions;

namespace Booking_System.Controllers
{
    public class VehiclesController : AdminController
    {
        private ABSDBEntities db = new ABSDBEntities();
        private Services.AdminService adminService = new Services.AdminService();

        //#region transfers

        //public ActionResult ListTransferProviders()
        //{
        //    var vehicles = db.transferPricings.Include(v => v.transfer);
        //    return View(vehicles.Where(x => x.is_active).ToList());
        //}



        //// GET: TestVehicle/Create
        //public ActionResult CreateTransferProvider()
        //{
        //    ViewBag.currency_id = new SelectList(db.currencies.Where(x => x.is_active), "currency_id", "code_desc");
        //    ViewBag.provider_id = new SelectList(db.providers.Where(x => x.provider_type == "R"), "provider_id", "name");

        //    return View();
        //}

        //// POST: TestVehicle/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        ////public ActionResult CreateVehicle([Bind(Include = @"vehicle_id,provider_id,service_name,vehicle_model,
        ////    vehicle_type,service_description,max_seat,is_active,is_transfer,includes_driver,engine_size,year,is_automatic,doors,fuel_type")] vehicle vehicle,
        ////    vehiclePricing vpricing, HttpPostedFileBase file, picture pic)

        //public ActionResult CreateTransferProvider(VehicleViewModel model, HttpPostedFileBase file)
        //{

        //    model.Vehicle.provider_id = model.provider_id.HasValue ? model.provider_id.Value : 0;
        //    model.TransferPricing.currency_id = model.currency_id.HasValue ? model.currency_id.Value : 0;
        //    model.Vehicle.vehicle_model = "Airport Transfer";
        //    if (ModelState.IsValid)
        //    {

        //        db.vehicles.Add(model.Vehicle);
        //        db.SaveChanges();

        //    }
        //    else {

        //        ViewBag.currency_id = new SelectList(db.currencies.Where(x => x.is_active), "currency_id", "code_desc");
        //        ViewBag.provider_id = new SelectList(db.providers.Where(x => x.provider_type == "R"), "provider_id", "name");

        //        return View();
        //    }


        //    //add errors for prices
        //    if (!model.TransferPricing.child_price.HasValue &&
        //        !model.TransferPricing.teen_price.HasValue &&
        //        !model.TransferPricing.adult_price.HasValue 
        //        )
        //    {
        //        ModelState.AddModelError("price_adult", "At least one price should be defined");
        //    }

        //    if (model.TransferPricing.currency_id == 0)
        //    {
        //        ModelState.AddModelError("currency_id", "Please select a currency");
        //    }

        //    //model.TransferPricing.vehicle_id = model.Vehicle.vehicle_id;


        //    db.transferPricings.Add(model.TransferPricing);
        //    db.SaveChanges();



        //    //3. save image
        //    //force the display order to -1
        //    model.VehiclePicture.display_order = -1;
        //    model.VehiclePicture.SectionId = model.Vehicle.vehicle_id;

        //    adminService.SaveResourceToDisk(file, model.VehiclePicture);


        //    return RedirectToAction("ListTransferProviders");
        //}

        //public ActionResult EditTransferProvider(int? id)
        //{
        //    try
        //    {

        //        if (id == null)
        //        {
        //            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //        }

        //        vehicle vehicle = db.vehicles.Find(id);
        //        if (vehicle == null)
        //        {
        //            return HttpNotFound();
        //        }

        //        ViewBag.provider_id = new SelectList(db.providers.Where(x => x.provider_type == "R"), "provider_id", "name");

        //        var currencies = db.currencies.Where(x => x.is_active).ToList();
        //        //var existCurrencies = db.transferPricings.Where(x => x.vehicle_id == id && x.is_active).Select(x => x.currency_id).ToList();
        //        //foreach (var ec in existCurrencies)
        //        //{
        //        //    var success = currencies.Remove(new currency { currency_id = ec });
        //        //}

        //        ViewBag.currency_id = new SelectList(currencies, "currency_id", "code_desc");

        //        return View(vehicle);
        //    }
        //    catch (Exception e)
        //    {
        //        throw;
        //    }
        //}


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult SaveTransferPricing([Bind(Include = "transfer_pricing_id,vehicle_id,currency_id,child_price,teen_price,adult_price")] transferPricing pricing)
        //{

        //    if (ModelState.IsValid)
        //    {
        //        if (pricing.transfer_pricing_id > 0)
        //        {
        //            db.Entry(pricing).State = EntityState.Modified;
        //            db.SaveChanges();

        //        }
        //        else {

        //        pricing.is_active = true;
        //        db.transferPricings.Add(pricing);
        //        db.SaveChanges();
        //        //return RedirectToAction("EditVehicle", new { id = pricing.vehicle_id });
        //        }


        //    }

        //    var currencies = db.currencies.Where(x => x.is_active).ToList();
        //    //var existCurrencies = db.transferPricings.Where(x => x.vehicle_id == pricing.vehicle_id && x.is_active).Select(x => x.currency_id).ToList();
        //    //foreach (var ec in existCurrencies)
        //    //{
        //    //    currencies.Remove(new currency { currency_id = ec });
        //    //}

        //    ViewBag.currency_id = new SelectList(currencies, "currency_id", "code_desc");


        //    return RedirectToAction("EditTransfer", new { id = pricing.transfer_id });
        //}



        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult EditTransfer([Bind(Include = "vehicle_id,provider_id,service_name,vehicle_model,vehicle_type,service_description,max_seat,is_active,is_transfer,includes_driver")] vehicle vehicle)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(vehicle).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("ListTransfers");
        //    }
        //    ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", vehicle.provider_id);
        //    return View(vehicle);
        //}




        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult EditTransferPricing([Bind(Include = "transfer_pricing_id,vehicle_id,currency_id,child_price,teen_price,adult_price")] transferPricing transferpricing)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(transferpricing).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("ListTransfers");
        //    }
        //    //ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", vehicle.provider_id);
        //    return RedirectToAction("EditTransfer", new { id = transferpricing.transfer_id });
        //}



        //// GET: TestVehicle/Delete/5
        //public ActionResult DeleteTransferPricing(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    transferPricing transferPricing = db.transferPricings.Find(id);
        //    if (transferPricing == null)
        //    {
        //        return HttpNotFound();
        //    }

        //    var transferId = transferPricing.transfer_id;
        //    db.transferPricings.Remove(transferPricing);
        //    db.SaveChanges();
        //    return RedirectToAction("EditTransfer", new { id = transferId });



        //}


        //// GET: TestVehicle/Delete/5
        //public ActionResult DeleteTransfer(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    vehicle vehicle = db.vehicles.Find(id);
        //    if (vehicle == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(vehicle);
        //}

        //// POST: TestVehicle/Delete/5
        //[HttpPost, ActionName("DeleteTransfer")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteTransferConfirmed(int id)
        //{
        //    vehicle vehicle = db.vehicles.Find(id);
        //    try
        //    {
        //        db.transferPricings.Where(x => x.transfer_id == id).Delete();

        //        db.vehicles.Remove(vehicle);
        //        db.SaveChanges();
        //        return RedirectToAction("ListTransfers");

        //    }
        //    catch (DbUpdateException ex)
        //    {
        //        db.Entry(vehicle).Reload();
        //        vehicle.is_active = false;
        //        db.Entry(vehicle).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("ListTransfers");

        //    }



        //}





        //#endregion

        #region new methods
        public ActionResult ListVehicles()
        {
            var vehicles = db.vehicles.Include(v => v.provider);
            return View(vehicles.Where(x=>x.is_active && !x.is_transfer).ToList());
        }

     
        // GET: TestVehicle/Create
        public ActionResult CreateVehicle()
        {
            ViewBag.currency_id = new SelectList(db.currencies.Where(x=>x.is_active), "currency_id", "code_desc");
            ViewBag.provider_id = new SelectList(db.providers.Where(x=>x.provider_type=="R"), "provider_id", "name");
            
            return View();
        }

        // POST: TestVehicle/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult CreateVehicle([Bind(Include = @"vehicle_id,provider_id,service_name,vehicle_model,
        //    vehicle_type,service_description,max_seat,is_active,is_transfer,includes_driver,engine_size,year,is_automatic,doors,fuel_type")] vehicle vehicle,
        //    vehiclePricing vpricing, HttpPostedFileBase file, picture pic)

        public ActionResult CreateVehicle(VehicleViewModel model, HttpPostedFileBase file)
        {
            //1. save vehicle
            model.Vehicle.provider_id = model.provider_id.HasValue? model.provider_id.Value:0;
            model.VehiclePricing.currency_id = model.currency_id.HasValue?model.currency_id.Value:0;
            
            if (ModelState.IsValid)
            {
                
                db.vehicles.Add(model.Vehicle);
                db.SaveChanges();
               
            }


            //add errors for prices
            if (!model.VehiclePricing.half_day_price.HasValue &&
                !model.VehiclePricing.half_day_price_with_driver.HasValue &&
                !model.VehiclePricing.daily_price_with_driver.HasValue &&
                !model.VehiclePricing.daily_price.HasValue
                )
            {
                ModelState.AddModelError("price_adult", "At least one price should be defined");
            }

            if(model.VehiclePricing.currency_id == 0)
            {
                ModelState.AddModelError("currency_id", "Please select a currency");
            }

            //1. save pricing
            model.VehiclePricing.vehicle_id = model.Vehicle.vehicle_id;
            

            db.vehiclePricings.Add(model.VehiclePricing);
                db.SaveChanges();

            //3. save image
            //force the display order to -1
            model.VehiclePicture.display_order = -1;
            model.VehiclePicture.SectionId = model.Vehicle.vehicle_id;

                adminService.SaveResourceToDisk(file, model.VehiclePicture);

            return RedirectToAction("ListVehicles");
        }

        [HttpPost]
        public ActionResult ReplaceVehicleImage(HttpPostedFileBase file,  int vehicleId)
        {
            try
            {
                IList<vehiclePicture> vehiclepics = db.vehiclePictures.Where(x => x.vehicle_id == vehicleId).ToList();
                db.vehiclePictures.Where(x => x.vehicle_id == vehicleId).Delete();
                foreach (vehiclePicture vpic in vehiclepics)
                {

                    var picToDelete = db.pictures.Find(vpic.picture_id);
                    picToDelete.imgType = ImagesType.Vehicle;
                    adminService.DeleteResourceFromDisk(picToDelete);

                    //var picture = db.pictures.Find(vpic.picture_id);
                    db.pictures.Where(p => p.picture_id == vpic.picture_id).Delete();
                    //db.SaveChanges();
                }

                picture pic = new picture
                {
                    SectionId = vehicleId,
                    imgType = ImagesType.Vehicle

                };


                bool isTransfer = false;
                adminService.SaveResourceToDisk(file, pic);
                var v = db.vehicles.Find(vehicleId);
                if (v != null)
                {
                    isTransfer = v.is_transfer;
                }


                if (isTransfer) {
                    return RedirectToAction("EditTransfer", new { id = vehicleId });
                } 

                return RedirectToAction("EditVehicle", new { id = vehicleId });
            }
            catch (Exception e)
            {

                throw;
            }

        }


        public ActionResult EditVehicle(int? id)
        {
            try
            {

                if (id==null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                vehicle vehicle = db.vehicles.Find(id);
                if (vehicle == null)
                {
                    return HttpNotFound();
                }
                
                ViewBag.provider_id = new SelectList(db.providers.Where(x => x.provider_type == "R"), "provider_id", "name");

                var currencies = db.currencies.Where(x => x.is_active).ToList();
                var existCurrencies = db.vehiclePricings.Where(x => x.vehicle_id == id && x.is_active).Select(x=>x.currency_id).ToList();
                foreach (var ec in existCurrencies)
                {
                    var success = currencies.Remove(new currency { currency_id = ec });
                }

                ViewBag.currency_id = new SelectList(currencies, "currency_id", "code_desc");

                return View(vehicle);
            }
            catch (Exception e)
            {
                throw;
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveVehiclePricing([Bind(Include = "vehicle_pricing_id,vehicle_id,currency_id,daily_price,daily_price_with_driver,half_day_price,half_day_price_with_driver")] vehiclePricing pricing)
        {
            


            if (ModelState.IsValid)
            {

                if (pricing.vehicle_pricing_id > 0)
                {
                    db.Entry(pricing).State = EntityState.Modified;
                    db.SaveChanges();

                }
                else
                {

                pricing.is_active = true;
                db.vehiclePricings.Add(pricing);
                db.SaveChanges();
                //return RedirectToAction("EditVehicle", new { id = pricing.vehicle_id });
                }

            }

            var currencies = db.currencies.Where(x => x.is_active).ToList();
            var existCurrencies = db.vehiclePricings.Where(x => x.vehicle_id == pricing.vehicle_id && x.is_active).Select(x => x.currency_id).ToList();
            foreach (var ec in existCurrencies)
            {
                currencies.Remove(new currency { currency_id = ec });
            }

            ViewBag.currency_id = new SelectList(currencies, "currency_id", "code_desc");


            return RedirectToAction("EditVehicle", new { id = pricing.vehicle_id});
        }



        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditVehicle([Bind(Include = "vehicle_id,provider_id,service_name,vehicle_model,vehicle_type,service_description,max_seat,is_active,is_transfer,includes_driver,engine_size,year,is_automatic,doors,fuel_type")] vehicle vehicle)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehicle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListVehicles");
            }
            ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", vehicle.provider_id);
            return View(vehicle);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditVehiclePricing([Bind(Include = "vehicle_pricing_id,vehicle_id,currency_id,daily_price,daily_price_with_driver,half_day_price,half_day_price_with_driver")] vehiclePricing vehiclepricing)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vehiclepricing).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListVehicles");
            }
            //ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", vehicle.provider_id);
            return RedirectToAction("EditVehicle", new { id = vehiclepricing.vehicle_id});
        }



        // GET: TestVehicle/Delete/5
        public ActionResult DeleteVehiclePricing(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            vehiclePricing vehiclePricing = db.vehiclePricings.Find(id);
            if (vehiclePricing == null)
            {
                return HttpNotFound();
            }

            var vehicleId = vehiclePricing.vehicle_id;
            db.vehiclePricings.Remove(vehiclePricing);
            db.SaveChanges();
            return RedirectToAction("EditVehicle", new { id= vehicleId });

            
            
        }


        // GET: TestVehicle/Delete/5
        public ActionResult DeleteVehicle(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vehicle vehicle = db.vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            return View(vehicle);
        }

        // POST: TestVehicle/Delete/5
        [HttpPost, ActionName("DeleteVehicle")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteVehicleConfirmed(int id)
        {
                vehicle vehicle = db.vehicles.Find(id);
            try
            {
                db.vehicleFeatures.Where(x => x.vehicle_id == id).Delete();
                db.vehiclePictures.Where(x => x.vehicle_id == id).Delete();
                db.vehiclePricings.Where(x => x.vehicle_id == id).Delete();

                db.vehicles.Remove(vehicle);
                db.SaveChanges();
                return RedirectToAction("ListVehicles");

            }
            catch (DbUpdateException ex)
            {
                db.Entry(vehicle).Reload();
                vehicle.is_active = false;
                db.Entry(vehicle).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListVehicles");

            }
           


        }


        #endregion


        #region Images
        [HttpPost]
        public ActionResult AddImage(HttpPostedFileBase file, picture pic)
        {
            Services.AdminService adm = new Services.AdminService();
            //force the display order to -1
            pic.display_order = -1;

            adm.SaveResourceToDisk(file, pic);

            switch (pic.imgType)
            {
                case ImagesType.User:
                    return RedirectToAction("Details", "Users", new { id = pic.SectionId, tab = AppEnums.UserTab.GALLERY });
                case ImagesType.Provider:
                    return RedirectToAction("ProviderDetails", "Providers", new { id = pic.SectionId, tab = AppEnums.ProviderTab.GALLERY });
                case ImagesType.Hotel:
                    return RedirectToAction("HotelDetails", "Hotels", new { id = pic.SectionId, tab = AppEnums.HotelTab.GALLERY });
                case ImagesType.Room:
                    return RedirectToAction("RoomDetails", "Rooms", new { id = pic.SectionId, tab = AppEnums.RoomTab.GALLERY });
                case ImagesType.Vehicle:
                    return RedirectToAction("VehicleOrTransferDetails", "Vehicles", new { id = pic.SectionId, tab = AppEnums.VehicleTab.GALLERY });
                case ImagesType.Activity:
                    return RedirectToAction("Details", "Activities", new { id = pic.SectionId, tab = AppEnums.ActivityTab.GALLERY });
                default:
                    break;
            }

            return RedirectToAction("Menu", "Home");

        }


        [HttpPost]
        public ActionResult EditImage(picture pic)
        {
            if (pic == null || pic.SectionId == 0 || pic.picture_id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            switch (pic.imgType)
            {
                case ImagesType.User:
                    break;
                case ImagesType.Hotel:
                case ImagesType.Provider:
                    var imgs = db.providerPictures.Where(e => e.picture_id == pic.picture_id && e.provider_id == pic.SectionId);
                    if (imgs == null || imgs.Count() == 0)
                    {
                        return HttpNotFound();
                    }

                    foreach (var img in imgs)
                    {
                        img.section = pic.Section.ToString();
                        img.display_order = pic.display_order;
                        db.Entry(img).State = EntityState.Modified;
                    }

                    //Dont update if the display order is -1
                    if (pic.display_order > 0)
                    {
                        var otherImgs = db.providerPictures.Where(e => e.display_order == pic.display_order);

                        if (otherImgs != null && otherImgs.Count() > 0)
                        {
                            foreach (var otherImg in otherImgs)
                            {
                                otherImg.display_order = -1;
                                db.Entry(otherImg).State = EntityState.Modified;
                            }
                        }
                    }
                    break;
                case ImagesType.Room:
                    var roomImgs = db.roomPictures.Where(e => e.picture_id == pic.picture_id && e.room_id == pic.SectionId);
                    if (roomImgs == null || roomImgs.Count() == 0)
                    {
                        return HttpNotFound();
                    }

                    foreach (var img in roomImgs)
                    {
                        img.section = pic.Section.ToString();
                        img.display_order = pic.display_order;
                        db.Entry(img).State = EntityState.Modified;
                    }

                    //Dont update if the display order is -1
                    if (pic.display_order > 0)
                    {
                        var otherImgs = db.roomPictures.Where(e => e.display_order == pic.display_order);

                        if (otherImgs != null && otherImgs.Count() > 0)
                        {
                            foreach (var otherImg in otherImgs)
                            {
                                otherImg.display_order = -1;
                                db.Entry(otherImg).State = EntityState.Modified;
                            }
                        }
                    }
                    break;
                case ImagesType.Vehicle:
                    var vehicleImgs = db.vehiclePictures.Where(e => e.picture_id == pic.picture_id && e.vehicle_id == pic.SectionId);
                    if (vehicleImgs == null || vehicleImgs.Count() == 0)
                    {
                        return HttpNotFound();
                    }

                    foreach (var img in vehicleImgs)
                    {
                        img.section = pic.Section.ToString();
                        img.display_order = pic.display_order;
                        db.Entry(img).State = EntityState.Modified;
                    }

                    //Dont update if the display order is -1
                    if (pic.display_order > 0)
                    {
                        var otherImgs = db.vehiclePictures.Where(e => e.display_order == pic.display_order);

                        if (otherImgs != null && otherImgs.Count() > 0)
                        {
                            foreach (var otherImg in otherImgs)
                            {
                                otherImg.display_order = -1;
                                db.Entry(otherImg).State = EntityState.Modified;
                            }
                        }
                    }
                    break;
                case ImagesType.Activity:
                    var activityImgs = db.activityPictures.Where(e => e.picture_id == pic.picture_id && e.activity_id == pic.SectionId);
                    if (activityImgs == null || activityImgs.Count() == 0)
                    {
                        return HttpNotFound();
                    }

                    foreach (var img in activityImgs)
                    {
                        img.section = pic.Section.ToString();
                        img.display_order = pic.display_order;
                        db.Entry(img).State = EntityState.Modified;
                    }

                    //Dont update if the display order is -1
                    if (pic.display_order > 0)
                    {
                        var otherImgs = db.activityPictures.Where(e => e.display_order == pic.display_order);

                        if (otherImgs != null && otherImgs.Count() > 0)
                        {
                            foreach (var otherImg in otherImgs)
                            {
                                otherImg.display_order = -1;
                                db.Entry(otherImg).State = EntityState.Modified;
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

            db.SaveChanges();

            switch (pic.imgType)
            {
                case ImagesType.User:
                    break;
                case ImagesType.Provider:
                    return RedirectToAction("ProviderDetails", "Providers", new { id = pic.SectionId, tab = AppEnums.ProviderTab.GALLERY });
                case ImagesType.Hotel:
                    return RedirectToAction("HotelDetails", "Hotels", new { id = pic.SectionId, tab = AppEnums.HotelTab.GALLERY });
                case ImagesType.Room:
                    return RedirectToAction("RoomDetails", "Rooms", new { id = pic.SectionId, tab = AppEnums.RoomTab.GALLERY });
                case ImagesType.Vehicle:
                    return RedirectToAction("Details", "Vehicles", new { id = pic.SectionId, tab = AppEnums.VehicleTab.GALLERY });
                case ImagesType.Activity:
                    return RedirectToAction("Details", "Activities", new { id = pic.SectionId, tab = AppEnums.ActivityTab.GALLERY });
                default:
                    break;
            }

            return RedirectToAction("Menu", "Home");

        }



        public ActionResult DeleteImage(int? id, int? sectionId, ImagesType type)
        {
            if (id == null || sectionId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            picture img = db.pictures.Find(id);

            if (img == null)
            {
                return HttpNotFound();
            }

            switch (type)
            {
                case ImagesType.User:
                    var userPics = db.userPictures.Where(e => e.picture_id == id);
                    if (userPics != null)
                    {
                        foreach (var userPic in userPics)
                        {
                            db.userPictures.Remove(userPic);
                        }
                    }
                    break;
                case ImagesType.Hotel:
                case ImagesType.Provider:
                    var providerPic = db.providerPictures.Where(e => e.picture_id == id);
                    if (providerPic != null)
                    {
                        foreach (var item in providerPic)
                        {
                            db.providerPictures.Remove(item);
                        }
                    }
                    break;
                case ImagesType.Room:
                    var roomPics = db.roomPictures.Where(e => e.picture_id == id);
                    if (roomPics != null)
                    {
                        foreach (var item in roomPics)
                        {
                            db.roomPictures.Remove(item);
                        }
                    }
                    break;
                case ImagesType.Vehicle:
                    var vehiclePics = db.vehiclePictures.Where(e => e.picture_id == id);
                    if (vehiclePics != null)
                    {
                        foreach (var item in vehiclePics)
                        {
                            db.vehiclePictures.Remove(item);
                        }
                    }
                    break;
                case ImagesType.Activity:
                    var activitiesPics = db.activityPictures.Where(e => e.picture_id == id);
                    if (activitiesPics != null)
                    {
                        foreach (var item in activitiesPics)
                        {
                            db.activityPictures.Remove(item);
                        }
                    }
                    break;
                default:
                    break;
            }

            db.pictures.Remove(img);
            db.SaveChanges();

            Services.AdminService adm = new Services.AdminService();
            img.imgType = type;

            adm.DeleteResourceFromDisk(img);

            switch (type)
            {
                case ImagesType.User:
                    return RedirectToAction("Details", "Users", new { id = sectionId, tab = AppEnums.UserTab.GALLERY });
                case ImagesType.Provider:
                    return RedirectToAction("ProviderDetails", "Providers", new { id = sectionId, tab = AppEnums.ProviderTab.GALLERY });
                case ImagesType.Hotel:
                    return RedirectToAction("HotelDetails", "Hotels", new { id = sectionId, tab = AppEnums.HotelTab.GALLERY });
                case ImagesType.Room:
                    return RedirectToAction("RoomDetails", "Rooms", new { id = sectionId, tab = AppEnums.RoomTab.GALLERY });
                case ImagesType.Vehicle:
                    return RedirectToAction("VehicleOrTransferDetails", "Vehicles", new { id = sectionId, tab = AppEnums.VehicleTab.GALLERY });
                case ImagesType.Activity:
                    return RedirectToAction("Details", "Activities", new { id = sectionId, tab = AppEnums.ActivityTab.GALLERY });
                default:
                    break;
            }

            return RedirectToAction("Menu", "Home");
        }
        #endregion

        // GET: Vehicle/Details/5
        public ActionResult VehicleOrTransferDetails(int? id, AppEnums.VehicleTab tab = AppEnums.VehicleTab.GALLERY)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vehicle vehicle = db.vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }

            ViewBag.tab = tab;

            return View(vehicle);
        }

        // GET: Contacts/Create
        public ActionResult CreateVehicleOrTransfer(int id = 0, int? hotelid = null, int? tab = 0)
        {
            var provider = db.providers.Find(id);
            bool istransfer = false;

            if (tab!=null)
            {
                istransfer = (tab == 2) ? true : false;
            }

            ViewBag.tab = tab;
            ViewBag.Provider = provider;

            vehicle objVeh = new vehicle();
            objVeh.is_transfer = istransfer;

            return View(objVeh);
        }



        // GET: Vehicle/Create
        //public ActionResult Create(int? id)
        //{

        //    ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name");


        //    return View();
        //}

        // POST: Vehicle/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "vehicle_id,provider_id,service_name,vehicle_type,vehicle_model,service_description,min_seat,max_seat,includes_driver,is_active")] vehicle vehicle)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            db.vehicles.Add(vehicle);
        //            db.SaveChanges();
        //            return RedirectToAction("Index");
        //        }

        //        ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", vehicle.provider_id);
        //        return View(vehicle);
        //    }
        //    catch (Exception e)
        //    {
        //        throw;
        //    }

        //}
        // GET: activityPricings/Create
        public ActionResult CreateVehiclePricing(int? id)
        {
            vehiclePricing vehiclePricing = new vehiclePricing();
            if (id != null && id > 0)
            {
                var vehicle = db.vehicles.Find(id);
                ViewBag.ServiceName = vehicle.service_name;
                ViewBag.VehicleId = vehicle.vehicle_id;
                ViewBag.providerid = vehicle.provider_id;
                ViewBag.providername = vehicle.provider.name;
                ViewBag.vehicle_id = new SelectList(db.vehicles.Where(x => x.vehicle_id == vehicle.vehicle_id), "vehicle_id", "service_name");
                ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "code_desc");
                vehiclePricing.vehicle_id = int.Parse(id.ToString());
            }
            else
            {
                ViewBag.activity_id = new SelectList(db.activities, "activity_id", "activity_name");
                ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "code_desc");
            }
            

            return View(vehiclePricing);
        }

        // POST: activityPricings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateVehiclePricing([Bind(Include = "vehicle_pricing_id,vehicle_id,currency_id,half_day_price_with_driver,half_day_price,daily_price_with_driver,daily_price")] vehiclePricing vehiclePricing)
        {
            //add errors for prices
            if (!vehiclePricing.half_day_price.HasValue&&
                !vehiclePricing.half_day_price_with_driver.HasValue &&
                !vehiclePricing.daily_price_with_driver.HasValue &&
                !vehiclePricing.daily_price.HasValue
                )
            {
                ModelState.AddModelError("price_adult", "At least one price should be defined");
            }


            if (ModelState.IsValid)
            {
                db.vehiclePricings.Add(vehiclePricing);
                db.SaveChanges();
                return RedirectToAction("VehicleOrTransferDetails", "Vehicles", new { id = vehiclePricing.vehicle_id });
            }

            var vehicle = db.vehicles.Find(vehiclePricing.vehicle_id);
            ViewBag.VehicleId = vehicle.vehicle_id;
            //ViewBag.VehicleId = activity..activity_id;
            //ViewBag.providerid = activity.provider_id;
            //ViewBag.providername = activity.provider.name;

            ViewBag.vehicle_id = new SelectList(db.vehicles, "vehicle_id", "activity_name", vehiclePricing.vehicle_id);
            ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "code_desc", vehiclePricing.currency_id);
            return View(vehiclePricing);
        }

        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateVehicleOrTransfer([Bind(Include = "vehicle_id,provider_id,service_name,vehicle_type,vehicle_model,service_description,max_seat,includes_driver,is_active,is_transfer")] vehicle vehicle)
        {
            //if(vehicle.min_seat.HasValue && vehicle.max_seat.HasValue  && vehicle.min_seat.Value> vehicle.max_seat.Value)
            //{
            //    ModelState.AddModelError("max_seat", "Maximum number of seats should be more than minimum seats");
            //}
            var provider = db.providers.Find(vehicle.provider_id);
            
            if (ModelState.IsValid)
            {
                //Always Set to True on Vehicle Creation
                vehicle.is_active = true;
                db.vehicles.Add(vehicle);
                db.SaveChanges();
                
                if (vehicle.is_transfer)
                    return RedirectToAction("ProviderDetails", "Providers", new { id = provider.provider_id, tab = 2 });
                else
                    return RedirectToAction("ProviderDetails", "Providers", new { id = provider.provider_id, tab = 1 });

            }

            ViewBag.HasProvider = false;
            if (vehicle.provider_id > 0)
            {
                ViewBag.HasProvider = true;
                ViewBag.SelectedProviderId = vehicle.provider_id;
                ViewBag.SelectedProviderName = provider.name;
            }
           

            ViewBag.tab = vehicle.is_transfer;
            ViewBag.Provider = provider;


            ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", vehicle.provider_id);
            return View(vehicle);
        }


        // GET: Vehicle/Edit/5
        public ActionResult EditVehicleOrTransfer(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vehicle vehicle = db.vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }
            ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", vehicle.provider_id);
            return View(vehicle);
        }

      

        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditVehicleOrTransfer([Bind(Include = "vehicle_id,provider_id,service_name,vehicle_type,vehicle_model,service_description,min_seat,max_seat,is_active,is_transfer,includes_driver")] vehicle vehicle)
        {
            //if (vehicle.min_seat.HasValue && vehicle.max_seat.HasValue && vehicle.min_seat.Value > vehicle.max_seat.Value)
            //{
            //    ModelState.AddModelError("max_seat", "Maximum number of seats should be more than minimum seats");
            //}
            if (ModelState.IsValid)
            {
                db.Entry(vehicle).State = EntityState.Modified;
                db.SaveChanges();


                //logic to redirect to hotel details if contact works for hotel. first find provider, then hotel
                var provider = db.providers.Find(vehicle.provider_id);

                if (vehicle.is_transfer)
                    return RedirectToAction("ProviderDetails", "Providers", new { id = provider.provider_id, tab = 2 });
                else
                    return RedirectToAction("ProviderDetails", "Providers", new { id = provider.provider_id, tab = 1 });

            }
            ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", vehicle.provider_id);
            return View(vehicle);
        }


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult EditVehiclePricing([Bind(Include = "vehicle_pricing_id,vehicle_id,currency_id,half_day_price_with_driver,half_day_price,daily_price_with_driver,daily_price,is_active")]vehiclePricing vehiclePricing)
        //{

            
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(vehiclePricing).State = EntityState.Modified;
        //        db.SaveChanges();


        //        //logic to redirect to hotel details if contact works for hotel. first find provider, then hotel
        //        var vehicle = db.providers.Find(vehiclePricing.vehicle_id);

        //        if (vehicle.IsTransfer)
        //            return RedirectToAction("VehicleOrTransferDetails", "Vehicles", new { id = vehiclePricing.vehicle_id });
        //        else
        //            return RedirectToAction("VehicleOrTransferDetails", "Vehicles", new { id = vehiclePricing.vehicle_id });

        //    }
        //    return View();
        //}

        //[HttpGet]
        //public ActionResult EditVehiclePricing(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    vehiclePricing vehiclePricing = db.vehiclePricings.Find(id);
        //    vehicle vehicle = db.vehicles.Find(vehiclePricing.vehicle_id);

        //    ViewBag.ServiceName = vehicle.service_name;
        //    ViewBag.VehicleId = vehicle.vehicle_id;
        //    ViewBag.providerid = vehicle.provider_id;
        //    ViewBag.providername = vehicle.provider.name;
        //    ViewBag.vehicle_id = new SelectList(db.vehicles.Where(x => x.vehicle_id == vehicle.vehicle_id), "vehicle_id", "service_name");
        //    ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "code_desc");

        //    if (vehiclePricing == null)
        //    {
        //        return HttpNotFound();
        //    }
            
        //    return View(vehiclePricing);

        //}
        //// GET: Vehicle/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    vehicle vehicle = db.vehicles.Find(id);
        //    if (vehicle == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(vehicle);
        //}

        //// POST: Vehicle/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    vehicle vehicle = db.vehicles.Find(id);
        //    db.vehicles.Remove(vehicle);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        // GET: Contacts/Delete/5
        public ActionResult DeleteVehicleOrTransfer(int? id, int? hotelid = null, int? tab = 0)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            vehicle vehicle = db.vehicles.Find(id);
            if (vehicle == null)
            {
                return HttpNotFound();
            }

            if (hotelid.HasValue)
            {
                ViewBag.HotelId = hotelid;
            }

            return View(vehicle);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("DeleteVehicleOrTransfer")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            //vehiclePicture vehiclePic = db.vehiclePictures.Select(x => x.vehicle_id = id);
            //var vehiclePicId = db.vehiclePictures.Find(.Select(x => x.vehicle_id == id);

            vehicle vehicle = db.vehicles.Find(id);
            var provider = vehicle.provider;
            var provider_id = provider.provider_id;

            //Fetch the PictueVehicle Object List related to the Vehicle
            var picvehicleId = db.vehiclePictures.Where(x => x.vehicle_id == id);

            try
            {
                db.Entry(vehicle).State = EntityState.Deleted;
                db.SaveChanges();

            }
            catch (DbUpdateException ex)
            {
                db.Entry(vehicle).Reload();
                vehicle.is_active = false;
                db.Entry(vehicle).State = EntityState.Modified;
                db.SaveChanges();
            }




            // redirect car rental provider
            return RedirectToAction("ProviderDetails", "Providers", new { id = provider_id, tab = 1});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
