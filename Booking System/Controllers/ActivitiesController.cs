﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using Booking_System.Helpers;

namespace Booking_System.Controllers
{
    public class ActivitiesController : AdminController
    {
        private ABSDBEntities db = new ABSDBEntities();


        #region Activities
        // GET: Activities
        //public ActionResult Index(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }

        //    var activities = db.activities.Where(x => x.provider_id == id);
        //    return View(activities.ToList());
        //}

        // GET: Activities/Details/5
        public ActionResult ActivityDetails(int? id, AppEnums.ActivityTab tab = AppEnums.ActivityTab.PRICING)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            activity activity = db.activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }


            var allcurrencies = db.currencies.Where(x => x.is_active);
            List<currency> currencies = new List<currency>();
            List<activityPricing> activityPricings = db.activityPricings.Where(x => x.activity_id == id && x.provider_id == 0).ToList();
            foreach (var curr in allcurrencies)
            {
                if (!activityPricings.Exists(s => s.currency_id == curr.currency_id))
                {
                    currencies.Add(curr);
                }
            }
            ViewBag.currency_id = new SelectList(currencies, "currency_id", "currency_code");


            ViewBag.Tab = tab;

            return View(activity);
        }



        public ActionResult ListActivities()
        {
            try
            {
                // var activities = db.activities.Where(x=>x.is_active).ToList();
                 var activities = db.activities.ToList();
                 return View(activities);
            }
            catch (Exception e)
            {
                
                throw;
            }
        }

        // GET: Activities/Create
        public ActionResult CreateActivity()
        {
            //if (providerId == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}

            activity a = new activity();
            a.is_active = true;
            //provider activityProvider = db.providers.Find(providerId);
            //if (activityProvider == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //else
            //{
            //    //a.provider = activityProvider;
            //    //a.provider_id = activityProvider.provider_id;
            //}

            return View(a);
        }

        // POST: Activities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateActivity([Bind(Include = "activity_id,activity_name,description,cancellation_policy,conditions,category,duration,min_age,max_child_age,is_active,valid_from,valid_to,includes_meal,features")] activity activity)
        {
            //addtionnal validations
            if (activity != null)
            {
                if (activity.valid_from.HasValue && activity.valid_to.HasValue &&
                    activity.valid_to < activity.valid_from)
                {
                    ModelState.AddModelError("valid_to", "Valid To should be after Valid From.");
                }

                

                
            }
            activity.is_active = true;
            if (ModelState.IsValid)
            {
                db.activities.Add(activity);
                db.SaveChanges();
                //return RedirectToAction("ProviderDetails", "Providers", new { id = activity.provider_id, tab = Helpers.AppEnums.ProviderTab.ACTIVITIES });
                return RedirectToAction("ListActivities");
            }

            //activity.provider = db.providers.Find(activity.provider_id);
            //ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", activity.provider_id);
            return View(activity);
        }

        // GET: Activities/Edit/5
        public ActionResult EditActivity(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            activity activity = db.activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            //ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", activity.provider_id);
            return View(activity);
        }

        // POST: Activities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditActivity([Bind(Include = "activity_id,provider_id,activity_name,description,cancellation_policy,conditions,category,duration,min_age,max_child_age,is_active,valid_from,valid_to,includes_meal,features")] activity activity)
        {
            //addtionnal validations
            if (activity != null)
            {
                if (activity.valid_from.HasValue && activity.valid_to.HasValue &&
                    activity.valid_to < activity.valid_from)
                {
                    ModelState.AddModelError("valid_to", "Valid To should be after Valid From.");
                }

            }

            if (ModelState.IsValid)
            {
                db.Entry(activity).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("ProviderDetails", "Providers", new { id = activity.provider_id, tab = Helpers.AppEnums.ProviderTab.ACTIVITIES });
                return RedirectToAction("ListActivities");
            }
           // activity.provider = db.providers.Find(activity.provider_id);
            //ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", activity.provider_id);
            return View(activity);
        }




        // GET: Activities/Delete/5
        public ActionResult DeleteActivity(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            activity activity = db.activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // POST: Activities/Delete/5
        [HttpPost, ActionName("DeleteActivity")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteActivityConfirmed(int id)
        {
            activity activity = db.activities.Find(id);
            db.activities.Remove(activity);
            db.SaveChanges();
            return RedirectToAction("ListActivities");
           // return RedirectToAction("ProviderDetails", "Providers", new { id = activity.provider_id, tab = Helpers.AppEnums.ProviderTab.ACTIVITIES });
        }
        #endregion



        #region Activities pricing

        // GET: activityPricings/Details/5
        public ActionResult ActivityPricingDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            activityPricing activityPricing = db.activityPricings.Find(id);
            if (activityPricing == null)
            {
                return HttpNotFound();
            }




            return View(activityPricing);
        }

        // GET: activityPricings/Create
        //public ActionResult CreateActivityPricing(int? id)
        //{
        //    try
        //    {
        //        if (id != null && id > 0)
        //        {
        //            var activity = db.activities.Find(id);
        //            ViewBag.ActivityName = activity.activity_name;
        //            ViewBag.ActivityId = activity.activity_id;
        //         //   ViewBag.providerid = activity.provider_id;
        //          //  ViewBag.providername = activity.provider.name;
        //            //ViewBag.activity_id = new SelectList(db.activities.Where(x => x.provider_id == activity.provider_id), "activity_id", "activity_name");

        //            ViewBag.provider_id = new SelectList(db.providers.Where(x => x.provider_type == "A" && x.is_active), "provider_id", "provider_name");
        //            ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "currency_code");
        //        }
        //        else
        //        {
        //            ViewBag.activity_id = new SelectList(db.activities, "activity_id", "activity_name");
        //            ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "currency_code");
        //        } 
        //        return View();
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //}

        // POST: activityPricings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateActivityPricing([Bind(Include = "activity_pricing_id,activity_id,currency_id,price_child,price_adult,price_adult_incl_sic_transfer,price_child_incl_sic_transfer,price_adult_incl_private_transfer,price_child_incl_private_transfer")] activityPricing activityPricing)
        {
            //add errors for prices
            if (
                !activityPricing.price_child.HasValue &&
                !activityPricing.price_adult.HasValue &&
                !activityPricing.price_adult_incl_sic_transfer.HasValue &&
                !activityPricing.price_child_incl_sic_transfer.HasValue &&
                !activityPricing.price_child_incl_private_transfer.HasValue &&
                !activityPricing.price_adult_incl_private_transfer.HasValue

                )
            {
                ModelState.AddModelError("price_adult", "At least one price should be defined");
            }


            if (ModelState.IsValid)
            {
               
                db.activityPricings.Add(activityPricing);
                db.SaveChanges();
                return RedirectToAction("ActivityDetails", "Activities", new { id = activityPricing.activity_id, tab = AppEnums.ActivityTab.PRICING });
            }

            var activity = db.activities.Find(activityPricing.activity_id);
            ViewBag.ActivityName = activity.activity_name;
            ViewBag.ActivityId = activity.activity_id;
          //  ViewBag.providerid = activity.provider_id;
          //  ViewBag.providername = activity.provider.name;
            ViewBag.provider_id = new SelectList(db.providers.Where(x => x.provider_type == "A" && x.is_active), "provider_id", "provider_name");

            ViewBag.activity_id = new SelectList(db.activities, "activity_id", "activity_name", activityPricing.activity_id);
            ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "currency_code", activityPricing.currency_id);
            return View(activityPricing);
        }

        // GET: activityPricings/Edit/5
        //public ActionResult EditActivityPricing(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    activityPricing activityPricing = db.activityPricings.Find(id);
        //    if (activityPricing == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.activity_id = new SelectList(db.activities, "activity_id", "activity_name", activityPricing.activity_id);
        //    ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "currency_code", activityPricing.currency_id);
        //    return View(activityPricing);
        //}

        // POST: activityPricings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditActivityPricing([Bind(Include = "activity_pricing_id,activity_id,currency_id,price_child,price_adult,,price_adult_incl_sic_transfer,price_child_incl_sic_transfer,price_adult_incl_private_transfer,price_child_incl_private_transfer")] activityPricing activityPricing)
        {

            //add errors for prices
            if (
                !activityPricing.price_child.HasValue &&
                !activityPricing.price_adult.HasValue &&
                !activityPricing.price_adult_incl_sic_transfer.HasValue &&
                !activityPricing.price_child_incl_sic_transfer.HasValue &&
                !activityPricing.price_child_incl_private_transfer.HasValue &&
                !activityPricing.price_adult_incl_private_transfer.HasValue 

                )
            {
                ModelState.AddModelError("price_adult", "At least one price should be defined");
            }
            if (ModelState.IsValid)
            {
                db.Entry(activityPricing).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ActivityDetails", "Activities", new { id = activityPricing.activity_id, tab = AppEnums.ActivityTab.PRICING });
            }
            ViewBag.activity_id = new SelectList(db.activities, "activity_id", "activity_name", activityPricing.activity_id);
            ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "currency_code", activityPricing.currency_id);
            var activity = db.activities.Find(activityPricing.activity_id);
            activityPricing.activity = activity;

            return View(activityPricing);
        }

        // GET: activityPricings/Delete/5
        public ActionResult DeleteActivityPricing(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var activityPricing = db.activityPricings.Where(x=>x.activity_pricing_id ==id);
            if (activityPricing == null)
            {
                return HttpNotFound();
            }

            var activityId = activityPricing.FirstOrDefault().activity_id;
            db.activityPricings.Remove(activityPricing.FirstOrDefault());
            db.SaveChanges();
            return RedirectToAction("ActivityDetails", "Activities", new { id = activityId, tab = AppEnums.ActivityTab.PRICING });

            
        }

        //// POST: activityPricings/Delete/5
        //[HttpPost, ActionName("DeleteActivityPricing")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteActivityPricingConfirmed(int id)
        //{
        //    activityPricing activityPricing = db.activityPricings.Find(id);
        //    db.activityPricings.Remove(activityPricing);
        //    db.SaveChanges();
        //    return RedirectToAction("ActivityDetails", "Activities", new { id = activityPricing.activity_id, tab = AppEnums.ActivityTab.PRICING });
        //}

        #endregion
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
