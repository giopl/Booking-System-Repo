﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using System.Data.Entity.Validation;
using libphonenumber;
using Booking_System.Helpers;

using System.Data.Entity.Infrastructure;

namespace Booking_System.Controllers
{
    public class ProvidersController : AdminController
    {
        private ABSDBEntities db = new ABSDBEntities();

        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);


        #region PhoneNumber
        
        public string GetCountryCodeFromName(string Name)
        {
            try
            {
                var ctry= db.countries.Where(x => x.name.ToUpper().Trim() == Name.Trim().ToUpper()).First();
                if (ctry != null)
                {
                    return ctry.country_code;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion






        // GET: providers
        public ActionResult Index()
        {
            return View(db.providers.ToList());
        }

        #region Car Rental providers
        public ActionResult ListCarRentals()
        {
            try
            {

                return View(db.providers.Where(x => x.provider_type == "R").ToList());
            }
            catch (Exception)
            {

                throw;
            }
        }



        #endregion

        #region Activity Providers
        public ActionResult ListTourOperators()
        {
            try
            {

                return View(db.providers.Where(x => x.provider_type == "A").ToList());
            }
            catch (Exception)
            {

                throw;
            }
        }




        #endregion

        #region Images
        [HttpPost]
        public ActionResult AddImage(HttpPostedFileBase file, picture pic)
        {
            Services.AdminService adm = new Services.AdminService();
            //force the display order to -1
            pic.display_order = -1;

            adm.SaveResourceToDisk(file, pic);

            switch (pic.imgType)
            {
                case ImagesType.User:
                    return RedirectToAction("UserDetails", "Users", new { id = pic.SectionId, tab = AppEnums.UserTab.GALLERY });
                case ImagesType.Provider:
                    return RedirectToAction("ProviderDetails", "Providers", new { id = pic.SectionId, tab = AppEnums.ProviderTab.GALLERY });
                case ImagesType.Hotel:
                    return RedirectToAction("HotelDetails", "Hotels", new { id = pic.SectionId, tab = AppEnums.HotelTab.GALLERY });
                case ImagesType.Room:
                    return RedirectToAction("RoomDetails", "Rooms", new { id = pic.SectionId, tab = AppEnums.RoomTab.GALLERY });
                case ImagesType.Vehicle:
                    return RedirectToAction("VehicleOrTransferDetails", "Vehicles", new { id = pic.SectionId, tab = AppEnums.VehicleTab.GALLERY });
                case ImagesType.Activity:
                    return RedirectToAction("ActivityDetails", "Activities", new { id = pic.SectionId, tab = AppEnums.ActivityTab.GALLERY });
                default:
                    break;
            }

            return RedirectToAction("Menu", "Home");

        }


        [HttpPost]
        public ActionResult EditImage(picture pic)
        {
            if (pic == null || pic.SectionId == 0 || pic.picture_id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            switch (pic.imgType)
            {
                case ImagesType.User:
                    break;
                case ImagesType.Hotel:
                case ImagesType.Provider:
                    var imgs = db.providerPictures.Where(e => e.picture_id == pic.picture_id && e.provider_id == pic.SectionId);
                    if (imgs == null || imgs.Count() == 0)
                    {
                        return HttpNotFound();
                    }

                    foreach (var img in imgs)
                    {
                        img.section = pic.Section.ToString();
                        img.display_order = pic.display_order;
                        db.Entry(img).State = EntityState.Modified;
                    }

                    //Dont update if the display order is -1
                    if (pic.display_order > 0)
                    {
                        var otherImgs = db.providerPictures.Where(e => e.display_order == pic.display_order && e.picture_id != pic.picture_id);

                        if (otherImgs != null && otherImgs.Count() > 0)
                        {
                            foreach (var otherImg in otherImgs)
                            {
                                otherImg.display_order = -1;
                                db.Entry(otherImg).State = EntityState.Modified;
                            }
                        }
                    }
                    break;
                case ImagesType.Room:
                    var roomImgs = db.roomPictures.Where(e => e.picture_id == pic.picture_id && e.room_id == pic.SectionId);
                    if (roomImgs == null || roomImgs.Count() == 0)
                    {
                        return HttpNotFound();
                    }

                    foreach (var img in roomImgs)
                    {
                        img.section = pic.Section.ToString();
                        img.display_order = pic.display_order;
                        db.Entry(img).State = EntityState.Modified;
                    }

                    //Dont update if the display order is -1
                    if (pic.display_order > 0)
                    {
                        var otherImgs = db.roomPictures.Where(e => e.display_order == pic.display_order && e.picture_id != pic.picture_id);

                        if (otherImgs != null && otherImgs.Count() > 0)
                        {
                            foreach (var otherImg in otherImgs)
                            {
                                otherImg.display_order = -1;
                                db.Entry(otherImg).State = EntityState.Modified;
                            }
                        }
                    }
                    break;
                case ImagesType.Vehicle:
                    var vehicleImgs = db.vehiclePictures.Where(e => e.picture_id == pic.picture_id && e.vehicle_id == pic.SectionId);
                    if (vehicleImgs == null || vehicleImgs.Count() == 0)
                    {
                        return HttpNotFound();
                    }

                    foreach (var img in vehicleImgs)
                    {
                        img.section = pic.Section.ToString();
                        img.display_order = pic.display_order;
                        db.Entry(img).State = EntityState.Modified;
                    }

                    //Dont update if the display order is -1
                    if (pic.display_order > 0)
                    {
                        var otherImgs = db.vehiclePictures.Where(e => e.display_order == pic.display_order && e.picture_id != pic.picture_id);

                        if (otherImgs != null && otherImgs.Count() > 0)
                        {
                            foreach (var otherImg in otherImgs)
                            {
                                otherImg.display_order = -1;
                                db.Entry(otherImg).State = EntityState.Modified;
                            }
                        }
                    }
                    break;
                case ImagesType.Activity:
                    var activityImgs = db.activityPictures.Where(e => e.picture_id == pic.picture_id && e.activity_id == pic.SectionId);
                    if (activityImgs == null || activityImgs.Count() == 0)
                    {
                        return HttpNotFound();
                    }

                    foreach (var img in activityImgs)
                    {
                        img.section = pic.Section.ToString();
                        img.display_order = pic.display_order;
                        db.Entry(img).State = EntityState.Modified;
                    }

                    //Dont update if the display order is -1
                    if (pic.display_order > 0)
                    {
                        var otherImgs = db.activityPictures.Where(e => e.display_order == pic.display_order && e.picture_id != pic.picture_id);

                        if (otherImgs != null && otherImgs.Count() > 0)
                        {
                            foreach (var otherImg in otherImgs)
                            {
                                otherImg.display_order = -1;
                                db.Entry(otherImg).State = EntityState.Modified;
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

            db.SaveChanges();

            switch (pic.imgType)
            {
                case ImagesType.User:
                    break;
                case ImagesType.Provider:
                    return RedirectToAction("ProviderDetails", "Providers", new { id = pic.SectionId, tab = AppEnums.ProviderTab.GALLERY });
                case ImagesType.Hotel:
                    return RedirectToAction("HotelDetails", "Hotels", new { id = pic.SectionId, tab = AppEnums.HotelTab.GALLERY });
                case ImagesType.Room:
                    return RedirectToAction("RoomDetails", "Rooms", new { id = pic.SectionId, tab = AppEnums.RoomTab.GALLERY });
                case ImagesType.Vehicle:
                    return RedirectToAction("VehicleOrTransferDetails", "Vehicles", new { id = pic.SectionId, tab = AppEnums.VehicleTab.GALLERY });
                case ImagesType.Activity:
                    return RedirectToAction("ActivityDetails", "Activities", new { id = pic.SectionId, tab = AppEnums.ActivityTab.GALLERY });
                default:
                    break;
            }

            return RedirectToAction("Menu", "Home");

        }



        public ActionResult DeleteImage(int? id, int? sectionId, ImagesType type)
        {
            if (id == null || sectionId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            picture img = db.pictures.Find(id);

            if (img == null)
            {
                return HttpNotFound();
            }

            switch (type)
            {
                case ImagesType.User:
                    var userPics = db.userPictures.Where(e => e.picture_id == id);
                    if (userPics != null)
                    {
                        foreach (var userPic in userPics)
                        {
                            db.userPictures.Remove(userPic);
                        }
                    }
                    break;
                case ImagesType.Hotel:
                case ImagesType.Provider:
                    var providerPic = db.providerPictures.Where(e => e.picture_id == id);
                    if (providerPic != null)
                    {
                        foreach (var item in providerPic)
                        {
                            db.providerPictures.Remove(item);
                        }
                    }
                    break;
                case ImagesType.Room:
                    var roomPics = db.roomPictures.Where(e => e.picture_id == id);
                    if (roomPics != null)
                    {
                        foreach (var item in roomPics)
                        {
                            db.roomPictures.Remove(item);
                        }
                    }
                    break;
                case ImagesType.Vehicle:
                    var vehiclePics = db.vehiclePictures.Where(e => e.picture_id == id);
                    if (vehiclePics != null)
                    {
                        foreach (var item in vehiclePics)
                        {
                            db.vehiclePictures.Remove(item);
                        }
                    }
                    break;
                case ImagesType.Activity:
                    var activitiesPics = db.activityPictures.Where(e => e.picture_id == id);
                    if (activitiesPics != null)
                    {
                        foreach (var item in activitiesPics)
                        {
                            db.activityPictures.Remove(item);
                        }
                    }
                    break;
                default:
                    break;
            }

            db.pictures.Remove(img);
            db.SaveChanges();

            Services.AdminService adm = new Services.AdminService();
            img.imgType = type;

            adm.DeleteResourceFromDisk(img);

            switch (type)
            {
                case ImagesType.User:
                    return RedirectToAction("UserDetails", "Users", new { id = sectionId, tab = AppEnums.UserTab.GALLERY });
                case ImagesType.Provider:
                    return RedirectToAction("ProviderDetails", "Providers", new { id = sectionId, tab = AppEnums.ProviderTab.GALLERY });
                case ImagesType.Hotel:
                    return RedirectToAction("HotelDetails", "Hotels", new { id = sectionId, tab = AppEnums.HotelTab.GALLERY });
                case ImagesType.Room:
                    return RedirectToAction("RoomDetails", "Rooms", new { id = sectionId, tab = AppEnums.RoomTab.GALLERY });
                case ImagesType.Vehicle:
                    return RedirectToAction("VehicleOrTransferDetails", "Vehicles", new { id = sectionId, tab = AppEnums.VehicleTab.GALLERY });
                case ImagesType.Activity:
                    return RedirectToAction("ActivityDetails", "Activities", new { id = sectionId, tab = AppEnums.ActivityTab.GALLERY });
                default:
                    break;
            }

            return RedirectToAction("Menu", "Home");
        }
        #endregion


        #region genericfileupload
        [HttpPost]
        public ActionResult AddFile(HttpPostedFileBase file, fileUpload fileUploaded)
        {
            Services.AdminService adm = new Services.AdminService();
            //force the display order to -1
            fileUploaded.display_order = -1;

            adm.SaveUploadedFileToDisk(file, fileUploaded);

            switch (fileUploaded.Source)
            {
                case FileSource.Provider:
                    return RedirectToAction("ProviderDetails", "Providers", new { id = fileUploaded.provider_id, tab = AppEnums.ProviderTab.FILEUPLOAD });
                case FileSource.Hotel:
                    return RedirectToAction("HotelDetails", "Hotels", new { id = fileUploaded.provider_id, tab = AppEnums.HotelTab.FILEUPLOAD });
                default:
                    break;
            }

            return RedirectToAction("Menu", "Home");


        }

        public ActionResult DeleteFile(int? id,int? providerId, FileSource type)
        {
            if (id == null || providerId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Services.AdminService adm = new Services.AdminService();
            adm.DeleteUploadedFileFromDisk(id.Value);

            switch (type)
            {
                case FileSource.Provider:
                    return RedirectToAction("ProviderDetails", "Providers", new { id = providerId, tab = AppEnums.ProviderTab.FILEUPLOAD });
                case FileSource.Hotel:
                    return RedirectToAction("HotelDetails", "Hotels", new { id = providerId, tab = AppEnums.HotelTab.FILEUPLOAD });
                default:
                    break;
            }

            return RedirectToAction("Menu", "Home");
        }

        public ActionResult DownloadFile(int? id)
        {
            if(id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            fileUpload file = db.fileUploads.Find(id);
            if(file == null)
            {
                return HttpNotFound();
            }


            string filename = file.filename;
            string filepath = String.Concat(DirectoryHelper.GetPath(file.FileVirtualPath), "\\", file.FullFileName);
            byte[] filedata = System.IO.File.ReadAllBytes(filepath);
            string contentType = MimeMapping.GetMimeMapping(filepath);

            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = filename,
                Inline = true,
            };

            Response.AppendHeader("Content-Disposition", cd.ToString());

            return File(filedata, contentType);
        }
        #endregion

        #region Provider (common to tour operators / car rental)

        // GET: providers/Details/5
        public ActionResult ProviderDetails(int? id, AppEnums.ProviderTab tab = AppEnums.ProviderTab.GALLERY)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            provider provider = db.providers.Find(id);
            if (provider == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tab = tab;

            return View(provider);
        }

        // GET: providers/Create
        public ActionResult CreateProvider(string type = "")
        {
            provider provider = new provider { provider_type = type, is_active = true };
            return View(provider);
        }

        // POST: providers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateProvider([Bind(Include = "provider_id,name,description,location,company,website,commission_rate,street1,street2,town,country,phone1,phone2,fax,email,google_location,provider_type,is_active")] provider provider)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    provider.is_active = true;
                    db.providers.Add(provider);
                    db.SaveChanges();

                    if (provider.provider_type == "R")
                    {
                        return RedirectToAction("ListCarRentals");
                    }
                    else if (provider.provider_type == "A")
                    {
                        return RedirectToAction("ListTourOperators");
                    }
                    return RedirectToAction("Index");
                }

                return View(provider);
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }


            catch (Exception e)
            {
                log.ErrorFormat("controller error {0} ", e.ToString());

                throw;
            }
        }

        // GET: providers/Edit/5
        public ActionResult EditProvider(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            provider provider = db.providers.Find(id);
            if (provider == null)
            {
                return HttpNotFound();
            }
            return View(provider);
        }

        // POST: providers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProvider([Bind(Include = "provider_id,name,description,location,company,website,commission_rate,street1,street2,town,country,phone1,phone2,fax,email,google_location,provider_type,is_active")] provider provider)
        {
            if (ModelState.IsValid)
            {
                db.Entry(provider).State = EntityState.Modified;
                db.SaveChanges();

                if (provider.provider_type == "R")
                {
                    return RedirectToAction("ListCarRentals");
                }

                if (provider.provider_type == "A")
                {
                    return RedirectToAction("ListTourOperators");
                }


                return RedirectToAction("Index");
            }
            return View(provider);
        }

        // GET: providers/Delete/5
        public ActionResult DeleteProvider(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            provider provider = db.providers.Find(id);
            if (provider == null)
            {
                return HttpNotFound();
            }
            return View(provider);
        }

        // POST: providers/Delete/5
        [HttpPost, ActionName("DeleteProvider")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteProviderConfirmed(int id)
        {
            provider provider = db.providers.Find(id);
            db.providers.Remove(provider);
            db.SaveChanges();

            var _view = provider.IsRental ? "ListCarRentals" : "ListTourOperators";
            return RedirectToAction(_view);
        }


        #endregion

        #region Contact
        public ActionResult Contacts()
        {
            try
            {

                var contacts = db.contacts.Include(h => h.provider);
                return View(contacts.ToList());


            }
            catch (Exception)
            {

                throw;
            }
        }



        // GET: Contacts/Details/5
        public ActionResult ContactDetails(int? id, int? hotelId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            contact contact = db.contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }

            ViewBag.HotelId = hotelId;

            return View(contact);
        }

        // GET: Contacts/Create
        public ActionResult CreateContact(int id = 0, int? hotelid = null)
        {

            var provider = db.providers.Find(id);

            ViewBag.Provider = provider;

            if (provider.IsHotel)
            {


                ViewBag.Hotel = db.providers.Where(x => x.provider_id == id).FirstOrDefault();
            }

            return View();
        }

        // POST: Contacts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateContact([Bind(Include = "provider_id,salutation,firstname,lastname,job_title,office_phone,mobile,email,notes")] contact contact)
        {

            var provider = db.providers.Find(contact.provider_id);
            var hotel_id = 0;
            if (provider != null && provider.provider_type == "H")
            {
                provider h = db.providers.Where(p => p.provider_id == contact.provider_id).FirstOrDefault();
                hotel_id = h.provider_id;
            }           

            if (ModelState.IsValid)
            {

                contact.is_active = true;
                db.contacts.Add(contact);
                db.SaveChanges();



                if (provider != null && provider.provider_type == "H")
                {
                    return RedirectToAction("HotelDetails", "Hotels", new { id = hotel_id, tab = 2 });

                }
                else if (provider != null && provider.provider_type == "A")
                {
                    return RedirectToAction("ProviderDetails", "Providers", new { id = provider.provider_id, tab = AppEnums.ProviderTab.CONTACTS });
                }

                return RedirectToAction("CreateContact");
            }

            ViewBag.HasProvider = false;
            if (contact.provider_id > 0)
            {
                ViewBag.HasProvider = true;
                ViewBag.SelectedProviderId = contact.provider_id;
                ViewBag.SelectedProviderName = provider.name;
            }


            ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", contact.provider_id);
            return View(contact);
        }

        // GET: Contacts/Edit/5
        public ActionResult EditContact(int? id, int? hotelId)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            contact contact = db.contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }

            var provider = db.providers.Find(contact.provider_id);

            ViewBag.Provider = provider;

            if (provider.IsHotel)
            {

                ViewBag.Hotel = db.providers.Where(x => x.provider_id == contact.provider_id).FirstOrDefault();
            }


            ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", contact.provider_id);
            return View(contact);
        }

        // POST: Contacts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditContact([Bind(Include = "contact_id,provider_id,salutation,firstname,lastname,job_title,office_phone,mobile,email,notes")] contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();


                //logic to redirect to hotel details if contact works for hotel. first find provider, then hotel
                var provider = db.providers.Find(contact.provider_id);

                if (provider != null && provider.provider_type == "H")
                {
                    provider h = db.providers.Where(p => p.provider_id == contact.provider_id).FirstOrDefault();
                    return RedirectToAction("HotelDetails", "Hotels", new { id = h.provider_id, tab = 2 });
                }

                return RedirectToAction("ProviderDetails", "Providers", new { id = provider.provider_id, tab = AppEnums.ProviderTab.CONTACTS });
            }
            ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", contact.provider_id);
            return View(contact);
        }

        // GET: Contacts/Delete/5
        public ActionResult DeleteContact(int? id, int? hotelid = null)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            contact contact = db.contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }

            if (hotelid.HasValue)
            {
                ViewBag.HotelId = hotelid;
            }

            return View(contact);
        }

        // POST: Contacts/Delete/5
        [HttpPost, ActionName("DeleteContact")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteContactConfirmed(int id)
        {
            contact contact = db.contacts.Find(id);
            var provider = contact.provider;

            var provider_id = provider.provider_id;
            db.contacts.Remove(contact);
            db.SaveChanges();




            if (provider.IsHotel)
            {
                return RedirectToAction("HotelDetails", "Hotels", new { id = provider.provider_id, tab = 2 });
            }

            // redirect based on provider
            return RedirectToAction("ProviderDetails", new { id = provider_id, tab = AppEnums.ProviderTab.CONTACTS });
        }
        #endregion


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
