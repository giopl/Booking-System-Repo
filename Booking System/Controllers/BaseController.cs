﻿using Booking_System.Helpers;
using Booking_System.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Controllers
{
    public class BaseController : Controller
    {
        private ABSDBEntities db = new ABSDBEntities();
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected override void OnException(ExceptionContext filterContext)
        {
            Exception ex = filterContext.Exception;
            filterContext.ExceptionHandled = true;

            var model = new HandleErrorInfo(filterContext.Exception, filterContext.Controller.ToString(),filterContext.RouteData.Values.Values.ElementAt(1).ToString() );

            errorLog errLog = new errorLog();

            errLog.action = model.ActionName??"";
            errLog.controller = model.ControllerName??"";
            errLog.inner_exception = model.Exception.InnerException == null ? "" :model.Exception.InnerException.ToString();
            errLog.message = model.Exception.Message ?? "";
            errLog.stack_trace = model.Exception.StackTrace??"";
            errLog.user_id = UserSession.Current.UserId;
            errLog.user_name = UserSession.Current.Fullname;
            errLog.error_date_time = DateTime.Now;
            

            try
            {
                db.errorLogs.Add(errLog);
                db.SaveChanges();
            }
            catch (Exception)
            {
                log.ErrorFormat("message: {0}, innerException: {1}, stacktrace = {2}, user = {3}", errLog.message, errLog.inner_exception, errLog.stack_trace, errLog.user_id);
                
            }
            

            
            filterContext.Result = new ViewResult()
            {
                ViewName = "Error",
                ViewData = new ViewDataDictionary(model)
            };

        }


        protected override void OnActionExecuting(ActionExecutingContext ctx)
        {
            base.OnActionExecuting(ctx);
            //check if session is valid
            if (!Helpers.UserSession.Current.IsValid)
            {
                //check if it is an ajax request
                if (!Request.IsAjaxRequest())
                {
                    ctx.Result = RedirectToAction("Error", "Error", new { Id = 401 });
                }
                else
                {
                    ctx.RequestContext.HttpContext.Response.StatusCode = 401;
                    //to test if the ajax call is redirected to this contentresult
                    //if it does not work, redirect to home and check in JS if status code is 401
                    ctx.Result = RedirectToAction("AjaxSessionExpired", "Home");
                }
            }
        }

/// <summary>
/// Generic method to save audit to the db
/// </summary>
/// <param name="operation">Operation being made</param>
/// <param name="entityType">Type of entity</param>
/// <param name="identifier">unique identifier of the entity</param>
        protected void SaveAudit(AppEnums.Event operation, string entityType, int identifier)
        {
            try
            {
                audit Audit = new audit()
                {
                    created_by = UserSession.Current.UserId,
                    create_date = DateTime.Now,
                    operation = operation.ToString(),
                    entity_type = entityType,
                    entity_id = identifier,
                    user_id = UserSession.Current.UserId
                    

                };
                db.audits.Add(Audit);
                db.SaveChanges(); //save audit to DB
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// allows to send content of a view to a string
        /// </summary>
        /// <param name="controllerContext"></param>
        /// <param name="viewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        protected static String RenderRazorViewToString(ControllerContext controllerContext, String viewName, Object model)
        {
            controllerContext.Controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var ViewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                var ViewContext = new ViewContext(controllerContext, ViewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, sw);
                ViewResult.View.Render(ViewContext, sw);
                ViewResult.ViewEngine.ReleaseView(controllerContext, ViewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }


        protected  void InsertLog(AccessLog aclog) {
            try
            {
 
                log log = new log();
                log.operation = aclog.Operation;
                log.user_id = UserSession.Current.UserId;
                log.description = aclog.Details;
                log.item_type= aclog.Type;
                log.item_id= aclog.ItemId;

                db.logs.Add(log);
                db.SaveChanges();



            }
            catch (Exception ex)
            {

                var errorMessages = ex.InnerException;
                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(ex.ToString(), " ", ex.Message, " The validation errors are: ", fullErrorMessage);
                TempData["Exception"] = exceptionMessage;
                throw ex;
               // return RedirectToAction("GeneralError", "Error");
            }
            }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        /// <summary>
        /// helper method that adds market, provider and package objects to booking, since the FKs have been removed from the model.
        /// </summary>
        /// <param name="booking"></param>
        protected void refurbishbooking(booking booking)
        {
            try
            {


                var m = db.markets.Find(booking.market_id);
                var p = db.providers.Find(booking.provider_id);
                var pa = db.packages.Find(booking.package_id);

                var ag = db.users.Find(booking.agent_user_id);


                if (ag != null)
                {
                    booking.agent = ag;
                }

                if (m != null)
                {
                
                    booking.market = m;
                    var c = db.currencies.Find(m.currency_id);
                    booking.currency = c;
                }

                if (p != null)
                {

                    booking.provider = p;
                    if(p.IsHotel)
                    {
                        booking.HotelName = p.name;
                    }
                }

                if (pa != null)
                {
                    booking.package = pa;

                }


            }
            catch (Exception e)
            {

                throw;
            }


        }


    }
}