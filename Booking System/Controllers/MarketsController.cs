﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using Booking_System.Helpers;
using System.Data.Entity.Infrastructure;

namespace Booking_System.Controllers
{
    public class MarketsController : AdminController
    {
        private ABSDBEntities db = new ABSDBEntities();


        #region Market
        // GET: Markets
        public ActionResult MarketList()
        {
            var markets = db.markets.Include(m => m.currency).Include(m => m.provider);
            return View(markets.ToList());
        }
        
        // GET: Markets/Create
        public ActionResult CreateMarket(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            provider hotel = db.providers.Find(id);

            if (hotel == null)
            {
                return HttpNotFound();
            }

            ViewBag.Hotel = hotel;


            ViewBag.currency_id = new SelectList(db.currencies.Where(x=>x.is_active), "currency_id", "code_desc");
       
            ViewBag.hotel_id = new SelectList(db.providers.Where(x => x.is_active == true), "provider_id", "name");

            
            //return all countries which are not already binded to a market for this provider 
            var freecountries = db.countries
                .Include(x => x.markets)
                .AsNoTracking()
                .Where(               
                    x =>                     
                    x.markets
                    .Where(y => y.provider_id == id)
                    .Count() == 0
                    );
         
            ViewBag.country_id = new MultiSelectList(freecountries, "country_id", "name");
            return View();
        }

        // POST: Markets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateMarket([Bind(Include = "market_id,market_name,currency_id,provider_id,is_active,is_default")] market market, int[] country_id)
    
        {

            try
            {
                if (!market.is_default && country_id==null)
                {
                    ModelState.AddModelError("country_id", "For non-default market, at least one country should be defined");
                }
                else
                {
                    //if there is a market for the same provider which is set to default and this one is also set to default 
                    if (db.markets.Where(x => x.provider_id == market.provider_id && x.is_default && market.is_default).AsNoTracking().Count() > 0)
                    {
                        ModelState.AddModelError("is_default", "Another Market is already defined as Default");
                    }
                }
                if (ModelState.IsValid)
                {



                    List<country> countries = new List<country>();
                    if (!market.is_default)
                    {

                        foreach (int ctry in country_id)
                        {
                            country ctrycode = db.countries.Find(ctry);
                            ctrycode.country_id = ctry;
                            countries.Add(ctrycode);
                        }
                    }
                    market.is_active = true;
                    market.countries = countries;
                    db.markets.Add(market);
                    db.SaveChanges();


                    SaveAudit(AppEnums.Event.CREATE, "Market", market.market_id);
                    //   return RedirectToAction("MarketList");
                    return RedirectToAction("HotelDetails", "Hotels", new { id = market.provider_id, tab = AppEnums.HotelTab.DETAILS });

                }
                provider hotel = db.providers.Find(market.provider_id);

                if (hotel == null)
                {
                    return HttpNotFound();
                }

                ViewBag.Hotel = hotel;
                ViewBag.currency_id = new SelectList(db.currencies.Where(x => x.is_active == true), "currency_id", "currency_code", market.currency_id);
                ViewBag.hotel_id = new SelectList(db.providers.Where(x => x.is_active == true), "provider_id", "name", market.provider_id);
                ViewBag.country_id = new MultiSelectList(db.countries, "country_id", "name", market.countries.Select(x => x.country_id).ToArray());
                return View(market);
            }
            catch (Exception)
            {

                throw;
            }

           
        }

        // GET: Markets/Edit/5
        public ActionResult EditMarket(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            market market = db.markets.Find(id);
            if (market == null)
            {
                return HttpNotFound();
            }
            

            ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "currency_code", market.currency_id);
          

            //return all countries which are not already binded to a market for this provider 
            var freecountries = db.countries
                .Include(x => x.markets)
                .AsNoTracking()
                .Where(
                    x => x.markets
                    .Where(y => y.provider_id == id)
                    .Count() == 0
                    );
            ViewBag.country_id = new MultiSelectList(freecountries, "country_id", "name", market.countries.Select(x => x.country_id).ToArray());


            return View(market);
        }

        // POST: Markets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditMarket([Bind(Include = "country_id,market_id,market_name,currency_id,provider_id,is_active,is_default")] market market, int[] country_id)
        {
            try
            {
            if (!market.is_default && country_id == null)
            {
                ModelState.AddModelError("country_id", "For non-default market, at least one country should be defined");
            }
            else
            {
                    //if there is a market for the same provider which is set to default and this one is also set to default 
                    if (db.markets.Where(x => x.provider_id == market.provider_id && x.is_default && market.is_default).AsNoTracking().Count() > 0)
                {
                    ModelState.AddModelError("is_default", "Another Market is already defined as Default");
                }
            }
            if (ModelState.IsValid)
            {
                market oldmarket = db.markets.Include(c => c.countries).Single(x => x.market_id == market.market_id);
                oldmarket.countries.Clear(); //first remove all existing countries assigned to market

                if (!market.is_default) //should not be the default market
                {
                    foreach (int ctry in country_id)  //create the new countries list
                    {
                        country ctrycode = db.countries.Find(ctry);
                        ctrycode.country_id = ctry;
                        oldmarket.countries.Add(ctrycode); //add new markets to the model
                    }
                }
              
                
                db.Entry(oldmarket).CurrentValues.SetValues(market);  //replace all existing values (only on main object; not the sub collections ) with the one from the form
         


                //db.Entry(market).State = EntityState.Modified;
                db.SaveChanges();


                SaveAudit(AppEnums.Event.UPDATE, "Market", market.market_id);
                return RedirectToAction("HotelDetails", "Hotels", new { id = market.provider_id, tab = AppEnums.HotelTab.DETAILS });
            }
            ViewBag.currency_id = new SelectList(db.currencies, "currency_id", "currency_code", market.currency_id);
            ViewBag.hotel_id = new SelectList(db.providers, "provider_id", "name", market.provider_id);
            ViewBag.country_id = new MultiSelectList(db.countries, "country_id", "name", market.countries.Select(x => x.country_id).ToArray());

            return View(market);
            }
            catch (Exception)
            {

                throw;
            }
           

            
        }

        // GET: Markets/Delete/5
        public ActionResult DeleteMarket(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            market market = db.markets.Find(id);
            if (market == null)
            {
                return HttpNotFound();
            }

            ViewBag.Hotel = db.providers.Find(market.provider.provider_id);
            return View(market);
        }

        // POST: Markets/Delete/5
        [HttpPost, ActionName("DeleteMarket")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteMarketConfirmed(int id)
        {
            market market = db.markets.Include(m => m.countries).Where(x => x.market_id == id).First();

            var provider_id = market.provider.provider_id;
            try
            {
                db.Entry(market).State = EntityState.Deleted;
                db.SaveChanges();

                SaveAudit(AppEnums.Event.DELETE, "Market", market.market_id);
            }
            catch (DbUpdateException ex)
            {
                db.Entry(market).Reload();
                market.is_active = false;
                db.Entry(market).State = EntityState.Modified;
                db.SaveChanges();

            }
            
          
      
            return RedirectToAction("HotelDetails", "Hotels", new { id = provider_id, tab  = AppEnums.HotelTab.DETAILS });

        }
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
