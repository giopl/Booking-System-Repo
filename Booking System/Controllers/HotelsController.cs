﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using System.Data.Entity.Validation;
using Booking_System.Helpers;
using Z.EntityFramework.Plus;
using System.Data.Entity.Infrastructure;
using Booking_System.Models.ViewModel;

namespace Booking_System.Controllers
{
    public class HotelsController : AdminController
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ABSDBEntities db = new ABSDBEntities();

        #region "provider"
        // GET: providers
        public ActionResult ListHotels()
        {
           
            return View(db.providers.Where(x=>x.provider_type=="H").ToList());
        }

        public ActionResult FeaturedHotels()
        {
            return View();
        }


        // GET: providers/Details/5
        public ActionResult HotelDetails(int? id, AppEnums.HotelTab tab = AppEnums.HotelTab.DETAILS)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            provider provider = db.providers.Find(id);

            if (provider == null)
            {
                return HttpNotFound();
            }
            ViewBag.Tab = tab;

            var availableFeatures = db.hotelFeatures.Where(h => h.provider_id == id).ToList();
            var systemfeatures = db.features.Where(x => x.feature_type == "H").ToList();

            foreach (var f in availableFeatures)
            {
                feature feature = new feature { feature_id = f.feature_id };
                if (systemfeatures.Contains(feature))
                {
                    systemfeatures.Remove(feature);
                }
            }
            
            ViewBag.HotelFeatures = systemfeatures;



            return View(provider);
        }

        // GET: providers/Create
        public ActionResult CreateHotel()
        {
            ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name");
            provider model = new provider { is_active = true, provider_type = "H" };
            return View(model);
        }
        
        // POST: providers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateHotel([Bind(Include = "provider_id,name,description,location,company,website,commission_rate,street1,street2,town,country,phone1,phone2,fax,email,google_location,provider_type,star_rating,cancellation_policy")] provider provider)
        {

            try
            {
                if (ModelState.IsValid)
                {
                 

                    provider.is_active = true;

                    db.providers.Add(provider);
                    db.SaveChanges();
                    SaveAudit(AppEnums.Event.CREATE, "Hotel", provider.provider_id);
                    


                    return RedirectToAction("HotelDetails", new { id = provider.provider_id, tab = AppEnums.HotelTab.DETAILS });
                }

                ViewBag.provider_id = new SelectList(db.providers.Where(x=>x.is_active==true) , "provider_id", "name", provider.provider_id);
                return View(provider);
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }


            catch (Exception e)
            {
                log.ErrorFormat("controller error {0} ", e.ToString());

                throw;
            }

        }

        // GET: providers/Edit/5
        public ActionResult EditHotel(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                provider provider = db.providers

                  .SingleOrDefault(x => x.provider_id == id);




                if (provider == null)
                {
                    return HttpNotFound();
                }

                /// GL: that's terrible! /// luke i'm your father! 
                /// basically take the base class and set it back at the child level ;) 
                /// until better comes along
                //provider.SetProvider(provider.provider);


                ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", provider.provider_id);
                return View(provider);
            }
            catch (Exception)
            {

                throw;
            }

        }

        // POST: providers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditHotel([Bind(Include = "provider_id,provider_id,name,description,location,company,website,commission_rate,street1,street2,town,country,phone1,phone2,fax,email,google_location,provider_type,is_active,star_rating,cancellation_policy")] provider provider)
        {

            try
            {


                if (ModelState.IsValid)
                {



                    /// db concurrency exception, code below resolves it see 
                    /// //https://msdn.microsoft.com/en-us/data/jj592904
                    bool saveFailed;

                    do
                    {
                        saveFailed = false;

                        try
                        {
                            db.Entry(provider).State = EntityState.Modified;
                            db.SaveChanges();


                            SaveAudit(AppEnums.Event.UPDATE, "Hotel", provider.provider_id);
                            

                            //provider prov = new provider(provider);
                            //db.Entry(prov).State = EntityState.Modified;
                            //db.SaveChanges();

                        }
                        catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException e)
                        {
                            saveFailed = true;
                            var entry = e.Entries.Single();
                            entry.OriginalValues.SetValues(entry.GetDatabaseValues());


                        }
                    } while (saveFailed);



                    return RedirectToAction("HotelDetails", new { id = provider.provider_id, tab = AppEnums.HotelTab.DETAILS });

                    //return RedirectToAction("Index");
                }
                ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", provider.provider_id);


                return View(provider);

            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;


            }
        }



        // GET: providers/Delete/5
        public ActionResult DeleteHotel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            provider provider = db.providers.Find(id);
            if (provider == null)
            {
                return HttpNotFound();
            }
            return View(provider);
        }

        // POST: providers/Delete/5
        [HttpPost, ActionName("DeleteHotel")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteHotelConfirmed(int id)
        {
            try
            {
                provider provider = db.providers.Find(id);
                
                try
                {
                    db.Entry(provider).State = EntityState.Deleted;
                    db.SaveChanges();


                }
                catch (DbUpdateException ex)
                {
                    db.Entry(provider).Reload();
                    provider.is_active = false;
                    db.Entry(provider).State = EntityState.Modified;
                    db.SaveChanges();

                }


                SaveAudit(AppEnums.Event.DELETE, "Hotel", provider.provider_id);





                return RedirectToAction("ListHotels");
            }
            catch (Exception)
            {

                throw;
            }
            
        }


        #endregion

        #region Hotel Features

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddHotelFeature([Bind(Include = "feature_id,provider_id,quantity")] hotelFeature hotelFeature)
        {
            if (ModelState.IsValid)
            {

                
                db.hotelFeatures.Add(hotelFeature);
                db.SaveChanges();


                SaveAudit(AppEnums.Event.CREATE, "Hotel Feature", hotelFeature.feature_id);
              
                
                return RedirectToAction("HotelDetails",  new { id = hotelFeature.provider_id, tab = AppEnums.HotelTab.FEATURES });
            }
       
            return RedirectToAction("HotelDetails",  new { id = hotelFeature.provider_id, tab = AppEnums.HotelTab.FEATURES });
        }


        public ActionResult DeleteHotelFeature(int? id, int? provider_id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

           
            hotelFeature hotelFeature = db.hotelFeatures.Where(x => x.feature_id == id.Value && x.provider_id == provider_id.Value).FirstOrDefault();

            if (hotelFeature == null)
            {
                return HttpNotFound();
            }

            db.hotelFeatures.Remove(hotelFeature);
            db.SaveChanges();



            SaveAudit(AppEnums.Event.DELETE, "Hotel Feature", hotelFeature.feature_id);
          


          
            return RedirectToAction("HotelDetails", "Hotels", new { id = provider_id, tab = AppEnums.HotelTab.FEATURES });

        }


        #endregion

        #region "Guest Type"

        // GET: GuestTypes/Create
        public ActionResult CreateGuestType(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var provider = db.providers.Find(id);
            ViewBag.provider = provider;

            // only one can exist per provider, if method is hit, it means was done by URL
            if (provider.guest_type.Count > 0)
            {
                RedirectToAction("HotelDetails", "Hotels", new { id = provider.provider_id, tab = AppEnums.HotelTab.DETAILS });
            }


            var model = new guestType { provider = provider };

            return View(model);
        }

        // POST: GuestTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateGuestType([Bind(Include = "guest_type_id,provider_id,infant_min_age,infant_max_age,child_min_age,child_max_age,teen_min_age,teen_max_age,adult_min_age,adult_max_age,senior_min_age,senior_max_age")] guestType guestType)
        {

            try
            {

                var provider = db.providers.Find(guestType.provider_id);
                ViewBag.provider = provider;


                List<String> errors = new List<String>();

                if (provider.guest_type.Count > 0)
                {
                    errors.Add("Guest Type Definition Exists and cannot be recreated");
                }


                if (guestType.infant_min_age >= guestType.infant_max_age ||
                    guestType.child_min_age >= guestType.child_max_age ||
                    guestType.teen_min_age >= guestType.teen_max_age ||
                    guestType.adult_min_age >= guestType.adult_max_age ||
                    guestType.senior_min_age >= guestType.senior_max_age ||
                    guestType.infant_max_age >= guestType.child_min_age ||
                    guestType.child_max_age >= guestType.teen_min_age ||
                    guestType.teen_max_age >= guestType.adult_min_age ||
                    guestType.adult_max_age >= guestType.senior_min_age)
                {
                    errors.Add("Guest Type Definition Error, Range is not valid, please review");

                }

                if (SafeCast(guestType.infant_min_age) + SafeCast(guestType.infant_max_age) +
               SafeCast(guestType.child_min_age) + SafeCast(guestType.child_max_age) +
               SafeCast(guestType.teen_min_age) + SafeCast(guestType.teen_max_age) +
               SafeCast(guestType.adult_min_age) + SafeCast(guestType.adult_max_age) +
               SafeCast(guestType.senior_min_age) + SafeCast(guestType.senior_max_age) +
               SafeCast(guestType.infant_max_age) + SafeCast(guestType.child_min_age) +
               SafeCast(guestType.child_max_age) + SafeCast(guestType.teen_min_age) +
               SafeCast(guestType.teen_max_age) + SafeCast(guestType.adult_min_age) +
               SafeCast(guestType.adult_max_age) + SafeCast(guestType.senior_min_age) == 0)
                {
                    errors.Add("Guest Type Definition Error, No range detected, please add some values");

                }


                if (ModelState.IsValid && errors.Count == 0)
                {
                    guestType.is_active = true;
                    db.guestTypes.Add(guestType);
                    db.SaveChanges();

                    SaveAudit(AppEnums.Event.CREATE, "Guest Type", guestType.guest_type_id);
                    

                    return RedirectToAction("HotelDetails", new { id = provider.provider_id, tab = AppEnums.HotelTab.DETAILS });

                }

             

                ViewBag.HasValidationErrors = errors.Count() > 0;
                ViewBag.ValidationErrors = errors;
                guestType.provider = provider;
                return View(guestType);
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        // GET: GuestTypes/Edit/5
        public ActionResult EditGuestType(int? id)
        {

            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                guestType guestType = db.guestTypes.Find(id);
                if (guestType == null)
                {
                    return HttpNotFound();
                }

                guestType.provider = db.providers.Find(guestType.provider_id);
                return View(guestType);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        /// <summary>
        /// checks nullable value and returns value or 0
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        private  int SafeCast(int? t)
        {
            return t.HasValue ? t.Value : 0;
        }

        // POST: GuestTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditGuestType([Bind(Include = "guest_type_id,provider_id,infant_min_age,infant_max_age,child_min_age,child_max_age,teen_min_age,teen_max_age,adult_min_age,adult_max_age,senior_min_age,senior_max_age")] guestType guestType)
        {
            try
            {
                //     bool HasValidationErrors = false;
                List<String> errors = new List<String>();


                if (guestType.infant_min_age >= guestType.infant_max_age ||
                    guestType.child_min_age >= guestType.child_max_age ||
                    guestType.teen_min_age >= guestType.teen_max_age ||
                    guestType.adult_min_age >= guestType.adult_max_age ||
                    guestType.senior_min_age >= guestType.senior_max_age ||
                    guestType.infant_max_age >= guestType.child_min_age ||
                    guestType.child_max_age >= guestType.teen_min_age ||
                    guestType.teen_max_age >= guestType.adult_min_age ||
                    guestType.adult_max_age >= guestType.senior_min_age)
                {
                    errors.Add("Guest Type Definition Error, Range is not valid, please review");
                }
                if (SafeCast(guestType.infant_min_age) + SafeCast(guestType.infant_max_age) +
                   SafeCast(guestType.child_min_age) + SafeCast(guestType.child_max_age) +
                   SafeCast(guestType.teen_min_age) + SafeCast(guestType.teen_max_age) +
                   SafeCast(guestType.adult_min_age) + SafeCast(guestType.adult_max_age) +
                   SafeCast(guestType.senior_min_age) + SafeCast(guestType.senior_max_age) +
                   SafeCast(guestType.infant_max_age) + SafeCast(guestType.child_min_age) +
                   SafeCast(guestType.child_max_age) + SafeCast(guestType.teen_min_age) +
                   SafeCast(guestType.teen_max_age) + SafeCast(guestType.adult_min_age) +
                   SafeCast(guestType.adult_max_age) + SafeCast(guestType.senior_min_age) == 0)
                {
                    errors.Add("Guest Type Definition Error, No range detected, please add some values");

                }



                if (ModelState.IsValid && errors.Count == 0)
                {
                    db.Entry(guestType).State = EntityState.Modified;
                    db.SaveChanges();


                    SaveAudit(AppEnums.Event.UPDATE, "Guest Type", guestType.guest_type_id);
                   


                    return RedirectToAction("HotelDetails", new { id = guestType.provider_id, tab = AppEnums.HotelTab.DETAILS });

                }
             
                ViewBag.HasValidationErrors = errors.Count() > 0;
                ViewBag.ValErrors = errors;
                var pv = db.providers.Find(guestType.provider_id);
                guestType.provider = pv;
                ViewBag.provider = pv;
                return View(guestType);
            }
            catch (Exception)
            {

                throw;
            }

           
        }

        #endregion

        #region "Base Price"

        // GET: basePrices/Create
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">id of room</param>
        /// <returns></returns>
        public ActionResult CreateBasePrice(int? id)
        {
            //ViewBag.HasHotel = false;


            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var Room = db.rooms.Find(id);
            ViewBag.Room = Room;
            var markets = db.markets.Where(p => p.provider_id == Room.provider.provider_id);

            var default_mealplan = db.mealPlans.Where(x=>x.code=="HB").FirstOrDefault();

            ViewBag.meal_plan_id = new SelectList(db.mealPlans.Where(x => x.is_active == true), "meal_plan_id", "name", default_mealplan.meal_plan_id);
            ViewBag.market_id = new SelectList(markets.Where(x => x.is_active == true), "market_id", "market_name");
   
            ViewBag.room_id = new SelectList(db.rooms.Where(x => x.is_active == true), "room_id", "name");
            return View();
        }

        // POST: basePrices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateBasePrice([Bind(Include = "room_id,market_id,meal_plan_id,start_date,end_date,price_single,price_twin,price_triple,price_infant,price_child,price_teen,price_quadruple,price_without_bed,price_ground_handling,price_honeymoon")] basePrice basePrice/*, int?[] promotion_id*/)
        {


            var room = db.rooms.Find(basePrice.room_id);
            int? pricecnt = room.base_price.Where(x =>x.is_active && x.market_id == basePrice.market_id &&  (basePrice.start_date >= x.start_date && basePrice.start_date <= x.end_date) || (basePrice.end_date >= x.start_date && basePrice.end_date <= x.end_date)).Count();
            if(basePrice.start_date >basePrice.end_date)
            {
                ModelState.AddModelError("start_date", "Start date cannot be greater than end date.");
            }

            if (pricecnt != null && pricecnt > 0)
            {
                ModelState.AddModelError("start_date", "This date range is overlapping another price for the same room.");

            }

            if (ModelState.IsValid)
            {

              

                basePrice.is_active = true;
              
                
                db.basePrices.Add(basePrice);
                db.SaveChanges();
                return RedirectToAction("HotelDetails", "Hotels", new { id = room.provider_id, tab = AppEnums.HotelTab.ROOMS });
            }
            var Room = db.rooms.Find(basePrice.room_id);
            ViewBag.Room = Room;


            ViewBag.meal_plan_id = new SelectList(db.mealPlans, "meal_plan_id", "name", basePrice.meal_plan_id);

            var markets = db.markets.Where(p => p.provider_id == Room.provider.provider_id);          
            ViewBag.market_id = new SelectList(markets.Where(x => x.is_active == true), "market_id", "market_name", basePrice.market_id);
            ViewBag.room_id = new SelectList(db.rooms.Where(x => x.is_active == true), "room_id", "name", basePrice.room_id);
            return View(basePrice);
        }

        // GET: basePrices/Edit/5
        public ActionResult EditBasePrice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            basePrice basePrice = db.basePrices.Find(id);

            var Room = db.rooms.Find(basePrice.room_id);
            ViewBag.Room = Room;
            if (basePrice == null)
            {
                return HttpNotFound();
            }

            ViewBag.meal_plan_id = new SelectList(db.mealPlans, "meal_plan_id", "name", basePrice.meal_plan_id);

            var markets = db.markets.Where(p => p.provider_id == Room.provider.provider_id);
       
            ViewBag.market_id = new SelectList(markets, "market_id", "market_name", basePrice.market_id);
            ViewBag.room_id = new SelectList(db.rooms, "room_id", "name", basePrice.room_id);
            return View(basePrice);
        }

        // POST: basePrices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBasePrice([Bind(Include = "base_price_id,room_id,market_id,meal_plan_id,is_active,start_date,end_date,price_single,price_twin,price_triple,price_infant,price_child,price_teen,price_quadruple,price_without_bed,price_ground_handling,price_honeymoon")] basePrice basePrice/*, int?[] promotion_id*/)
        {

            var Room = db.rooms.Find(basePrice.room_id);
            if (basePrice.start_date > basePrice.end_date)
            {
                ModelState.AddModelError("start_date", "Start date cannot be greater than end date.");
            }
            int? pricecnt = Room.base_price.Where(x => x.is_active  && x.market_id==basePrice.market_id  && x.base_price_id != basePrice.base_price_id && ( (basePrice.start_date >= x.start_date && basePrice.start_date <= x.end_date) || (basePrice.end_date >= x.start_date && basePrice.end_date <= x.end_date))).Count();
           
            if (pricecnt != null && pricecnt > 0)
            {
                ModelState.AddModelError("start_date", "This date range is overlapping another price for the same room.");

            }
            if (ModelState.IsValid)
            {
                var oldbp = db.basePrices.Find(basePrice.base_price_id);
                db.Entry(oldbp).CurrentValues.SetValues(basePrice);                
                db.SaveChanges();              

                return RedirectToAction("HotelDetails", "Hotels", new { id = Room.provider_id, tab = AppEnums.HotelTab.ROOMS });
            }
            //if model is not valid, rebuild all viewbags

            ViewBag.Room = Room;
            ViewBag.meal_plan_id = new SelectList(db.mealPlans.Where(x => x.is_active == true), "meal_plan_id", "name", basePrice.meal_plan_id);

            var markets = db.markets.Where(p => p.provider_id == Room.provider.provider_id);
            var baseprice = Room.base_price.Where(x => x.base_price_id == basePrice.base_price_id).First();
  
            ViewBag.market_id = new SelectList(markets, "market_id", "market_name", basePrice.market_id);
            ViewBag.room_id = new SelectList(db.rooms, "room_id", "name", basePrice.room_id);
            return View(basePrice);
        }

        // GET: basePrices/Delete/5
        public ActionResult DeleteBasePrice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            basePrice basePrice = db.basePrices.Find(id);
            if (basePrice == null)
            {
                return HttpNotFound();
            }
            return View(basePrice);
        }

        // POST: basePrices/Delete/5
        [HttpPost, ActionName("DeleteBasePrice")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteBasePriceConfirmed(int? id)
        {
            basePrice basePrice = db.basePrices.Find(id);

            try
            {
                db.Entry(basePrice).State = EntityState.Deleted;
                db.SaveChanges();

            }
            catch (DbUpdateException ex)
            {
                db.Entry(basePrice).Reload();
                basePrice.is_active = false;
                db.Entry(basePrice).State = EntityState.Modified;
                db.SaveChanges();

            }
          
            var Room = db.rooms.Find(basePrice.room_id);
            return RedirectToAction("HotelDetails", "Hotels", new { id = Room.provider_id, tab = AppEnums.HotelTab.ROOMS });
        }

        #endregion

        #region Supplements


        // GET: supplements/Details/5
        public ActionResult ChargeSupplementDetails(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                supplement supplement = db.supplements.Where(x => x.supplement_id == id).First();
                if (supplement == null)
                {
                    return HttpNotFound();
                }
                return View(supplement);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // GET: supplements/Create
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type">S for supplement and C for Charge</param>
        /// <param name="id">id of Hotel</param>
        /// <returns></returns>
        public ActionResult CreateChargeSupplement(string type, int? id)
        {
            try
            {
                if (id == null)
                {
                    ViewBag.room_id = new SelectList(db.rooms, "room_id", "name");
                }
                else
                {
                    ViewBag.room_id = new SelectList(db.rooms.Where(x => x.provider_id == id), "room_id", "name");
                }

                ViewBag.provider_id = new SelectList(db.providers.Where(x => x.is_active == true), "provider_id", "provider.name");
                ViewBag.market_id = new SelectList(db.markets.Where(x => x.is_active == true), "market_id", "market_name");
                ViewBag.meal_plan_id = new SelectList(db.mealPlans.Where(x => x.is_active == true), "meal_plan_id", "name");


                ViewBag.Hasprovider = false;
                ViewBag.IsCharge = (type == "C");
                if (id.HasValue && id.Value > 0)
                {
                    var provider = db.providers.Find(id);
                    ViewBag.providerName = provider.name;
                    ViewBag.providerId = id;
                    ViewBag.Hasprovider = true;

                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // POST: supplements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateChargeSupplement([Bind(Include = "supplement_id,provider_id,market_id,name,description,valid_from,valid_to,is_per_person,is_per_room,is_per_night,is_per_item,valid_all_rooms,meal_plan_id")] supplement supplement, string SaveAndPricing, string type, int?[] room_id)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    supplement.is_compulsory = (type == "C");
                    supplement.is_active = true;

                    if (!supplement.is_per_room || supplement.valid_all_rooms)
                    {
                        room_id = null;
                    }
                    if (room_id != null && room_id.Count() > 0)
                    {
                        foreach (int roomid in room_id)
                        {
                            room room = db.rooms.Find(roomid);

                            supplement.rooms.Add(room);
                        }
                    }
                    supplement.is_active = true;
                    db.supplements.Add(supplement);
                    db.SaveChanges();

                    SaveAudit(AppEnums.Event.CREATE, supplement.is_compulsory ? "Charge" : "Supplement", supplement.supplement_id);
                    

                    if (SaveAndPricing == "SaveAndCreatePrice")
                    {
                        return RedirectToAction("CreateChargeSupplementPricing", "Hotels", new { id = supplement.supplement_id });
                      
                    }
                    else
                    {
                        return RedirectToAction("HotelDetails", "Hotels", new { id = supplement.provider_id, tab = supplement.hoteltab });
                    }

                }

                ViewBag.IsCharge = (type == "C");
                ViewBag.meal_plan_id = new SelectList(db.mealPlans.Where(x => x.is_active == true), "meal_plan_id", "name",supplement.meal_plan_id);
                ViewBag.provider_id = new SelectList(db.providers.Where(x => x.is_active == true), "provider_id", "provider.name", supplement.provider_id);
                // ViewBag.market_id = new SelectList(db.markets, "market_id", "market_name", supplement);
                return View(supplement);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // GET: supplements/Edit/5
        public ActionResult EditChargeSupplement(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                supplement supplement = db.supplements.Find(id);
                if (supplement == null)
                {
                    return HttpNotFound();
                }
                var provider = supplement.provider;
                ViewBag.providerName = provider.name;
                ViewBag.providerId = provider.provider_id;
                ViewBag.Hasprovider = true;


                ViewBag.room_id = new MultiSelectList(db.rooms.Where(x => x.provider_id == supplement.provider_id), "room_id", "name", supplement.rooms.Select(x => x.room_id).ToArray());
                ViewBag.meal_plan_id = new SelectList(db.mealPlans, "meal_plan_id", "name", supplement.meal_plan_id);

                ViewBag.provider_id = new SelectList(db.providers, "provider_id", "provider.name");
                //    ViewBag.market_id = new SelectList(db.markets, "market_id", "market_name", supplement);
                return View(supplement);
            }
            catch (Exception)
            {

                throw;
            }
            


       
        }

        // POST: supplements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditChargeSupplement([Bind(Include = "supplement_id,provider_id,market_id,name,description,valid_from,valid_to,is_per_person,is_per_room,is_per_night,is_per_item,is_compulsory,valid_all_rooms,is_active,meal_plan_id")] supplement supplement, int?[] room_id)
        {

            try
            {
                if (ModelState.IsValid)
                {
                


                    supplement oldsupplement = db.supplements.Include(c => c.rooms).Single(x => x.supplement_id == supplement.supplement_id);
                    oldsupplement.rooms.Clear(); //first remove all existing countries assigned to market
                    
                    if (!supplement.is_per_room || supplement.valid_all_rooms)
                    {
                        room_id = null;
                    }

                    if (room_id != null && room_id.Count() > 0)
                    {
                        foreach (int roomid in room_id)  //create the new countries list
                        {
                            room room = db.rooms.Find(roomid);
                            oldsupplement.rooms.Add(room); //add new markets to the model
                        }
                    }
                    supplement.is_compulsory = oldsupplement.is_compulsory;
                    db.Entry(oldsupplement).CurrentValues.SetValues(supplement);  //replace all existing values (only on main object; not the sub collections ) with the one from the form
                    db.SaveChanges();
                    ViewBag.meal_plan_id = new SelectList(db.mealPlans, "meal_plan_id", "name");

                    SaveAudit(AppEnums.Event.UPDATE, supplement.is_compulsory ? "Charge" : "Supplement", supplement.supplement_id);
               


                    return RedirectToAction("HotelDetails",  new { id = supplement.provider_id, tab = supplement.hoteltab });
                }



                ViewBag.room_id = new MultiSelectList(db.rooms.Where(x => x.provider_id == supplement.provider_id), "room_id", "name", room_id);
                ViewBag.provider_id = new SelectList(db.providers, "provider_id", "provider.name", supplement.provider_id);
              
                return View(supplement);
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        // GET: supplements/Delete/5
        public ActionResult DeleteChargeSupplement(int? id)
        {

            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                supplement supplement = db.supplements.Find(id);


                if (supplement == null)
                {
                    return HttpNotFound();
                }

                var provider = supplement.provider;
                ViewBag.providerName = provider.name;
                ViewBag.providerId = provider.provider_id;
                ViewBag.Hasprovider = true;


                return View(supplement);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // POST: supplements/Delete/5
        [HttpPost, ActionName("DeleteChargeSupplement")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteChargeSupplementConfirmed(int id)
        {

            try
            {
                supplement supplement = db.supplements.Where(x => x.supplement_id == id).Include(x => x.provider).First();
                var provider_id = supplement.provider_id;

                try
                {
                    db.Entry(supplement).State = EntityState.Deleted;
                    db.SaveChanges();

                }
                catch (DbUpdateException ex)
                {
                    db.Entry(supplement).Reload();
                    //supplement.is_active = false;
                    //db.Entry(supplement).State = EntityState.Modified;
                    //db.SaveChanges();

                }


                SaveAudit(AppEnums.Event.UPDATE, supplement.is_compulsory ? "Charge" : "Supplement", supplement.supplement_id);
              


                return RedirectToAction("HotelDetails",  new { id = provider_id, tab = supplement.hoteltab });
            }
            catch (Exception)
            {

                throw;
            }
           
        }


        #endregion

        #region supplement pricing
        // GET: supplementPricings
        public ActionResult PricingList(int? id)
        {
            var supplementPricings = db.supplementPricings.Include(s => s.market).Include(s => s.supplement).Where(x => x.supplement_id == id);
            return View(supplementPricings.ToList());
        }
     
        // GET: supplementPricings/Create
        public ActionResult CreateChargeSupplementPricing(int? id)
        {
            try
            {
                if (id != null && id > 0)
                {
                    var supplement = db.supplements.Find(id);
                    ViewBag.SupplementName = supplement.name;
                    ViewBag.IsPerPerson = supplement.is_per_person;
                    ViewBag.SupplementId = id;
                    ViewBag.SupplementTab = (supplement.is_compulsory ? Helpers.AppEnums.HotelTab.CHARGES : Helpers.AppEnums.HotelTab.SUPPLEMENTS);
                    ViewBag.SupplementType = (supplement.is_compulsory ? "Charge" : "Supplement");
                    ViewBag.providerid = supplement.provider_id;
                    ViewBag.providername = supplement.provider.name;
                    //make sure to filter the dropdowns to only include lists related to provider
                    ViewBag.market_id = new SelectList(db.markets.Where(x => x.provider_id == supplement.provider_id && x.is_active == true), "market_id", "market_name");
                    ViewBag.supplement_id = new SelectList(db.supplements.Where(x => x.provider_id == supplement.provider_id && x.is_active == true), "supplement_id", "name");
                }
                else
                {
                    ViewBag.market_id = new SelectList(db.markets.Where(x => x.is_active == true), "market_id", "market_name");
                    ViewBag.supplement_id = new SelectList(db.supplements.Where(x => x.is_active == true), "supplement_id", "name");
                }
                return View();
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        // POST: supplementPricings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateChargeSupplementPricing([Bind(Include = "supplement_pricing_id,supplement_id,market_id,valid_from,valid_to,price_per_item,price_infant,price_child,price_teen,price_adult,price_senior,price_honeymoon")] supplementPricing supplementPricing)
        {
            try
            {

                //add error checks for dates 
                if (supplementPricing.valid_from > supplementPricing.valid_to)
                {
                    ModelState.AddModelError("DateRange", "Valid From date should be less than valid Until date ");

                }
                //add error checks for amounts 
                if (
                    !supplementPricing.price_adult.HasValue && 
                    !supplementPricing.price_child.HasValue &&
                    !supplementPricing.price_infant.HasValue &&
                     !supplementPricing.price_per_item.HasValue &&
                      !supplementPricing.price_senior.HasValue &&
                       !supplementPricing.price_teen.HasValue &&
                       !supplementPricing.price_honeymoon.HasValue           
                                    
                    )
                {
                    ModelState.AddModelError("Prices", "At least one price should be defined");
                }
                    

                    if (ModelState.IsValid)
                {
                    supplementPricing.is_active = true;
                    db.supplementPricings.Add(supplementPricing);
                    db.SaveChanges();


                    SaveAudit(AppEnums.Event.CREATE, "Supplement Pricing", supplementPricing.supplement_id);                    
                    var supprice = db.supplementPricings.Where(x=>x.supplement_pricing_id== supplementPricing.supplement_pricing_id).Include(x=>x.supplement).First();
                    return RedirectToAction("HotelDetails", "Hotels", new { id = supprice.supplement.provider_id, tab = supprice.supplement.is_compulsory? Booking_System.Helpers.AppEnums.HotelTab.CHARGES: Booking_System.Helpers.AppEnums.HotelTab.SUPPLEMENTS});
                }
               
                var supplement = db.supplements.Find(supplementPricing.supplement_id);
                ViewBag.SupplementName = supplement.name;
                ViewBag.SupplementId = supplementPricing.supplement_id;
                ViewBag.IsPerPerson = supplement.is_per_person;
                ViewBag.SupplementTab = (supplement.is_compulsory ? Helpers.AppEnums.HotelTab.CHARGES : Helpers.AppEnums.HotelTab.SUPPLEMENTS);
                ViewBag.SupplementType = (supplement.is_compulsory ? "Charge" : "Supplement");
                ViewBag.providerid = supplement.provider_id;
                ViewBag.providername = supplement.provider.name;

                ViewBag.market_id = new SelectList(db.markets.Where(x => x.provider_id == supplement.provider_id && x.is_active == true), "market_id", "market_name", supplementPricing.market_id);
                ViewBag.supplement_id = new SelectList(db.supplements.Where(x => x.provider_id == supplement.provider_id && x.is_active == true), "supplement_id", "name", supplementPricing.supplement_id);
                return View(supplementPricing);
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        // GET: supplementPricings/Edit/5
        public ActionResult EditChargeSupplementPricing(int? id)
        {

            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                supplementPricing supplementPricing = db.supplementPricings.Find(id);
                if (supplementPricing == null)
                {
                    return HttpNotFound();
                }
                ViewBag.market_id = new SelectList(db.markets.Where(x => x.provider_id == supplementPricing.supplement.provider_id), "market_id", "market_name", supplementPricing.market_id);
                ViewBag.supplement_id = new SelectList(db.supplements.Where(x => x.provider_id == supplementPricing.supplement.provider_id), "supplement_id", "name", supplementPricing.supplement_id);
                return View(supplementPricing);
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        // POST: supplementPricings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditChargeSupplementPricing([Bind(Include = "supplement_pricing_id,supplement_id,market_id,valid_from,valid_to,price_per_item,price_infant,price_child,price_teen,price_adult,price_senior,price_honeymoon,is_active")] supplementPricing supplementPricing)
        {
            try
            {

                //add error checks for dates 
                if (supplementPricing.valid_from > supplementPricing.valid_to)
                {
                    ModelState.AddModelError("DateRange", "Valid From date should be less than valid Until date ");

                }
                //add errors for prices
                if (
                    !supplementPricing.price_adult.HasValue &&
                    !supplementPricing.price_child.HasValue &&
                    !supplementPricing.price_infant.HasValue &&
                     !supplementPricing.price_per_item.HasValue &&
                      !supplementPricing.price_senior.HasValue &&
                       !supplementPricing.price_teen.HasValue
                    )
                {
                    ModelState.AddModelError("Prices", "At least one price should be defined");
                }


                if (ModelState.IsValid)
                {
                    db.Entry(supplementPricing).State = EntityState.Modified;
                    db.SaveChanges();
                    SaveAudit(AppEnums.Event.UPDATE, "Supplement Pricing", supplementPricing.supplement_pricing_id);



                    var supprice = db.supplementPricings.Where(x=>x.supplement_pricing_id== supplementPricing.supplement_pricing_id).Include(x=>x.supplement).First();
                    return RedirectToAction("HotelDetails", "Hotels", new { id = supprice.supplement.provider_id, tab = supprice.supplement.hoteltab });

                }
                var supplement = db.supplements.Find(supplementPricing.supplement_id);
                ViewBag.market_id = new SelectList(db.markets.Where(x => x.provider_id == supplement.provider_id), "market_id", "market_name", supplementPricing.market_id);
                ViewBag.supplement_id = new SelectList(db.supplements.Where(x => x.provider_id == supplement.provider_id), "supplement_id", "name", supplementPricing.supplement_id);

                supplementPricing.supplement = supplement;
                return View(supplementPricing);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // GET: supplementPricings/Delete/5
        public ActionResult DeleteChargeSupplementPricing(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                supplementPricing supplementPricing = db.supplementPricings.Find(id);
                if (supplementPricing == null)
                {
                    return HttpNotFound();
                }
                return View(supplementPricing);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // POST: supplementPricings/Delete/5
        [HttpPost, ActionName("DeleteChargeSupplementPricing")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteChargeSupplementPricingConfirmed(int id)
        {
            try
            {
                supplementPricing supplementPricing = db.supplementPricings.Find(id);
                var supplement_id = supplementPricing.supplement_id;
                db.supplementPricings.Remove(supplementPricing);
                db.SaveChanges();
                SaveAudit(AppEnums.Event.DELETE, "Supplement Pricing", supplementPricing.supplement_pricing_id);

                var supp = db.supplements.Find(supplement_id);
                return RedirectToAction("HotelDetails", "Hotels", new { id = supp.provider_id, tab = supp.hoteltab });
            }
            catch (Exception)
            {

                throw;
            }
       
        }

        #endregion

        #region Package
        // GET: packages
        public ActionResult PackagesList()
        {
            var packages = db.packages;
            return View(packages.ToList());
        }

        // GET: packages/Details/5
        public ActionResult packageDetails(int? id)
        {

            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                package package = db.packages.Find(id);
                if (package == null)
                {
                    return HttpNotFound();
                }

                return View(package);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // GET: packages/Create
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">id of provider</param>
        /// <returns></returns>
        public ActionResult CreatePackage(int? id)
        {
            try
            {
                ViewBag.Hasprovider = false;

                if (id != null && id > 0)
                {
                    var provider = db.providers.Find(id);
                    ViewBag.providerName = provider.name;
                    ViewBag.providerId = id;
                    ViewBag.Hasprovider = true;




                }


                ViewBag.market_id = new SelectList(db.markets.Where(x => x.provider_id == id && x.is_active == true), "market_id", "market_name");
                ViewBag.room_id = new MultiSelectList(db.rooms.Where(x => x.provider_id == id && x.is_active == true), "room_id", "name");
               
                ViewBag.provider_id = new SelectList(db.providers.Where(x => x.is_active == true), "provider_id", "name");

         
                


                return View();
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        // POST: packages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePackage([Bind(Include = "package_id,provider_id,valid_from,valid_to,valid_all_rooms,market_id, name,description,number_nights,contracted_offer,cancellation_policy,price_ground_handling_adult,price_ground_handling_child,price_ground_handling_teen,apply_all_rooms")] package package,int?[] room_id)
        {

            try
            {

                if (package.valid_from > package.valid_to)
                {
                    ModelState.AddModelError("valid_from", "Promotion start date should be less than end date");
                }
                var prov = db.providers.Find(package.provider_id);
                if (ModelState.IsValid)
                {
                    package.is_active = true;

                    //initialise multivalues
                    List<room> Rooms = new List<room>();
                    List<market> Markets = new List<market>();



                    //add rooms  if any
                    if (!package.apply_all_rooms && room_id != null && room_id.Count() > 0)
                    {
                        foreach (int r in room_id)
                        {
                            room rm = db.rooms.Find(r);
                            rm.room_id = r;
                            Rooms.Add(rm);
                        }
                        package.rooms = Rooms;
                    }


                    db.packages.Add(package);
                    db.SaveChanges();
                    SaveAudit(AppEnums.Event.CREATE, "Package", package.package_id);                
                    return RedirectToAction("HotelDetails", new { id = package.provider_id, tab = AppEnums.HotelTab.PACKAGES });
                    

                }

                ViewBag.market_id = new SelectList(db.markets.Where(x => x.provider_id == prov.provider_id ), "market_id", "market_name", package.market_id);
                ViewBag.room_id = new MultiSelectList(db.rooms.Where(x => x.provider_id == prov.provider_id), "room_id", "name", room_id);                
                ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", package.provider_id);
         
                return View(package);
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        // GET: packages/Edit/5
        public ActionResult EditPackage(int? id)
        {

            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                package package = db.packages.Find(id);
                if (package == null)
                {
                    return HttpNotFound();
                }

                var provider_id = package.provider.provider_id;

                ViewBag.market_id = new SelectList(db.markets.Where(x => x.provider_id == package.provider_id), "market_id", "market_name", package.market_id);
              
                ViewBag.room_id = new MultiSelectList(db.rooms.Where(x => x.provider_id == package.provider_id), "room_id", "name", package.rooms.Select(x => x.room_id).ToArray());
                ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", package.provider_id);
            
                return View(package);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // POST: packages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPackage([Bind(Include = "package_id,provider_id,valid_from,valid_to,valid_all_rooms,market_id,name,description,number_nights,contracted_offer,cancellation_policy,price_ground_handling_adult,price_ground_handling_child,price_ground_handling_teen,apply_all_rooms,is_active")] package package, int?[] market_id, int?[] room_id)
        {

            try
            {

                if (package.valid_from > package.valid_to)
                {
                    ModelState.AddModelError("valid_from", "Promotion start date should be less than end date");
                }
                if (ModelState.IsValid)
                {


                    package oldpack = db.packages.Include(c => c.rooms).Single(x => x.package_id == package.package_id);

                 
                    oldpack.rooms.Clear(); // remove all existing rooms assigned to promo
                 


                    //add rooms  if any
                    if (!package.apply_all_rooms && room_id != null && room_id.Count() > 0)
                    {
                        foreach (int r in room_id)
                        {
                            room rm = db.rooms.Find(r);
                            rm.room_id = r;
                            oldpack.rooms.Add(rm);
                        }
                    }

                    db.Entry(oldpack).CurrentValues.SetValues(package);
                    db.SaveChanges();

                    SaveAudit(AppEnums.Event.UPDATE, "Package", package.package_id);
                    
                    return RedirectToAction("HotelDetails", new { id = package.provider_id, tab = AppEnums.HotelTab.PACKAGES });
                }
                

                package curpackage = db.packages.Find(package.package_id);
                package.provider = curpackage.provider;
                ViewBag.market_id = new SelectList(db.markets.Where(x => x.provider_id == curpackage.provider_id), "market_id", "market_name", curpackage.market_id);
                ViewBag.room_id = new MultiSelectList(db.rooms.Where(x => x.provider_id == curpackage.provider_id), "room_id", "name", curpackage.rooms.Select(x => x.room_id).ToArray());

                ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", package.provider_id);
            
                return View(package);
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        // GET: packages/Delete/5
        public ActionResult DeletePackage(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                package package = db.packages.Find(id);
                if (package == null)
                {
                    return HttpNotFound();
                }
                return View(package);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // POST: packages/Delete/5
        [HttpPost, ActionName("DeletePackage")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePackageConfirmed(int id)
        {
            try
            {
                package package = db.packages.Find(id);
                var providerid = package.provider_id;
             
                try
                {
                    db.Entry(package).State = EntityState.Deleted;
                    db.SaveChanges();

                }
                catch (DbUpdateException ex)
                {
                    db.Entry(package).Reload();
                    package.is_active = false;
                    db.Entry(package).State = EntityState.Modified;
                    db.SaveChanges();

                }


                SaveAudit(AppEnums.Event.DELETE, "Package", package.package_id);
                

                return RedirectToAction("HotelDetails", new { id = providerid, tab = AppEnums.HotelTab.PACKAGES });
            }
            catch (Exception)
            {

                throw;
            }
          
        }


        #endregion

        #region Promotion



        // GET: promotions/Details/5
        public ActionResult PromotionDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            promotion promotion = db.promotions.Find(id);
            if (promotion == null)
            {
                return HttpNotFound();
            }
            return View(promotion);
        }

        // GET: promotions/Create
        /// <summary>
        /// Create a new promotion
        /// </summary>
        /// <param name="id">id of provider </param>
        /// <returns></returns>
        public ActionResult CreatePromotion(int? id)
        {
            try
            {
                if (id != null && id > 0)
                {
                    var provider = db.providers.Find(id);
                    ViewBag.providerName = provider.name;
                    ViewBag.providerId = id;
                    ViewBag.Hasprovider = true;

                }
                ViewBag.promotion_group_id = new SelectList(db.configurations.Where(x => x.conf_type == "PROMOCATEG"), "configuration_id", "conf_value");
                ViewBag.market_id = new MultiSelectList(db.markets.Where(x => x.provider_id == id && x.is_active == true), "market_id", "market_name");             
                ViewBag.room_id = new MultiSelectList(db.rooms.Where(x => x.provider_id == id && x.is_active == true), "room_id", "name");

            }
            catch (Exception)
            {

                throw;
            }
       
            
            return View();
        }

        // POST: promotions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePromotion([Bind(Include = "promotion_id,provider_id,apply_all_markets,apply_all_rooms,name,code,description,contracted_offer,cancellation_policy,book_before_days_min,book_before_days_max,book_from_date,book_until_date,travel_from_date,travel_until_date,valid_from,valid_to,min_nights,discount,is_active,is_honeymoon,free_nights,promotion_group_id,flat_single,flat_twin,flat_triple,flat_quadruple,flat_child1,flat_child2,flat_teen1")] promotion promotion, int?[] market_id, int?[] room_id)
        {
            try
            {
                if (promotion.book_before_days_min.HasValue && promotion.book_before_days_min > promotion.book_before_days_max)
                {
                    ModelState.AddModelError("book_before_days_min", "The booking range is invalid ");
                }
                //Validate dates 
                if (promotion.travel_from_date>promotion.travel_until_date)
                {
                    ModelState.AddModelError("travel_from_date", "Travel start date should be less than Travel end date");
                }
                if (promotion.valid_from > promotion.valid_to)
                {
                    ModelState.AddModelError("valid_from", "Promotion start date should be less than end date");
                }
                if (promotion.book_from_date > promotion.book_until_date)
                {
                    ModelState.AddModelError("book_from_date", "Booking start date should be less than end date");
                }
                if (!promotion.apply_all_markets && market_id == null || (market_id != null && market_id.Count() == 0))
                {
                    ModelState.AddModelError("market_id", "If a promotion is not valid for all markets, at least one should be selected");
                }
                if (!promotion.apply_all_rooms && room_id == null || (room_id != null && room_id.Count() == 0))
                {
                    ModelState.AddModelError("room_id", "If a promotion is not valid for all rooms, at least one should be selected");
                }


                //validate free nights
                if(!promotion.min_nights.HasValue && promotion.free_nights.HasValue && promotion.free_nights.Value>0)
                {
                    ModelState.AddModelError("min_nights", "Min nights is required if free nights is defined");
                }
                else   if (promotion.min_nights.HasValue && promotion.free_nights.HasValue && promotion.free_nights.Value >=promotion.min_nights.Value )
                {
                    ModelState.AddModelError("free_nights", "The number of free nights should be less than minimum number of nights");

                }
                


                var prov  = db.providers.Find(promotion.provider_id);
                if (ModelState.IsValid)
                {
                    promotion.is_active = true;

                    //initialise multivalues
                    List<room> Rooms = new List<room>();
                    List<market> Markets = new List<market>();


                    //add markets  if any
                    if (!promotion.apply_all_markets && market_id != null && market_id.Count() > 0)
                    {
                        foreach (int mk in market_id)
                        {
                            market m = db.markets.Find(mk);
                            m.market_id = mk;
                            Markets.Add(m);
                        }
                        promotion.markets = Markets;
                    }


                    //add rooms  if any
                    if (!promotion.apply_all_rooms && room_id != null && room_id.Count() > 0)
                    {
                        foreach (int r in room_id)
                        {
                            room rm = db.rooms.Find(r);
                            rm.room_id = r;
                            Rooms.Add(rm);
                        }
                        promotion.rooms = Rooms;
                    }



                    db.promotions.Add(promotion);
                    db.SaveChanges();
                    SaveAudit(AppEnums.Event.CREATE, "Promotion", promotion.promotion_id);
                    return RedirectToAction("HotelDetails", new { id = prov.provider_id, tab = AppEnums.HotelTab.PROMOTIONS });
                }


                ///repopulate everything in case of error
                var provider = prov;
                ViewBag.providerName = prov.name;
                ViewBag.providerId = prov.provider_id;
                ViewBag.Hasprovider = true;
           
                ViewBag.promotion_group_id = new SelectList(db.configurations.Where(x => x.conf_type == "PROMOCATEG"), "configuration_id", "conf_value", promotion.promotion_group_id);
                ViewBag.market_id = new MultiSelectList(db.markets.Where(x => x.provider_id == prov.provider_id && x.is_active == true), "market_id", "market_name",market_id);
                ViewBag.room_id = new MultiSelectList(db.rooms.Where(x => x.provider_id == prov.provider_id && x.is_active == true), "room_id", "name",room_id);

                return View(promotion);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // GET: promotions/Edit/5
        public ActionResult EditPromotion(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                promotion promotion = db.promotions.Find(id);
                if (promotion == null)
                {
                    return HttpNotFound();
                }

                ViewBag.promotion_group_id = new SelectList(db.configurations.Where(x => x.conf_type == "PROMOCATEG"), "configuration_id", "conf_value", promotion.promotion_group_id);
                ViewBag.market_id = new MultiSelectList(db.markets.Where(x => x.provider_id == promotion.provider_id), "market_id", "market_name", promotion.markets.Select(x=>x.market_id).ToArray());
                //ViewBag.market_id = new SelectList(db.markets.Where(x=>x.provider_id ==promotion.provider_id  ), "market_id", "market_name", promotion.market_id);
                ViewBag.room_id = new MultiSelectList(db.rooms.Where(x => x.provider_id == promotion.provider_id), "room_id", "name", promotion.rooms.Select(x=>x.room_id).ToArray());
                return View(promotion);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // POST: promotions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPromotion([Bind(Include = "promotion_id,provider_id,apply_all_markets,apply_all_rooms,name,code,description,contracted_offer,cancellation_policy,book_before_days_min,book_before_days_max,book_from_date,book_until_date,travel_from_date,travel_until_date,valid_from,valid_to,min_nights,discount,is_active,is_honeymoon,free_nights,promotion_group_id,flat_single,flat_twin,flat_triple,flat_quadruple,flat_child1,flat_child2,flat_teen1")] promotion promotion, int?[] market_id, int?[] room_id)
        {

            try
            {

                //Validate dates 

                if (promotion.book_before_days_min.HasValue && promotion.book_before_days_min > promotion.book_before_days_max)
                {
                    ModelState.AddModelError("book_before_days_min", "The booking range is invalid ");
                }
                if (promotion.travel_from_date > promotion.travel_until_date)
                {
                    ModelState.AddModelError("travel_from_date", "Travel start date should be less than Travel end date");
                }
                if (promotion.valid_from > promotion.valid_to)
                {
                    ModelState.AddModelError("valid_from", "Promotion start date should be less than end date");
                }
                if (promotion.book_from_date > promotion.book_until_date)
                {
                    ModelState.AddModelError("book_from_date", "Booking start date should be less than end date");
                }
                if (!promotion.apply_all_markets && market_id==null || (market_id != null && market_id.Count()==0))
                {
                    ModelState.AddModelError("market_id", "If a promotion is not valid for all markets, at least one should be selected");
                }
                if (!promotion.apply_all_rooms && room_id == null || (room_id != null && room_id.Count() == 0))
                {
                    ModelState.AddModelError("room_id", "If a promotion is not valid for all rooms, at least one should be selected");
                }
                //validate free nights
                if (!promotion.min_nights.HasValue && promotion.free_nights.HasValue && promotion.free_nights.Value > 0)
                {
                    ModelState.AddModelError("min_nights", "Min nights is required if free nights is defined");
                }
                else if (promotion.min_nights.HasValue && promotion.free_nights.HasValue && promotion.free_nights.Value >= promotion.min_nights.Value)
                {
                    ModelState.AddModelError("free_nights", "The number of free nights should be less than minimum number of nights");

                }
                if (ModelState.IsValid)
                {
                    
                 
                    promotion oldpromo = db.promotions.Include(c => c.rooms).Include(x=>x.markets).Single(x => x.promotion_id == promotion.promotion_id);

                    oldpromo.markets.Clear(); //first remove all existing markets assigned to promo
                    oldpromo.rooms.Clear(); // remove all existing rooms assigned to promo
                    //add markets  if any
                    if (!promotion.apply_all_markets && market_id != null && market_id.Count() > 0)
                    {
                        foreach (int mk in market_id)
                        {
                            market m = db.markets.Find(mk);
                            m.market_id = mk;
                            oldpromo.markets.Add(m);
                        }
                       
                    }


                    //add rooms  if any
                    if (!promotion.apply_all_rooms && room_id != null && room_id.Count() > 0)
                    {
                        foreach (int r in room_id)
                        {
                            room rm = db.rooms.Find(r);
                            rm.room_id = r;
                            oldpromo.rooms.Add(rm);
                        }                      
                    }

                    db.Entry(oldpromo).CurrentValues.SetValues(promotion);


                    db.SaveChanges();
                    SaveAudit(AppEnums.Event.UPDATE, "Promotion", promotion.promotion_id);
                    return RedirectToAction("HotelDetails", new { id = promotion.provider_id, tab = AppEnums.HotelTab.PROMOTIONS });
                }
                promotion promo = db.promotions.Find(promotion.promotion_id);
                promotion.provider = promo.provider;
                ViewBag.promotion_group_id = new SelectList(db.configurations.Where(x => x.conf_type=="PROMOCATEG"), "configuration_id", "conf_value", promo.promotion_group_id);
                ViewBag.market_id = new MultiSelectList(db.markets.Where(x => x.provider_id == promotion.provider_id), "market_id", "market_name", promo.markets.Select(x => x.market_id).ToArray());
                ViewBag.room_id = new MultiSelectList(db.rooms.Where(x => x.provider_id == promo.provider_id), "room_id", "name", promo.rooms.Select(x=>x.room_id).ToArray());
                return View(promotion);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // GET: promotions/Delete/5
        public ActionResult DeletePromotion(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            promotion promotion = db.promotions.Find(id);
            if (promotion == null)
            {
                return HttpNotFound();
            }
            return View(promotion);
        }

        // POST: promotions/Delete/5
        [HttpPost, ActionName("DeletePromotion")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePromotionConfirmed(int id)
        {
            try
            {
                promotion promotion = db.promotions.Find(id);
                try
                {
                    db.Entry(promotion).State = EntityState.Deleted;
                    db.SaveChanges();

                }
                catch (DbUpdateException ex)
                {
                    db.Entry(promotion).Reload();
                    promotion.is_active = false;
                    db.Entry(promotion).State = EntityState.Modified;
                    db.SaveChanges();

                }


                SaveAudit(AppEnums.Event.DELETE, "Promotion", promotion.promotion_id);
                return RedirectToAction("HotelDetails", new { id = promotion.provider_id, tab = AppEnums.HotelTab.PROMOTIONS });
            }
            catch (Exception)
            {

                throw;
            }
            
        }


        #endregion

        #region promotionSet


        // GET: promotionSets/Create
        public ActionResult CreatePromotionSet(int id)
        {
            try
            {
                var prov = db.providers.Find(id);
                ViewBag.providerName = prov.name;
                ViewBag.providerId = id;
                ViewBag.Hasprovider = true;
                ViewBag.promotion_id = new MultiSelectList(db.promotions.Where(x => x.provider_id == id), "promotion_id", "name");
                return View();
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // POST: promotionSets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreatePromotionSet([Bind(Include = "promotion_set_id,name,description,provider_id")] promotionSet promotionSet, int?[] promotion_id)
        {
            try
            {
                var nameexist = db.promotionSets.Where(x => x.name == promotionSet.name && x.provider_id== promotionSet.provider_id).Count() > 0;
                if (nameexist)
                {
                    ModelState.AddModelError("name", "This name already exists. Please choose another one ");
                }
                if (ModelState.IsValid)
                {
                    promotionSet.is_active = true;
                    List<promotion> promos = new List<promotion>();
                    //add promos  if any
                    if (promotion_id.Count() > 0)
                    {
                       
                        foreach (int pr in promotion_id)
                        {
                            promotion p= db.promotions.Find(pr);
                            p.promotion_id = pr;
                            promos.Add(p);
                        }
                        promotionSet.promotions = promos;
                    }


                    db.promotionSets.Add(promotionSet);
                    db.SaveChanges();
                    return RedirectToAction("HotelDetails", new { id = promotionSet.provider_id, tab = AppEnums.HotelTab.PROMOTION_SETS });
                }
                var prov = db.providers.Find(promotionSet.provider_id);
                ViewBag.providerName = prov.name;
                ViewBag.providerId = promotionSet.provider_id;
                ViewBag.Hasprovider = true;
                ViewBag.promotion_id = new MultiSelectList(db.promotions.Where(x => x.provider_id == promotionSet.provider_id), "promotion_id", "name",promotion_id);
                return View(promotionSet);
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        // GET: promotionSets/Edit/5
        public ActionResult EditPromotionSet(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                promotionSet promotionSet = db.promotionSets.Where(x=>x.promotion_set_id==id).Include(x=>x.provider).FirstOrDefault();
                if (promotionSet == null)
                {
                    return HttpNotFound();
                }
                ViewBag.promotion_id = new MultiSelectList(db.promotions.Where(x => x.provider_id == promotionSet.provider_id), "promotion_id", "name", promotionSet.promotions.Select(x=>x.promotion_id).ToArray() );
                return View(promotionSet);
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        // POST: promotionSets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPromotionSet([Bind(Include = "promotion_set_id,name,description,provider_id,is_active")] promotionSet promotionSet,int?[] promotion_id)
        {
            try
            {
                var nameexist = db.promotionSets.Where(x => x.name == promotionSet.name && x.provider_id == promotionSet.provider_id && x.promotion_set_id!= promotionSet.promotion_set_id).Count() > 0;
                if (nameexist)
                {
                    ModelState.AddModelError("name", "This name already exists. Please choose another one ");
                }
                if (ModelState.IsValid)
                {
                    promotionSet oldpromoset = db.promotionSets.Include(x => x.promotions).Single(x => x.promotion_set_id == promotionSet.promotion_set_id);

                    oldpromoset.promotions.Clear(); //first remove all existing promotions assigned to promo set

                   // List<promotion> promos = new List<promotion>();
                    //add promos  if any
                    if (promotion_id.Count() > 0)
                    {

                        foreach (int pr in promotion_id)
                        {
                            promotion p = db.promotions.Find(pr);
                            p.promotion_id = pr;
                            oldpromoset.promotions.Add(p);
                            //promos.Add(p);
                        }
                        
                    }
                    db.Entry(oldpromoset).CurrentValues.SetValues(promotionSet);
                    
                    db.SaveChanges();
                    return RedirectToAction("HotelDetails", new { id = promotionSet.provider_id, tab = AppEnums.HotelTab.PROMOTION_SETS });
                }
                var prov = db.providers.Find(promotionSet.provider_id);
                promotionSet.provider = prov;
                ViewBag.provider_id = new SelectList(db.providers, "provider_id", "name", promotionSet.provider_id);
                ViewBag.promotion_id = new MultiSelectList(db.promotions.Where(x => x.provider_id == promotionSet.provider_id), "promotion_id", "name", promotion_id);
                return View(promotionSet);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // GET: promotionSets/Delete/5
        public ActionResult DeletePromotionSet(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            promotionSet promotionSet = db.promotionSets.Find(id);
            if (promotionSet == null)
            {
                return HttpNotFound();
            }
            return View(promotionSet);
        }

        // POST: promotionSets/Delete/5
        [HttpPost, ActionName("DeletePromotionSet")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePromotionSetConfirmed(int id)
        {
            promotionSet promotionSet = db.promotionSets.Find(id);
            db.promotionSets.Remove(promotionSet);
            db.SaveChanges();
            return RedirectToAction("HotelDetails", new { id = promotionSet.provider_id, tab = AppEnums.HotelTab.PROMOTION_SETS });
        }

        #endregion

        #region "Room Allotment"

        // GET: ATESTpackageAllotments/Details/5
        public ActionResult RoomAllotmentDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            roomAllotment roomAllotment = db.roomAllotments.Find(id);
            if (roomAllotment == null)
            {
                return HttpNotFound();
            }
            return View(roomAllotment);
        }

        // GET: ATESTpackageAllotments/Create
        public ActionResult CreateRoomAllotment(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            provider provider = db.providers.Find(id);

            if (provider == null)
            {
                return HttpNotFound();
            }

            ViewBag.package = provider;

            //ViewBag.package_id = new SelectList(db.packages, "package_id", "name");
            //ViewBag.room_id = new SelectList(db.rooms, "room_id", "name");
            return View();
        }

        // POST: ATESTpackageAllotments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateRoomAllotment(RoomAllotmentViewModel model)
        {


            //retrieve each date separately splitted by ;
            string[] multipledates = model.MultipleDates.Split(';');

            //using hashset to avoid duplicates in bag
            HashSet<DateTime> dateRange = new HashSet<DateTime>();
            List<room> Rooms = new List<room>();

            foreach (var dt in multipledates)
            {
                DateTime result;
                if (DateTime.TryParse(dt.Trim(), out result))
                {
                    //re-build the date manually from the string so that it doesn't break the format depending on culture
                    var dd = Convert.ToInt32(dt.Trim().Substring(0, 2));
                    var mm = Convert.ToInt32(dt.Trim().Substring(3, 2));
                    var yy = Convert.ToInt32(dt.Trim().Substring(6, 4));

                    DateTime addme = new DateTime(yy, mm, dd);
                    dateRange.Add(addme);
                }
            }


        
            foreach (var room in model.Rooms)
            {
                foreach (var dt in dateRange)
                {
                    var allotmentExists = db.roomAllotments.Where(a => a.room_id == room && a.allotment_date == dt).Any();
                    //added dummy params because of validation errors since these notMapped fields are required on form
                    if(!allotmentExists)
                    {
                        var alloment = new roomAllotment { allotment_date = dt, room_id = room, allotment_amount = model.NumberAllotments, provider_id = model.ProviderId };
                        db.roomAllotments.Add(alloment);                    

                    }
                }
            }

            db.SaveChanges();
            

            return RedirectToAction("HotelDetails", new { id = model.ProviderId , tab = AppEnums.HotelTab.ROOM_ALLOTMENT });
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditRoomAllotment(int id , int amount )
        {
            var roomAllotment = db.roomAllotments.Where(x=>x.room_allotment_id == id).FirstOrDefault();
            if(roomAllotment != null)
            {

            roomAllotment.allotment_amount = amount;

            db.Entry(roomAllotment).State = EntityState.Modified;
                db.SaveChanges();
            
            }
                return RedirectToAction("HotelDetails", new { id = roomAllotment.provider_id, tab = AppEnums.HotelTab.ROOM_ALLOTMENT });
        }

               public ActionResult DeleteRoomAllotment(int id)
        {
            roomAllotment roomAllotment = db.roomAllotments.Where(x=>x.room_allotment_id == id).FirstOrDefault();
            var providerId = roomAllotment.provider_id;
            db.roomAllotments.Remove(roomAllotment);
            db.SaveChanges();
            return RedirectToAction("HotelDetails", new { id = providerId, tab = AppEnums.HotelTab.ROOM_ALLOTMENT });
        }


        
        
        #endregion

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
