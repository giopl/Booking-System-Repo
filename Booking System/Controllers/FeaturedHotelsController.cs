﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using System.Data.Entity.Validation;
using Booking_System.Helpers;
using Z.EntityFramework.Plus;
using System.Data.Entity.Infrastructure;

namespace Booking_System.Controllers
{
    public class FeaturedHotelsController : AdminController
    {
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private ABSDBEntities db = new ABSDBEntities();

        // GET: FeaturedHotels
        [HttpGet]
        public ActionResult ListFeaturedHotels()
        {
            return View(db.featuredHotels.ToList());
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditFeaturedHotel([Bind(Include = "featured_hotel_id, provider_id, title, description, section, from_date, to_date, picture_id")]featuredHotel featuredHotelObj)
        {
            //featuredHotel dbObj = db.featuredHotels.AsNoTracking().SingleOrDefault(x => x.featured_hotel_id == featuredHotelObj.featured_hotel_id);
            //var dbObj = db.featuredHotels.Where(x => x.featured_hotel_id == featuredHotelObj.featured_hotel_id).FirstOrDefault();

            //Get the Initial DB Object to delete
            featuredHotel dbObj = db.featuredHotels.SingleOrDefault(x => x.featured_hotel_id == featuredHotelObj.featured_hotel_id);

            //Build the new object
            featuredHotelObj.picture_id = dbObj.picture_id;
            featuredHotelObj.provider = dbObj.provider;

            try
            { 
            if (ModelState.IsValid)
            {
                    //db.Entry(featuredHotelObj).State = EntityState.Modified;
                    //db.SaveChanges();
                    //return RedirectToAction("ListFeaturedHotels");
                    db.featuredHotels.Remove(dbObj);
                    db.featuredHotels.Add(featuredHotelObj);
                    db.SaveChanges();
                }
                return RedirectToAction("ListFeaturedHotels");
            }
            catch (DbUpdateException e)
            {
                log.ErrorFormat("controller error {0} ", e.ToString());

                #region ProviderDropDownList
                //All providers
                var allproviderList = db.providers.Where(x => x.provider_type == "H");

                //Provider with Feature Hotel already
                var existingFeaturedProvider = db.featuredHotels.ToList().Select(x => x.provider_id);

                if (existingFeaturedProvider != null)
                {
                    //Return List of Provider that for which Featured Hotels has not been created
                    ViewBag.provider_id = new SelectList(allproviderList.Where(x => !existingFeaturedProvider.Contains(x.provider_id)), "provider_id", "name");
                }
                else
                {
                    ViewBag.provider_id = new SelectList(allproviderList, "provider_id", "name");
                }
                ModelState.AddModelError(string.Empty, string.Concat("Section ", featuredHotelObj.section.ToString(), " is already occupied. Choose another section"));
                return View(featuredHotelObj);
                #endregion
            }
            catch (Exception ex)
            {

            }
            ViewBag.provider_id = new SelectList(db.providers.Where(x => x.provider_type == "H"), "provider_id", "name", featuredHotelObj.provider);
            return View(featuredHotelObj);
        }

        // POST: TestVehicle/Delete/5
        [HttpPost, ActionName("DeleteFeaturedHotel")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteFeaturedHotel(int id)
        {
            featuredHotel featuredHotel = db.featuredHotels.SingleOrDefault(x => x.featured_hotel_id == id);
            try
            {
                //db.featuredHotels.Where(x => x.featured_hotel_id == id).Delete();
                //db.featuredHotels.Where(x => x.vehicle_id == id).Delete();
                //db.featuredHotels.Where(x => x.vehicle_id == id).Delete();

                db.featuredHotels.Remove(featuredHotel);
                db.SaveChanges();
                return RedirectToAction("ListFeaturedHotels");

            }
            catch (DbUpdateException ex)
            {
                db.Entry(featuredHotel).Reload();
                db.Entry(featuredHotel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListFeaturedHotels");

            }



        }

        [HttpGet]
        public ActionResult DeleteFeaturedHotel(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            featuredHotel featuredHotel = db.featuredHotels.SingleOrDefault(x => x.featured_hotel_id == id);
            if (featuredHotel == null)
            {
                return HttpNotFound();
            }
            return View(featuredHotel);
        }

        [HttpGet]
        public ActionResult EditFeaturedHotel(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                featuredHotel featuredHotel = db.featuredHotels.SingleOrDefault(x => x.featured_hotel_id == id);


                if (featuredHotel == null)
                {
                    return HttpNotFound();
                }

                /// GL: that's terrible! /// luke i'm your father! 
                /// basically take the base class and set it back at the child level ;) 
                /// until better comes along
                //provider.SetProvider(provider.provider);


                ViewBag.provider_id = new SelectList(db.providers.Where(x=> x.provider_type =="H"), "provider_id", "name", featuredHotel.provider);
                //ViewBag.hotelpictures = db.providerPictures.SingleOrDefault(x => x.provider_id==featuredHotel.provider_id);
                return View(featuredHotel);
            }
            catch (Exception)
            {

                throw;
            }

        }



        public ActionResult UpdateFeatureImage(int? id, int? pictureId, ImagesType type)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                featuredHotel featuredHotel = db.featuredHotels.SingleOrDefault(x => x.featured_hotel_id == id);
                featuredHotel.picture_id = pictureId;
                db.Entry(featuredHotel).State = EntityState.Modified;
                db.SaveChanges();


                if (featuredHotel == null)
                {
                    return HttpNotFound();
                }

                /// GL: that's terrible! /// luke i'm your father! 
                /// basically take the base class and set it back at the child level ;) 
                /// until better comes along
                //provider.SetProvider(provider.provider);


                ViewBag.provider_id = new SelectList(db.providers.Where(x => x.provider_type == "H"), "provider_id", "name", featuredHotel.provider);
                //ViewBag.hotelpictures = db.providerPictures.SingleOrDefault(x => x.provider_id==featuredHotel.provider_id);
                return View("EditFeaturedHotel",featuredHotel);
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet]
        public ActionResult Create()
        {
            featuredHotel model = new featuredHotel();


            //All providers
            var allproviderList = db.providers.Where(x => x.provider_type == "H");

            //Provider with Feature Hotel already
            var existingFeaturedProvider = db.featuredHotels.ToList().Select(x=> x.provider_id);

            if (existingFeaturedProvider!=null)
            { 
                //Return List of Provider that for which Featured Hotels has not been created
                ViewBag.provider_id = new SelectList(allproviderList.Where(x => !existingFeaturedProvider.Contains(x.provider_id)), "provider_id", "name");
            }
            else
            {
                ViewBag.provider_id = new SelectList(allproviderList, "provider_id", "name");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(featuredHotel featuredHotel)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    db.featuredHotels.Add(featuredHotel);
                    db.SaveChanges();
                    SaveAudit(AppEnums.Event.CREATE, "Featured Hotel", featuredHotel.featured_hotel_id);

                    return RedirectToAction("ListFeaturedHotels");
                }

                //ViewBag.fee.provider_id = new SelectList(db.providers, "provider_id", "name", fea provider.provider_id);
                return View(featuredHotel);
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
            catch (DbUpdateException e)
            {
                log.ErrorFormat("controller error {0} ", e.ToString());

                #region ProviderDropDownList
                //All providers
                var allproviderList = db.providers.Where(x => x.provider_type == "H");

                //Provider with Feature Hotel already
                var existingFeaturedProvider = db.featuredHotels.ToList().Select(x => x.provider_id);

                if (existingFeaturedProvider != null)
                {
                    //Return List of Provider that for which Featured Hotels has not been created
                    ViewBag.provider_id = new SelectList(allproviderList.Where(x => !existingFeaturedProvider.Contains(x.provider_id)), "provider_id", "name");
                }
                else
                {
                    ViewBag.provider_id = new SelectList(allproviderList, "provider_id", "name");
                }
                ModelState.AddModelError(string.Empty, string.Concat("Section ", featuredHotel.section.ToString(), " is already occupied. Choose another section"));
                return View(featuredHotel);
                #endregion
            }
            catch (Exception e)
            {
                log.ErrorFormat("controller error {0} ", e.ToString());
                throw;
            }
        }
    }
}