﻿using Booking_System.Helpers;
using Booking_System.Models;
using Booking_System.Services;
using Booking_System.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Controllers
{
    public class LoginController : Controller
    {

        private ABSDBEntities db = new ABSDBEntities();

        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ActionResult Index(LoginStateEnum loginState = LoginStateEnum.Not_Set)
        {
            try
            {
                LoginState state = new LoginState { LoginStateEnum = loginState };
                return View(state);
            }
            catch (Exception e)
            {
                log.Error("Exception Caught" + e.ToString());
                TempData["errorLog"] = new ErrorLog(e);
                return RedirectToAction("ShowError", "Error");
            }
        }

        public ActionResult Login(LoginState loginstate)
        {

            IAdminService AdminService = new AdminService();

            LoginState result = new LoginState();
            if (Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["PasswordEnabled"]))
            {
                result = AdminService.VerifyUser(loginstate);
            }
            else
            {
                //force role to admin
                result.role = Models.UserRoles.Administrator;
                result.LoginStateEnum = ValidateLogin(loginstate);
            }

            if (result.LoginStateEnum == LoginStateEnum.Login_Successful)
            {


                //Audit entry
                audit Audit = new audit()
                {
                   create_date = DateTime.Now,operation = "LOGIN_SUCCESS",entity_type = "LOGIN",entity_id = 0, description = loginstate.Username
                };
                db.audits.Add(Audit);
                db.SaveChanges(); //save audit to DB

                if (result.role == Models.UserRoles.Agent || loginstate.IsFront)
                {
                    
                    return RedirectToAction("Index", "Front");
                }

                return RedirectToAction("Index", "Home");
            }
            else
            {
                //Audit entry
                audit Audit = new audit()
                {
               
                    create_date = DateTime.Now,
                    operation = AppEnums.Event.LOGIN_FAILED.ToString(),
                    entity_type = "LOGIN",
                    entity_id = 0,
                    description = loginstate.Username
                };
                db.audits.Add(Audit);
                db.SaveChanges(); //save audit to DB



                return RedirectToAction("Index", new { loginState = result.LoginStateEnum });

            }

        }

        [HttpPost]
        public ActionResult ResetPassword(LoginState loginstate)
        {
            IAdminService loginService = new AdminService();

            var user = loginService.GetUser(loginstate);

            if (user == null)
            {
                return RedirectToAction("Index", new { loginState = LoginStateEnum.Authentication_Failed });
            }

            loginstate.LoginStateEnum = LoginStateEnum.Password_Reset;
            loginService.ResetPassword(user.username);

            return RedirectToAction("Index", new { loginState = loginstate.LoginStateEnum });

        }

        [HttpPost]
        public ActionResult ChangePassword(LoginState loginstate)
        {
            IAdminService loginService = new AdminService();

            var user = loginService.GetUser(loginstate);

            if (user == null)
            {
                return RedirectToAction("Index", new { loginState = LoginStateEnum.Authentication_Failed });
            }

            loginstate.LoginStateEnum = LoginStateEnum.Password_Changed_Error;
            bool pwdChanged = loginService.ChangePassword(loginstate);
            if(pwdChanged)
            {
                loginstate.LoginStateEnum = LoginStateEnum.Password_Changed;
            }

            return RedirectToAction("Index", new { loginState = loginstate.LoginStateEnum });

        }


        public ActionResult ConfirmResetPassword(string id = "", string reference = "")
        {
            if (string.IsNullOrWhiteSpace(id) || string.IsNullOrWhiteSpace(reference))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            string userId = Utils.base64Decode(id);
            var users = db.users.Where(e => e.username.ToUpper() == userId.ToUpper() && e.temp_password == reference);
            if (users != null && users.Count() > 0)
            {
                var user = users.First();

                if (user.temp_password_expiry.HasValue && user.temp_password_expiry.Value >= DateTime.Now.Date)
                {
                    user.temp_password_expiry = null;
                    user.temp_password = null;

                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();

                    LoginState state = new LoginState();
                    state.Username = user.username;
                    state.OldPassword = user.password;
                    state.Email = user.email;
                    state.LoginStateEnum = LoginStateEnum.Password_Reset;

                    return View("Index", state);
                }
            }

            return RedirectToAction("Index");
        }


        private LoginStateEnum ValidateLogin(LoginState state)
        {

            LoginState result = new LoginState();
            var username = state.Username;
            var password = state.Password;
            if (string.IsNullOrEmpty(username) || string.IsNullOrWhiteSpace(password))
            {

                return LoginStateEnum.Username_Or_Password_Missing;
            }




            //var u = ConfigurationManager.AppSettings["username"];
            var p = ConfigurationManager.AppSettings["TestPassword"];

            //if (u.ToLower() == username.ToLower() && p == password)
            if (p == password)
            {
                UserSession.Current.IsValid = true;
                UserSession.Current.Username = username;
                UserSession.Current.UserRole = Models.UserRoles.Administrator;
                return LoginStateEnum.Use_of_password_mandatory;

            }
            else
            {
                return LoginStateEnum.Authentication_Failed;
            }

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}