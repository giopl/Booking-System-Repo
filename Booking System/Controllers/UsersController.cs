﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using System.Data.Entity.Validation;
using Booking_System.Helpers;
using Booking_System.Services;
using Booking_System.Services.Abstract;
using System.Data.Entity.Infrastructure;

namespace Booking_System.Controllers
{
    public class UsersController : AdminController
    {
        private ABSDBEntities db = new ABSDBEntities();
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: Users
        public ActionResult Index()
        {
            return RedirectToAction("ListUsers");
        }

        public ActionResult ListUsers()
        {
            return View(db.users.ToList());
        }


        // GET: Users/Details/5
        public ActionResult UserDetails(int? id, AppEnums.UserTab tab = AppEnums.UserTab.GALLERY)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            ViewBag.Tab = tab;

            return View(user);
        }

        // GET: Users/Create
        public ActionResult CreateUser()
        {

            var currencies = db.currencies.Where(x => x.is_active).ToList();
            ViewBag.currency_id = new SelectList(currencies, "currency_id", "currency_code");


            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateUser([Bind(Include = "Salutation,firstname,lastname,date_of_birth,gender,nationality,role,officephone,mobile,email,company,job_title,is_active,markup_percentage,markup_fixed,username,credit_limit,credit_remaining,currency_id")] user user)
        {
            try
            {
                if (user != null )
                {
                    if (!String.IsNullOrWhiteSpace(user.username) && !String.IsNullOrEmpty(user.email))
                    {
                        var users = db.users.Where(e => e.username.ToUpper() == user.username.ToUpper() || e.email.ToUpper() == user.email.ToUpper());
                        if (users != null && users.Count() > 0)
                        {
                            var usr = users.First();
                            if (!string.IsNullOrEmpty(usr.email))
                            { 
                                if(user.email.ToUpper() == usr.email.ToUpper())
                                {
                                    ModelState.AddModelError("email", String.Format("This email {0} is already linked to another account.", user.email));
                                }
                            }

                            if (!string.IsNullOrEmpty(usr.username))
                            { 
                                if (user.username.ToUpper() == usr.username.ToUpper())
                                {
                                    ModelState.AddModelError("username", String.Format("The Username {0} is already in use.", user.username));
                                }
                            }
                        }
                    }
                    else
                    {
                        if(string.IsNullOrEmpty(user.username))
                        {
                            ModelState.AddModelError("username", "Username is required");
                        }

                        if (string.IsNullOrEmpty(user.email))
                        {
                            ModelState.AddModelError("email", "Email is required");
                        }
                    }
                }

                if (ModelState.IsValid)
                {
                    //var newPassword = Guid.NewGuid().ToString().Split('-')[1];
                    //user.password = Helpers.Utils.base64Encode(newPassword);
                    user.is_credit_based = user.credit_limit.HasValue && user.credit_limit.Value > 0;

                    db.users.Add(user);
                    db.SaveChanges();

                    AdminService service = new AdminService();
                    service.ResetPassword(user.username);


                    return RedirectToAction("ListUsers", "Users");
                }
                var currencies = db.currencies.Where(x => x.is_active).ToList();
                ViewBag.currency_id = new SelectList(currencies, "currency_id", "currency_code");

                return View(user);
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }


            catch (Exception e)
            {
                log.ErrorFormat("controller error {0} ", e.ToString());

                throw;
            }
        }

        // GET: Users/Edit/5
        public ActionResult EditUser(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            var currencies = db.currencies.Where(x => x.is_active).ToList();
            ViewBag.currency_id = new SelectList(currencies, "currency_id", "currency_code", user.currency_id);
            return View(user);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser([Bind(Include = "user_id,Salutation,firstname,lastname,date_of_birth,gender,nationality,officephone,mobile,email,company,job_title,is_active,markup_percentage,markup_fixed,credit_limit,credit_remaining,currency_id")] user user)
        {
            try
            {

                if(user != null)
                {
                    if(String.IsNullOrWhiteSpace(user.email))
                    {
                        ModelState.AddModelError("email", "Email is required.");
                    }
                    else
                    {
                        var count = db.users.Count(e => e.email == user.email && e.user_id != user.user_id);
                        if(count > 0)
                        {
                            ModelState.AddModelError("email", "This email is already in use by another account.");
                        }
                    }
                }

                if (ModelState.IsValid)
                {
                    db.Entry(user).State = EntityState.Modified;
                    // skip password
                    db.Entry(user).Property(x => x.password).IsModified = false;
                    db.Entry(user).Property(x => x.username).IsModified = false;
                    db.Entry(user).Property(x => x.role).IsModified = false;

                    user.is_credit_based = user.credit_limit.HasValue && user.credit_limit.Value > 0;

                    db.SaveChanges();
                    return RedirectToAction("ListUsers", "Users");
                }
                var currencies = db.currencies.Where(x => x.is_active).ToList();
                ViewBag.currency_id = new SelectList(currencies, "currency_id", "currency_code", user.currency_id);
                return View(user);
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }


            catch (Exception e)
            {
                log.ErrorFormat("controller error {0} ", e.ToString());

                throw;
            }
        }

        
        [HttpPost]
        public ActionResult EditUserRole(int? id, int role)
        {
            try
            {

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                user user = db.users.Find(id);

                if (user == null)
                {
                    return HttpNotFound();
                }

                user.role = role;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("UserDetails", "Users", new { id = id, tab = AppEnums.UserTab.ROLE });
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }


            catch (Exception e)
            {
                log.ErrorFormat("controller error {0} ", e.ToString());

                throw;
            }
        }

        public ActionResult ResetUserPassword(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            user user = db.users.Find(id);

            if (user == null)
            {
                return HttpNotFound();
            }

            IAdminService loginService = new AdminService();

            TempData["PasswordResetSuccess"] = loginService.ResetPassword(user.username);

            return RedirectToAction("UserDetails","Users", new { id = id, tab = AppEnums.UserTab.PASSWORD});
        }


        // GET: Users/Delete/5
        public ActionResult DeleteUser(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            user user = db.users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("DeleteUser")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteUserConfirmed(int id)
        {
            user user = db.users.Find(id);
            try
            {
                db.Entry(user).State = EntityState.Deleted;
                db.SaveChanges();


            }
            catch (DbUpdateException ex)
            {
                db.Entry(user).Reload();
                user.is_active = false;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();

            }
            
            return RedirectToAction("ListUsers","Users");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
