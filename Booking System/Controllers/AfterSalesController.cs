﻿using Booking_System.Helpers;
using Booking_System.Models;
using Booking_System.Services;
using Booking_System.Services.Abstract;
using EntityFramework.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Booking_System.Controllers
{
    [CustomAuthorize(UserRoles.Administrator, UserRoles.SuperAdministrator, UserRoles.Sales)]
    public class AfterSalesController : BaseController
    {
        private ABSDBEntities db = new ABSDBEntities();
        log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // GET: AfterSales
        public ActionResult ViewAfterSales()
        {
            List<booking> allAfterSalesBookings = new List<booking>();

            //All completed bookings only
            var allBookings = db.bookings.Where(e=> e.status == 6);
            if (allBookings != null && allBookings.Count() > 0)
            {
                foreach (var bk in allBookings)
                {
                    if(IsValidBookingForAfterSales(bk))
                    {
                        allAfterSalesBookings.Add(bk);
                    }
                }
            }

            return View(allAfterSalesBookings);
        }

        // GET: Reservation/Details/5
        public ActionResult BookingDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);
            refurbishbooking(booking);

            if (booking == null || !IsValidBookingForAfterSales(booking))
            {
                return HttpNotFound();
            }
            return View(booking);
        }


        public ActionResult AddActivities(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);
            if (booking == null || !IsValidBookingForAfterSales(booking))
            {
                return HttpNotFound();
            }

            refurbishbooking(booking);

            var vBooking = db.viewBookings.Where(x => x.booking_ref == booking.booking_ref).FirstOrDefault();
            if (vBooking != null)
            {
                ViewBag.VBooking = vBooking;
            }

            ViewBag.BookingRef = booking.booking_ref;
            ViewBag.Booking = booking;

            IReservationService res = new ReservationService();
            List<viewActivity> vActivity = res.GetReservationActivities(id.Value, booking);

            return View(vActivity);

        }

        [HttpPost]
        public ActionResult AddActivities(int booking_id, int[] activity_id, int[] transfer_type, int[] child, int[] adult, int[] activity_pricing_id, double[] priceAdult, double[] priceChild)
        {
            try
            {

                IReservationService res = new ReservationService();
                res.SaveReservationActivities(booking_id, activity_id, transfer_type, child, adult, activity_pricing_id, true, priceAdult, priceChild);


                return RedirectToAction("BookingDetails", new { id = booking_id });

            }
            catch (Exception e)
            {

                throw;
            }
        }

        public ActionResult AddRental(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);

            if (booking == null || !IsValidBookingForAfterSales(booking))
            {
                return HttpNotFound();
            }

            refurbishbooking(booking);

            var vBooking = db.viewBookings.Where(x => x.booking_ref == booking.booking_ref).FirstOrDefault();
            if (vBooking != null)
            {
                ViewBag.VBooking = vBooking;
            }

            IReservationService res = new ReservationService();
            IList<vehicle> vehiclesToDisplay = res.GetReservationRentals(id.Value, booking);

            ViewBag.Booking = booking;

            return View(vehiclesToDisplay);
        }

        [HttpPost]
        public ActionResult AddRental(int booking_id, double[] price, int[] days, int[] vehicle_id, bool[] hasDriver)
        {
            try
            {
                IReservationService res = new ReservationService();
                var pricewithdriver = price;

                res.SaveReservationRentals(booking_id, price, pricewithdriver, days, vehicle_id, true, hasDriver);

                return RedirectToAction("BookingDetails", new { id = booking_id });

            }
            catch (Exception e)
            {

                throw;
            }

        }

        public ActionResult AddTransfer(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var booking = db.bookings.Find(id);

            if (booking == null || !IsValidBookingForAfterSales(booking))
            {
                return HttpNotFound();
            }

            refurbishbooking(booking);

            IReservationService res = new ReservationService();
            IList<transferPricing> transfers = res.GetReservationTransfers(id.Value, booking);

            int currency_id = 0;
            market m = db.markets.Find(booking.market_id);
            if (m != null)
            {
                currency_id = m.currency_id;
            }


            var pricings = db.transferPricings.Where(x => x.currency_id == currency_id).ToList();

            ViewBag.TransferPricings = pricings;

            ViewBag.Booking = booking;

            return View(transfers);
        }

        //[HttpPost]
        //public ActionResult AddTransfer(int booking_id, int? transfer, int? coach, double? totalPriceTransfer, double? totalPriceCoach)
        //{
        //    try
        //    {

        //        IReservationService res = new ReservationService();
        //        res.SaveReservationTransfers(booking_id, transfer, totalPriceTransfer, totalPriceCoach, true);

        //        return RedirectToAction("BookingDetails", new { id = booking_id });

        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        [HttpPost]
        public ActionResult AddTransfer(int booking_id, int? transfer, double? totalPriceTransfer)
        {
            try
            {

                IReservationService res = new ReservationService();
                res.SaveReservationTransfers(booking_id, transfer, totalPriceTransfer, null, true);

                return RedirectToAction("BookingDetails", new { id = booking_id });

            }
            catch (Exception)
            {

                throw;
            }
        }


        private bool IsValidBookingForAfterSales(booking bk)
        {
            bool result = false;
            var today = DateTime.Now.Date;
            if (bk != null)
            {
                if (bk.checkin_date.HasValue)
                {
                    if (bk.checkin_date.Value.Date <= today)
                    {
                        if(bk.checkout_date.HasValue)
                        {
                            if(bk.checkout_date.Value.Date < today)
                            {
                                return false;
                            }
                        }
                        return true;
                    }
                }
            }

            return result;
        }
    }
}