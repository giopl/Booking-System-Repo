﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Booking_System.Models;
using System.Data.Entity.Infrastructure;
using Booking_System.Helpers;
using System.Web.Script.Serialization;
using System.IO;
using System.Text;

namespace Booking_System.Controllers
{
    public class SystemSettingsController : AdminController
    {
        private ABSDBEntities db = new ABSDBEntities();

        public ActionResult Icons()
        {
            
            return View();
        }

        #region "Hotel & Room Features"
        public ActionResult Features(string id = "H")
        {
            ViewBag.FeatureType = id;
            return View(db.features.Where(x => x.feature_type == id).ToList());
        }


        // GET: Features/Create
        public ActionResult CreateFeature(string id)
        {
            ViewBag.FeatureType = id;
            return View();
        }

        // POST: Features/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateFeature([Bind(Include = "feature_id,category,description,feature_type,name,icon")] feature feature)
        {
            if (ModelState.IsValid)
            {

                feature.is_active = true;
                db.features.Add(feature);
                db.SaveChanges();
                SaveAudit(AppEnums.Event.CREATE, "Feature", feature.feature_id);


                return RedirectToAction("Features", new { id = feature.feature_type });
            }

            return View(feature);
        }

        // GET: Features/Edit/5
        public ActionResult EditFeature(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            feature feature = db.features.Find(id);
            if (feature == null)
            {
                return HttpNotFound();
            }
          
            return View(feature);
        }

        // POST: Features/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditFeature([Bind(Include = "feature_id,category,description,feature_type,name,icon")] feature feature)
        {
            if (ModelState.IsValid)
            {
                db.Entry(feature).State = EntityState.Modified;
                db.SaveChanges();
                SaveAudit(AppEnums.Event.UPDATE, "Feature", feature.feature_id);
                return RedirectToAction("Features", new { id = feature.feature_type });
            }
            return View(feature);
        }

        // GET: Features/Delete/5
        public ActionResult DeleteFeature(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            feature feature = db.features.Find(id);
            if (feature == null)
            {
                return HttpNotFound();
            }
            return View(feature);
        }

        // POST: Features/Delete/5
        [HttpPost, ActionName("DeleteFeature")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                feature feature = db.features.Find(id);
                var type = feature.feature_type;
                db.features.Remove(feature);
                db.SaveChanges();
                SaveAudit(AppEnums.Event.DELETE, "Feature", feature.feature_id);

                return RedirectToAction("Features", new { id = type });
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        #endregion


        #region currency


        public ActionResult Currencies(bool ShowAll=false)
        {

            var currencies = db.currencies.ToList();
            if (!ShowAll)
                currencies = currencies.Where(x => x.is_active).ToList();

            ViewBag.ShowAll = ShowAll;
            return View(currencies);
        }

        public ActionResult CreateCurrency()
        {
                     
            return View();
        }


        [HttpPost]
        public ActionResult CreateCurrency([Bind(Include = "currency_code,currency_desc,symbol,exchange_rate")] currency currency)
        {

            try
            {

                var obj = db.currencies.Where(x => x.currency_code == currency.currency_code).Count();
                if (obj >0)
                {
                    ModelState.AddModelError("currency_code", "The currency code already exist");
                    return View(currency);
                }

                if (ModelState.IsValid)
                {
                    currency.is_active = true;  
                        db.currencies.Add(currency);
                        db.SaveChanges();
                            SaveAudit(AppEnums.Event.CREATE, "Currency", currency.currency_id);
                        return RedirectToAction("Currencies");
                    
                }
                return View(currency);
            }
            catch (Exception)
            {

                throw;
                
            }
           
        }


        public ActionResult EditCurrency(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                currency currency = db.currencies.Find(id);
                if (currency == null)
                {
                    return HttpNotFound();
                }


                return View(currency);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpGet]
        public ActionResult DisableCurrency(string id, string value)
        {
            try
            {
                var currency = db.currencies.Find(Int32.Parse(id));

                currency.is_active = (value == "1") ? true: false;
                db.Entry(currency).State = EntityState.Modified;
                db.SaveChanges();

                SaveAudit(AppEnums.Event.UPDATE, "Currency", currency.currency_id);
                return Json("ok", JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
               
                return Json(ex.InnerException, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult EditCurrency([Bind(Include = "currency_id,currency_code,currency_desc,symbol,exchange_rate,is_active")] currency currency)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(currency).State = EntityState.Modified;
                    db.SaveChanges();
                    SaveAudit(AppEnums.Event.UPDATE, "Currency", 0);
                    return RedirectToAction("Currencies");
                }
                return View(currency);
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        [HttpPost, ActionName("DeleteCurrency")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCurrency([Bind(Include = "currency_id,currency_code,currency_desc,symbol")] currency currency)
        {
            try
            {
                var curr = db.currencies.Find(currency.currency_id);
                db.currencies.Remove(curr);
                db.SaveChanges();
                SaveAudit(AppEnums.Event.DELETE, "Currency", 0);
                return RedirectToAction("Currencies");
            }
            catch (Exception)
            {

                throw;
            }
        }

        // POST: currencies/Delete/5

        public ActionResult DeleteCurrency(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            currency currency = db.currencies.Find(id);
            if (currency == null)
            {
                return HttpNotFound();
            }

            return View(currency);
        }
        #endregion

        #region occupancies

        public ActionResult Occupancies()
        {
            return View(db.occupancies.ToList());
        }

        // GET: Occupancies/Create
        public ActionResult CreateOccupancy()
        {
            return View();
        }

        // POST: Occupancies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateOccupancy([Bind(Include = "occupancy_id,name,description,infant,child,teen,adult,senior,without_bed")] occupancy occupancy)
        {

            bool HasValidationErrors = false;
            List<String> errors = new List<String>();
            if (occupancy.occupancyCode == 0)
            {
                HasValidationErrors = true;
                errors.Add("At least on occupancy needs to be more than 0");
            }

            var existingOccupancies = db.occupancies.ToList();

            if (existingOccupancies.Contains(occupancy))
            {
                HasValidationErrors = true;
                errors.Add("This combination exists already and cannot be created again.");
            }

            if (ModelState.IsValid && !HasValidationErrors)
            {

                occupancy.infant = occupancy.infant.HasValue ? occupancy.infant : 0;
                occupancy.child = occupancy.child.HasValue ? occupancy.child : 0;
                occupancy.without_bed = occupancy.without_bed.HasValue ? occupancy.without_bed : 0;
                occupancy.teen = occupancy.teen.HasValue ? occupancy.teen : 0;
                occupancy.adult = occupancy.adult.HasValue ? occupancy.adult : 0;
                occupancy.senior = occupancy.senior.HasValue ? occupancy.senior : 0;

             
                db.occupancies.Add(occupancy);
                db.SaveChanges();
                SaveAudit(AppEnums.Event.CREATE, "Occupancy", occupancy.occupancy_id);

                return RedirectToAction("Occupancies");
            }

            ViewBag.HasValidationErrors = HasValidationErrors;
            ViewBag.ValidationErrors = errors;
            return View(occupancy);
        }

        // GET: Occupancies/Edit/5
        public ActionResult EditOccupancy(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            occupancy occupancy = db.occupancies.Find(id);
            if (occupancy == null)
            {
                return HttpNotFound();
            }


            return View(occupancy);
        }

        // POST: Occupancies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditOccupancy([Bind(Include = "occupancy_id,name,description,infant,child,teen,adult,senior,without_bed")] occupancy occupancy)
        {
            try
            {
                bool HasValidationErrors = false;


                occupancy.infant = occupancy.infant.HasValue ? occupancy.infant : 0;
                occupancy.child = occupancy.child.HasValue ? occupancy.child : 0;
                occupancy.without_bed = occupancy.without_bed.HasValue ? occupancy.without_bed : 0;
                occupancy.teen = occupancy.teen.HasValue ? occupancy.teen : 0;
                occupancy.adult = occupancy.adult.HasValue ? occupancy.adult : 0;
                occupancy.senior = occupancy.senior.HasValue ? occupancy.senior : 0;


                List<String> errors = new List<String>();
                if (occupancy.occupancyCode == 0)
                {
                    HasValidationErrors = true;
                    errors.Add("At least on occupancy needs to be more than 0");
                }

                var existingOccupancies = db.occupancies.ToList();
                var thisOccupancyFromDB = existingOccupancies.Where(x => x.occupancy_id == occupancy.occupancy_id).FirstOrDefault();
                existingOccupancies.Remove(thisOccupancyFromDB);

                if (existingOccupancies.Contains(occupancy))
                {
                    HasValidationErrors = true;
                    errors.Add("This combination exists already and cannot be created again.");
                }

                if (ModelState.IsValid && !HasValidationErrors)

                {
                    var occupancyfromDB = db.occupancies.Find(occupancy.occupancy_id);

                    occupancyfromDB.name = occupancy.name;
                    occupancyfromDB.description = occupancy.description;
                    occupancyfromDB.infant = occupancy.infant;
                    occupancyfromDB.child = occupancy.child;
                    occupancyfromDB.without_bed = occupancy.without_bed;
                    occupancyfromDB.teen = occupancy.teen;
                    occupancyfromDB.adult = occupancy.adult;

                    occupancyfromDB.senior = occupancy.senior;


                    db.Entry(occupancyfromDB).State = EntityState.Modified;
                    db.SaveChanges();
                    SaveAudit(AppEnums.Event.UPDATE, "Occupancy", occupancyfromDB.occupancy_id);

                    return RedirectToAction("Occupancies");
                }
                ViewBag.HasValidationErrors = HasValidationErrors;
                ViewBag.ValidationErrors = errors;

                return View(occupancy);
            }
            catch (Exception ex)
            {

                var errorMessages = ex.InnerException;
                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                var exceptionMessage = string.Concat(ex.ToString(), " ", ex.Message, " The validation errors are: ", fullErrorMessage);
                TempData["Exception"] = exceptionMessage;

                return RedirectToAction("GeneralError", "Error");
            }
        }

        // GET: Occupancies/Delete/5
        public ActionResult DeleteOccupancy(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            occupancy occupancy = db.occupancies.Find(id);
            if (occupancy == null)
            {
                return HttpNotFound();
            }
            return View(occupancy);
        }

        // POST: Occupancies/Delete/5
        [HttpPost, ActionName("DeleteOccupancy")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmedOccupancy(int id)
        {

            try
            {
                occupancy occupancy = db.occupancies.Find(id);
                db.occupancies.Remove(occupancy);
                db.SaveChanges();
                SaveAudit(AppEnums.Event.DELETE, "Occupancy", occupancy.occupancy_id);
                return RedirectToAction("Occupancies");
            }
            catch (Exception)
            {

                throw;
            }
        
        }

        #endregion



        #region Meal Plans

        // GET: mealPlans
        public ActionResult MealPlans()
        {
            return View(db.mealPlans.ToList());
        }

        // GET: mealPlans/Details/5
        public ActionResult MealPlanDetails(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mealPlan mealPlan = db.mealPlans.Find(id);
            if (mealPlan == null)
            {
                return HttpNotFound();
            }
            return View(mealPlan);
        }

        // GET: mealPlans/Create
        public ActionResult CreateMealPlan()
        {
            return View();
        }

        // POST: mealPlans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateMealPlan([Bind(Include = "meal_plan_id,name,description,meal_order,code")] mealPlan mealPlan)
        {

            try
            {
                if (ModelState.IsValid)
                {

                    mealPlan.is_active = true;
                    db.mealPlans.Add(mealPlan);
                    db.SaveChanges();
                    SaveAudit(AppEnums.Event.CREATE, "Meal Plan", mealPlan.meal_plan_id);

                    return RedirectToAction("MealPlans");
                }

                return View(mealPlan);
            }
            catch (Exception)
            {

                throw;
            }
          
        }

        // GET: mealPlans/Edit/5
        public ActionResult EditMealPlan(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            mealPlan mealPlan = db.mealPlans.Find(id);
            if (mealPlan == null)
            {
                return HttpNotFound();
            }
            return View(mealPlan);
        }

        // POST: mealPlans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditMealPlan([Bind(Include = "meal_plan_id,name,description,meal_order,code")] mealPlan mealPlan)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(mealPlan).State = EntityState.Modified;
                    db.SaveChanges();
                    SaveAudit(AppEnums.Event.UPDATE, "Meal Plan", mealPlan.meal_plan_id);
                    return RedirectToAction("MealPlans");
                }
                return View(mealPlan);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // GET: mealPlans/Delete/5
        public ActionResult DeleteMealPlan(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                mealPlan mealPlan = db.mealPlans.Find(id);
                if (mealPlan == null)
                {
                    return HttpNotFound();
                }
                return View(mealPlan);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // POST: mealPlans/Delete/5
        [HttpPost, ActionName("DeleteMealPlan")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteMealPlanConfirmed(int id)
        {
            try
            {
                mealPlan mealPlan = db.mealPlans.Find(id);

                try
                {
                    db.Entry(mealPlan).State = EntityState.Deleted;
                    db.SaveChanges();


                }
                catch (DbUpdateException ex)
                {
                    db.Entry(mealPlan).Reload();
                    mealPlan.is_active = false;
                    db.Entry(mealPlan).State = EntityState.Modified;
                    db.SaveChanges();

                }
                SaveAudit(AppEnums.Event.DELETE, "Meal Plan", mealPlan.meal_plan_id);
                return RedirectToAction("MealPlans");
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        #endregion


        #region Country



        // GET: country
        public ActionResult Countries()
        {
            return View(db.countries.ToList());
        }

        // GET: country/Details/5
        public ActionResult CountryDetails(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            country country = db.countries.Find(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        // GET: country/Create
        public ActionResult CreateCountry()
        {
            return View();
        }

        // POST: country/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCountry([Bind(Include = "country_id,name,country_code,currency,currency_name,is_independent,language,nationality")] country country)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    
                    db.countries.Add(country);
                    db.SaveChanges();

                    SaveAudit(AppEnums.Event.CREATE, "Country", 0);

                    return RedirectToAction("Countries");
                }

                return View(country);
            }
            catch (Exception)
            {

                throw;
            }
            
        }

        // GET: country/Edit/5
        public ActionResult EditCountry(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                country country = db.countries.Find(id);
                if (country == null)
                {
                    return HttpNotFound();
                }
                return View(country);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // POST: country/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCountry([Bind(Include = "country_id,country_code,name,currency,currency_name,is_independent,language,nationality")] country country)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Entry(country).State = EntityState.Modified;
                    db.SaveChanges();

                    SaveAudit(AppEnums.Event.UPDATE, "Country", 0);

                    return RedirectToAction("Countries");
                }
                return View(country);
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // GET: country/Delete/5
        public ActionResult DeleteCountry(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            country country = db.countries.Find(id);
            if (country == null)
            {
                return HttpNotFound();
            }
            return View(country);
        }

        // POST: country/Delete/5
        [HttpPost, ActionName("DeleteCountry")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteCountryConfirmed(int id)
        {
            try
            {
                country country = db.countries.Find(id);
                db.countries.Remove(country);
                db.SaveChanges();
                SaveAudit(AppEnums.Event.DELETE, "Country", 0);

                return RedirectToAction("Countries");
            }
            catch (Exception)
            {

                throw;
            }
           
        }



        #endregion

        #region  transfers
        public ActionResult ListTransfers()
        {
            return View(db.transfers.ToList());
        }

        // GET: transfers1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transfer transfer = db.transfers.Find(id);
            if (transfer == null)
            {
                return HttpNotFound();
            }
            return View(transfer);
        }

        // GET: transfers1/Create
        public ActionResult CreateTransfer()
        {
            return View();
        }

        // POST: transfers1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateTransfer([Bind(Include = "transfer_id,name,description,quantity")] transfer transfer)
        {
            if (ModelState.IsValid)
            {
                db.transfers.Add(transfer);
                db.SaveChanges();
                return RedirectToAction("ListTransfers");
            }

            return View(transfer);
        }

        // GET: transfers1/Edit/5
        public ActionResult EditTransfer(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transfer transfer = db.transfers.Find(id);
            if (transfer == null)
            {
                return HttpNotFound();
            }
            return View(transfer);
        }

        // POST: transfers1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditTransfer([Bind(Include = "transfer_id,name,description,quantity")] transfer transfer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transfer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ListTransfers");
            }
            return View(transfer);
        }

        // GET: transfers1/Delete/5
        public ActionResult DeleteTransfer(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            transfer transfer = db.transfers.Find(id);
            if (transfer == null)
            {
                return HttpNotFound();
            }
            return View(transfer);
        }

        // POST: transfers1/Delete/5
        [HttpPost, ActionName("DeleteTransfer")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteTransferConfirmed(int id)
        {
            transfer transfer = db.transfers.Find(id);
            db.transfers.Remove(transfer);
            db.SaveChanges();
            return RedirectToAction("ListTransfers");
        }


        #endregion

        #region Configuration

        // GET: configurations
        public ActionResult Configurations()
        {
            return View(db.configurations.ToList());
        }

        // GET: configurations/Create
        public ActionResult CreateConfiguration()
        {
            try
            {
                ViewBag.conf_type = new SelectList(db.configurations.Where(x => x.conf_type == "CONF_TYPE"), "conf_key", "conf_value");
                return View();
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        // POST: configurations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateConfiguration([Bind(Include = "configuration_id,conf_key,conf_type,conf_value")] configuration configuration)
        {
            if (ModelState.IsValid)
            {
                db.configurations.Add(configuration);
                db.SaveChanges();
                return RedirectToAction("Configurations");
            }
            ViewBag.conf_type = new SelectList(db.configurations.Where(x => x.conf_type == "CONF_TYPE"), "conf_key", "conf_value", configuration.conf_type);
            return View(configuration);
        }

        // GET: configurations/Edit/5
        public ActionResult EditConfiguration(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            configuration configuration = db.configurations.Find(id);
            if (configuration == null)
            {
                return HttpNotFound();
            }
            ViewBag.conf_type = new SelectList(db.configurations.Where(x => x.conf_type == "CONF_TYPE"), "conf_key", "conf_value",configuration.conf_type);
            return View(configuration);
        }

        // POST: configurations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditConfiguration([Bind(Include = "configuration_id,conf_key,conf_type,conf_value")] configuration configuration)
        {
            if (ModelState.IsValid)
            {
                db.Entry(configuration).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Configurations");
            }
            ViewBag.conf_type = new SelectList(db.configurations.Where(x => x.conf_type == "CONF_TYPE"), "conf_key", "conf_value", configuration.conf_type);
            return View(configuration);
        }

        // GET: configurations/Delete/5
        public ActionResult DeleteConfiguration(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            configuration configuration = db.configurations.Find(id);
            if (configuration == null)
            {
                return HttpNotFound();
            }
            return View(configuration);
        }

        // POST: configurations/Delete/5
        [HttpPost, ActionName("DeleteConfiguration")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfigurationConfirmed(int id)
        {
            configuration configuration = db.configurations.Find(id);
            db.configurations.Remove(configuration);
            db.SaveChanges();
            return RedirectToAction("Configurations");
        }



        #endregion



        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
