﻿using System.Web;
using System.Web.Optimization;

namespace Booking_System
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/scrollnav").Include(
                        "~/Scripts/plugins/jquery.scrolling-tabs.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                        "~/Scripts/jquery.validate.unobtrusive"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/app.js",
                      "~/Scripts/respond.js"));

            //http://digitalbush.com/projects/masked-input-plugin/
            bundles.Add(new ScriptBundle("~/bundles/maskedinput").Include(
                    "~/Scripts/front/jquery.maskedinput.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/front").Include(
                    "~/Scripts/bootstrap.js",        
                    "~/Scripts/front/slimmenu.js",
                    "~/Scripts/front/bootstrap-datepicker.js" ,
        "~/Scripts/front/bootstrap-timepicker.js" ,
        "~/Scripts/front/nicescroll.js" ,
        "~/Scripts/front/dropit.js" ,
        "~/Scripts/front/ionrangeslider.js" ,
        "~/Scripts/front/icheck.js" ,
        "~/Scripts/front/fotorama.js" ,
        "~/Scripts/front/typeahead.js" ,
        "~/Scripts/front/card-payment.js" ,
        "~/Scripts/front/magnific.js" ,
        "~/Scripts/front/owl-carousel.js" ,
        "~/Scripts/front/fitvids.js" ,
        "~/Scripts/front/tweet.js" ,
        "~/Scripts/front/countdown.js" ,
        "~/Scripts/front/gridrotator.js" ,
        "~/Scripts/plugins/nice_select.js",
                      "~/Scripts/plugins/readmore.js"                    

                       ));

            bundles.Add(new ScriptBundle("~/bundles/datepicker").Include(
       // "~/Scripts/front/bootstrap-datepicker.js",
"~/Scripts/front/bootstrap-timepicker.js"
           ));



            bundles.Add(new ScriptBundle("~/bundles/front-custom").Include(
                             "~/Scripts/front/custom.js"
                   ));

            bundles.Add(new ScriptBundle("~/bundles/front-home").Include(
                            "~/Scripts/front/custom-home.js"
                  ));

            bundles.Add(new ScriptBundle("~/bundles/front-result").Include(
                            "~/Scripts/front/custom-hotelresults.js"
                  ));


            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                      "~/Scripts/plugins/jquery.autocomplete.js",
                      "~/Scripts/plugins/select2.js",
                      "~/Scripts/plugins/NicEdit.js",
                      "~/Scripts/plugins/readmore.js",                    
 
                    "~/Scripts/bootstrap-datepicker.js"
                      ));


            bundles.Add(new ScriptBundle("~/bundles/listjs").Include(
                     "~/Scripts/front/list.js",
                     "~/Scripts/front/list.pagination.js"
                 
                    ));

            bundles.Add(new ScriptBundle("~/bundles/table2excel").Include(
                     "~/Scripts/plugins/table2excel.js"
               
                     ));

            bundles.Add(new ScriptBundle("~/bundles/shared").Include(
                                    "~/Scripts/custom/shared.js"
               ));
            bundles.Add(new ScriptBundle("~/bundles/market-custom").Include(
                            "~/Scripts/custom/market-custom.js"
                  ));

            bundles.Add(new ScriptBundle("~/bundles/supplement-custom").Include(
                      "~/Scripts/custom/supplement-custom.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/promotion-custom").Include(
                      "~/Scripts/custom/promotion-custom.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/package-custom").Include(
                     "~/Scripts/custom/package-custom.js"
           ));

            bundles.Add(new ScriptBundle("~/bundles/custom").Include(
                    "~/Scripts/custom/autocomplete-lists.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/phonevalidation").Include(
                                    "~/Scripts/plugins/intlTelInput.js",
                      //"~/Scripts/plugins/utils.js",
                    "~/Scripts/custom/phonevalidation-custom.js"
               ));

            bundles.Add(new ScriptBundle("~/bundles/datatable").Include(
                      "~/Scripts/plugins/datatables.js"
                 ));



            bundles.Add(new StyleBundle("~/Content/css").Include
                (
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.css",
                      "~/Content/ionicons.css",
                      "~/Content/front/icomoon.css",
                      "~/Content/AdminLTE.css",
                      "~/Content/skins/skin-yellow.css",
                      "~/Content/plugins/jquery.autocomplete.css",
                      "~/Content/plugins/select2.css",
                      "~/Content/bootstrap-datepicker.css",
                      "~/Content/plugins/intlTelInput.css",
                      "~/Content/plugins/datatables.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/front-css").Include
            (
                  "~/Content/bootstrap.css",
                  "~/Content/font-awesome.css",
                  "~/Content/front/icomoon.css",
                  "~/Content/front/styles.css",
                  "~/Content/plugins/nice_select.css",
                  "~/Content/front/mystyles.css",
                  "~/Content/front/StepProgressIndicator.css",
                  "~/Content/site.css",
                   "~/Content/front/override.css"

              ));


            bundles.Add(new StyleBundle("~/Content/icon-css").Include
            (
                  "~/Content/bootstrap.css",
                  "~/Content/font-awesome.css",
                  "~/Content/ionicons.css",
                  "~/Content/front/icomoon.css"

              ));
            bundles.Add(new StyleBundle("~/Content/scrollnav-css").Include
          (
                "~/Content/plugins/jquery.scrolling-tabs.css"

            ));

        }
    }
}
