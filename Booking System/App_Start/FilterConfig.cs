﻿using Booking_System.Filters;
using System.Web;
using System.Web.Mvc;

namespace Booking_System
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ExtendedExceptionFilter()); //use logger to log all errors in application
        }
    }
}
