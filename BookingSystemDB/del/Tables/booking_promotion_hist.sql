﻿CREATE TABLE [del].[booking_promotion_hist] (
    [booking_promotion_id]  INT        NOT NULL,
    [booking_id]            INT            NOT NULL,
    [booking_room_id]       INT            NULL,
    [promotion_id]          INT            NULL,
    [promotion_name]        NVARCHAR (100) NULL,
    [promotion_description] NVARCHAR (MAX) NULL,
    [free_nights]           INT            NULL,
    [discount]              FLOAT (53)     NULL,
    [load_timestamp]        DATETIME       DEFAULT (getdate()) NULL
);

