﻿CREATE TABLE [del].[vehicle_hist] (
    [vehicle_id]          INT     NOT NULL,
    [provider_id]         INT            NOT NULL,
    [service_name]        NVARCHAR (200) NULL,
    [vehicle_model]       NVARCHAR (100) NULL,
    [vehicle_type]        NVARCHAR (50)  NULL,
    [service_description] NVARCHAR (MAX) NULL,
    [max_seat]            INT            NULL,
    [is_active]           BIT            NOT NULL,
    [is_transfer]         BIT            NOT NULL,
    [includes_driver]     BIT            NOT NULL,
    [engine_size]         SMALLINT       NULL,
    [year]                SMALLINT       NULL,
    [is_automatic]        BIT            NOT NULL,
    [doors]               TINYINT        NULL,
    [fuel_type]           TINYINT        NULL,
    [load_timestamp]      DATETIME       DEFAULT (getdate()) NULL
);



