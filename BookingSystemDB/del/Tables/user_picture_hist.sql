﻿CREATE TABLE [del].[user_picture_hist] (
    [picture_id]     INT          NOT NULL,
    [user_id]        INT          NOT NULL,
    [display_order]  INT          NULL,
    [section]        VARCHAR (15) NULL,
    [load_timestamp] DATETIME     DEFAULT (getdate()) NULL
);

