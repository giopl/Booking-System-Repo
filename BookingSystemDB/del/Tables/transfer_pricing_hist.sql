﻿CREATE TABLE [del].[transfer_pricing_hist] (
    [transfer_pricing_id] INT      NOT NULL,
    [transfer_id]         INT        NOT NULL,
    [currency_id]         INT        NOT NULL,
    [child_price]         FLOAT (53) NULL,
    [teen_price]          FLOAT (53) NULL,
    [adult_price]         FLOAT (53) NULL,
    [is_active]           BIT        NOT NULL,
    [provider_id]         INT        NULL,
    [is_selling_price]    BIT        NOT NULL,
    [load_timestamp]      DATETIME   DEFAULT (getdate()) NULL
);



