﻿CREATE TABLE [del].[policy_hist] (
    [policy_id]      INT      NOT NULL,
    [name]           NVARCHAR (50)  NULL,
    [description]    NVARCHAR (300) NULL,
    [type]           NVARCHAR (100) NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



