﻿CREATE TABLE [del].[vehicle_pricing_hist] (
    [vehicle_pricing_id]         INT   NOT NULL,
    [vehicle_id]                 INT        NOT NULL,
    [currency_id]                INT        NOT NULL,
    [half_day_price_with_driver] FLOAT (53) NULL,
    [half_day_price]             FLOAT (53) NULL,
    [daily_price_with_driver]    FLOAT (53) NULL,
    [daily_price]                FLOAT (53) NULL,
    [is_active]                  BIT        NOT NULL,
    [load_timestamp]             DATETIME   DEFAULT (getdate()) NULL
);



