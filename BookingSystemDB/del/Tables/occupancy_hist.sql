﻿CREATE TABLE [del].[occupancy_hist] (
    [occupancy_id]   INT        NOT NULL,
    [name]           NVARCHAR (30)  NULL,
    [description]    NVARCHAR (MAX) NULL,
    [infant]         INT            NULL,
    [child]          INT            NULL,
    [teen]           INT            NULL,
    [adult]          INT            NULL,
    [senior]         INT            NULL,
    [without_bed]    INT            NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



