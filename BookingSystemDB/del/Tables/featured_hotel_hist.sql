﻿CREATE TABLE [del].[featured_hotel_hist] (
    [featured_hotel_id] INT  NOT NULL,
    [provider_id]       INT            NOT NULL,
    [title]             NVARCHAR (300) NULL,
    [description]       NVARCHAR (MAX) NULL,
    [section]           NVARCHAR (30)  NOT NULL,
    [from_date]         DATE           NULL,
    [to_date]           DATE           NULL,
    [picture_id]        INT            NULL,
    [load_timestamp]    DATETIME       DEFAULT (getdate()) NULL
);



