﻿CREATE TABLE [del].[booking_rental_hist] (
    [booking_rental_id] INT NOT NULL,
    [booking_id]        INT            NOT NULL,
    [vehicle_id]        INT            NOT NULL,
    [booking_guest_id]  INT            NULL,
    [num_days]          FLOAT (53)     NULL,
    [total_price]       FLOAT (53)     NULL,
    [markup_amt]        FLOAT (53)     NULL,
    [pickup_date]       DATETIME       NULL,
    [return_date]       DATETIME       NULL,
    [pickup_location]   VARCHAR (100)  NULL,
    [return_location]   VARCHAR (100)  NULL,
    [is_aftersales]     BIT            NOT NULL,
    [aftersales_admin]  INT            NULL,
    [has_driver]        BIT            NOT NULL,
    [provider_name]     NVARCHAR (100) NULL,
    [vehicle_model]     NVARCHAR (100) NULL,
    [load_timestamp]    DATETIME       DEFAULT (getdate()) NULL
);



