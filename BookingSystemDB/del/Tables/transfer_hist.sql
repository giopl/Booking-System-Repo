﻿CREATE TABLE [del].[transfer_hist] (
    [transfer_id]    INT      NOT NULL,
    [name]           NVARCHAR (100) NOT NULL,
    [description]    NVARCHAR (MAX) NULL,
    [is_active]      BIT            NOT NULL,
    [category]       NVARCHAR (100) NULL,
    [is_upgrade]     BIT            NOT NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



