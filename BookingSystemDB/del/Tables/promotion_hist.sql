﻿CREATE TABLE [del].[promotion_hist] (
    [promotion_id]         INT            NOT NULL,
    [provider_id]          INT            NOT NULL,
    [name]                 NVARCHAR (100) NULL,
    [description]          NVARCHAR (MAX) NULL,
    [contracted_offer]     NVARCHAR (MAX) NULL,
    [cancellation_policy]  NVARCHAR (MAX) NULL,
    [book_before_days_min] INT            NULL,
    [book_before_days_max] INT            NULL,
    [book_from_date]       DATE           NULL,
    [book_until_date]      DATE           NULL,
    [travel_from_date]     DATE           NULL,
    [travel_until_date]    DATE           NULL,
    [valid_from]           DATE           NOT NULL,
    [valid_to]             DATE           NOT NULL,
    [min_nights]           INT            NULL,
    [discount]             FLOAT (53)     NOT NULL,
    [apply_all_markets]    BIT            NOT NULL,
    [apply_all_rooms]      BIT            NOT NULL,
    [is_active]            BIT            NOT NULL,
    [is_honeymoon]         BIT            NOT NULL,
    [free_nights]          INT            NULL,
    [promotion_group_id]   INT            NULL,
    [load_timestamp]       DATETIME       DEFAULT (getdate()) NULL
);







