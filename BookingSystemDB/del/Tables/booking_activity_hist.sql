﻿CREATE TABLE [del].[booking_activity_hist] (
    [booking_activity_id] INT          NOT NULL,
    [booking_id]          INT            NOT NULL,
    [activity_id]         INT            NOT NULL,
    [adults]              INT            NULL,
    [children]            INT            NULL,
    [price_per_adult]     FLOAT (53)     NULL,
    [price_per_child]     FLOAT (53)     NULL,
    [total_price]         FLOAT (53)     NULL,
    [markup_amt]          FLOAT (53)     NULL,
    [is_aftersales]       BIT            NOT NULL,
    [aftersales_admin]    INT            NULL,
    [provider_name]       NVARCHAR (100) NULL,
    [activity_name]       NVARCHAR (200) NULL,
    [transfer_type]       INT            NULL,
    [load_timestamp]      DATETIME       DEFAULT (getdate()) NULL
);



