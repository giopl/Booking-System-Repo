﻿CREATE TABLE [del].[booking_payment_hist] (
    [booking_payment_id] INT NOT NULL,
    [booking_id]         INT             NOT NULL,
    [payment_amt]        FLOAT (53)      NULL,
    [payment_date]       DATETIME        NULL,
    [payment_method]     INT             NULL,
    [payment_details]    NVARCHAR (1000) NULL,
    [received_by]        INT             NULL,
    [load_timestamp]     DATETIME        DEFAULT (getdate()) NULL
);



