﻿CREATE TABLE [del].[promotion_set_hist] (
    [promotion_set_id] INT    NOT NULL,
    [name]             NVARCHAR (100) NOT NULL,
    [description]      NVARCHAR (300) NULL,
    [provider_id]      INT            NOT NULL,
    [is_active]        BIT            NOT NULL,
    [load_timestamp]   DATETIME       DEFAULT (getdate()) NULL
);

