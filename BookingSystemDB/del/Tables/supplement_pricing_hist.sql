﻿CREATE TABLE [del].[supplement_pricing_hist] (
    [supplement_pricing_id] INT     NOT NULL,
    [supplement_id]         INT        NOT NULL,
    [market_id]             INT        NOT NULL,
    [valid_from]            DATETIME   NULL,
    [valid_to]              DATETIME   NULL,
    [price_per_item]        FLOAT (53) NULL,
    [price_infant]          FLOAT (53) NULL,
    [price_child]           FLOAT (53) NULL,
    [price_teen]            FLOAT (53) NULL,
    [price_adult]           FLOAT (53) NULL,
    [price_senior]          FLOAT (53) NULL,
    [price_honeymoon]       FLOAT (53) NULL,
    [is_active]             BIT        NOT NULL,
    [load_timestamp]        DATETIME   DEFAULT (getdate()) NULL
);



