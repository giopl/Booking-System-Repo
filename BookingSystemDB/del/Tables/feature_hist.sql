﻿CREATE TABLE [del].[feature_hist] (
    [feature_id]     INT        NOT NULL,
    [category]       NVARCHAR (300) NULL,
    [description]    NVARCHAR (300) NULL,
    [feature_type]   NCHAR (1)      NULL,
    [name]           NVARCHAR (100) NOT NULL,
    [icon]           NVARCHAR (100) NULL,
    [is_active]      BIT            NOT NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



