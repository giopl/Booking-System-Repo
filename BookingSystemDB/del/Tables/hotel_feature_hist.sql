﻿CREATE TABLE [del].[hotel_feature_hist] (
    [feature_id]     INT      NOT NULL,
    [provider_id]    INT      NOT NULL,
    [quantity]       INT      NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

