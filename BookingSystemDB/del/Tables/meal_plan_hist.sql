﻿CREATE TABLE [del].[meal_plan_hist] (
    [meal_plan_id]   INT          NOT NULL,
    [name]           NVARCHAR (50)  NOT NULL,
    [description]    NVARCHAR (MAX) NULL,
    [meal_order]     SMALLINT       NULL,
    [code]           NCHAR (2)      NOT NULL,
    [is_active]      BIT            NOT NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



