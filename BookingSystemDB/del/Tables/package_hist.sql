﻿CREATE TABLE [del].[package_hist] (
    [package_id]                  INT  NOT NULL,
    [provider_id]                 INT            NOT NULL,
    [market_id]                   INT            NOT NULL,
    [valid_from]                  DATE           NOT NULL,
    [valid_to]                    DATE           NOT NULL,
    [name]                        NVARCHAR (100) NULL,
    [description]                 NVARCHAR (MAX) NULL,
    [number_nights]               INT            NULL,
    [price_ground_handling_adult] FLOAT (53)     NULL,
    [price_ground_handling_child] FLOAT (53)     NULL,
    [price_ground_handling_teen]  FLOAT (53)     NULL,
    [contracted_offer]            NVARCHAR (MAX) NULL,
    [cancellation_policy]         NVARCHAR (MAX) NULL,
    [apply_all_rooms]             BIT            NOT NULL,
    [is_active]                   BIT            NOT NULL,
    [load_timestamp]              DATETIME       DEFAULT (getdate()) NULL
);



