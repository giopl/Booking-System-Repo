﻿CREATE TABLE [del].[configuration_hist] (
    [configuration_id] INT    NOT NULL,
    [conf_key]         NVARCHAR (50)  NULL,
    [conf_type]        NVARCHAR (50)  NULL,
    [conf_value]       NVARCHAR (300) NULL,
    [system_based]     BIT            NOT NULL,
    [load_timestamp]   DATETIME       DEFAULT (getdate()) NULL
);



