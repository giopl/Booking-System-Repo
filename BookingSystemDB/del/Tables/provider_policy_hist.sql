﻿CREATE TABLE [del].[provider_policy_hist] (
    [provider_id]    INT      NOT NULL,
    [policy_id]      INT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

