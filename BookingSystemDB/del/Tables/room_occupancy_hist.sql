﻿CREATE TABLE [del].[room_occupancy_hist] (
    [room_id]        INT      NOT NULL,
    [occupancy_id]   INT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

