﻿CREATE TABLE [del].[booking_room_hist] (
    [booking_room_id] INT    NOT NULL,
    [booking_id]      INT           NOT NULL,
    [room_id]         INT           NOT NULL,
    [base_price_id]   INT           NOT NULL,
    [meal_plan_id]    INT           NULL,
    [room_number]     INT           NOT NULL,
    [booking_ref]     NVARCHAR (32) NULL,
    [adults]          INT           NULL,
    [infants]         INT           NULL,
    [children]        INT           NULL,
    [teens]           INT           NULL,
    [room_name]       NVARCHAR (50) NULL,
    [load_timestamp]  DATETIME      DEFAULT (getdate()) NULL
);



