﻿CREATE TABLE [del].[log_hist] (
    [log_id]         INT          NOT NULL,
    [user_id]        INT            NOT NULL,
    [operation]      NVARCHAR (30)  NULL,
    [item_type]      NVARCHAR (50)  NULL,
    [item_id]        INT            NULL,
    [description]    NVARCHAR (300) NULL,
    [created_on]     DATETIME       NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



