﻿CREATE TABLE [del].[market_hist] (
    [market_id]      INT         NOT NULL,
    [market_name]    NVARCHAR (50) NULL,
    [currency_id]    INT           NOT NULL,
    [provider_id]    INT           NOT NULL,
    [is_active]      BIT           NOT NULL,
    [is_default]     BIT           NOT NULL,
    [load_timestamp] DATETIME      DEFAULT (getdate()) NULL
);



