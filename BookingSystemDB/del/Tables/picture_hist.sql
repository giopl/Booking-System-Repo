﻿CREATE TABLE [del].[picture_hist] (
    [picture_id]     INT        NOT NULL,
    [file_name]      NVARCHAR (200)   NULL,
    [title]          NVARCHAR (100)   NULL,
    [description]    NVARCHAR (150)   NULL,
    [picture_guid]   UNIQUEIDENTIFIER NULL,
    [display_order]  INT              NULL,
    [load_timestamp] DATETIME         DEFAULT (getdate()) NULL
);



