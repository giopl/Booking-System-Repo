﻿CREATE TABLE [del].[contact_hist] (
    [contact_id]     INT     NOT NULL,
    [provider_id]    INT            NULL,
    [salutation]     NVARCHAR (10)  NULL,
    [firstname]      NVARCHAR (100) NULL,
    [lastname]       NVARCHAR (300) NULL,
    [job_title]      NVARCHAR (100) NULL,
    [office_phone]   NVARCHAR (50)  NULL,
    [mobile]         NVARCHAR (50)  NULL,
    [email]          NVARCHAR (100) NULL,
    [notes]          NVARCHAR (MAX) NULL,
    [is_active]      BIT            NOT NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



