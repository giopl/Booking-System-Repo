﻿CREATE TABLE [del].[file_upload_hist] (
    [file_id]        INT   NOT NULL,
    [provider_id]    INT              NULL,
    [filename]       NVARCHAR (200)   NULL,
    [title]          NVARCHAR (100)   NULL,
    [description]    NVARCHAR (500)   NULL,
    [filetype]       NVARCHAR (50)    NULL,
    [file_guid]      UNIQUEIDENTIFIER NULL,
    [display_order]  INT              NULL,
    [load_timestamp] DATETIME         DEFAULT (getdate()) NULL
);



