﻿CREATE TABLE [del].[supplement_hist] (
    [supplement_id]   INT      NOT NULL,
    [provider_id]     INT            NULL,
    [name]            NVARCHAR (100) NULL,
    [description]     NVARCHAR (MAX) NULL,
    [is_per_person]   BIT            NOT NULL,
    [is_per_room]     BIT            NOT NULL,
    [is_per_night]    BIT            NOT NULL,
    [is_per_item]     BIT            NOT NULL,
    [is_compulsory]   BIT            NOT NULL,
    [valid_all_rooms] BIT            NOT NULL,
    [is_active]       BIT            NOT NULL,
    [meal_plan_id]    INT            NULL,
    [load_timestamp]  DATETIME       DEFAULT (getdate()) NULL
);



