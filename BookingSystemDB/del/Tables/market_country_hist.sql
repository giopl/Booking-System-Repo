﻿CREATE TABLE [del].[market_country_hist] (
    [market_id]      INT      NOT NULL,
    [country_id]     INT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

