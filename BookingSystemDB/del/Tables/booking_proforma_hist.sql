﻿CREATE TABLE [del].[booking_proforma_hist] (
    [booking_proforma_id]     INT             NOT NULL,
    [booking_id]              INT             NOT NULL,
    [invoice_num]             NVARCHAR (50)   NULL,
    [agent_details]           NVARCHAR (500)  NULL,
    [booking_details]         NVARCHAR (1500) NULL,
    [reference]               NVARCHAR (50)   NULL,
    [due_date]                DATE            NULL,
    [sales_category]          NVARCHAR (200)  NULL,
    [sales_description]       NVARCHAR (1000) NULL,
    [currency]                NVARCHAR (10)   NULL,
    [nett_price_accomodation] FLOAT (53)      NULL,
    [nett_price_supplement]   FLOAT (53)      NULL,
    [nett_price_transfer]     FLOAT (53)      NULL,
    [nett_price_activity]     FLOAT (53)      NULL,
    [nett_price_rental]       FLOAT (53)      NULL,
    [nett_price]              FLOAT (53)      NULL,
    [handling_fees]           FLOAT (53)      NULL,
    [sold_by]                 NVARCHAR (100)  NULL,
    [issued_by]               NVARCHAR (100)  NULL,
    [checked_by]              NVARCHAR (100)  NULL,
    [approved_by]             NVARCHAR (100)  NULL,
    [remarks]                 NVARCHAR (1000) NULL,
    [issue_date]              DATE            NULL,
    [finalized]               BIT             NOT NULL,
    [load_timestamp]          DATETIME        DEFAULT (getdate()) NULL
);





