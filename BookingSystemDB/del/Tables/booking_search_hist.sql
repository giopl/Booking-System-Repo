﻿CREATE TABLE [del].[booking_search_hist] (
    [booking_ref]    NVARCHAR (32) NOT NULL,
    [search_details] XML           NULL,
    [hotel_id]       INT           NULL,
    [timestamp]      DATETIME      NULL,
    [package_id]     INT           NULL,
    [load_timestamp] DATETIME      DEFAULT (getdate()) NULL
);

