﻿CREATE TABLE [del].[room_hist] (
    [room_id]        INT   NOT NULL,
    [provider_id]    INT            NOT NULL,
    [name]           NVARCHAR (50)  NULL,
    [description]    NVARCHAR (MAX) NULL,
    [min_adult]      INT            NULL,
    [max_adult]      INT            NULL,
    [room_size]      NVARCHAR (30)  NULL,
    [is_active]      BIT            NOT NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



