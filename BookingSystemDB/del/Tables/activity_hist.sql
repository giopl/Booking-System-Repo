﻿CREATE TABLE [del].[activity_hist] (
    [activity_id]         INT  NOT NULL,
    [activity_name]       NVARCHAR (200)  NULL,
    [description]         NVARCHAR (MAX)  NULL,
    [cancellation_policy] NVARCHAR (MAX)  NULL,
    [conditions]          NVARCHAR (MAX)  NULL,
    [category]            NVARCHAR (50)   NULL,
    [duration]            NVARCHAR (50)   NULL,
    [min_age]             INT             NULL,
    [max_child_age]       INT             NULL,
    [is_active]           BIT             NOT NULL,
    [valid_from]          DATE            NULL,
    [valid_to]            DATE            NULL,
    [features]            NVARCHAR (1000) NULL,
    [includes_meal]       BIT             NOT NULL,
    [load_timestamp]      DATETIME        DEFAULT (getdate()) NULL
);



