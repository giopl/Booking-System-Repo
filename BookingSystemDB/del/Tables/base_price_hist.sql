﻿CREATE TABLE [del].[base_price_hist] (
    [base_price_id]     INT        NOT NULL,
    [room_id]           INT        NOT NULL,
    [market_id]         INT        NOT NULL,
    [meal_plan_id]      INT        NOT NULL,
    [start_date]        DATE       NOT NULL,
    [end_date]          DATE       NOT NULL,
    [price_single]      FLOAT (53) NULL,
    [price_twin]        FLOAT (53) NULL,
    [price_triple]      FLOAT (53) NULL,
    [price_infant]      FLOAT (53) NULL,
    [price_child]       FLOAT (53) NULL,
    [price_teen]        FLOAT (53) NULL,
    [price_quadruple]   FLOAT (53) NULL,
    [price_without_bed] FLOAT (53) NULL,
    [price_honeymoon]   FLOAT (53) NULL,
    [is_active]         BIT        NOT NULL,
    [load_timestamp]    DATETIME   DEFAULT (getdate()) NULL
);



