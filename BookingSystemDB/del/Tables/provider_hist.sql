﻿CREATE TABLE [del].[provider_hist] (
    [provider_id]     INT      NOT NULL,
    [name]            NVARCHAR (100) NULL,
    [description]     NVARCHAR (MAX) NULL,
    [location]        NVARCHAR (100) NULL,
    [company]         NVARCHAR (100) NULL,
    [website]         NVARCHAR (100) NULL,
    [commission_rate] FLOAT (53)     NULL,
    [street1]         NVARCHAR (100) NULL,
    [street2]         NVARCHAR (100) NULL,
    [town]            NVARCHAR (100) NULL,
    [country]         NVARCHAR (100) NULL,
    [phone1]          NVARCHAR (50)  NULL,
    [phone2]          NVARCHAR (50)  NULL,
    [fax]             NVARCHAR (50)  NULL,
    [email]           NVARCHAR (75)  NULL,
    [google_location] NVARCHAR (50)  NULL,
    [provider_type]   CHAR (1)       NOT NULL,
    [postcode]        VARCHAR (20)   NULL,
    [is_active]       BIT            NOT NULL,
    [star_rating]     INT            NULL,
    [load_timestamp]  DATETIME       DEFAULT (getdate()) NULL
);



