﻿CREATE TABLE [del].[user_hist] (
    [user_id]              INT        NOT NULL,
    [title]                NVARCHAR (20)  NULL,
    [firstname]            NVARCHAR (50)  NULL,
    [lastname]             NVARCHAR (50)  NULL,
    [date_of_birth]        DATE           NULL,
    [gender]               INT            NULL,
    [nationality]          NVARCHAR (60)  NULL,
    [role]                 INT            NOT NULL,
    [officephone]          NVARCHAR (20)  NULL,
    [mobile]               NVARCHAR (20)  NULL,
    [email]                NVARCHAR (100) NULL,
    [company]              NVARCHAR (100) NULL,
    [job_title]            NVARCHAR (100) NULL,
    [password]             NVARCHAR (200) NULL,
    [markup_percentage]    FLOAT (53)     NULL,
    [markup_fixed]         FLOAT (53)     NULL,
    [username]             NVARCHAR (50)  NULL,
    [credit_limit]         FLOAT (53)     NULL,
    [credit_used]          FLOAT (53)     NULL,
    [is_active]            BIT            NOT NULL,
    [temp_password_expiry] DATE           NULL,
    [temp_password]        NVARCHAR (200) NULL,
    [is_credit_based]      BIT            NULL,
    [currency_id]          INT            NULL,
    [load_timestamp]       DATETIME       DEFAULT (getdate()) NULL
);



