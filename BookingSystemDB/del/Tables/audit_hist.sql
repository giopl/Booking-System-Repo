﻿CREATE TABLE [del].[audit_hist] (
    [audit_id]       BIGINT        NOT NULL,
    [user_id]        INT            NULL,
    [entity_type]    NVARCHAR (100) NULL,
    [entity_id]      INT            NULL,
    [operation]      NVARCHAR (30)  NULL,
    [description]    NVARCHAR (255) NULL,
    [created_by]     INT            NULL,
    [create_date]    DATETIME       NOT NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



