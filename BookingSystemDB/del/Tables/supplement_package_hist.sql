﻿CREATE TABLE [del].[supplement_package_hist] (
    [supplement_id]  INT      NOT NULL,
    [package_id]     INT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

