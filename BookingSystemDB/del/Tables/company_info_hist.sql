﻿CREATE TABLE [del].[company_info_hist] (
    [Id]               INT          NOT NULL,
    [name]             NVARCHAR (100) NULL,
    [addr_line1]       NVARCHAR (200) NULL,
    [addr_line2]       NVARCHAR (200) NULL,
    [addr_line3]       NVARCHAR (200) NULL,
    [town]             NVARCHAR (100) NULL,
    [country]          NVARCHAR (100) NULL,
    [vat_reg_num]      NVARCHAR (100) NULL,
    [business_reg_num] NVARCHAR (100) NULL,
    [phone_num]        NVARCHAR (20)  NULL,
    [fax_num]          NVARCHAR (20)  NULL,
    [email]            NVARCHAR (100) NULL,
    [load_timestamp]   DATETIME       DEFAULT (getdate()) NULL
);

