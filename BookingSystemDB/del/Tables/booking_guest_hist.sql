﻿CREATE TABLE [del].[booking_guest_hist] (
    [booking_guest_id]         INT             NOT NULL,
    [booking_room_id]          INT            NOT NULL,
    [package_id]               INT            NULL,
    [salutation]               VARCHAR (15)   NULL,
    [firstname]                VARCHAR (60)   NULL,
    [lastname]                 VARCHAR (60)   NULL,
    [date_of_birth]            DATE           NULL,
    [nationality]              VARCHAR (50)   NULL,
    [age]                      INT            NULL,
    [guest_type]               INT            NULL,
    [arrival_flight_no]        VARCHAR (20)   NULL,
    [departure_flight_no]      VARCHAR (20)   NULL,
    [arrival_datetime]         DATETIME       NULL,
    [departure_datetime]       DATETIME       NULL,
    [price_per_night]          FLOAT (53)     NULL,
    [markup_amt]               FLOAT (53)     NULL,
    [booking_ref]              VARCHAR (32)   NULL,
    [passport_number]          NVARCHAR (20)  NULL,
    [passport_expiry_date]     DATE           NULL,
    [gender]                   INT            NULL,
    [booking_id]               INT            NULL,
    [package_name]             NVARCHAR (100) NULL,
    [compulsory_charge_amt]    FLOAT (53)     NULL,
    [ground_handling_amt]      FLOAT (53)     NULL,
    [promotional_discount_amt] FLOAT (53)     NULL,
    [load_timestamp]           DATETIME       DEFAULT (getdate()) NULL
);



