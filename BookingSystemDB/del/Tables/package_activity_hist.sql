﻿CREATE TABLE [del].[package_activity_hist] (
    [package_id]     INT      NOT NULL,
    [activity_id]    INT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

