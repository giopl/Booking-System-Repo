﻿CREATE TABLE [del].[occupant_hist] (
    [occupant_id]           INT           NOT NULL,
    [name]                  NVARCHAR (100) NULL,
    [description]           NVARCHAR (MAX) NULL,
    [min_adult]             INT            NULL,
    [ground_handling_price] FLOAT (53)     NULL,
    [is_active]             BIT            NOT NULL,
    [load_timestamp]        DATETIME       DEFAULT (getdate()) NULL
);

