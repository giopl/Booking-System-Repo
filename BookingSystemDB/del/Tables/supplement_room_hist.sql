﻿CREATE TABLE [del].[supplement_room_hist] (
    [supplement_id]  INT      NOT NULL,
    [room_id]        INT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

