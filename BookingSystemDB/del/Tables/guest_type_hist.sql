﻿CREATE TABLE [del].[guest_type_hist] (
    [guest_type_id]  INT   NOT NULL,
    [provider_id]    INT      NOT NULL,
    [infant_min_age] INT      NULL,
    [infant_max_age] INT      NULL,
    [child_min_age]  INT      NULL,
    [child_max_age]  INT      NULL,
    [teen_min_age]   INT      NULL,
    [teen_max_age]   INT      NULL,
    [adult_min_age]  INT      NULL,
    [adult_max_age]  INT      NULL,
    [senior_min_age] INT      NULL,
    [senior_max_age] INT      NULL,
    [is_active]      BIT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);



