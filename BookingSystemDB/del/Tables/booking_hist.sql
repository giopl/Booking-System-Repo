﻿CREATE TABLE [del].[booking_hist] (
    [booking_id]            INT             NOT NULL,
    [market_id]             INT             NOT NULL,
    [agent_user_id]         INT             NOT NULL,
    [agent_name]            NVARCHAR (100)  NULL,
    [booking_name]          VARCHAR (300)   NOT NULL,
    [checkin_date]          DATETIME        NULL,
    [checkout_date]         DATETIME        NULL,
    [num_nights]            INT             NULL,
    [create_date]           DATETIME        NULL,
    [booking_date]          DATETIME        NULL,
    [status]                INT             NULL,
    [booking_expiry_date]   DATETIME        NULL,
    [backoffice_user_id]    INT             NULL,
    [backoffice_user_name]  NVARCHAR (100)  NULL,
    [booking_ref]           NVARCHAR (32)   NULL,
    [adults]                INT             NULL,
    [infants]               INT             NULL,
    [children]              INT             NULL,
    [teens]                 INT             NULL,
    [rooms]                 INT             NULL,
    [phone_number]          NVARCHAR (20)   NULL,
    [email]                 NVARCHAR (75)   NULL,
    [provider_id]           INT             NULL,
    [discount_amt]          FLOAT (53)      NULL,
    [discount_reason]       NVARCHAR (2000) NULL,
    [discounted_by_user_id] INT             NULL,
    [booking_type]          INT             NULL,
    [package_id]            INT             NULL,
    [booking_amt]           FLOAT (53)      NULL,
    [markup_amt]            FLOAT (53)      NULL,
    [credit_amt]            FLOAT (53)      NULL,
    [paid_amt]              FLOAT (53)      NULL,
    [provider_name]         NVARCHAR (100)  NULL,
    [market_name]           NVARCHAR (50)   NULL,
    [booking_file_number]   NVARCHAR (100)  NULL,
    [package_name]          VARCHAR (100)   NULL,
    [free_nights]           INT             NULL,
    [remarks]               NVARCHAR (1000) NULL,
    [load_timestamp]        DATETIME        DEFAULT (getdate()) NULL
);







