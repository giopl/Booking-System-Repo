﻿CREATE TABLE [del].[promotion_market_hist] (
    [promotion_id]   INT      NOT NULL,
    [market_id]      INT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

