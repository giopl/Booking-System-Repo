﻿CREATE TABLE [del].[promotion_room_hist] (
    [promotion_id]   INT      NOT NULL,
    [room_id]        INT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

