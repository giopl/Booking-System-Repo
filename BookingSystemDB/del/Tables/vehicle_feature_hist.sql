﻿CREATE TABLE [del].[vehicle_feature_hist] (
    [feature_id]     INT      NOT NULL,
    [vehicle_id]     INT      NOT NULL,
    [quantity]       INT      NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

