﻿CREATE TABLE [del].[note_hist] (
    [note_id]        INT        NOT NULL,
    [item_type]      NVARCHAR (30)   NULL,
    [item_id]        INT             NULL,
    [category]       NVARCHAR (100)  NULL,
    [detail]         NVARCHAR (1000) NULL,
    [user_id]        INT             NOT NULL,
    [create_date]    DATETIME        NULL,
    [load_timestamp] DATETIME        DEFAULT (getdate()) NULL
);



