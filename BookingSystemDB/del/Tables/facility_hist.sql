﻿CREATE TABLE [del].[facility_hist] (
    [facility_id]    INT          NOT NULL,
    [provider_id]    INT            NOT NULL,
    [name]           NVARCHAR (100) NULL,
    [type]           NVARCHAR (100) NULL,
    [description]    NVARCHAR (300) NULL,
    [opening_hours]  NVARCHAR (300) NULL,
    [conditions]     NVARCHAR (300) NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



