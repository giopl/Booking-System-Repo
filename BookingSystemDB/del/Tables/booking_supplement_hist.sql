﻿CREATE TABLE [del].[booking_supplement_hist] (
    [booking_supplement_id] INT     NOT NULL,
    [booking_id]            INT            NOT NULL,
    [supplement_id]         INT            NULL,
    [quantity]              INT            NULL,
    [price]                 FLOAT (53)     NULL,
    [booking_room_id]       INT            NULL,
    [infants]               INT            NULL,
    [children]              INT            NULL,
    [teens]                 INT            NULL,
    [adults]                INT            NULL,
    [markup_amt]            FLOAT (53)     NULL,
    [supplement_name]       NVARCHAR (100) NULL,
    [load_timestamp]        DATETIME       DEFAULT (getdate()) NULL
);



