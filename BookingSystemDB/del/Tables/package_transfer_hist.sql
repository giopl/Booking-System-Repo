﻿CREATE TABLE [del].[package_transfer_hist] (
    [package_id]     INT      NOT NULL,
    [transfer_id]    INT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

