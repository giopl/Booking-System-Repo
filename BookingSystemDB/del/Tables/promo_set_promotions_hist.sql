﻿CREATE TABLE [del].[promo_set_promotions_hist] (
    [promotion_set_id] INT      NOT NULL,
    [promotion_id]     INT      NOT NULL,
    [load_timestamp]   DATETIME DEFAULT (getdate()) NULL
);

