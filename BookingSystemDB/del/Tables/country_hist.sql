﻿CREATE TABLE [del].[country_hist] (
    [country_id]     INT     NOT NULL,
    [country_code]   NCHAR (2)      NOT NULL,
    [name]           NVARCHAR (50)  NULL,
    [currency]       NVARCHAR (3)   NULL,
    [currency_name]  NVARCHAR (30)  NULL,
    [is_independent] NVARCHAR (30)  NULL,
    [language]       CHAR (2)       NULL,
    [nationality]    NVARCHAR (100) NULL,
    [load_timestamp] DATETIME       DEFAULT (getdate()) NULL
);



