﻿CREATE TABLE [del].[activity_pricing_hist] (
    [activity_pricing_id]               INT     NOT NULL,
    [activity_id]                       INT        NOT NULL,
    [currency_id]                       INT        NOT NULL,
    [provider_id]                       INT        NOT NULL,
    [price_child]                       FLOAT (53) NULL,
    [price_adult]                       FLOAT (53) NULL,
    [price_child_incl_sic_transfer]     FLOAT (53) NULL,
    [price_adult_incl_sic_transfer]     FLOAT (53) NULL,
    [price_child_incl_private_transfer] FLOAT (53) NULL,
    [price_adult_incl_private_transfer] FLOAT (53) NULL,
    [load_timestamp]                    DATETIME   DEFAULT (getdate()) NULL
);



