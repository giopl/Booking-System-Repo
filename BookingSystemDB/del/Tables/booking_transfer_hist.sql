﻿CREATE TABLE [del].[booking_transfer_hist] (
    [booking_transfer_id] INT   NOT NULL,
    [booking_id]          INT            NOT NULL,
    [transfer_id]         INT            NOT NULL,
    [children]            INT            NULL,
    [teens]               INT            NULL,
    [adults]              INT            NULL,
    [total_price]         FLOAT (53)     NULL,
    [markup_amt]          FLOAT (53)     NULL,
    [is_aftersales]       BIT            NOT NULL,
    [aftersales_admin]    INT            NULL,
    [transfer_pricing_id] INT            NULL,
    [provider_name]       NVARCHAR (100) NULL,
    [transfer_name]       NVARCHAR (100) NULL,
    [load_timestamp]      DATETIME       DEFAULT (getdate()) NULL
);



