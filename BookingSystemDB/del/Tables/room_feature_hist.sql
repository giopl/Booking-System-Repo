﻿CREATE TABLE [del].[room_feature_hist] (
    [feature_id]     INT      NOT NULL,
    [room_id]        INT      NOT NULL,
    [quantity]       INT      NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

