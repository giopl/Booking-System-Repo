﻿CREATE TABLE [del].[currency_hist] (
    [currency_id]    INT        NOT NULL,
    [currency_code]  CHAR (3)      NOT NULL,
    [currency_desc]  NVARCHAR (50) NULL,
    [symbol]         NVARCHAR (10) NULL,
    [code_desc]      NVARCHAR (56) NULL,
    [is_active]      BIT           NOT NULL,
    [exchange_rate]  FLOAT (53)    NULL,
    [load_timestamp] DATETIME      DEFAULT (getdate()) NULL
);



