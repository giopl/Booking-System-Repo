﻿CREATE TABLE [del].[package_room_hist] (
    [package_id]     INT      NOT NULL,
    [room_id]        INT      NOT NULL,
    [load_timestamp] DATETIME DEFAULT (getdate()) NULL
);

