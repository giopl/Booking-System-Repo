﻿
MERGE booking AS T
     USING (	select distinct b.agent_user_id , u.is_credit_based
	from booking b
	inner join [user] u
	on b.agent_user_id = u.user_id
		) AS S (user_id, is_credit)  
     ON T.agent_user_id = S.user_id
    WHEN MATCHED    
        THEN UPDATE SET T.is_credit  = s.is_credit;
