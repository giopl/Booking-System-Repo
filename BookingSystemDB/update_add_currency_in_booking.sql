﻿
MERGE booking AS T
     USING (select distinct b.market_id , c.currency_code, c.symbol
	from booking b
	inner join market m 
	on b.market_id = m.market_id 
	inner join currency c 
	on c.currency_id = m.currency_id) AS S (market_id,currency_code, symbol)  
     ON T.market_id = S.market_id
    WHEN MATCHED    
        THEN UPDATE SET T.currency_code = s.currency_code, T.symbol = S.symbol;
