﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

DECLARE @cnt int;
select @cnt=count(1) from dbo.tbl_configuration where [conf_type]='PROMOCATEG' ;
if(@cnt =0 ) 
INSERT INTO [dbo].[configuration]
           ([conf_key]
           ,[conf_type]
           ,[conf_value]
           ,[system_based])
     VALUES
         (
		 'NA',
		 'PROMOCATEG',
		 'Default',
		 1
		 );

		 
INSERT INTO [dbo].[configuration]
           ([conf_key]
           ,[conf_type]
           ,[conf_value]
           ,[system_based])
     VALUES
          (
				'PROMOCATEG',
				'CONF_TYPE',
				'Promotion Category',
				1


		  );
GO
	
