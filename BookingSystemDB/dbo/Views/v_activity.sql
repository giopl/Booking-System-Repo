﻿

CREATE view [dbo].[v_activity]
 as
select a.activity_id, ap.activity_pricing_id,  a.activity_name, a.description,a.category,  a.min_age, a.max_child_age, 
a.features, a.includes_meal,

c.currency_id, c.symbol, c.currency_code, 
ap.price_adult, ap.price_child
,ap.price_adult_incl_sic_transfer, ap.price_child_incl_sic_transfer
,ap.price_adult_incl_private_transfer, ap.price_child_incl_private_transfer
from activity a
--inner join provider p 
--on p.provider_id = a.provider_id
inner join activity_pricing ap
on a.activity_id = ap.activity_id
inner join currency c
on c.currency_id = ap.currency_id