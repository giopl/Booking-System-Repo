﻿




CREATE view [dbo].[v_price] as

select bp.base_price_id, bp.market_id, bp.room_id, r.name room,  p.provider_id, p.name  hotel,  bp.meal_plan_id, dt.fulldate, 
bp.price_single, bp.price_twin, bp.price_triple, bp.price_quadruple, bp.price_honeymoon,
bp.price_infant, bp.price_child, bp.price_teen, bp.price_without_bed,
c.currency_code, c.symbol, c.currency_id,
chg.price_infant chg_infant, chg.price_child chg_child, chg.price_teen chg_teen, chg.price_adult chg_adult

from base_price bp

inner join [date] dt
on dt.fulldate between bp.start_date and bp.end_date

inner join room r 
on bp.room_id = r.room_id

inner join provider p
on r.provider_id = p.provider_id

inner join market m
on bp.market_id = m.market_id

inner join currency c
on m.currency_id = c.currency_id


left join 

(
select s.supplement_id, s.provider_id, sp.market_id, d.fulldate, sp.price_infant, sp.price_child, sp.price_teen, sp.price_adult
from supplement s

inner join supplement_pricing sp
on s.supplement_id = sp.supplement_id

inner join [date] d
on d.fulldate between sp.valid_from and sp.valid_to

where s.is_compulsory =1 
and s.is_active = 1
--and sp.is_active = 1
) chg

on chg.provider_id = p.provider_id
and dt.fulldate = chg.fulldate
and chg.market_id = m.market_id



where bp.is_active = 1
and r.is_active = 1
and p.is_active = 1
and m.is_active =1 
and c.is_active =1 
and dt.fulldate > dateAdd(day,-100,getDate())


--select * from v_baseprice
--select * from base_price