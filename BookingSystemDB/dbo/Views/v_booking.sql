﻿


CREATE view [dbo].[v_booking] as

SELECT b.rooms num_rooms
	,isnull(b.booking_id,-999) booking_id
	,b.booking_ref
	,b.booking_name
	,p.provider_id
	,b.package_id
	,p.name
	,b.checkin_date
	,b.checkout_date
	,coalesce(b.num_nights,0) as num_nights
	,b.create_date created_on
	,coalesce(b.adults,0) as adults
	,coalesce(b.infants,0) as infants
	,coalesce(b.children,0) as children
	,coalesce(b.teens,0) as teens
	,coalesce(bg.price,0) AS price
	,coalesce(bg.price_per_night,0) price_per_night
	,coalesce(bg.markup_amt,0) markup_amt_room
	,c.symbol
	,b.agent_user_id
	,b.status
	,coalesce(r.cnt,0) as num_rental
	,coalesce(r.total_price,0) AS price_rental
	,coalesce(r.total_price_no_after_sales,0) as price_rental_no_after_sales
	,coalesce(a.cnt,0) as num_activity
	,coalesce(a.total_price,0) AS price_activity
	,coalesce(a.total_price_no_after_sales,0) as price_activity_no_after_sales
	,coalesce(s.cnt,0) as num_supplement
	,coalesce(s.total_price,0) AS price_supplement
	,coalesce(t.cnt,0) as num_transfer
	,coalesce(t.total_price,0) AS price_transfer
	,coalesce(t.total_price_no_after_sales,0) as price_transfer_no_after_sales
	,coalesce(b.discount_amt,0) as discount_amt
	,coalesce(bg.ground_handling_amt,0) as ground_handling_amt
	,coalesce(bg.compulsory_charge_amt,0) as compulsory_charge_amt
	,coalesce(bg.promotional_discount_amt,0) as promotional_discount_amt
	,coalesce(b.free_nights,0) free_nights
	,agent.is_credit_based
	,coalesce(proforma.has_proforma_finalized, 0) has_proforma_finalized
	,b.remarks
	,b.timelimit_date
	,b.is_timelimit
	,b.is_new_timelimit
	,b.cancellation_fee
	,voucher.has_voucher_generated
FROM booking b
--INNER JOIN booking_search bs ON b.booking_ref = bs.booking_ref
INNER JOIN market m ON b.market_id = m.market_id
INNER JOIN currency c ON m.currency_id = c.currency_id
INNER JOIN provider p ON b.provider_id = p.provider_id
INNER JOIN [user] agent on agent.user_id = b.agent_user_id

LEFT JOIN (
	SELECT booking_id
		,sum((coalesce(price_per_night,0) + coalesce(markup_amt,0)) - coalesce(promotional_discount_amt,0)) price
		,sum(coalesce(price_per_night,0)) price_per_night
		,sum(coalesce(markup_amt,0)) markup_amt
		,sum(coalesce(ground_handling_amt,0)) ground_handling_amt
		,sum(coalesce(promotional_discount_amt,0)) promotional_discount_amt
		,sum(coalesce(compulsory_charge_amt,0)) compulsory_charge_amt
	FROM booking_guest bg
	GROUP BY booking_id
	) bg ON b.booking_id = bg.booking_id
LEFT JOIN (
	SELECT count(1) cnt
		,booking_id
		,sum(coalesce(total_price,0) * coalesce(num_days,0)) AS total_price
		,sum(coalesce(markup_amt,0)) markup_amt
		,sum(case when coalesce(is_aftersales,0) = 0 then coalesce(total_price,0) * coalesce(num_days,0) else 0 end) AS total_price_no_after_sales
		
	FROM booking_rental
	GROUP BY booking_id
	) r ON b.booking_id = r.booking_id
LEFT JOIN (
	SELECT count(1) cnt
		,booking_id
		,sum(coalesce(total_price,0)) AS total_price
		,sum(case when coalesce(is_aftersales,0) = 0 then coalesce(total_price,0) else 0 end) AS total_price_no_after_sales
		,sum(coalesce(markup_amt,0)) markup_amt
	FROM booking_activity
	GROUP BY booking_id
	) a ON b.booking_id = a.booking_id
LEFT JOIN (
	SELECT count(1) cnt
		,booking_id
		,sum(coalesce(price,0)) AS total_price
		,sum(coalesce(markup_amt,0)) markup_amt
	FROM booking_supplement
	GROUP BY booking_id
	) s ON b.booking_id = s.booking_id
LEFT JOIN (
	SELECT count(1) cnt
		,booking_id
		,sum(coalesce(total_price,0)) AS total_price
		,sum(case when coalesce(is_aftersales,0) = 0 then coalesce(total_price,0) else 0 end) AS total_price_no_after_sales
		,sum(coalesce(markup_amt,0)) markup_amt
	FROM booking_transfer
	GROUP BY booking_id
	) t ON b.booking_id = t.booking_id
	
LEFT JOIN (
	SELECT 1 has_proforma_finalized
		,booking_id
	FROM booking_proforma

	GROUP BY booking_id
	) proforma ON proforma.booking_id = b.booking_id

LEFT JOIN (
	select 1 has_voucher_generated, booking_id 
	from booking_voucher
	group by booking_id
) voucher on voucher.booking_id = b.booking_id
	
GO

