﻿CREATE VIEW [dbo].[v_unavailability]
	AS 
	
	SELECT  
	distinct
	u.room_id , d.fulldate unavailable_date
	from [dbo].[room_unavailability] u
	inner join dbo.date d
	on d.fulldate between u.start_date and u.end_date

