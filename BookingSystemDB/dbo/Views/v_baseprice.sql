﻿CREATE view v_baseprice as
select 
cast(p.provider_id as varchar(3))+'-'+ p.name as hotel,
cast(r.room_id as varchar(3))+'-'+ r.name as room,
cast(bp.price_single as varchar(5)) +'/'+ cast(bp.price_twin as varchar(5)) +'/'+ cast(bp.price_triple as varchar(5)) as price_adult, 
bp.price_infant, bp.price_child,bp.price_teen,
bp.start_date, bp.end_date
 from base_price bp
 inner join room r 
 on bp.room_id = r.room_id
 inner join provider p 
 on p.provider_id = r.provider_id
 where bp.is_active=1 and r.is_active=1 and p.is_active=1