﻿create view dbo.v_rate_sheet as 
select 
	   data.price_type
	  ,bp.start_date
	  ,bp.end_date
	  ,data.[package_id]
      ,data.[package_name]
	  ,data.[number_nights]
      ,data.[room_id]
      ,data.[room_name]
      ,data.[provider_id]
      ,data.[provider_name]
      ,data.[base_price_id]
      ,data.[description]
      ,data.[market_id]
      ,data.[currency_code]
      ,data.[currency_id]
      ,data.[symbol]
      ,data.[price_single]
      ,data.[price_twin]
      ,data.[price_triple]
      ,data.[price_quadruple]
      ,data.[price_honeymoon]
      ,data.[price_infant]
      ,data.[price_child]
      ,data.[price_teen]
      ,data.[price_ground_handling_adult]
      ,data.[price_ground_handling_teen]
      ,data.[price_ground_handling_child]
      ,data.[chg_infant]
      ,data.[chg_child]
      ,data.[chg_teen]
      ,data.[chg_adult]	  
      ,data.[incl_grnd_handling]
      ,data.[is_active]
	  ,data.[contracted_offer]
	  from
(
SELECT 
'PC' price_type
		,v.[package_id]
      ,v.[package_name]
	  ,v.[number_nights]
      ,v.[room_id]
      ,v.[room_name]
      ,v.[provider_id]
      ,v.[provider_name]
      ,v.[base_price_id]
      ,v.[description]
      ,v.[market_id]
      ,v.[currency_code]
      ,v.[currency_id]
      ,v.[symbol]
      ,v.[price_single]
      ,v.[price_twin]
      ,v.[price_triple]
      ,v.[price_quadruple]
      ,v.[price_honeymoon]
      ,v.[price_infant]
      ,v.[price_child]
      ,v.[price_teen]
      ,v.[price_ground_handling_adult]
      ,v.[price_ground_handling_teen]
      ,v.[price_ground_handling_child]
      ,v.[chg_infant]
      ,v.[chg_child]
      ,v.[chg_teen]
      ,v.[chg_adult]  
      ,v.[incl_grnd_handling]
      ,v.[is_active]
	  ,p.[contracted_offer]
  FROM [dbo].[v_package] v
  inner join dbo.package p 
  on p.package_id=v.package_id

  union all

SELECT 
'BP' price_type
,0 package_id
	   ,'Base price' package_name
	   ,'0' number_nights
	   ,[room_id]
	   ,[room]      
      ,[provider_id]
      ,[hotel]
	  ,[base_price_id]
	   ,'Flat rate' price_name
	   ,[market_id]     
	     ,[currency_code]
		  ,[currency_id]
		    ,[symbol]

     -- ,[fulldate]
      ,[price_single]
      ,[price_twin]
      ,[price_triple]
      ,[price_quadruple]
      ,[price_honeymoon]
      ,[price_infant]
      ,[price_child]
      ,[price_teen]
     -- ,[price_without_bed]
     , 0 [price_ground_handling_adult]
      ,0 [price_ground_handling_teen]
      ,0 [price_ground_handling_child]        
      ,[chg_infant]
      ,[chg_child]
      ,[chg_teen]
      ,[chg_adult]
	  , 0 [incl_grnd_handling]
      ,1 [is_active]
	  , null [contracted_offer]
  FROM [dbo].[v_price]
  ) data
  inner join [dbo].[base_price] bp
  on bp.[base_price_id]=data.base_price_id
  where bp.is_active=1
