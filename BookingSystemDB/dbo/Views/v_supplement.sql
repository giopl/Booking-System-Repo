﻿
CREATE view [dbo].[v_supplement]

as

select 
distinct s.supplement_id, s.provider_id, s.name, s.description, s.is_per_person, s.is_per_room, s.is_per_night, s.is_per_item,
s.valid_all_rooms, s.meal_plan_id, sr.room_id, sp.market_id, c.symbol, c.currency_code, sp.valid_from, sp.valid_to,
sp.price_per_item, sp.price_adult, sp.price_infant,  sp.price_child, sp.price_teen ,s.is_compulsory


from supplement s

left join supplement_room sr 
on s.supplement_id = sr.supplement_id

inner join supplement_pricing sp
on sp.supplement_id = s.supplement_id

inner join market m
on sp.market_id = m.market_id

inner join currency c 
on m.currency_id = c.currency_id

where 
sp.is_active = 1 
and s.is_active = 1
and m.is_active =1
and c.is_active =1
and sp.is_active =1