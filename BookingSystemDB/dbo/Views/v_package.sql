﻿
--select * from v_package

CREATE view [dbo].[v_package] as

select 
p.package_id,
p.number_nights,
p.name package_name,
r.room_id,
r.name room_name,
h.provider_id,
h.name provider_name,
b.base_price_id,
p.description,

m.market_id,
c.currency_code,
c.currency_id,
c.symbol,


 b.price_single ,
 
 b.price_twin   ,

 b.price_triple  , 
 b.price_quadruple  ,
 b.price_honeymoon, 
 b.price_infant ,
 b.price_child   , 
 b.price_teen   , 

price_ground_handling_adult ,
price_ground_handling_teen,
price_ground_handling_child,

chg.price_infant chg_infant, chg.price_child chg_child, chg.price_teen chg_teen, chg.price_adult chg_adult,


dt.fulldate,

case when p.price_ground_handling_adult > 0 then 1 else 0 end as incl_grnd_handling,
p.is_active
--select count(1)
from 
base_price b

inner join date dt
on dt.fulldate between b.start_date and b.end_date


inner join room r
on b.room_id = r.room_id

inner join provider h
on r.provider_id = h.provider_id

inner join market m
on b.market_id = m.market_id

inner join currency c
on m.currency_id = c.currency_id

left join 

(
select s.supplement_id, s.provider_id, sp.market_id, d.fulldate, sp.price_infant, sp.price_child, sp.price_teen, sp.price_adult
from supplement s

inner join supplement_pricing sp
on s.supplement_id = sp.supplement_id

inner join [date] d
on d.fulldate between sp.valid_from and sp.valid_to

where s.is_compulsory =1 
and s.is_active = 1
and sp.is_active = 1
) chg

on chg.provider_id = h.provider_id
and dt.fulldate = chg.fulldate
and chg.market_id = m.market_id



inner join   --get all rooms applicable 
(
	select 
	m.market_id,
	p.package_id,
	p.apply_all_rooms,
	r.room_id,
	r.name room_name,
	r.provider_id

	from 
	[dbo].[package] p
	inner join dbo.market m
	on p.market_id=m.market_id
	left join [dbo].[package_room] pr
	on p.package_id=pr.package_id
	right join [dbo].[room] r 
	on (r.room_id=pr.room_id  --either it applies to a specific room or 
	or  p.apply_all_rooms=1)
	   ---or to all rooms )
	where p.package_id is not null --there must be a promotion associated
	and p.is_active=1
) rooms 
on rooms.room_id=b.room_id
and rooms.provider_id=r.provider_id
and rooms.market_id=m.market_id

inner join [dbo].[package] p 
on p.package_id=rooms.package_id
where
p.is_active=1 and 
m.is_active=1 and
p.is_active =1 and
r.is_active =1 and
b.is_active =1 and
h.is_active = 1 and
dt.fulldate between  p.valid_from and p.valid_to    --only return prices for dates within booking
  --and p.valid_from between b.start_date and b.end_date  --give records only if present in base price for given period
--and p.valid_to   between b.start_date and b.end_date