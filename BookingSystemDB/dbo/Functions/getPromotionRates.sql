﻿
create function [dbo].[getPromotionRates] (	
	@start date, --checkin_date
	@end date, --checkout_date
	--additional optional parameters in case an outer apply is required (no join allowed for outer apply)
	@provider int =0,  
	@market int =0,
	@room int =0,
	@fulldate datetime='1900-01-01'
	)
returns @PromoInfo  table
(
fulldate datetime,
provider_id int,
market_id int,
market_name nvarchar(50),
room_id int,
room_name nvarchar(50),
final_discount float
)
AS
BEGIN
Insert @PromoInfo
select 
d.fulldate,
p.provider_id,
markets.market_id,
markets.market_name,
rooms.room_id,
rooms.room_name,
EXP(SUM(LOG((100 - coalesce(p.discount,0)))))/100   final_discount
from  [dbo].[promotion] p 

inner join dbo.date d
on d.fulldate between p.valid_from and p.valid_from
inner join  --get all markets applicable 
(
	select 
	p.promotion_id,
	p.apply_all_markets,
	m.market_id,
	m.market_name,
	m.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_market] pm
	on p.promotion_id=pm.promotion_id
	right join [dbo].[market] m
	on (m.market_id=pm.market_id  --either it applies to a specific market or 
	or  p.apply_all_markets=1   --to all markets 
	)
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) markets 
on p.promotion_id=markets.promotion_id
and markets.provider_id=p.provider_id
inner join   --get all rooms applicable 
(
	select 
	p.promotion_id,
	p.apply_all_rooms,
	r.room_id,
	r.name room_name,
	r.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_room] pr
	on p.promotion_id=pr.promotion_id
	right join [dbo].[room] r 
	on (r.room_id=pr.room_id  --either it applies to a specific room or 
	or  p.apply_all_rooms=1   ---or to all rooms 
	)
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) rooms 
on 
rooms.promotion_id=markets.promotion_id
and rooms.provider_id=p.provider_id

where
p.is_active=1
and 
d.fulldate between  @start and  @end    --only return prices for dates within booking
and  /* handle additional parameters if required */
(@provider=0 or @provider =p.provider_id)
and 
( @market=0 or @market= markets.market_id)
and
(  @room= 0 or @room=rooms.room_id)
and
(@fulldate ='1900-01-01' or @fulldate =d.fulldate )
and  
--apply all logic for promotion filtering 
(
	
		--if booking is made between the promotion booking dates 
	getdate() between coalesce(p.valid_from,'1990-01-01') and coalesce(p.valid_to ,'1990-01-01')
	or
		--the travel dates must match 
		(
			@start between coalesce(p.[travel_from_date],'1990-01-01') and coalesce(p.[travel_until_date],'1990-01-01')
			and @end between coalesce(p.[travel_from_date],'1990-01-01') and coalesce(p.[travel_until_date],'1990-01-01')
		)
)
	and datediff(day,@start,@end) >=coalesce(p.min_nights,0)
group by
d.fulldate,
p.provider_id,
markets.market_id,
markets.market_name,
rooms.room_id,
rooms.room_name

return 
end
