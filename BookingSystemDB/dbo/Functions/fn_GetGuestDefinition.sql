﻿-- =============================================
-- Author:		Giovanni L'Etourdi
-- Create date: 25-Jun=2016
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetGuestDefinition]
(	
	-- Add the parameters for the function here
	@provider_id int,
	@age1 int=-1,
	@age2 int=-1,
	@age3 int=-1,
	@age4 int=-1,
	@age5 int=-1
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT 
	gt.provider_id,
	case when @age1 >-1 and @age1 <= infant_max_age then 1 else 0 end+
	case when @age2 >-1 and @age2 <= infant_max_age then 1 else 0 end+
	case when @age3 >-1 and @age3 <= infant_max_age then 1 else 0 end+
	case when @age4 >-1 and @age4 <= infant_max_age then 1 else 0 end+
	case when @age5 >-1 and @age5 <= infant_max_age then 1 else 0 end infant,
					
	case when @age1 >-1 and @age1 > infant_max_Age and @age1 <= child_max_age then 1 else 0 end +
	case when @age2 >-1 and @age2 > infant_max_Age and @age2 <= child_max_age then 1 else 0 end +
	case when @age3 >-1 and @age3 > infant_max_Age and @age3 <= child_max_age then 1 else 0 end +
	case when @age4 >-1 and @age4 > infant_max_Age and @age4 <= child_max_age then 1 else 0 end +
	case when @age5 >-1 and @age5 > infant_max_Age and @age5 <= child_max_age then 1 else 0 end child,
					
	case when @age1 >-1 and @age1 > child_max_Age and @age1<= teen_max_age then 1 else 0 end +
	case when @age2 >-1 and @age2 > child_max_Age and @age2<= teen_max_age then 1 else 0 end +
	case when @age3 >-1 and @age3 > child_max_Age and @age3<= teen_max_age then 1 else 0 end +
	case when @age4 >-1 and @age4 > child_max_Age and @age4<= teen_max_age then 1 else 0 end +
	case when @age5 >-1 and @age5 > child_max_Age and @age5<= teen_max_age then 1 else 0 end teen

	from guest_type gt
	where gt.provider_id = @provider_id
)