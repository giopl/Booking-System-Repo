﻿--script to copy all tables 
select 'select * into del.[' + table_name + '_hist] from dbo.[' +table_name + '] where 1=0;'
from INFORMATION_SCHEMA.TABLES 
where TABLE_SCHEMA = 'dbo' and Table_type='BASE TABLE'
union all
--script to add load_timestamp to all tables 
select 'alter table del.[' + table_name + '_hist] add load_timestamp datetime default getdate() ;'
from INFORMATION_SCHEMA.TABLES 
where TABLE_SCHEMA = 'dbo' and Table_type='BASE TABLE';


DECLARE 
    @cr VARCHAR(2) = CHAR(13) + CHAR(10),
    @t  VARCHAR(1) = CHAR(9),
    @s  NVARCHAR(MAX) = N'';

;WITH t AS

(
    SELECT [object_id], 
     s = OBJECT_SCHEMA_NAME([object_id]),
     n = OBJECT_NAME([object_id])
    FROM sys.tables WHERE is_ms_shipped = 0 
	and schema_id in (select schema_id  from    sys.schemas where name='dbo'  ) 
)

SELECT @s += 'IF OBJECT_ID(''dbo.delTrigger_' + t.s + '_' + t.n + ''') IS NOT NULL
    DROP TRIGGER [dbo].[delTrigger_' + t.s + '_' + t.n + '];
G' + 'O
CREATE TRIGGER dbo.delTrigger_' + t.s + '_' + t.n + '
    ON ' + QUOTENAME(t.s) + '.' + QUOTENAME(t.n) + '
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT ' + 'del' + '.' + QUOTENAME(t.n+'_hist') + '
    (
' + 
(
    SELECT @t + @t + name+CASE WHEN 
ROW_NUMBER() OVER (PARTITION BY t.[object_id] ORDER BY c.Name DESC)  
<>
COUNT(name) OVER (PARTITION BY t.[object_id])   then ',' else '' END + @cr
        FROM sys.columns AS c
        WHERE c.[object_id] = t.[object_id]        
        AND is_rowguidcol = 0
        
    AND system_type_id <> 189
    FOR XML PATH(''), TYPE
).value('.[1]', 'NVARCHAR(MAX)') + '--' 
+ @cr + @t + ')'
+ @cr + @t + 'SELECT 
' + 
(
    SELECT @t + @t + name + CASE WHEN 
ROW_NUMBER() OVER (PARTITION BY t.[object_id] ORDER BY c.Name DESC)  
<>
COUNT(name) OVER (PARTITION BY t.[object_id])   then ',' else '' END  + @cr
        FROM sys.columns AS c
        WHERE c.[object_id] = t.[object_id]        
        AND is_rowguidcol = 0
        
    AND system_type_id <> 189
    FOR XML PATH(''), TYPE
).value('.[1]', 'NVARCHAR(MAX)') + '--'
+ @cr + @t + 'FROM
        deleted;
END' + @cr + 'G' + 'O' + @cr
FROM t
ORDER BY t.s, t.n;

SELECT CONVERT(XML, @s);


