CREATE TABLE [dbo].[room_occupancy] (
    [room_id]      INT NOT NULL,
    [occupancy_id] INT NOT NULL,
    CONSTRAINT [PK_room_occupancy] PRIMARY KEY CLUSTERED ([room_id] ASC, [occupancy_id] ASC),
    CONSTRAINT [FK_room_occupancy_occupancy_id] FOREIGN KEY ([occupancy_id]) REFERENCES [dbo].[occupancy] ([occupancy_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_room_occupancy_room_type_id] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_room_occupancy
    ON [dbo].[room_occupancy]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[room_occupancy_hist]
    (
		room_id,
		occupancy_id
--
	)
	SELECT 
		room_id,
		occupancy_id
--
	FROM
        deleted;
END