CREATE TABLE [dbo].[picture] (
    [picture_id]    INT              IDENTITY (100, 1) NOT NULL,
    [file_name]     NVARCHAR (200)   NULL,
    [title]         NVARCHAR (100)   NULL,
    [description]   NVARCHAR (150)   NULL,
    [picture_guid]  UNIQUEIDENTIFIER NULL,
    [display_order] INT              NULL,
    PRIMARY KEY CLUSTERED ([picture_id] ASC)
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_picture
    ON [dbo].[picture]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[picture_hist]
    (
		title,
		picture_id,
		picture_guid,
		file_name,
		display_order,
		description
--
	)
	SELECT 
		title,
		picture_id,
		picture_guid,
		file_name,
		display_order,
		description
--
	FROM
        deleted;
END