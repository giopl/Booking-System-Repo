﻿CREATE TABLE [dbo].[added_rate] (
    [added_rate_id] INT        IDENTITY (100, 1) NOT NULL,
    [added_id]      INT        NOT NULL,
    [provider_id]   INT        NOT NULL,
    [market_id]     INT        NOT NULL,
    [period_id]     INT        NOT NULL,
    [occupancy_id]  INT        NOT NULL,
    [room_id]       INT        NOT NULL,
    [percentage]    FLOAT (53) NULL,
    [adult_rate]    FLOAT (53) NULL,
    [child_rate]    FLOAT (53) NULL,
    [is_active]     CHAR (1)   NULL,
    [infant_rate]   FLOAT (53) NULL,
    [teen_rate]     FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([added_rate_id] ASC),
    CONSTRAINT [fk_added_rate_added_id] FOREIGN KEY ([added_id]) REFERENCES [dbo].[added] ([added_id]),
    CONSTRAINT [fk_added_rate_market_id] FOREIGN KEY ([market_id]) REFERENCES [dbo].[market] ([market_id]),
    CONSTRAINT [fk_added_rate_occupancy_id] FOREIGN KEY ([occupancy_id]) REFERENCES [dbo].[occupancy] ([occupancy_id]),
    CONSTRAINT [fk_added_rate_period_id] FOREIGN KEY ([period_id]) REFERENCES [dbo].[period] ([period_id]),
    CONSTRAINT [fk_added_rate_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]),
    CONSTRAINT [fk_added_rate_room_id] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id])
);

