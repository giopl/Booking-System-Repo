CREATE TABLE [dbo].[guest_type] (
    [guest_type_id]  INT IDENTITY (100, 1) NOT NULL,
    [provider_id]    INT NOT NULL,
    [infant_min_age] INT NULL,
    [infant_max_age] INT NULL,
    [child_min_age]  INT NULL,
    [child_max_age]  INT NULL,
    [teen_min_age]   INT NULL,
    [teen_max_age]   INT NULL,
    [adult_min_age]  INT NULL,
    [adult_max_age]  INT NULL,
    [senior_min_age] INT NULL,
    [senior_max_age] INT NULL,
    [is_active]      BIT DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([guest_type_id] ASC),
    CONSTRAINT [FK_guest_type_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_guest_type
    ON [dbo].[guest_type]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[guest_type_hist]
    (
		teen_min_age,
		teen_max_age,
		senior_min_age,
		senior_max_age,
		provider_id,
		is_active,
		infant_min_age,
		infant_max_age,
		guest_type_id,
		child_min_age,
		child_max_age,
		adult_min_age,
		adult_max_age
--
	)
	SELECT 
		teen_min_age,
		teen_max_age,
		senior_min_age,
		senior_max_age,
		provider_id,
		is_active,
		infant_min_age,
		infant_max_age,
		guest_type_id,
		child_min_age,
		child_max_age,
		adult_min_age,
		adult_max_age
--
	FROM
        deleted;
END