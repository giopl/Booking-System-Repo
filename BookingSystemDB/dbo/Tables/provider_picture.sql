CREATE TABLE [dbo].[provider_picture] (
    [picture_id]    INT          NOT NULL,
    [provider_id]   INT          NOT NULL,
    [display_order] INT          NULL,
    [section]       VARCHAR (15) NULL,
    CONSTRAINT [PK_provider_picture] PRIMARY KEY CLUSTERED ([provider_id] ASC, [picture_id] ASC),
    CONSTRAINT [FK_provider_picture_picture_id] FOREIGN KEY ([picture_id]) REFERENCES [dbo].[picture] ([picture_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_provider_picture_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_provider_picture
    ON [dbo].[provider_picture]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[provider_picture_hist]
    (
		section,
		provider_id,
		picture_id,
		display_order
--
	)
	SELECT 
		section,
		provider_id,
		picture_id,
		display_order
--
	FROM
        deleted;
END