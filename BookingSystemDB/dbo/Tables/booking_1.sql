﻿CREATE TABLE [dbo].[booking] (
    [booking_id]             INT             IDENTITY (100, 1) NOT NULL,
    [market_id]              INT             NOT NULL,
    [agent_user_id]          INT             NOT NULL,
    [agent_name]             NVARCHAR (100)  NULL,
    [booking_name]           VARCHAR (300)   NOT NULL,
    [checkin_date]           DATETIME        NULL,
    [checkout_date]          DATETIME        NULL,
    [num_nights]             INT             NULL,
    [create_date]            DATETIME        NULL,
    [booking_date]           DATETIME        NULL,
    [status]                 INT             NULL,
    [booking_expiry_date]    DATETIME        NULL,
    [backoffice_user_id]     INT             NULL,
    [backoffice_user_name]   NVARCHAR (100)  NULL,
    [booking_ref]            NVARCHAR (32)   NULL,
    [adults]                 INT             NULL,
    [infants]                INT             NULL,
    [children]               INT             NULL,
    [teens]                  INT             NULL,
    [rooms]                  INT             NULL,
    [phone_number]           NVARCHAR (20)   NULL,
    [email]                  NVARCHAR (75)   NULL,
    [provider_id]            INT             NULL,
    [discount_amt]           FLOAT (53)      NULL,
    [discount_reason]        NVARCHAR (2000) NULL,
    [discounted_by_user_id]  INT             NULL,
    [booking_type]           INT             NULL,
    [package_id]             INT             NULL,
    [booking_amt]            FLOAT (53)      NULL,
    [markup_amt]             FLOAT (53)      NULL,
    [credit_amt]             FLOAT (53)      NULL,
    [paid_amt]               FLOAT (53)      NULL,
    [provider_name]          NVARCHAR (100)  NULL,
    [market_name]            NVARCHAR (50)   NULL,
    [booking_file_number]    NVARCHAR (100)  NULL,
    [package_name]           VARCHAR (100)   NULL,
    [free_nights]            INT             NULL,
    [remarks]                NVARCHAR (1000) NULL,
    [is_timelimit]           BIT             DEFAULT ((0)) NOT NULL,
    [is_new_timelimit]       BIT             DEFAULT ((0)) NOT NULL,
    [timelimit_date]         DATE            NULL,
    [currency_code]          CHAR (3)        NULL,
    [symbol]                 NVARCHAR (10)   NULL,
    [is_credit]              BIT             DEFAULT ((0)) NOT NULL,
    [compulsory_charges_amt] FLOAT (53)      NULL,
    [charge_name]            NVARCHAR (100)  NULL,
    [cancellation_fee]       FLOAT (53)      NULL,
    [is_allotted]            BIT             DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([booking_id] ASC),
    CONSTRAINT [FK_booking_backoffice_user_id] FOREIGN KEY ([backoffice_user_id]) REFERENCES [dbo].[user] ([user_id]),
    CONSTRAINT [FK_booking_booking_ref] FOREIGN KEY ([booking_ref]) REFERENCES [dbo].[booking_search] ([booking_ref]),
    CONSTRAINT [FK_booking_discount_user_id] FOREIGN KEY ([discounted_by_user_id]) REFERENCES [dbo].[user] ([user_id]),
    CONSTRAINT [FK_booking_user_id] FOREIGN KEY ([agent_user_id]) REFERENCES [dbo].[user] ([user_id])
);




















































GO
CREATE TRIGGER dbo.delTrigger_dbo_booking
    ON [dbo].[booking]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_hist]
    (
		teens,
		status,
		rooms,
		remarks,
		provider_name,
		provider_id,
		phone_number,
		paid_amt,
		package_name,
		package_id,
		num_nights,
		markup_amt,
		market_name,
		market_id,
		infants,
		free_nights,
		email,
		discounted_by_user_id,
		discount_reason,
		discount_amt,
		credit_amt,
		create_date,
		children,
		checkout_date,
		checkin_date,
		booking_type,
		booking_ref,
		booking_name,
		booking_id,
		booking_file_number,
		booking_expiry_date,
		booking_date,
		booking_amt,
		backoffice_user_name,
		backoffice_user_id,
		agent_user_id,
		agent_name,
		adults
--
	)
	SELECT 
		teens,
		status,
		rooms,
		remarks,
		provider_name,
		provider_id,
		phone_number,
		paid_amt,
		package_name,
		package_id,
		num_nights,
		markup_amt,
		market_name,
		market_id,
		infants,
		free_nights,
		email,
		discounted_by_user_id,
		discount_reason,
		discount_amt,
		credit_amt,
		create_date,
		children,
		checkout_date,
		checkin_date,
		booking_type,
		booking_ref,
		booking_name,
		booking_id,
		booking_file_number,
		booking_expiry_date,
		booking_date,
		booking_amt,
		backoffice_user_name,
		backoffice_user_id,
		agent_user_id,
		agent_name,
		adults
--
	FROM
        deleted;
END