CREATE TABLE [dbo].[activity_pricing] (
    [activity_pricing_id]               INT        IDENTITY (100, 1) NOT NULL,
    [activity_id]                       INT        NOT NULL,
    [currency_id]                       INT        NOT NULL,
    [provider_id]                       INT        NOT NULL,
    [price_child]                       FLOAT (53) NULL,
    [price_adult]                       FLOAT (53) NULL,
    [price_child_incl_sic_transfer]     FLOAT (53) NULL,
    [price_adult_incl_sic_transfer]     FLOAT (53) NULL,
    [price_child_incl_private_transfer] FLOAT (53) NULL,
    [price_adult_incl_private_transfer] FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([activity_id] ASC, [currency_id] ASC, [provider_id] ASC),
    CONSTRAINT [FK_activity_pricing_activity_id] FOREIGN KEY ([activity_id]) REFERENCES [dbo].[activity] ([activity_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_activity_pricing_currency_id] FOREIGN KEY ([currency_id]) REFERENCES [dbo].[currency] ([currency_id]),
    CONSTRAINT [FK_activity_pricing_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id])
);




















GO
CREATE TRIGGER dbo.delTrigger_dbo_activity_pricing
    ON [dbo].[activity_pricing]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[activity_pricing_hist]
    (
		provider_id,
		price_child_incl_sic_transfer,
		price_child_incl_private_transfer,
		price_child,
		price_adult_incl_sic_transfer,
		price_adult_incl_private_transfer,
		price_adult,
		currency_id,
		activity_pricing_id,
		activity_id
--
	)
	SELECT 
		provider_id,
		price_child_incl_sic_transfer,
		price_child_incl_private_transfer,
		price_child,
		price_adult_incl_sic_transfer,
		price_adult_incl_private_transfer,
		price_adult,
		currency_id,
		activity_pricing_id,
		activity_id
--
	FROM
        deleted;
END