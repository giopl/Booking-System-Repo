CREATE TABLE [dbo].[policy] (
    [policy_id]   INT            IDENTITY (100, 1) NOT NULL,
    [name]        NVARCHAR (50)  NULL,
    [description] NVARCHAR (300) NULL,
    [type]        NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([policy_id] ASC)
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_policy
    ON [dbo].[policy]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[policy_hist]
    (
		type,
		policy_id,
		name,
		description
--
	)
	SELECT 
		type,
		policy_id,
		name,
		description
--
	FROM
        deleted;
END