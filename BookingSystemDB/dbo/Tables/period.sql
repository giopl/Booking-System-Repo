﻿CREATE TABLE [dbo].[period] (
    [period_id]   INT           IDENTITY (100, 1) NOT NULL,
    [provider_id] INT           NOT NULL,
    [period_name] NVARCHAR (50) NULL,
    [start_date]  DATE          NULL,
    [end_date]    DATE          NULL,
    [is_active]   CHAR (1)      NULL,
    PRIMARY KEY CLUSTERED ([period_id] ASC),
    CONSTRAINT [fk_period_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id])
);

