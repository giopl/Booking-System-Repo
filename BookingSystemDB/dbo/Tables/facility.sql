CREATE TABLE [dbo].[facility] (
    [facility_id]   INT            IDENTITY (100, 1) NOT NULL,
    [provider_id]   INT            NOT NULL,
    [name]          NVARCHAR (100) NULL,
    [type]          NVARCHAR (100) NULL,
    [description]   NVARCHAR (300) NULL,
    [opening_hours] NVARCHAR (300) NULL,
    [conditions]    NVARCHAR (300) NULL,
    CONSTRAINT [PK_facility] PRIMARY KEY CLUSTERED ([facility_id] ASC),
    CONSTRAINT [FK_facility_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_facility
    ON [dbo].[facility]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[facility_hist]
    (
		type,
		provider_id,
		opening_hours,
		name,
		facility_id,
		description,
		conditions
--
	)
	SELECT 
		type,
		provider_id,
		opening_hours,
		name,
		facility_id,
		description,
		conditions
--
	FROM
        deleted;
END