CREATE TABLE [dbo].[booking_supplement] (
    [booking_supplement_id] INT            IDENTITY (100, 1) NOT NULL,
    [booking_id]            INT            NOT NULL,
    [supplement_id]         INT            NULL,
    [quantity]              INT            NULL,
    [price]                 FLOAT (53)     NULL,
    [booking_room_id]       INT            NULL,
    [infants]               INT            NULL,
    [children]              INT            NULL,
    [teens]                 INT            NULL,
    [adults]                INT            NULL,
    [markup_amt]            FLOAT (53)     NULL,
    [supplement_name]       NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([booking_supplement_id] ASC),
    CONSTRAINT [FK_booking_supplement_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_booking_supplement_booking_room_id] FOREIGN KEY ([booking_room_id]) REFERENCES [dbo].[booking_room] ([booking_room_id])
);


























GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_supplement
    ON [dbo].[booking_supplement]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_supplement_hist]
    (
		teens,
		supplement_name,
		supplement_id,
		quantity,
		price,
		markup_amt,
		infants,
		children,
		booking_supplement_id,
		booking_room_id,
		booking_id,
		adults
--
	)
	SELECT 
		teens,
		supplement_name,
		supplement_id,
		quantity,
		price,
		markup_amt,
		infants,
		children,
		booking_supplement_id,
		booking_room_id,
		booking_id,
		adults
--
	FROM
        deleted;
END