CREATE TABLE [dbo].[user_picture] (
    [picture_id]    INT          NOT NULL,
    [user_id]       INT          NOT NULL,
    [display_order] INT          NULL,
    [section]       VARCHAR (15) NULL,
    CONSTRAINT [PK_user_picture] PRIMARY KEY CLUSTERED ([user_id] ASC, [picture_id] ASC),
    CONSTRAINT [FK_user_picture_picture_id] FOREIGN KEY ([picture_id]) REFERENCES [dbo].[picture] ([picture_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_user_picture_user_id] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user] ([user_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_user_picture
    ON [dbo].[user_picture]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[user_picture_hist]
    (
		user_id,
		section,
		picture_id,
		display_order
--
	)
	SELECT 
		user_id,
		section,
		picture_id,
		display_order
--
	FROM
        deleted;
END