CREATE TABLE [dbo].[date] (
    [fulldate] DATE NOT NULL,
    PRIMARY KEY CLUSTERED ([fulldate] ASC)
);








GO
CREATE TRIGGER dbo.delTrigger_dbo_date
    ON [dbo].[date]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[date_hist]
    (
		fulldate
--
	)
	SELECT 
		fulldate
--
	FROM
        deleted;
END