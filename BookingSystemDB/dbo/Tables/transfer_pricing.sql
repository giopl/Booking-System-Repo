CREATE TABLE [dbo].[transfer_pricing] (
    [transfer_pricing_id] INT        IDENTITY (100, 1) NOT NULL,
    [transfer_id]         INT        NOT NULL,
    [currency_id]         INT        NOT NULL,
    [child_price]         FLOAT (53) NULL,
    [teen_price]          FLOAT (53) NULL,
    [adult_price]         FLOAT (53) NULL,
    [is_active]           BIT        DEFAULT ((1)) NOT NULL,
    [provider_id]         INT        NULL,
    [is_selling_price]    BIT        NOT NULL,
    CONSTRAINT [PK_transfer_pricing] PRIMARY KEY CLUSTERED ([transfer_pricing_id] ASC),
    CONSTRAINT [FK_transfer_pricing_currency_id] FOREIGN KEY ([currency_id]) REFERENCES [dbo].[currency] ([currency_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_transfer_pricing_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_transfer_pricing_transfer_id] FOREIGN KEY ([transfer_id]) REFERENCES [dbo].[transfer] ([transfer_id]) ON DELETE CASCADE
);


















GO
CREATE TRIGGER dbo.delTrigger_dbo_transfer_pricing
    ON [dbo].[transfer_pricing]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[transfer_pricing_hist]
    (
		transfer_pricing_id,
		transfer_id,
		teen_price,
		provider_id,
		is_selling_price,
		is_active,
		currency_id,
		child_price,
		adult_price
--
	)
	SELECT 
		transfer_pricing_id,
		transfer_id,
		teen_price,
		provider_id,
		is_selling_price,
		is_active,
		currency_id,
		child_price,
		adult_price
--
	FROM
        deleted;
END