﻿CREATE TABLE [dbo].[room] (
    [room_id]       INT            IDENTITY (100, 1) NOT NULL,
    [provider_id]   INT            NOT NULL,
    [room_name]     NVARCHAR (100) NULL,
    [description]   NVARCHAR (300) NULL,
    [min_occupancy] INT            NULL,
    [max_occupancy] INT            NULL,
    [is_active]     CHAR (1)       NULL,
    PRIMARY KEY CLUSTERED ([room_id] ASC),
    CONSTRAINT [fk_room_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id])
);

