
CREATE TABLE [dbo].[package_room] (
    [package_id] INT NOT NULL,
    [room_id]    INT NOT NULL,
    CONSTRAINT [PK_package_room] PRIMARY KEY CLUSTERED ([package_id] ASC, [room_id] ASC),
    CONSTRAINT [FK_package_market_room] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_package_room_package] FOREIGN KEY ([package_id]) REFERENCES [dbo].[package] ([package_id]) ON DELETE CASCADE
);








GO
CREATE TRIGGER dbo.delTrigger_dbo_package_room
    ON [dbo].[package_room]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[package_room_hist]
    (
		room_id,
		package_id
--
	)
	SELECT 
		room_id,
		package_id
--
	FROM
        deleted;
END