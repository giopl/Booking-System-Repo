CREATE TABLE [dbo].[vehicle_feature] (
    [feature_id] INT NOT NULL,
    [vehicle_id] INT NOT NULL,
    [quantity]   INT NULL,
    CONSTRAINT [PK_vehicle_feature] PRIMARY KEY CLUSTERED ([feature_id] ASC, [vehicle_id] ASC),
    CONSTRAINT [FK_vehicle_feature_feature_id] FOREIGN KEY ([feature_id]) REFERENCES [dbo].[feature] ([feature_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_vehicle_feature_vehicle_id] FOREIGN KEY ([vehicle_id]) REFERENCES [dbo].[vehicle] ([vehicle_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_vehicle_feature
    ON [dbo].[vehicle_feature]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[vehicle_feature_hist]
    (
		vehicle_id,
		quantity,
		feature_id
--
	)
	SELECT 
		vehicle_id,
		quantity,
		feature_id
--
	FROM
        deleted;
END