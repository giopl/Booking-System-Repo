CREATE TABLE [dbo].[promotion_set] (
    [promotion_set_id] INT            IDENTITY (1, 1) NOT NULL,
    [name]             NVARCHAR (100) NOT NULL,
    [description]      NVARCHAR (300) NULL,
    [provider_id]      INT            NOT NULL,
    [is_active]        BIT            DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([promotion_set_id] ASC),
    CONSTRAINT [FK_promo_set_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id])
);







GO
CREATE TRIGGER dbo.delTrigger_dbo_promotion_set
    ON [dbo].[promotion_set]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[promotion_set_hist]
    (
		provider_id,
		promotion_set_id,
		name,
		is_active,
		description
--
	)
	SELECT 
		provider_id,
		promotion_set_id,
		name,
		is_active,
		description
--
	FROM
        deleted;
END