CREATE TABLE [dbo].[supplement_pricing] (
    [supplement_pricing_id] INT        IDENTITY (100, 1) NOT NULL,
    [supplement_id]         INT        NOT NULL,
    [market_id]             INT        NOT NULL,
    [valid_from]            DATETIME   DEFAULT (getdate()) NULL,
    [valid_to]              DATETIME   DEFAULT ('2999-12-31') NULL,
    [price_per_item]        FLOAT (53) NULL,
    [price_infant]          FLOAT (53) NULL,
    [price_child]           FLOAT (53) NULL,
    [price_teen]            FLOAT (53) NULL,
    [price_adult]           FLOAT (53) NULL,
    [price_senior]          FLOAT (53) NULL,
    [price_honeymoon]       FLOAT (53) NULL,
    [is_active]             BIT        DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_supplement_pricing] PRIMARY KEY CLUSTERED ([supplement_pricing_id] ASC),
    CONSTRAINT [FK_supplement_id] FOREIGN KEY ([supplement_id]) REFERENCES [dbo].[supplement] ([supplement_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_supplement_market_id] FOREIGN KEY ([market_id]) REFERENCES [dbo].[market] ([market_id])
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_supplement_pricing
    ON [dbo].[supplement_pricing]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[supplement_pricing_hist]
    (
		valid_to,
		valid_from,
		supplement_pricing_id,
		supplement_id,
		price_teen,
		price_senior,
		price_per_item,
		price_infant,
		price_honeymoon,
		price_child,
		price_adult,
		market_id,
		is_active
--
	)
	SELECT 
		valid_to,
		valid_from,
		supplement_pricing_id,
		supplement_id,
		price_teen,
		price_senior,
		price_per_item,
		price_infant,
		price_honeymoon,
		price_child,
		price_adult,
		market_id,
		is_active
--
	FROM
        deleted;
END