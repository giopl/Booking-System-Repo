﻿CREATE TABLE [dbo].[room_capacity] (
    [room_capacity_id] INT      IDENTITY (100, 1) NOT NULL,
    [room_id]          INT      NOT NULL,
    [num_adult]        INT      NULL,
    [num_infant]       INT      NULL,
    [num_children]     INT      NULL,
    [num_teen]         INT      NULL,
    [is_active]        CHAR (1) NULL,
    PRIMARY KEY CLUSTERED ([room_capacity_id] ASC),
    CONSTRAINT [fk_room_capacity_room_id] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id])
);

