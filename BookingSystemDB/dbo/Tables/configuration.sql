﻿CREATE TABLE [dbo].[configuration] (
    [configuration_id] INT            IDENTITY (100, 1) NOT NULL,
    [conf_key]         NVARCHAR (50)  NULL,
    [conf_type]        NVARCHAR (50)  NULL,
    [conf_value]       NVARCHAR (300) NULL,
    [system_based]     BIT            DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([configuration_id] ASC)
);














GO

GO

CREATE UNIQUE INDEX [IX_configuration_keytype] ON [dbo].[configuration] (conf_key,conf_type)

GO
CREATE TRIGGER dbo.delTrigger_dbo_configuration
    ON [dbo].[configuration]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[configuration_hist]
    (
		system_based,
		configuration_id,
		conf_value,
		conf_type,
		conf_key
--
	)
	SELECT 
		system_based,
		configuration_id,
		conf_value,
		conf_type,
		conf_key
--
	FROM
        deleted;
END