﻿CREATE TABLE [dbo].[booking_service] (
    [booking_id]      INT NOT NULL,
    [guest_id]        INT NOT NULL,
    [service_rate_id] INT NULL,
    [units]           INT NULL,
    CONSTRAINT [fk_booking_service_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]),
    CONSTRAINT [fk_booking_service_person_id] FOREIGN KEY ([guest_id]) REFERENCES [dbo].[person] ([person_id])
);

