CREATE TABLE [dbo].[occupancy] (
    [occupancy_id] INT            IDENTITY (100, 1) NOT NULL,
    [name]         NVARCHAR (30)  NULL,
    [description]  NVARCHAR (MAX) NULL,
    [infant]       INT            NULL,
    [child]        INT            NULL,
    [teen]         INT            NULL,
    [adult]        INT            NULL,
    [senior]       INT            NULL,
    [without_bed]  INT            NULL,
    CONSTRAINT [PK_occupancy] PRIMARY KEY CLUSTERED ([occupancy_id] ASC)
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_occupancy
    ON [dbo].[occupancy]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[occupancy_hist]
    (
		without_bed,
		teen,
		senior,
		occupancy_id,
		name,
		infant,
		description,
		child,
		adult
--
	)
	SELECT 
		without_bed,
		teen,
		senior,
		occupancy_id,
		name,
		infant,
		description,
		child,
		adult
--
	FROM
        deleted;
END