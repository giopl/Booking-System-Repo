﻿CREATE TABLE [dbo].[service_rate] (
    [service_rate_id] INT        IDENTITY (100, 1) NOT NULL,
    [provider_id]     INT        NULL,
    [vehicle_id]      INT        NULL,
    [activity_id]     INT        NULL,
    [adult_rate]      FLOAT (53) NULL,
    [child_rate]      FLOAT (53) NULL,
    [commission]      FLOAT (53) NULL,
    [is_active]       CHAR (1)   NULL,
    [unit_rate]       FLOAT (53) NULL,
    [currency]        CHAR (3)   NULL,
    PRIMARY KEY CLUSTERED ([service_rate_id] ASC),
    CONSTRAINT [fk_service_rate_activity_id] FOREIGN KEY ([activity_id]) REFERENCES [dbo].[activity] ([activity_id]),
    CONSTRAINT [fk_service_rate_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]),
    CONSTRAINT [fk_service_rate_vehicle_id] FOREIGN KEY ([vehicle_id]) REFERENCES [dbo].[vehicle] ([vehicle_id])
);

