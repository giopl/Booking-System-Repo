CREATE TABLE [dbo].[package_activity] (
    [package_id]  INT NOT NULL,
    [activity_id] INT NOT NULL,
    CONSTRAINT [PK_package_activity] PRIMARY KEY CLUSTERED ([package_id] ASC, [activity_id] ASC),
    CONSTRAINT [FK_package_activity_activity_id] FOREIGN KEY ([activity_id]) REFERENCES [dbo].[activity] ([activity_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_package_activity_package_id] FOREIGN KEY ([package_id]) REFERENCES [dbo].[package] ([package_id])
);








GO
CREATE TRIGGER dbo.delTrigger_dbo_package_activity
    ON [dbo].[package_activity]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[package_activity_hist]
    (
		package_id,
		activity_id
--
	)
	SELECT 
		package_id,
		activity_id
--
	FROM
        deleted;
END