CREATE TABLE [dbo].[supplement_room] (
    [supplement_id] INT NOT NULL,
    [room_id]       INT NOT NULL,
    CONSTRAINT [PK_supplement_room] PRIMARY KEY CLUSTERED ([supplement_id] ASC, [room_id] ASC),
    CONSTRAINT [FK_supplement_room_ToRoom] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_supplement_room_Tosupplement] FOREIGN KEY ([supplement_id]) REFERENCES [dbo].[supplement] ([supplement_id]) ON DELETE CASCADE
);








GO
CREATE TRIGGER dbo.delTrigger_dbo_supplement_room
    ON [dbo].[supplement_room]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[supplement_room_hist]
    (
		supplement_id,
		room_id
--
	)
	SELECT 
		supplement_id,
		room_id
--
	FROM
        deleted;
END