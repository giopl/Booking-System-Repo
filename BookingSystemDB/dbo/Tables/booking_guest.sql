CREATE TABLE [dbo].[booking_guest] (
    [booking_guest_id]         INT            IDENTITY (100, 1) NOT NULL,
    [booking_room_id]          INT            NOT NULL,
    [package_id]               INT            NULL,
    [salutation]               VARCHAR (15)   NULL,
    [firstname]                VARCHAR (60)   NULL,
    [lastname]                 VARCHAR (60)   NULL,
    [date_of_birth]            DATE           NULL,
    [nationality]              VARCHAR (50)   NULL,
    [age]                      INT            NULL,
    [guest_type]               INT            NULL,
    [arrival_flight_no]        VARCHAR (20)   NULL,
    [departure_flight_no]      VARCHAR (20)   NULL,
    [arrival_datetime]         DATETIME       NULL,
    [departure_datetime]       DATETIME       NULL,
    [price_per_night]          FLOAT (53)     NULL,
    [markup_amt]               FLOAT (53)     NULL,
    [booking_ref]              VARCHAR (32)   NULL,
    [passport_number]          NVARCHAR (20)  NULL,
    [passport_expiry_date]     DATE           NULL,
    [gender]                   INT            NULL,
    [booking_id]               INT            NULL,
    [package_name]             NVARCHAR (100) NULL,
    [compulsory_charge_amt]    FLOAT (53)     NULL,
    [ground_handling_amt]      FLOAT (53)     NULL,
    [promotional_discount_amt] FLOAT (53)     NULL,
    PRIMARY KEY CLUSTERED ([booking_guest_id] ASC),
    CONSTRAINT [FK_booking_guest_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_booking_guest_booking_room_id] FOREIGN KEY ([booking_room_id]) REFERENCES [dbo].[booking_room] ([booking_room_id])
);




























GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_guest
    ON [dbo].[booking_guest]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_guest_hist]
    (
		salutation,
		promotional_discount_amt,
		price_per_night,
		passport_number,
		passport_expiry_date,
		package_name,
		package_id,
		nationality,
		markup_amt,
		lastname,
		guest_type,
		ground_handling_amt,
		gender,
		firstname,
		departure_flight_no,
		departure_datetime,
		date_of_birth,
		compulsory_charge_amt,
		booking_room_id,
		booking_ref,
		booking_id,
		booking_guest_id,
		arrival_flight_no,
		arrival_datetime,
		age
--
	)
	SELECT 
		salutation,
		promotional_discount_amt,
		price_per_night,
		passport_number,
		passport_expiry_date,
		package_name,
		package_id,
		nationality,
		markup_amt,
		lastname,
		guest_type,
		ground_handling_amt,
		gender,
		firstname,
		departure_flight_no,
		departure_datetime,
		date_of_birth,
		compulsory_charge_amt,
		booking_room_id,
		booking_ref,
		booking_id,
		booking_guest_id,
		arrival_flight_no,
		arrival_datetime,
		age
--
	FROM
        deleted;
END