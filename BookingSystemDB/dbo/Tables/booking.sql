﻿CREATE TABLE [dbo].[booking] (
    [booking_id]       INT           IDENTITY (100, 1) NOT NULL,
    [agent_id]         INT           NOT NULL,
    [arrival_time]     DATETIME      NULL,
    [departure_time]   DATETIME      NULL,
    [flight_number]    NVARCHAR (40) NULL,
    [num_adults]       INT           NULL,
    [num_children]     INT           NULL,
    [payment_due_date] DATE          NULL,
    [booking_status]   NVARCHAR (30) NULL,
    [customer_id]      INT           NULL,
    PRIMARY KEY CLUSTERED ([booking_id] ASC),
    CONSTRAINT [fk_booking_agent_id] FOREIGN KEY ([agent_id]) REFERENCES [dbo].[person] ([person_id]),
    CONSTRAINT [fk_booking_customer_id] FOREIGN KEY ([customer_id]) REFERENCES [dbo].[person] ([person_id])
);

