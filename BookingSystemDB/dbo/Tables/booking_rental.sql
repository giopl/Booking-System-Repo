CREATE TABLE [dbo].[booking_rental] (
    [booking_rental_id] INT            IDENTITY (100, 1) NOT NULL,
    [booking_id]        INT            NOT NULL,
    [vehicle_id]        INT            NOT NULL,
    [booking_guest_id]  INT            NULL,
    [num_days]          FLOAT (53)     NULL,
    [total_price]       FLOAT (53)     NULL,
    [markup_amt]        FLOAT (53)     NULL,
    [pickup_date]       DATETIME       NULL,
    [return_date]       DATETIME       NULL,
    [pickup_location]   VARCHAR (100)  NULL,
    [return_location]   VARCHAR (100)  NULL,
    [is_aftersales]     BIT            DEFAULT ((0)) NOT NULL,
    [aftersales_admin]  INT            NULL,
    [has_driver]        BIT            DEFAULT ((0)) NOT NULL,
    [provider_name]     NVARCHAR (100) NULL,
    [vehicle_model]     NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([booking_rental_id] ASC),
    CONSTRAINT [FK_booking_rental_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]) ON DELETE CASCADE
);


















GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_rental
    ON [dbo].[booking_rental]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_rental_hist]
    (
		vehicle_model,
		vehicle_id,
		total_price,
		return_location,
		return_date,
		provider_name,
		pickup_location,
		pickup_date,
		num_days,
		markup_amt,
		is_aftersales,
		has_driver,
		booking_rental_id,
		booking_id,
		booking_guest_id,
		aftersales_admin
--
	)
	SELECT 
		vehicle_model,
		vehicle_id,
		total_price,
		return_location,
		return_date,
		provider_name,
		pickup_location,
		pickup_date,
		num_days,
		markup_amt,
		is_aftersales,
		has_driver,
		booking_rental_id,
		booking_id,
		booking_guest_id,
		aftersales_admin
--
	FROM
        deleted;
END