CREATE TABLE [dbo].[transfer] (
    [transfer_id] INT            IDENTITY (100, 1) NOT NULL,
    [name]        NVARCHAR (100) NOT NULL,
    [description] NVARCHAR (MAX) NULL,
    [is_active]   BIT            DEFAULT ((1)) NOT NULL,
    [category]    NVARCHAR (100) NULL,
    [is_upgrade]  BIT            DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([transfer_id] ASC)
);


























GO
CREATE TRIGGER dbo.delTrigger_dbo_transfer
    ON [dbo].[transfer]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[transfer_hist]
    (
		transfer_id,
		name,
		is_upgrade,
		is_active,
		description,
		category
--
	)
	SELECT 
		transfer_id,
		name,
		is_upgrade,
		is_active,
		description,
		category
--
	FROM
        deleted;
END