CREATE TABLE [dbo].[vehicle_pricing] (
    [vehicle_pricing_id]         INT        IDENTITY (100, 1) NOT NULL,
    [vehicle_id]                 INT        NOT NULL,
    [currency_id]                INT        NOT NULL,
    [half_day_price_with_driver] FLOAT (53) NULL,
    [half_day_price]             FLOAT (53) NULL,
    [daily_price_with_driver]    FLOAT (53) NULL,
    [daily_price]                FLOAT (53) NULL,
    [is_active]                  BIT        DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_vehicle_pricing] PRIMARY KEY CLUSTERED ([vehicle_pricing_id] ASC),
    CONSTRAINT [FK_vehicle_pricing_currency_id] FOREIGN KEY ([currency_id]) REFERENCES [dbo].[currency] ([currency_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_vehicle_pricing_vehicle_id] FOREIGN KEY ([vehicle_id]) REFERENCES [dbo].[vehicle] ([vehicle_id]) ON DELETE CASCADE
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_vehicle_pricing
    ON [dbo].[vehicle_pricing]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[vehicle_pricing_hist]
    (
		vehicle_pricing_id,
		vehicle_id,
		is_active,
		half_day_price_with_driver,
		half_day_price,
		daily_price_with_driver,
		daily_price,
		currency_id
--
	)
	SELECT 
		vehicle_pricing_id,
		vehicle_id,
		is_active,
		half_day_price_with_driver,
		half_day_price,
		daily_price_with_driver,
		daily_price,
		currency_id
--
	FROM
        deleted;
END