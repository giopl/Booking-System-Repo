﻿/*CREATE TABLE [dbo].[supplement_package] (
    [supplement_id] INT NOT NULL,
    [package_id]    INT NOT NULL,
    CONSTRAINT [PK_supplement_package] PRIMARY KEY CLUSTERED ([supplement_id] ASC, [package_id] ASC),
    CONSTRAINT [FK_supplement_package_package_id] FOREIGN KEY ([package_id]) REFERENCES [dbo].[package] ([package_id]) on delete cascade,
    CONSTRAINT [FK_supplement_package_supplement_id] FOREIGN KEY ([supplement_id]) REFERENCES [dbo].[supplement] ([supplement_id]) on delete cascade
);




GO
CREATE TRIGGER dbo.delTrigger_dbo_supplement_package
    ON [dbo].[supplement_package]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[supplement_package_hist]
    (
		supplement_id,
		package_id
--
	)
	SELECT 
		supplement_id,
		package_id
--
	FROM
        deleted;
END*/