CREATE TABLE [dbo].[booking_proforma] (
    [booking_proforma_id]     INT             IDENTITY (100, 1) NOT NULL,
    [booking_id]              INT             NOT NULL,
    [invoice_num]             NVARCHAR (50)   NULL,
    [agent_details]           NVARCHAR (500)  NULL,
    [booking_details]         NVARCHAR (1500) NULL,
    [reference]               NVARCHAR (50)   NULL,
    [due_date]                DATE            NULL,
    [sales_category]          NVARCHAR (200)  NULL,
    [sales_description]       NVARCHAR (1000) NULL,
    [currency]                NVARCHAR (10)   NULL,
    [nett_price_accomodation] FLOAT (53)      NULL,
    [nett_price_supplement]   FLOAT (53)      NULL,
    [nett_price_transfer]     FLOAT (53)      NULL,
    [nett_price_activity]     FLOAT (53)      NULL,
    [nett_price_rental]       FLOAT (53)      NULL,
    [nett_price]              FLOAT (53)      NULL,
    [handling_fees]           FLOAT (53)      NULL,
    [sold_by]                 NVARCHAR (100)  NULL,
    [issued_by]               NVARCHAR (100)  NULL,
    [checked_by]              NVARCHAR (100)  NULL,
    [approved_by]             NVARCHAR (100)  NULL,
    [remarks]                 NVARCHAR (1000) NULL,
    [issue_date]              DATE            NULL,
    [finalized]               BIT             DEFAULT ((0)) NOT NULL,
    [bankcharges]             FLOAT (53)      NULL,
    PRIMARY KEY CLUSTERED ([booking_proforma_id] ASC),
    CONSTRAINT [FK_booking_proforma_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_proforma
    ON [dbo].[booking_proforma]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_proforma_hist]
    (
		sold_by,
		sales_description,
		sales_category,
		remarks,
		reference,
		nett_price_transfer,
		nett_price_supplement,
		nett_price_rental,
		nett_price_activity,
		nett_price_accomodation,
		nett_price,
		issued_by,
		issue_date,
		invoice_num,
		handling_fees,
		finalized,
		due_date,
		currency,
		checked_by,
		booking_proforma_id,
		booking_id,
		booking_details,
		approved_by,
		agent_details
--
	)
	SELECT 
		sold_by,
		sales_description,
		sales_category,
		remarks,
		reference,
		nett_price_transfer,
		nett_price_supplement,
		nett_price_rental,
		nett_price_activity,
		nett_price_accomodation,
		nett_price,
		issued_by,
		issue_date,
		invoice_num,
		handling_fees,
		finalized,
		due_date,
		currency,
		checked_by,
		booking_proforma_id,
		booking_id,
		booking_details,
		approved_by,
		agent_details
--
	FROM
        deleted;
END