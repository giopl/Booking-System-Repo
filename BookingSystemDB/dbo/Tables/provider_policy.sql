CREATE TABLE [dbo].[provider_policy] (
    [provider_id] INT NOT NULL,
    [policy_id]   INT NOT NULL,
    CONSTRAINT [PK_provider_policy] PRIMARY KEY CLUSTERED ([provider_id] ASC, [policy_id] ASC),
    CONSTRAINT [FK_provider_policy_policy_id] FOREIGN KEY ([policy_id]) REFERENCES [dbo].[policy] ([policy_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_provider_policy_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE
);








GO
CREATE TRIGGER dbo.delTrigger_dbo_provider_policy
    ON [dbo].[provider_policy]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[provider_policy_hist]
    (
		provider_id,
		policy_id
--
	)
	SELECT 
		provider_id,
		policy_id
--
	FROM
        deleted;
END