CREATE TABLE [dbo].[package] (
    [package_id]                  INT            IDENTITY (100, 1) NOT NULL,
    [provider_id]                 INT            NOT NULL,
    [market_id]                   INT            DEFAULT ((100)) NOT NULL,
    [valid_from]                  DATE           NOT NULL,
    [valid_to]                    DATE           NOT NULL,
    [name]                        NVARCHAR (100) NULL,
    [description]                 NVARCHAR (MAX) NULL,
    [number_nights]               INT            NULL,
    [price_ground_handling_adult] FLOAT (53)     NULL,
    [price_ground_handling_child] FLOAT (53)     NULL,
    [price_ground_handling_teen]  FLOAT (53)     NULL,
    [contracted_offer]            NVARCHAR (MAX) NULL,
    [cancellation_policy]         NVARCHAR (MAX) NULL,
    [apply_all_rooms]             BIT            DEFAULT ((0)) NOT NULL,
    [is_active]                   BIT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_package] PRIMARY KEY CLUSTERED ([package_id] ASC),
    CONSTRAINT [FK_package_market] FOREIGN KEY ([market_id]) REFERENCES [dbo].[market] ([market_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_package_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id])
);






--p







GO
CREATE TRIGGER dbo.delTrigger_dbo_package
    ON [dbo].[package]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[package_hist]
    (
		valid_to,
		valid_from,
		provider_id,
		price_ground_handling_teen,
		price_ground_handling_child,
		price_ground_handling_adult,
		package_id,
		number_nights,
		name,
		market_id,
		is_active,
		description,
		contracted_offer,
		cancellation_policy,
		apply_all_rooms
--
	)
	SELECT 
		valid_to,
		valid_from,
		provider_id,
		price_ground_handling_teen,
		price_ground_handling_child,
		price_ground_handling_adult,
		package_id,
		number_nights,
		name,
		market_id,
		is_active,
		description,
		contracted_offer,
		cancellation_policy,
		apply_all_rooms
--
	FROM
        deleted;
END