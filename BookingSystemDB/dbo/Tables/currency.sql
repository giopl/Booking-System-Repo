CREATE TABLE [dbo].[currency] (
    [currency_id]   INT           IDENTITY (100, 1) NOT NULL,
    [currency_code] CHAR (3)      NOT NULL,
    [currency_desc] NVARCHAR (50) NULL,
    [symbol]        NVARCHAR (10) NULL,
    [code_desc]     AS            (([currency_code]+' - ')+[currency_desc]),
    [is_active]     BIT           DEFAULT ((1)) NOT NULL,
    [exchange_rate] FLOAT (53)    NULL,
    PRIMARY KEY CLUSTERED ([currency_id] ASC)
);


















GO
CREATE TRIGGER dbo.delTrigger_dbo_currency
    ON [dbo].[currency]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[currency_hist]
    (
		symbol,
		is_active,
		exchange_rate,
		currency_id,
		currency_desc,
		currency_code,
		code_desc
--
	)
	SELECT 
		symbol,
		is_active,
		exchange_rate,
		currency_id,
		currency_desc,
		currency_code,
		code_desc
--
	FROM
        deleted;
END