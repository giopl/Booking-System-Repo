CREATE TABLE [dbo].[promotion_market] (
    [promotion_id] INT NOT NULL,
    [market_id]    INT NOT NULL,
    CONSTRAINT [PK_promo_market] PRIMARY KEY CLUSTERED ([promotion_id] ASC, [market_id] ASC),
    CONSTRAINT [FK_promo_market_market] FOREIGN KEY ([market_id]) REFERENCES [dbo].[market] ([market_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_promo_market_promo] FOREIGN KEY ([promotion_id]) REFERENCES [dbo].[promotion] ([promotion_id]) ON DELETE CASCADE
);








GO
CREATE TRIGGER dbo.delTrigger_dbo_promotion_market
    ON [dbo].[promotion_market]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[promotion_market_hist]
    (
		promotion_id,
		market_id
--
	)
	SELECT 
		promotion_id,
		market_id
--
	FROM
        deleted;
END