CREATE TABLE [dbo].[room_picture] (
    [picture_id]    INT          NOT NULL,
    [room_id]       INT          NOT NULL,
    [display_order] INT          NULL,
    [section]       VARCHAR (15) NULL,
    CONSTRAINT [PK_room_picture] PRIMARY KEY CLUSTERED ([room_id] ASC, [picture_id] ASC),
    CONSTRAINT [FK_room_picture_picture_id] FOREIGN KEY ([picture_id]) REFERENCES [dbo].[picture] ([picture_id]),
    CONSTRAINT [FK_room_picture_room_id] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_room_picture
    ON [dbo].[room_picture]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[room_picture_hist]
    (
		section,
		room_id,
		picture_id,
		display_order
--
	)
	SELECT 
		section,
		room_id,
		picture_id,
		display_order
--
	FROM
        deleted;
END