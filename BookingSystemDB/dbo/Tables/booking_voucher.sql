﻿CREATE TABLE [dbo].[booking_voucher] (
    [booking_voucher_id]  INT             IDENTITY (100, 1) NOT NULL,
    [booking_id]          INT             NOT NULL,
    [booking_file_number] NVARCHAR (100)  NULL,
    [guest_names]         NVARCHAR (1000) NULL,
    [num_guest]           INT             NULL,
    [flight_details]      NVARCHAR (200)  NULL,
    [hotel_name]          NVARCHAR (150)  NULL,
    [hotel_address]       NVARCHAR (350)  NULL,
    [room_types]          NVARCHAR (300)  NULL,
    [check_in]            NVARCHAR (100)  NULL,
    [check_out]           NVARCHAR (100)  NULL,
    [num_nights]          INT             NULL,
    [services_details]    NVARCHAR (4000) NULL,
    [offer]               NVARCHAR (150)  NULL,
    [remarks]             NVARCHAR (1000) NULL,
    [cost_per_pax]        NVARCHAR (500)  NULL,
    [total_cost]          NVARCHAR (150)  NULL,
    [note]                NVARCHAR (2000) NULL,
    [disclaimer]          NVARCHAR (2000) NULL,
    [package_name]        NVARCHAR (200)  NULL,
    [package_description] NVARCHAR (MAX)  NULL,
    [finalized]           BIT             DEFAULT ((0)) NOT NULL,
    [cancellation_policy] VARCHAR (MAX)   NULL,
    [occupation_summary]  VARCHAR (75)    NULL,
    PRIMARY KEY CLUSTERED ([booking_voucher_id] ASC),
    CONSTRAINT [FK_booking_voucher_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_voucher
    ON [dbo].[booking_voucher]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_voucher_hist]
    (
		total_cost,
		services_details,
		room_types,
		remarks,
		package_name,
		package_description,
		offer,
		num_nights,
		num_guest,
		note,
		hotel_name,
		hotel_address,
		guest_names,
		flight_details,
		finalized,
		disclaimer,
		cost_per_pax,
		check_out,
		check_in,
		booking_voucher_id,
		booking_id,
		booking_file_number
--
	)
	SELECT 
		total_cost,
		services_details,
		room_types,
		remarks,
		package_name,
		package_description,
		offer,
		num_nights,
		num_guest,
		note,
		hotel_name,
		hotel_address,
		guest_names,
		flight_details,
		finalized,
		disclaimer,
		cost_per_pax,
		check_out,
		check_in,
		booking_voucher_id,
		booking_id,
		booking_file_number
--
	FROM
        deleted;
END