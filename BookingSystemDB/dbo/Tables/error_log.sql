﻿CREATE TABLE [dbo].[error_log] (
    [log_id]          INT            IDENTITY (100, 1) NOT NULL,
    [user_id]         INT            NULL,
    [user_name]       VARCHAR (100)  NULL,
    [controller]      NVARCHAR (100) NULL,
    [action]          NVARCHAR (50)  NULL,
    [inner_exception] VARCHAR (MAX)  NULL,
    [message]         NVARCHAR (300) NULL,
    [stack_trace]     VARCHAR (MAX)  NULL,
    [error_date_time] DATETIME       DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([log_id] ASC)
);

