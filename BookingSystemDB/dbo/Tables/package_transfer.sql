CREATE TABLE [dbo].[package_transfer] (
    [package_id]  INT NOT NULL,
    [transfer_id] INT NOT NULL,
    CONSTRAINT [PK_package_transfer] PRIMARY KEY CLUSTERED ([package_id] ASC, [transfer_id] ASC),
    CONSTRAINT [FK_package_transfer_package_id] FOREIGN KEY ([package_id]) REFERENCES [dbo].[package] ([package_id]),
    CONSTRAINT [FK_package_transfer_transfer_id] FOREIGN KEY ([transfer_id]) REFERENCES [dbo].[transfer] ([transfer_id]) ON DELETE CASCADE
);








GO
CREATE TRIGGER dbo.delTrigger_dbo_package_transfer
    ON [dbo].[package_transfer]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[package_transfer_hist]
    (
		transfer_id,
		package_id
--
	)
	SELECT 
		transfer_id,
		package_id
--
	FROM
        deleted;
END