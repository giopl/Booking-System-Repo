﻿CREATE TABLE [dbo].[provider] (
    [provider_id]         INT            IDENTITY (100, 1) NOT NULL,
    [name]                NVARCHAR (100) NULL,
    [description]         NVARCHAR (MAX) NULL,
    [location]            NVARCHAR (100) NULL,
    [company]             NVARCHAR (100) NULL,
    [website]             NVARCHAR (100) NULL,
    [commission_rate]     FLOAT (53)     NULL,
    [street1]             NVARCHAR (100) NULL,
    [street2]             NVARCHAR (100) NULL,
    [town]                NVARCHAR (100) NULL,
    [country]             NVARCHAR (100) NULL,
    [phone1]              NVARCHAR (50)  NULL,
    [phone2]              NVARCHAR (50)  NULL,
    [fax]                 NVARCHAR (50)  NULL,
    [email]               NVARCHAR (75)  NULL,
    [google_location]     NVARCHAR (50)  NULL,
    [provider_type]       CHAR (1)       NOT NULL,
    [postcode]            VARCHAR (20)   NULL,
    [is_active]           BIT            NOT NULL,
    [star_rating]         INT            NULL,
    [cancellation_policy] NVARCHAR (MAX) NULL,
    CONSTRAINT [PK_provider] PRIMARY KEY CLUSTERED ([provider_id] ASC)
);






















GO
CREATE TRIGGER [dbo].[delTrigger_dbo_provider]
    ON [dbo].[provider]
   INSTEAD OF  DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

--first of all copy the provider record in hist


BEGIN TRANSACTION


    INSERT del.[provider_hist]
    (
		website,
		town,
		street2,
		street1,
		star_rating,
		provider_type,
		provider_id,
		postcode,
		phone2,
		phone1,
		name,
		location,
		is_active,
		google_location,
		fax,
		email,
		description,
		country,
		company,
		commission_rate
--
	)
	SELECT 
		website,
		town,
		street2,
		street1,
		star_rating,
		provider_type,
		provider_id,
		postcode,
		phone2,
		phone1,
		name,
		location,
		is_active,
		google_location,
		fax,
		email,
		description,
		country,
		company,
		commission_rate
--
	FROM
        deleted;

	BEGIN TRY
		delete from dbo.package               where provider_id in (select provider_id from deleted);
		delete from dbo.promotion             where provider_id in (select provider_id from deleted);
			delete from dbo.supplement            where provider_id in (select provider_id from deleted);
			delete from dbo.room                  where provider_id in (select provider_id from deleted);
		--	delete from dbo.activity              where provider_id in (select provider_id from deleted);
		delete from dbo.featured_hotel        where provider_id in (select provider_id from deleted);
		delete from dbo.transfer_pricing      where provider_id in (select provider_id from deleted);
		delete from dbo.contact               where provider_id in (select provider_id from deleted);
		delete from dbo.facility              where provider_id in (select provider_id from deleted);
		delete from dbo.file_upload           where provider_id in (select provider_id from deleted);
		delete from dbo.guest_type            where provider_id in (select provider_id from deleted);
		delete from dbo.hotel_feature         where provider_id in (select provider_id from deleted);
		delete from dbo.market                where provider_id in (select provider_id from deleted);
		
		delete from dbo.provider_picture      where provider_id in (select provider_id from deleted);
		
		delete from dbo.provider_policy       where provider_id in (select provider_id from deleted);
		
	
		delete from dbo.provider              where provider_id in (select provider_id from deleted);	
	
		
		END TRY
	BEGIN CATCH
	 declare @ErrorMessage nvarchar(max), @ErrorSeverity int, @ErrorState int;
    select @ErrorMessage = ERROR_MESSAGE() + ' Line ' + cast(ERROR_LINE() as nvarchar(5)), @ErrorSeverity = ERROR_SEVERITY(), @ErrorState = ERROR_STATE();

IF @@TRANCOUNT > 0
	  
        ROLLBACK TRANSACTION ;
raiserror (@ErrorMessage, @ErrorSeverity, @ErrorState);		
END CATCH
IF @@TRANCOUNT > 0
    COMMIT TRANSACTION ;
END