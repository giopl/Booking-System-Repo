CREATE TABLE [dbo].[feature] (
    [feature_id]   INT            IDENTITY (100, 1) NOT NULL,
    [category]     NVARCHAR (300) NULL,
    [description]  NVARCHAR (300) NULL,
    [feature_type] NCHAR (1)      NULL,
    [name]         NVARCHAR (100) NOT NULL,
    [icon]         NVARCHAR (100) NULL,
    [is_active]    BIT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_feature] PRIMARY KEY CLUSTERED ([feature_id] ASC)
);
















GO
CREATE TRIGGER dbo.delTrigger_dbo_feature
    ON [dbo].[feature]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[feature_hist]
    (
		name,
		is_active,
		icon,
		feature_type,
		feature_id,
		description,
		category
--
	)
	SELECT 
		name,
		is_active,
		icon,
		feature_type,
		feature_id,
		description,
		category
--
	FROM
        deleted;
END