CREATE TABLE [dbo].[file_upload] (
    [file_id]       INT              IDENTITY (100, 1) NOT NULL,
    [provider_id]   INT              NULL,
    [filename]      NVARCHAR (200)   NULL,
    [title]         NVARCHAR (100)   NULL,
    [description]   NVARCHAR (500)   NULL,
    [filetype]      NVARCHAR (50)    NULL,
    [file_guid]     UNIQUEIDENTIFIER NULL,
    [display_order] INT              NULL,
    PRIMARY KEY CLUSTERED ([file_id] ASC),
    CONSTRAINT [FK_file_upload_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_file_upload
    ON [dbo].[file_upload]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[file_upload_hist]
    (
		title,
		provider_id,
		filetype,
		filename,
		file_id,
		file_guid,
		display_order,
		description
--
	)
	SELECT 
		title,
		provider_id,
		filetype,
		filename,
		file_id,
		file_guid,
		display_order,
		description
--
	FROM
        deleted;
END