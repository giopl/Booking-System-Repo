CREATE TABLE [dbo].[room_feature] (
    [feature_id] INT NOT NULL,
    [room_id]    INT NOT NULL,
    [quantity]   INT NULL,
    CONSTRAINT [PK_room_feature] PRIMARY KEY CLUSTERED ([feature_id] ASC, [room_id] ASC),
    CONSTRAINT [FK_room_feature_feature_id] FOREIGN KEY ([feature_id]) REFERENCES [dbo].[feature] ([feature_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_room_feature_room_id] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id]) ON DELETE CASCADE
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_room_feature
    ON [dbo].[room_feature]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[room_feature_hist]
    (
		room_id,
		quantity,
		feature_id
--
	)
	SELECT 
		room_id,
		quantity,
		feature_id
--
	FROM
        deleted;
END