CREATE TABLE [dbo].[audit] (
    [audit_id]    BIGINT         IDENTITY (1, 1) NOT NULL,
    [user_id]     INT            NULL,
    [entity_type] NVARCHAR (100) NULL,
    [entity_id]   INT            NULL,
    [operation]   NVARCHAR (30)  NULL,
    [description] NVARCHAR (255) NULL,
    [created_by]  INT            NULL,
    [create_date] DATETIME       NOT NULL,
    CONSTRAINT [PK_dbo.AuditEntries] PRIMARY KEY CLUSTERED ([audit_id] ASC)
);









GO
CREATE TRIGGER dbo.delTrigger_dbo_audit
    ON [dbo].[audit]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[audit_hist]
    (
		user_id,
		operation,
		entity_type,
		entity_id,
		description,
		created_by,
		create_date,
		audit_id
--
	)
	SELECT 
		user_id,
		operation,
		entity_type,
		entity_id,
		description,
		created_by,
		create_date,
		audit_id
--
	FROM
        deleted;
END