CREATE TABLE [dbo].[user] (
    [user_id]              INT            IDENTITY (100, 1) NOT NULL,
    [title]                NVARCHAR (20)  NULL,
    [firstname]            NVARCHAR (50)  NULL,
    [lastname]             NVARCHAR (50)  NULL,
    [date_of_birth]        DATE           NULL,
    [gender]               INT            NULL,
    [nationality]          NVARCHAR (60)  NULL,
    [role]                 INT            NOT NULL,
    [officephone]          NVARCHAR (20)  NULL,
    [mobile]               NVARCHAR (20)  NULL,
    [email]                NVARCHAR (100) NULL,
    [company]              NVARCHAR (100) NULL,
    [job_title]            NVARCHAR (100) NULL,
    [password]             NVARCHAR (200) NULL,
    [markup_percentage]    FLOAT (53)     NULL,
    [markup_fixed]         FLOAT (53)     NULL,
    [username]             NVARCHAR (50)  NULL,
    [credit_limit]         FLOAT (53)     NULL,
    [credit_used]          FLOAT (53)     NULL,
    [is_active]            BIT            DEFAULT ((1)) NOT NULL,
    [temp_password_expiry] DATE           NULL,
    [temp_password]        NVARCHAR (200) NULL,
    [is_credit_based]      BIT            DEFAULT ((0)) NOT NULL,
    [currency_id]          INT            DEFAULT ((100)) NOT NULL,
    CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED ([user_id] ASC),
    CONSTRAINT [FK_user_credit_currency_id] FOREIGN KEY ([currency_id]) REFERENCES [dbo].[currency] ([currency_id])
);




























GO
CREATE TRIGGER dbo.delTrigger_dbo_user
    ON [dbo].[user]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[user_hist]
    (
		username,
		user_id,
		title,
		temp_password_expiry,
		temp_password,
		role,
		password,
		officephone,
		nationality,
		mobile,
		markup_percentage,
		markup_fixed,
		lastname,
		job_title,
		is_credit_based,
		is_active,
		gender,
		firstname,
		email,
		date_of_birth,
		currency_id,
		credit_used,
		credit_limit,
		company
--
	)
	SELECT 
		username,
		user_id,
		title,
		temp_password_expiry,
		temp_password,
		role,
		password,
		officephone,
		nationality,
		mobile,
		markup_percentage,
		markup_fixed,
		lastname,
		job_title,
		is_credit_based,
		is_active,
		gender,
		firstname,
		email,
		date_of_birth,
		currency_id,
		credit_used,
		credit_limit,
		company
--
	FROM
        deleted;
END