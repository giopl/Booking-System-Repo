CREATE TABLE [dbo].[booking_payment] (
    [booking_payment_id] INT             IDENTITY (100, 1) NOT NULL,
    [booking_id]         INT             NOT NULL,
    [payment_amt]        FLOAT (53)      NULL,
    [payment_date]       DATETIME        NULL,
    [payment_method]     INT             NULL,
    [payment_details]    NVARCHAR (1000) NULL,
    [received_by]        INT             NULL,
    PRIMARY KEY CLUSTERED ([booking_payment_id] ASC),
    CONSTRAINT [FK_booking_payment_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_payment
    ON [dbo].[booking_payment]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_payment_hist]
    (
		received_by,
		payment_method,
		payment_details,
		payment_date,
		payment_amt,
		booking_payment_id,
		booking_id
--
	)
	SELECT 
		received_by,
		payment_method,
		payment_details,
		payment_date,
		payment_amt,
		booking_payment_id,
		booking_id
--
	FROM
        deleted;
END