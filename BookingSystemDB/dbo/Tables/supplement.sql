CREATE TABLE [dbo].[supplement] (
    [supplement_id]   INT            IDENTITY (100, 1) NOT NULL,
    [provider_id]     INT            NULL,
    [name]            NVARCHAR (100) NULL,
    [description]     NVARCHAR (MAX) NULL,
    [is_per_person]   BIT            DEFAULT ((0)) NOT NULL,
    [is_per_room]     BIT            DEFAULT ((0)) NOT NULL,
    [is_per_night]    BIT            DEFAULT ((0)) NOT NULL,
    [is_per_item]     BIT            DEFAULT ((0)) NOT NULL,
    [is_compulsory]   BIT            DEFAULT ((0)) NOT NULL,
    [valid_all_rooms] BIT            DEFAULT ((0)) NOT NULL,
    [is_active]       BIT            DEFAULT ((1)) NOT NULL,
    [meal_plan_id]    INT            NULL,
    PRIMARY KEY CLUSTERED ([supplement_id] ASC),
    CONSTRAINT [FK_supplement_meal_plan_id] FOREIGN KEY ([meal_plan_id]) REFERENCES [dbo].[meal_plan] ([meal_plan_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_supplement_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE
);














GO
CREATE TRIGGER dbo.delTrigger_dbo_supplement
    ON [dbo].[supplement]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[supplement_hist]
    (
		valid_all_rooms,
		supplement_id,
		provider_id,
		name,
		meal_plan_id,
		is_per_room,
		is_per_person,
		is_per_night,
		is_per_item,
		is_compulsory,
		is_active,
		description
--
	)
	SELECT 
		valid_all_rooms,
		supplement_id,
		provider_id,
		name,
		meal_plan_id,
		is_per_room,
		is_per_person,
		is_per_night,
		is_per_item,
		is_compulsory,
		is_active,
		description
--
	FROM
        deleted;
END