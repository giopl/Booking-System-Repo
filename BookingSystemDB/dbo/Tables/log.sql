CREATE TABLE [dbo].[log] (
    [log_id]      INT            IDENTITY (100, 1) NOT NULL,
    [user_id]     INT            NOT NULL,
    [operation]   NVARCHAR (30)  NULL,
    [item_type]   NVARCHAR (50)  NULL,
    [item_id]     INT            NULL,
    [description] NVARCHAR (300) NULL,
    [created_on]  DATETIME       DEFAULT (getdate()) NULL,
    PRIMARY KEY CLUSTERED ([log_id] ASC),
    CONSTRAINT [FK_log_user_id] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user] ([user_id])
);
















GO
CREATE TRIGGER dbo.delTrigger_dbo_log
    ON [dbo].[log]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[log_hist]
    (
		user_id,
		operation,
		log_id,
		item_type,
		item_id,
		description,
		created_on
--
	)
	SELECT 
		user_id,
		operation,
		log_id,
		item_type,
		item_id,
		description,
		created_on
--
	FROM
        deleted;
END