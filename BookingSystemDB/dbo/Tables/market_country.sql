CREATE TABLE [dbo].[market_country] (
    [market_id]  INT NOT NULL,
    [country_id] INT NOT NULL,
    CONSTRAINT [PK_market_country] PRIMARY KEY CLUSTERED ([country_id] ASC, [market_id] ASC),
    CONSTRAINT [FK_market_country_country_id] FOREIGN KEY ([country_id]) REFERENCES [dbo].[country] ([country_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_market_country_market_id] FOREIGN KEY ([market_id]) REFERENCES [dbo].[market] ([market_id]) ON DELETE CASCADE
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_market_country
    ON [dbo].[market_country]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[market_country_hist]
    (
		market_id,
		country_id
--
	)
	SELECT 
		market_id,
		country_id
--
	FROM
        deleted;
END