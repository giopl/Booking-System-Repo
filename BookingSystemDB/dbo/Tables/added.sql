﻿CREATE TABLE [dbo].[added] (
    [added_id]     INT            IDENTITY (100, 1) NOT NULL,
    [provider_id]  INT            NOT NULL,
    [name]         NVARCHAR (100) NULL,
    [description]  NVARCHAR (500) NULL,
    [is_per_night] CHAR (1)       NULL,
    [is_per_room]  CHAR (1)       NULL,
    [is_free]      CHAR (1)       NULL,
    [type]         CHAR (1)       NULL,
    [category]     NVARCHAR (100) NULL,
    [condition]    NVARCHAR (500) NULL,
    [start_date]   DATE           NULL,
    [end_date]     DATE           NULL,
    [min_value]    INT            NULL,
    [max_value]    INT            NULL,
    [adult_rate]   FLOAT (53)     NULL,
    [child_rate]   FLOAT (53)     NULL,
    [is_active]    CHAR (1)       NULL,
    [package_id]   INT            NULL,
    PRIMARY KEY CLUSTERED ([added_id] ASC),
    CONSTRAINT [fk_added_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id])
);

