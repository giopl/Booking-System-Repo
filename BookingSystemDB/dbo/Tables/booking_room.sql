﻿CREATE TABLE [dbo].[booking_room] (
    [booking_room_id]     INT           IDENTITY (100, 1) NOT NULL,
    [booking_id]          INT           NOT NULL,
    [room_id]             INT           NOT NULL,
    [base_price_id]       INT           NOT NULL,
    [meal_plan_id]        INT           NULL,
    [room_number]         INT           NOT NULL,
    [booking_ref]         NVARCHAR (32) NULL,
    [adults]              INT           NULL,
    [infants]             INT           NULL,
    [children]            INT           NULL,
    [teens]               INT           NULL,
    [room_name]           NVARCHAR (50) NULL,
    [allotments]          INT           DEFAULT ((0)) NULL,
    [allotment_confirmed] BIT           DEFAULT ((0)) NULL,
    PRIMARY KEY CLUSTERED ([booking_room_id] ASC),
    CONSTRAINT [FK_booking_room_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]) ON DELETE CASCADE
);
















GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_room
    ON [dbo].[booking_room]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_room_hist]
    (
		teens,
		room_number,
		room_name,
		room_id,
		meal_plan_id,
		infants,
		children,
		booking_room_id,
		booking_ref,
		booking_id,
		base_price_id,
		adults
--
	)
	SELECT 
		teens,
		room_number,
		room_name,
		room_id,
		meal_plan_id,
		infants,
		children,
		booking_room_id,
		booking_ref,
		booking_id,
		base_price_id,
		adults
--
	FROM
        deleted;
END