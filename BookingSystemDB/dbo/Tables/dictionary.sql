﻿CREATE TABLE [dbo].[dictionary] (
    [dictionary_id] INT            IDENTITY (100, 1) NOT NULL,
    [code_key]      NVARCHAR (40)  NULL,
    [code_value]    NVARCHAR (100) NULL,
    [applies_to]    NVARCHAR (100) NULL,
    [code_desc]     NVARCHAR (500) NULL
);

