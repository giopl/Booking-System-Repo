﻿CREATE TABLE [dbo].[room_unavailability] (
    [room_unavailability_id] INT  IDENTITY (100, 1) NOT NULL,
    [room_id]                INT  NOT NULL,
    [start_date]             DATE NOT NULL,
    [end_date]               DATE NOT NULL,
    PRIMARY KEY CLUSTERED ([room_unavailability_id] ASC),
    CONSTRAINT [FK_unavailability_room_id] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id]) ON DELETE CASCADE
);





GO
