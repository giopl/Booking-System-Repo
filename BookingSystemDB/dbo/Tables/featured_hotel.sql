CREATE TABLE [dbo].[featured_hotel] (
    [featured_hotel_id] INT            IDENTITY (100, 1) NOT NULL,
    [provider_id]       INT            NOT NULL,
    [title]             NVARCHAR (300) NULL,
    [description]       NVARCHAR (MAX) NULL,
    [section]           NVARCHAR (30)  NOT NULL,
    [from_date]         DATE           NULL,
    [to_date]           DATE           NULL,
    [picture_id]        INT            NULL,
    PRIMARY KEY CLUSTERED ([section] ASC),
    CONSTRAINT [FK_featured_hotel_picture_id] FOREIGN KEY ([picture_id]) REFERENCES [dbo].[picture] ([picture_id]),
    CONSTRAINT [FK_featured_hotel_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id])
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_featured_hotel
    ON [dbo].[featured_hotel]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[featured_hotel_hist]
    (
		to_date,
		title,
		section,
		provider_id,
		picture_id,
		from_date,
		featured_hotel_id,
		description
--
	)
	SELECT 
		to_date,
		title,
		section,
		provider_id,
		picture_id,
		from_date,
		featured_hotel_id,
		description
--
	FROM
        deleted;
END