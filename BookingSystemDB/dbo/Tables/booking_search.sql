CREATE TABLE [dbo].[booking_search] (
    [booking_ref]    NVARCHAR (32) NOT NULL,
    [search_details] XML           NULL,
    [hotel_id]       INT           NULL,
    [timestamp]      DATETIME      NULL,
    [package_id]     INT           NULL,
    PRIMARY KEY CLUSTERED ([booking_ref] ASC)
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_search
    ON [dbo].[booking_search]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_search_hist]
    (
		timestamp,
		search_details,
		package_id,
		hotel_id,
		booking_ref
--
	)
	SELECT 
		timestamp,
		search_details,
		package_id,
		hotel_id,
		booking_ref
--
	FROM
        deleted;
END