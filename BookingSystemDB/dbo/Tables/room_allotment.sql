﻿CREATE TABLE [dbo].[room_allotment] (
    [room_allotment_id] INT  IDENTITY (100, 1) NOT NULL,
    [room_id]           INT  NOT NULL,
    [provider_id]       INT  NOT NULL,
    [allotment_date]    DATE NOT NULL,
    [allotment_amount]  INT  NOT NULL,
    PRIMARY KEY CLUSTERED ([room_id] ASC, [allotment_date] ASC),
    CONSTRAINT [FK_room_allotment_room_id] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id])
);

