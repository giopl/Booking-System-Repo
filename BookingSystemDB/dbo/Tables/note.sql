CREATE TABLE [dbo].[note] (
    [note_id]     INT             IDENTITY (100, 1) NOT NULL,
    [item_type]   NVARCHAR (30)   NULL,
    [item_id]     INT             NULL,
    [category]    NVARCHAR (100)  NULL,
    [detail]      NVARCHAR (1000) NULL,
    [user_id]     INT             NOT NULL,
    [create_date] DATETIME        NULL,
    PRIMARY KEY CLUSTERED ([note_id] ASC),
    CONSTRAINT [FK_note_user_id] FOREIGN KEY ([user_id]) REFERENCES [dbo].[user] ([user_id]) ON DELETE CASCADE
);














GO
CREATE TRIGGER dbo.delTrigger_dbo_note
    ON [dbo].[note]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[note_hist]
    (
		user_id,
		note_id,
		item_type,
		item_id,
		detail,
		create_date,
		category
--
	)
	SELECT 
		user_id,
		note_id,
		item_type,
		item_id,
		detail,
		create_date,
		category
--
	FROM
        deleted;
END