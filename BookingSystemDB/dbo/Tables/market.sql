CREATE TABLE [dbo].[market] (
    [market_id]   INT           IDENTITY (100, 1) NOT NULL,
    [market_name] NVARCHAR (50) NULL,
    [currency_id] INT           NOT NULL,
    [provider_id] INT           NOT NULL,
    [is_active]   BIT           DEFAULT ((1)) NOT NULL,
    [is_default]  BIT           DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([market_id] ASC),
    CONSTRAINT [FK_market_currency_id] FOREIGN KEY ([currency_id]) REFERENCES [dbo].[currency] ([currency_id]),
    CONSTRAINT [FK_market_ToProvider] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE
);


















GO
CREATE TRIGGER dbo.delTrigger_dbo_market
    ON [dbo].[market]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[market_hist]
    (
		provider_id,
		market_name,
		market_id,
		is_default,
		is_active,
		currency_id
--
	)
	SELECT 
		provider_id,
		market_name,
		market_id,
		is_default,
		is_active,
		currency_id
--
	FROM
        deleted;
END