CREATE TABLE [dbo].[base_price] (
    [base_price_id]     INT        IDENTITY (100, 1) NOT NULL,
    [room_id]           INT        NOT NULL,
    [market_id]         INT        NOT NULL,
    [meal_plan_id]      INT        NOT NULL,
    [start_date]        DATE       NOT NULL,
    [end_date]          DATE       NOT NULL,
    [price_single]      FLOAT (53) NULL,
    [price_twin]        FLOAT (53) NULL,
    [price_triple]      FLOAT (53) NULL,
    [price_infant]      FLOAT (53) NULL,
    [price_child]       FLOAT (53) NULL,
    [price_teen]        FLOAT (53) NULL,
    [price_quadruple]   FLOAT (53) NULL,
    [price_without_bed] FLOAT (53) NULL,
    [price_honeymoon]   FLOAT (53) NULL,
    [is_active]         BIT        DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_base_price] PRIMARY KEY CLUSTERED ([base_price_id] ASC),
    CONSTRAINT [FK_base_price_market_id] FOREIGN KEY ([market_id]) REFERENCES [dbo].[market] ([market_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_base_price_room_id] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_meal_plan_base_price] FOREIGN KEY ([meal_plan_id]) REFERENCES [dbo].[meal_plan] ([meal_plan_id]) ON DELETE CASCADE
);






















GO
CREATE TRIGGER dbo.delTrigger_dbo_base_price
    ON [dbo].[base_price]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[base_price_hist]
    (
		start_date,
		room_id,
		price_without_bed,
		price_twin,
		price_triple,
		price_teen,
		price_single,
		price_quadruple,
		price_infant,
		price_honeymoon,
		price_child,
		meal_plan_id,
		market_id,
		is_active,
		end_date,
		base_price_id
--
	)
	SELECT 
		start_date,
		room_id,
		price_without_bed,
		price_twin,
		price_triple,
		price_teen,
		price_single,
		price_quadruple,
		price_infant,
		price_honeymoon,
		price_child,
		meal_plan_id,
		market_id,
		is_active,
		end_date,
		base_price_id
--
	FROM
        deleted;
END