CREATE TABLE [dbo].[vehicle] (
    [vehicle_id]          INT            IDENTITY (100, 1) NOT NULL,
    [provider_id]         INT            NOT NULL,
    [service_name]        NVARCHAR (200) NULL,
    [vehicle_model]       NVARCHAR (100) NULL,
    [vehicle_type]        NVARCHAR (50)  NULL,
    [service_description] NVARCHAR (MAX) NULL,
    [max_seat]            INT            NULL,
    [is_active]           BIT            DEFAULT ((1)) NOT NULL,
    [is_transfer]         BIT            DEFAULT ((0)) NOT NULL,
    [includes_driver]     BIT            CONSTRAINT [DF_vehicle_includes_driver] DEFAULT ((0)) NOT NULL,
    [engine_size]         SMALLINT       NULL,
    [year]                SMALLINT       NULL,
    [is_automatic]        BIT            DEFAULT ((0)) NOT NULL,
    [doors]               TINYINT        NULL,
    [fuel_type]           TINYINT        NULL,
    PRIMARY KEY CLUSTERED ([vehicle_id] ASC),
    CONSTRAINT [FK_vehicle_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE
);






















GO
CREATE TRIGGER dbo.delTrigger_dbo_vehicle
    ON [dbo].[vehicle]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[vehicle_hist]
    (
		year,
		vehicle_type,
		vehicle_model,
		vehicle_id,
		service_name,
		service_description,
		provider_id,
		max_seat,
		is_transfer,
		is_automatic,
		is_active,
		includes_driver,
		fuel_type,
		engine_size,
		doors
--
	)
	SELECT 
		year,
		vehicle_type,
		vehicle_model,
		vehicle_id,
		service_name,
		service_description,
		provider_id,
		max_seat,
		is_transfer,
		is_automatic,
		is_active,
		includes_driver,
		fuel_type,
		engine_size,
		doors
--
	FROM
        deleted;
END