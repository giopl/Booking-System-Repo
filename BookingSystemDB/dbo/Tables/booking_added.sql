﻿CREATE TABLE [dbo].[booking_added] (
    [booking_id]    INT NOT NULL,
    [guest_id]      INT NOT NULL,
    [added_rate_id] INT NULL,
    [split_number]  INT NULL,
    [num_nights]    INT NULL,
    CONSTRAINT [fk_booking_added_added_rate_id] FOREIGN KEY ([added_rate_id]) REFERENCES [dbo].[added_rate] ([added_rate_id]),
    CONSTRAINT [fk_booking_added_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]),
    CONSTRAINT [fk_booking_added_person_id] FOREIGN KEY ([guest_id]) REFERENCES [dbo].[person] ([person_id])
);

