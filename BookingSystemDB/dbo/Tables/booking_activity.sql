CREATE TABLE [dbo].[booking_activity] (
    [booking_activity_id] INT            IDENTITY (100, 1) NOT NULL,
    [booking_id]          INT            NOT NULL,
    [activity_id]         INT            NOT NULL,
    [adults]              INT            NULL,
    [children]            INT            NULL,
    [price_per_adult]     FLOAT (53)     NULL,
    [price_per_child]     FLOAT (53)     NULL,
    [total_price]         FLOAT (53)     NULL,
    [markup_amt]          FLOAT (53)     NULL,
    [is_aftersales]       BIT            DEFAULT ((0)) NOT NULL,
    [aftersales_admin]    INT            NULL,
    [provider_name]       NVARCHAR (100) NULL,
    [activity_name]       NVARCHAR (200) NULL,
    [transfer_type]       INT            NULL,
    PRIMARY KEY CLUSTERED ([booking_activity_id] ASC),
    CONSTRAINT [FK_booking_activity_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]) ON DELETE CASCADE
);


























GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_activity
    ON [dbo].[booking_activity]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_activity_hist]
    (
		transfer_type,
		total_price,
		provider_name,
		price_per_child,
		price_per_adult,
		markup_amt,
		is_aftersales,
		children,
		booking_id,
		booking_activity_id,
		aftersales_admin,
		adults,
		activity_name,
		activity_id
--
	)
	SELECT 
		transfer_type,
		total_price,
		provider_name,
		price_per_child,
		price_per_adult,
		markup_amt,
		is_aftersales,
		children,
		booking_id,
		booking_activity_id,
		aftersales_admin,
		adults,
		activity_name,
		activity_id
--
	FROM
        deleted;
END