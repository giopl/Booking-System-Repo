CREATE TABLE [dbo].[activity] (
    [activity_id]         INT             IDENTITY (100, 1) NOT NULL,
    [activity_name]       NVARCHAR (200)  NULL,
    [description]         NVARCHAR (MAX)  NULL,
    [cancellation_policy] NVARCHAR (MAX)  NULL,
    [conditions]          NVARCHAR (MAX)  NULL,
    [category]            NVARCHAR (50)   NULL,
    [duration]            NVARCHAR (50)   NULL,
    [min_age]             INT             NULL,
    [max_child_age]       INT             NULL,
    [is_active]           BIT             NOT NULL,
    [valid_from]          DATE            NULL,
    [valid_to]            DATE            NULL,
    [features]            NVARCHAR (1000) NULL,
    [includes_meal]       BIT             DEFAULT ((0)) NOT NULL,
    PRIMARY KEY CLUSTERED ([activity_id] ASC)
);






















GO
CREATE TRIGGER dbo.delTrigger_dbo_activity
    ON [dbo].[activity]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[activity_hist]
    (
		valid_to,
		valid_from,
		min_age,
		max_child_age,
		is_active,
		includes_meal,
		features,
		duration,
		description,
		conditions,
		category,
		cancellation_policy,
		activity_name,
		activity_id
--
	)
	SELECT 
		valid_to,
		valid_from,
		min_age,
		max_child_age,
		is_active,
		includes_meal,
		features,
		duration,
		description,
		conditions,
		category,
		cancellation_policy,
		activity_name,
		activity_id
--
	FROM
        deleted;
END