CREATE TABLE [dbo].[company_info] (
    [Id]               INT            IDENTITY (1, 1) NOT NULL,
    [name]             NVARCHAR (100) NULL,
    [addr_line1]       NVARCHAR (200) NULL,
    [addr_line2]       NVARCHAR (200) NULL,
    [addr_line3]       NVARCHAR (200) NULL,
    [town]             NVARCHAR (100) NULL,
    [country]          NVARCHAR (100) NULL,
    [vat_reg_num]      NVARCHAR (100) NULL,
    [business_reg_num] NVARCHAR (100) NULL,
    [phone_num]        NVARCHAR (20)  NULL,
    [fax_num]          NVARCHAR (20)  NULL,
    [email]            NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);







GO
CREATE TRIGGER dbo.delTrigger_dbo_company_info
    ON [dbo].[company_info]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[company_info_hist]
    (
		vat_reg_num,
		town,
		phone_num,
		name,
		Id,
		fax_num,
		email,
		country,
		business_reg_num,
		addr_line3,
		addr_line2,
		addr_line1
--
	)
	SELECT 
		vat_reg_num,
		town,
		phone_num,
		name,
		Id,
		fax_num,
		email,
		country,
		business_reg_num,
		addr_line3,
		addr_line2,
		addr_line1
--
	FROM
        deleted;
END