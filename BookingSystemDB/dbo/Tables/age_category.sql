﻿CREATE TABLE [dbo].[age_category] (
    [age_category_id] INT      IDENTITY (100, 1) NOT NULL,
    [provider_id]     INT      NOT NULL,
    [infant_min_age]  INT      NULL,
    [infant_max_age]  INT      NULL,
    [child_min_age]   INT      NULL,
    [child_max_age]   INT      NULL,
    [teen_min_age]    INT      NULL,
    [teen_max_age]    INT      NULL,
    [is_active]       CHAR (1) NULL,
    PRIMARY KEY CLUSTERED ([age_category_id] ASC),
    CONSTRAINT [fk_age_category_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id])
);

