CREATE TABLE [dbo].[vehicle_picture] (
    [picture_id]    INT          NOT NULL,
    [vehicle_id]    INT          NOT NULL,
    [display_order] INT          NULL,
    [section]       VARCHAR (15) NULL,
    CONSTRAINT [PK_vehicle_picture] PRIMARY KEY CLUSTERED ([vehicle_id] ASC, [picture_id] ASC),
    CONSTRAINT [FK_vehicle_picture_picture_id] FOREIGN KEY ([picture_id]) REFERENCES [dbo].[picture] ([picture_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_vehicle_picture_vehicle_id] FOREIGN KEY ([vehicle_id]) REFERENCES [dbo].[vehicle] ([vehicle_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_vehicle_picture
    ON [dbo].[vehicle_picture]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[vehicle_picture_hist]
    (
		vehicle_id,
		section,
		picture_id,
		display_order
--
	)
	SELECT 
		vehicle_id,
		section,
		picture_id,
		display_order
--
	FROM
        deleted;
END