CREATE TABLE [dbo].[meal_plan] (
    [meal_plan_id] INT            IDENTITY (100, 1) NOT NULL,
    [name]         NVARCHAR (50)  NOT NULL,
    [description]  NVARCHAR (MAX) NULL,
    [meal_order]   SMALLINT       NULL,
    [code]         NCHAR (2)      NOT NULL,
    [is_active]    BIT            DEFAULT ((1)) NOT NULL,
    PRIMARY KEY CLUSTERED ([meal_plan_id] ASC),
    UNIQUE NONCLUSTERED ([code] ASC)
);









GO
CREATE TRIGGER dbo.delTrigger_dbo_meal_plan
    ON [dbo].[meal_plan]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[meal_plan_hist]
    (
		name,
		meal_plan_id,
		meal_order,
		is_active,
		description,
		code
--
	)
	SELECT 
		name,
		meal_plan_id,
		meal_order,
		is_active,
		description,
		code
--
	FROM
        deleted;
END