CREATE TABLE [dbo].[hotel_feature] (
    [feature_id]  INT NOT NULL,
    [provider_id] INT NOT NULL,
    [quantity]    INT NULL,
    CONSTRAINT [PK_hotel_feature] PRIMARY KEY CLUSTERED ([feature_id] ASC, [provider_id] ASC),
    CONSTRAINT [FK_hotel_feature_To_feature] FOREIGN KEY ([feature_id]) REFERENCES [dbo].[feature] ([feature_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_hotel_feature_To_provider] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_hotel_feature
    ON [dbo].[hotel_feature]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[hotel_feature_hist]
    (
		quantity,
		provider_id,
		feature_id
--
	)
	SELECT 
		quantity,
		provider_id,
		feature_id
--
	FROM
        deleted;
END