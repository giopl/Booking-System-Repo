CREATE TABLE [dbo].[promo_set_promotions] (
    [promotion_set_id] INT NOT NULL,
    [promotion_id]     INT NOT NULL,
    CONSTRAINT [PK_promo_set_promo] PRIMARY KEY CLUSTERED ([promotion_set_id] ASC, [promotion_id] ASC),
    CONSTRAINT [FK_promoset] FOREIGN KEY ([promotion_set_id]) REFERENCES [dbo].[promotion_set] ([promotion_set_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_promoset_promo] FOREIGN KEY ([promotion_id]) REFERENCES [dbo].[promotion] ([promotion_id]) ON DELETE CASCADE
);







GO
CREATE TRIGGER dbo.delTrigger_dbo_promo_set_promotions
    ON [dbo].[promo_set_promotions]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[promo_set_promotions_hist]
    (
		promotion_set_id,
		promotion_id
--
	)
	SELECT 
		promotion_set_id,
		promotion_id
--
	FROM
        deleted;
END