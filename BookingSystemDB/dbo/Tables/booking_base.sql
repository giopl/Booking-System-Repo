﻿CREATE TABLE [dbo].[booking_base] (
    [booking_id]   INT NOT NULL,
    [guest_id]     INT NOT NULL,
    [base_rate_id] INT NULL,
    [split_number] INT NULL,
    [num_nights]   INT NULL,
    CONSTRAINT [fk_booking_base_base_rate_id] FOREIGN KEY ([base_rate_id]) REFERENCES [dbo].[base_rate] ([base_rate_id]),
    CONSTRAINT [fk_booking_base_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]),
    CONSTRAINT [fk_booking_base_person_id] FOREIGN KEY ([guest_id]) REFERENCES [dbo].[person] ([person_id])
);

