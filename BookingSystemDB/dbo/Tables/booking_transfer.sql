CREATE TABLE [dbo].[booking_transfer] (
    [booking_transfer_id] INT            IDENTITY (100, 1) NOT NULL,
    [booking_id]          INT            NOT NULL,
    [transfer_id]         INT            NOT NULL,
    [children]            INT            NULL,
    [teens]               INT            NULL,
    [adults]              INT            NULL,
    [total_price]         FLOAT (53)     NULL,
    [markup_amt]          FLOAT (53)     NULL,
    [is_aftersales]       BIT            DEFAULT ((0)) NOT NULL,
    [aftersales_admin]    INT            NULL,
    [transfer_pricing_id] INT            NULL,
    [provider_name]       NVARCHAR (100) NULL,
    [transfer_name]       NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([booking_transfer_id] ASC),
    CONSTRAINT [FK_booking_transfer_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]) ON DELETE CASCADE
);
























GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_transfer
    ON [dbo].[booking_transfer]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_transfer_hist]
    (
		transfer_pricing_id,
		transfer_name,
		transfer_id,
		total_price,
		teens,
		provider_name,
		markup_amt,
		is_aftersales,
		children,
		booking_transfer_id,
		booking_id,
		aftersales_admin,
		adults
--
	)
	SELECT 
		transfer_pricing_id,
		transfer_name,
		transfer_id,
		total_price,
		teens,
		provider_name,
		markup_amt,
		is_aftersales,
		children,
		booking_transfer_id,
		booking_id,
		aftersales_admin,
		adults
--
	FROM
        deleted;
END