CREATE TABLE [dbo].[promotion_room] (
    [promotion_id] INT NOT NULL,
    [room_id]      INT NOT NULL,
    CONSTRAINT [PK_promo_room] PRIMARY KEY CLUSTERED ([promotion_id] ASC, [room_id] ASC),
    CONSTRAINT [FK_promo_room_promo] FOREIGN KEY ([promotion_id]) REFERENCES [dbo].[promotion] ([promotion_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_promo_room_room] FOREIGN KEY ([room_id]) REFERENCES [dbo].[room] ([room_id]) ON DELETE CASCADE
);








GO
CREATE TRIGGER dbo.delTrigger_dbo_promotion_room
    ON [dbo].[promotion_room]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[promotion_room_hist]
    (
		room_id,
		promotion_id
--
	)
	SELECT 
		room_id,
		promotion_id
--
	FROM
        deleted;
END