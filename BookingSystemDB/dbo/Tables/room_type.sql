﻿CREATE TABLE [dbo].[room_type] (
    [room_type_id] INT            IDENTITY (100, 1) NOT NULL,
    [hotel_id]     INT            NOT NULL,
    [name]         NVARCHAR (50)  NULL,
    [description]  NVARCHAR (300) NULL,
    [min_adult]    INT            NULL,
    [max_adult]    INT            NULL,
    [room_size]    NVARCHAR (30)  NULL, 
    CONSTRAINT [PK_room_type] PRIMARY KEY ([room_type_id]), 
    CONSTRAINT [FK_room_type_ToHotel] FOREIGN KEY (hotel_id) REFERENCES dbo.hotel(hotel_id)
);

