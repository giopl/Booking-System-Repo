


CREATE TABLE [dbo].[booking_promotion] (
    [booking_promotion_id]  INT            IDENTITY (100, 1) NOT NULL,
    [booking_id]            INT            NOT NULL,
    [booking_room_id]       INT            NULL,
    [promotion_id]          INT            NULL,
    [promotion_name]        NVARCHAR (100) NULL,
    [promotion_description] NVARCHAR (MAX) NULL,
    [free_nights]           INT            NULL,
    [discount]              FLOAT (53)     NULL,
    PRIMARY KEY CLUSTERED ([booking_promotion_id] ASC),
    CONSTRAINT [FK_booking_promotion_booking_id] FOREIGN KEY ([booking_id]) REFERENCES [dbo].[booking] ([booking_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_booking_promotion_booking_room_id] FOREIGN KEY ([booking_room_id]) REFERENCES [dbo].[booking_room] ([booking_room_id])
);









GO
CREATE TRIGGER dbo.delTrigger_dbo_booking_promotion
    ON [dbo].[booking_promotion]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[booking_promotion_hist]
    (
		promotion_name,
		promotion_id,
		promotion_description,
		free_nights,
		discount,
		booking_room_id,
		booking_promotion_id,
		booking_id
--
	)
	SELECT 
		promotion_name,
		promotion_id,
		promotion_description,
		free_nights,
		discount,
		booking_room_id,
		booking_promotion_id,
		booking_id
--
	FROM
        deleted;
END