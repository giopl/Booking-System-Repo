CREATE TABLE [dbo].[country] (
    [country_id]     INT            IDENTITY (100, 1) NOT NULL,
    [country_code]   NCHAR (2)      NOT NULL,
    [name]           NVARCHAR (50)  NULL,
    [currency]       NVARCHAR (3)   NULL,
    [currency_name]  NVARCHAR (30)  NULL,
    [is_independent] NVARCHAR (30)  NULL,
    [language]       CHAR (2)       NULL,
    [nationality]    NVARCHAR (100) NULL,
    PRIMARY KEY CLUSTERED ([country_id] ASC)
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_country
    ON [dbo].[country]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[country_hist]
    (
		nationality,
		name,
		language,
		is_independent,
		currency_name,
		currency,
		country_id,
		country_code
--
	)
	SELECT 
		nationality,
		name,
		language,
		is_independent,
		currency_name,
		currency,
		country_id,
		country_code
--
	FROM
        deleted;
END