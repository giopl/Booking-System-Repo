CREATE TABLE [dbo].[promotion] (
    [promotion_id]         INT            IDENTITY (100, 1) NOT NULL,
    [provider_id]          INT            NOT NULL,
    [name]                 NVARCHAR (100) NULL,
    [description]          NVARCHAR (MAX) NULL,
    [code]                 NVARCHAR (100) NULL,
    [contracted_offer]     NVARCHAR (MAX) NULL,
    [cancellation_policy]  NVARCHAR (MAX) NULL,
    [book_before_days_min] INT            NULL,
    [book_before_days_max] INT            NULL,
    [book_from_date]       DATE           NULL,
    [book_until_date]      DATE           NULL,
    [travel_from_date]     DATE           NULL,
    [travel_until_date]    DATE           NULL,
    [valid_from]           DATE           NOT NULL,
    [valid_to]             DATE           NOT NULL,
    [min_nights]           INT            NULL,
    [discount]             FLOAT (53)     DEFAULT ((1.0)) NOT NULL,
    [apply_all_markets]    BIT            DEFAULT ((0)) NOT NULL,
    [apply_all_rooms]      BIT            DEFAULT ((0)) NOT NULL,
    [is_active]            BIT            DEFAULT ((1)) NOT NULL,
    [is_honeymoon]         BIT            DEFAULT ((0)) NOT NULL,
    [free_nights]          INT            NULL,
    [promotion_group_id]   INT            NULL,
    [flat_single]          FLOAT (53)     NULL,
    [flat_twin]            FLOAT (53)     NULL,
    [flat_triple]          FLOAT (53)     NULL,
    [flat_quadruple]       FLOAT (53)     NULL,
    [flat_child1]          FLOAT (53)     NULL,
    [flat_child2]          FLOAT (53)     NULL,
    [flat_teen1]           FLOAT (53)     NULL,
    [flat_teen2]           FLOAT (53)     NULL,
    CONSTRAINT [PK_promotion] PRIMARY KEY CLUSTERED ([promotion_id] ASC),
    CONSTRAINT [FK_promotion_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id])
);




















GO
CREATE TRIGGER dbo.delTrigger_dbo_promotion
    ON [dbo].[promotion]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[promotion_hist]
    (
		valid_to,
		valid_from,
		travel_until_date,
		travel_from_date,
		provider_id,
		promotion_id,
		promotion_group_id,
		name,
		min_nights,
		is_honeymoon,
		is_active,
		free_nights,
		discount,
		description,
		contracted_offer,
		cancellation_policy,
		book_until_date,
		book_from_date,
		book_before_days_min,
		book_before_days_max,
		apply_all_rooms,
		apply_all_markets
--
	)
	SELECT 
		valid_to,
		valid_from,
		travel_until_date,
		travel_from_date,
		provider_id,
		promotion_id,
		promotion_group_id,
		name,
		min_nights,
		is_honeymoon,
		is_active,
		free_nights,
		discount,
		description,
		contracted_offer,
		cancellation_policy,
		book_until_date,
		book_from_date,
		book_before_days_min,
		book_before_days_max,
		apply_all_rooms,
		apply_all_markets
--
	FROM
        deleted;
END