CREATE TABLE [dbo].[contact] (
    [contact_id]   INT            IDENTITY (100, 1) NOT NULL,
    [provider_id]  INT            NULL,
    [salutation]   NVARCHAR (10)  NULL,
    [firstname]    NVARCHAR (100) NULL,
    [lastname]     NVARCHAR (300) NULL,
    [job_title]    NVARCHAR (100) NULL,
    [office_phone] NVARCHAR (50)  NULL,
    [mobile]       NVARCHAR (50)  NULL,
    [email]        NVARCHAR (100) NULL,
    [notes]        NVARCHAR (MAX) NULL,
    [is_active]    BIT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_contact] PRIMARY KEY CLUSTERED ([contact_id] ASC),
    CONSTRAINT [FK_contact_ToProvider] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id]) ON DELETE CASCADE
);












GO
CREATE TRIGGER dbo.delTrigger_dbo_contact
    ON [dbo].[contact]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[contact_hist]
    (
		salutation,
		provider_id,
		office_phone,
		notes,
		mobile,
		lastname,
		job_title,
		is_active,
		firstname,
		email,
		contact_id
--
	)
	SELECT 
		salutation,
		provider_id,
		office_phone,
		notes,
		mobile,
		lastname,
		job_title,
		is_active,
		firstname,
		email,
		contact_id
--
	FROM
        deleted;
END