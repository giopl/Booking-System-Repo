CREATE TABLE [dbo].[activity_picture] (
    [picture_id]    INT          NOT NULL,
    [activity_id]   INT          NOT NULL,
    [display_order] INT          NULL,
    [section]       VARCHAR (15) NULL,
    CONSTRAINT [PK_activity_picture] PRIMARY KEY CLUSTERED ([activity_id] ASC, [picture_id] ASC),
    CONSTRAINT [FK_activity_picture_activity_id] FOREIGN KEY ([activity_id]) REFERENCES [dbo].[activity] ([activity_id]) ON DELETE CASCADE,
    CONSTRAINT [FK_activity_picture_picture_id] FOREIGN KEY ([picture_id]) REFERENCES [dbo].[picture] ([picture_id]) ON DELETE CASCADE
);










GO
CREATE TRIGGER dbo.delTrigger_dbo_activity_picture
    ON [dbo].[activity_picture]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[activity_picture_hist]
    (
		section,
		picture_id,
		display_order,
		activity_id
--
	)
	SELECT 
		section,
		picture_id,
		display_order,
		activity_id
--
	FROM
        deleted;
END