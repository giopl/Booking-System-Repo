CREATE TABLE [dbo].[room] (
    [room_id]     INT            IDENTITY (100, 1) NOT NULL,
    [provider_id] INT            NOT NULL,
    [name]        NVARCHAR (50)  NULL,
    [description] NVARCHAR (MAX) NULL,
    [min_adult]   INT            NULL,
    [max_adult]   INT            NULL,
    [room_size]   NVARCHAR (30)  NULL,
    [is_active]   BIT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_room] PRIMARY KEY CLUSTERED ([room_id] ASC),
    CONSTRAINT [FK_room_type_provider_id] FOREIGN KEY ([provider_id]) REFERENCES [dbo].[provider] ([provider_id])
);














GO
CREATE TRIGGER dbo.delTrigger_dbo_room
    ON [dbo].[room]
   FOR DELETE
AS 
BEGIN
    SET NOCOUNT ON;

-- surely you must want to put some other code here?

    INSERT del.[room_hist]
    (
		room_size,
		room_id,
		provider_id,
		name,
		min_adult,
		max_adult,
		is_active,
		description
--
	)
	SELECT 
		room_size,
		room_id,
		provider_id,
		name,
		min_adult,
		max_adult,
		is_active,
		description
--
	FROM
        deleted;
END