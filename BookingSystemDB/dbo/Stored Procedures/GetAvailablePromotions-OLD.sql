﻿create PROCEDURE [dbo].[GetAvailablePromotions] (	
	@start date, --checkin_date
	@end date --checkout_date
	)
AS
BEGIN
	
	
select 
d.fulldate,
rm.provider_id,
p.promotion_id,
p.name promotion_name,
p.apply_all_markets,
p.apply_all_rooms,
markets.market_id,
markets.market_name,
rooms.room_id,
rooms.room_name,
bp.base_price_id,
bp.[price_single],
bp.[price_twin],
bp.[price_triple],
bp.[price_infant],
bp.[price_child],
bp.[price_teen],
bp.[price_quadruple],
bp.[price_without_bed],
bp.[price_honeymoon],
p.discount,
coalesce(bp.[price_single]		  ,0) * (100 - coalesce(p.discount,0))/100 [discounted_price_single],		 
coalesce(bp.[price_twin]		  ,0) * (100 - coalesce(p.discount,0))/100 [discounted_price_twin],		 
coalesce(bp.[price_triple]		  ,0) * (100 - coalesce(p.discount,0))/100 [discounted_price_triple]	,	 
coalesce(bp.[price_infant]		  ,0) * (100 - coalesce(p.discount,0))/100 [discounted_price_infant],		 
coalesce(bp.[price_child]		  ,0) * (100 - coalesce(p.discount,0))/100 [discounted_price_child]	,	 
coalesce(bp.[price_teen]		  ,0) * (100 - coalesce(p.discount,0))/100 [discounted_price_teen],		 
coalesce(bp.[price_quadruple]	  ,0) * (100 - coalesce(p.discount,0))/100 [discounted_price_quadruple]	, 
coalesce(bp.[price_without_bed]	  ,0) * (100 - coalesce(p.discount,0))/100 [discounted_price_without_bed],	 
coalesce(bp.[price_honeymoon]	  ,0) * (100 - coalesce(p.discount,0))/100 [discounted_price_honeymoon]	 

from 
[dbo].[base_price] bp
inner join dbo.room rm
on rm.room_id=bp.room_id
inner join dbo.date d
on d.fulldate between bp.start_date and bp.end_date
inner join  --get all markets applicable 
(
	select 
	p.promotion_id,
	p.apply_all_markets,
	m.market_id,
	m.market_name,
	m.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_market] pm
	on p.promotion_id=pm.promotion_id
	right join [dbo].[market] m
	on (m.market_id=pm.market_id  --either it applies to a specific market or 
	or  p.apply_all_markets=1   --to all markets 
	)
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) markets 
on markets.market_id=bp.market_id
and markets.provider_id=rm.provider_id
inner join   --get all rooms applicable 
(
	select 
	p.promotion_id,
	p.apply_all_rooms,
	r.room_id,
	r.name room_name,
	r.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_room] pr
	on p.promotion_id=pr.promotion_id
	right join [dbo].[room] r 
	on (r.room_id=pr.room_id  --either it applies to a specific room or 
	or  p.apply_all_rooms=1   ---or to all rooms 
	)
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) rooms 
on rooms.room_id=bp.room_id
and rooms.promotion_id=markets.promotion_id
and rooms.provider_id=rm.provider_id


inner join [dbo].[promotion] p 
on p.promotion_id=markets.promotion_id
where
p.is_active=1 and
bp.is_active=1 and 
rm.is_active =1 and
d.fulldate between  @start and  @end    --only return prices for dates within booking
  and 
 @start between bp.start_date and bp.end_date  --give records only if present in base price for given period
and @end between bp.start_date and bp.end_date
and  
--apply all logic for promotion filtering 
(
	
		--if booking is made between the promotion booking dates 
	getdate() between coalesce(p.[book_from_date],'1990-01-01') and coalesce(p.[book_until_date] ,'2999-01-01')
	or
		--the travel dates must match 
		(
			@start between coalesce(p.[travel_from_date],'1990-01-01') and coalesce(p.[travel_until_date],'2999-01-01')
			and @end between coalesce(p.[travel_from_date],'1990-01-01') and coalesce(p.[travel_until_date],'2999-01-01')
		)


)
	and datediff(day,@start,@end) >=coalesce(p.min_nights,0)
	--booking should be made before number of days 
	and datediff(day,getdate(),@start)>=coalesce([book_before_days],0)
	and getdate() between coalesce(p.valid_from,'1990-01-01') and coalesce(p.valid_to ,'2999-01-01')
end
