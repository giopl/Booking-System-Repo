﻿

CREATE PROCEDURE [dbo].[FindRoom](
	-- Add the parameters for the stored procedure here
	@start date,
	@end date,
	@adults int,
	@age1 int=-1,
	@age2 int=-1,
	@age3 int=-1,
	@age4 int=-1,
	@age5 int=-1,	
	@occNum int,
	@ishoneymoon int = 0,		
	@providerId int = 0,
	@currencyId int = 0,
	@onlyBestPrice int =0

	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
		-- onlyBestPrice (0 or 1)
		--0: returns all rooms found
		--1: returns lowest priced room per hotel

	SET NOCOUNT ON;

	declare @ds int = Datediff(day,@start,@end)

select * from

(

select 
	rank() over(partition by provider_id order by T.total_price) as rn,
T.*,
STUFF(( SELECT  '|'+ convert(varchar(10),unavailable_date,103) FROM dbo.v_unavailability a
WHERE T.room_id = a.room_id and a.unavailable_date between @start and @end FOR XML PATH('')),1 ,1, '')   unavailable_dates

from
(
	
select  

	@occNum as room_sequence,
	p.provider_id,
	p.name provider_name,
	min(bp.base_price_id) base_price_id,
	m.market_id,
	c.currency_id,
	c.currency_code,
	c.symbol,
	r.room_id, 
	r.name room_name,
	
	@ds nights,

	/* fields below added to match return type of package */

		'' as [description],
		@start from_date,
		null ground_handling_adult,
		null ground_handling_child,
		null ground_handling_teen,
		0 includes_ground_handling,
		0 package_id,
		'' package_name,
		@end to_date,
		null total_cost_ground_handling,



	
	sum(case @ishoneymoon when 1 then 
	/* honeymoon */
	(coalesce(price_honeymoon, price_twin, price_single)) * 2
	else 
	(
		(case @adults when 1 then (price_single) 
	when 2 then (coalesce(price_twin, price_single))  
	When 3 then (coalesce(price_triple, price_single)) 
	when 4 then (coalesce(price_quadruple, price_single)) 
	else (price_single) end) +
	(coalesce(price_infant,0)) * gt.infant+
	(coalesce(price_child,0)) *gt.child +
	(coalesce(price_teen,0) ) * gt.teen +

	(coalesce(bp.chg_infant,0)) * gt.infant+
	(coalesce(bp.chg_child,0)) * gt.child+
	(coalesce(bp.chg_child,0)) * gt.teen+
	(coalesce(bp.chg_adult,0)) * @adults
	)
	end) total_price,

	sum(case @adults when 1 then (price_single) 
	when 2 then (coalesce(price_twin, price_single))  
	When 3 then (coalesce(price_triple, price_single)) 
	when 4 then (coalesce(price_quadruple, price_single)) 
	else (price_single) end) total_per_adult,
	
	sum(coalesce(price_honeymoon, price_twin, price_single)) total_per_honeymooner,

	sum(coalesce(price_infant,0)) total_per_infant,
	sum(coalesce(price_child,0)) total_per_child,
	sum(coalesce(price_teen,0) ) total_per_teen,

	sum(coalesce(bp.chg_infant,0)) charges_infant,
	sum(coalesce(bp.chg_child,0)) charges_child,
	sum(coalesce(bp.chg_child,0)) charges_teen,
	sum(coalesce(bp.chg_adult,0)) charges_adult,
	@adults num_adults,
	max(gt.infant) num_infants,
	max(gt.child) num_children,
	max(gt.teen) num_teens,
	cast(null as int) as allotment_id,
	cast(null as int) as allotments,

	case when sum(case when una.room_id is not null then 1 else 0 end )>0 then 'Y' else 'N' end as unavailable_flag
	
from v_price bp

inner join market m
on bp.market_id = m.market_id

inner join currency c
on m.currency_id = c.currency_id

inner join room r 
on bp.room_id = r.room_id

inner join provider p 
on r.provider_id = p.provider_id

left join room_occupancy ro
on bp.room_id = ro.room_id

left join occupancy o 
on o.occupancy_id = ro.occupancy_id

left join dbo.v_unavailability una 
on una.room_id=bp.room_id
and una.unavailable_date=bp.fulldate

Cross apply  [dbo].[fn_GetGuestDefinition](r.provider_id,@age1,@age2,@age3,@age4,@age5) gt

where 

bp.fulldate between @start and dateadd(DAY,-1,@end)
 

 and o.adult = @adults
 and o.infant = gt.infant
 and o.teen = gt.teen
 and o.child = gt.child

and (@providerId = 0 or p.provider_id = @providerId)
and (@currencyId = 0 or c.currency_id = @currencyId)


and r.is_active = 1
and p.is_active = 1
and m.is_active =1 

group by 
c.currency_id,
c.currency_code,
c.symbol,
	r.room_id,
	r.name,
	m.market_id,
	p.provider_id,
	p.name
	having count(bp.fulldate) = @ds

	)  as T

) OT
	where 
	(@onlyBestPrice = 0  or OT.rn = @onlyBestPrice)

	
END