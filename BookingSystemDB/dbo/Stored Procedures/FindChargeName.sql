﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[FindChargeName] 
	-- Add the parameters for the stored procedure here
	(
		@booking_id int = 0,
		@start datetime = '2000-01-01',
		@end datetime = '2000-01-01',
		@provider_id int = 0,
		@currency_id int =0
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--declare @start datetime,
	--@end datetime,
	--@provider_id int,
	--@market_id int
	SET NOCOUNT ON;


	DECLARE @Parameters TABLE
(
   start date,
   [end] date,
   provider_id int,
   market_id int
);



if(@booking_id >0)

INSERT INTO 
    @Parameters(start, [end], provider_id, market_id)
	select cast(checkin_date as date), cast(checkout_date as date), provider_id , market_id from booking where booking_id = @booking_id; 

	else
INSERT INTO 
    @Parameters(start, [end], provider_id, market_id)
	
	select cast(@start as date), cast(@end as date), @provider_id, 
	(select market_id from market where provider_id = @provider_id and currency_id =  @currency_id) ;
    -- Insert statements for procedure here



	select s.supplement_id, s.provider_id, sp.market_id, d.fulldate,
	case when 
coalesce(sp.price_infant,0)+ coalesce(sp.price_child,0)+ coalesce(sp.price_teen,0)+ 
coalesce(sp.price_adult,0) + coalesce(price_honeymoon,0) + coalesce(price_per_item,0) +
coalesce(sp.price_infant,0) > 0 then 1 else 0 end hasprice, 
 s.name, s.[description]

 

from supplement s

inner join supplement_pricing sp
on s.supplement_id = sp.supplement_id

inner join [date] d
on d.fulldate between sp.valid_from and sp.valid_to

where s.is_compulsory =1 
and s.is_active = 1
and s.provider_id = (select provider_id from @Parameters)
and sp.market_id = (select market_id from @Parameters)
and d.fulldate between (select start from @Parameters) and (select [end] from @Parameters)


END