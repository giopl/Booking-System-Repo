﻿
CReate PROCEDURE [dbo].[GetCharges](
	-- Add the parameters for the stored procedure here
	@start date,
	@end date,
	@provider_id int,	
	@market_id int = 0
	

	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	SET NOCOUNT ON;
	Select 
supplement_id, name, description, sum(price_infant) price_infant, sum(price_child) price_child, 
sum(price_teen) price_teen, sum(price_adult) price_adult 

from 


(

select s.supplement_id, s.name, s.description,  s.provider_id, sp.market_id, d.fulldate, sp.price_infant, sp.price_child, sp.price_teen, sp.price_adult
from supplement s

inner join supplement_pricing sp
on s.supplement_id = sp.supplement_id

inner join [date] d
on d.fulldate between sp.valid_from and sp.valid_to

where s.is_compulsory =1 
and s.is_active = 1
and sp.is_active = 1

) T

inner join [date] dt
on dt.fulldate between @start and dateadd(day,-1,@end)



where T.provider_id = @provider_id
and (T.market_id = 0 or T.market_id = @market_id)
and T.fulldate = dt.fulldate

group by
supplement_id, name, description


END