﻿CREATE PROCEDURE [dbo].[GetSupplements]
	@start date, --checkin_date
	@end date, --checkout_date
	@compulsory bit =0 , 
	--additional optional parameters in case an outer apply is required
	@provider int =0,  
	@market int =0,
	@room int =0,	
	@fulldate datetime='1900-01-01',
	@adults int=0,
	@infants int=0,
	@children int=0,
	@teens int=0,
	@room_num int = 0
AS

BEGIN
	select 
	distinct
	[supplement_id]
      ,[provider_id]
      ,[name]
      ,[description]
      ,[is_per_person]
      ,[is_per_room]
      ,[is_per_night]
      ,[is_per_item]
      ,[valid_all_rooms]
      ,[meal_plan_id]
      ,[room_id]
      ,[market_id]
      ,[symbol]
      ,[currency_code]
      ,[valid_from]
      ,[valid_to]
      ,[price_per_item]
      ,[price_adult]
      ,[price_infant]
      ,[price_child]
      ,[price_teen]
	  ,case when [is_per_room] =1 then @room_num else 1 end * case when [is_per_night] =1 then datediff(day,@start,@end) else 1 end * [price_adult]	 * @adults as		[total_price_adult]
      ,case when [is_per_room] =1 then @room_num else 1 end * case when [is_per_night] =1 then datediff(day,@start,@end) else 1 end * [price_infant]	 * @infants as  [total_price_infant]
      ,case when [is_per_room] =1 then @room_num else 1 end * case when [is_per_night] =1 then datediff(day,@start,@end) else 1 end * [price_child]	 * @children as		[total_price_child]	
      ,case when [is_per_room] =1 then @room_num else 1 end * case when [is_per_night] =1 then datediff(day,@start,@end) else 1 end * [price_teen]		 * @teens  as	[total_price_teen]


      ,[is_compulsory]
	from 
	[dbo].[v_supplement]
	where is_compulsory=@compulsory 	
	and 
	@start >= valid_from and @start <=valid_to
	and 
	@end >= valid_from and @end <=valid_to
	 --only return prices for dates within booking
	and  /* handle additional parameters if required */
	(@provider=0 or @provider =provider_id)
	and 
	( @market=0 or @market= market_id)
	and
	(  case when [valid_all_rooms]=1 then 1 when [valid_all_rooms]=0  and( @room=room_id or @room=0 ) then 1 else 0 end =1 )
	
end