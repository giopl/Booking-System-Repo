﻿


CREATE PROCEDURE [dbo].[FindPackage](
	-- Add the parameters for the stored procedure here
	@start date,
	@adults int,
	@age1 int=-1,
	@age2 int=-1,
	@age3 int=-1,
	@age4 int=-1,
	@age5 int=-1,		
	@nights int =0,
	@currency_id int=0,
	@hotel_id int=0,
	@room_num int = 0,
	@package_id int = 0,
	@is_honeymoon int = 0,
	@onlyBestPrice int = 0


	)
AS
BEGIN

select * from
(

select T.* ,

rank() over(partition by provider_id, nights order by T.total_price) as rn,
STUFF(( SELECT  '|'+ convert(varchar(10),unavailable_date,103) FROM dbo.v_unavailability a
WHERE T.room_id = a.room_id and a.unavailable_date between @start and dateadd(day,@nights-1,@start) FOR XML PATH('')),1 ,1, '')   unavailable_dates




from
(
select 
pk.package_id,
pk.package_name,
pk.room_id,
pk.room_name,
pk.provider_id,
pk.provider_name,
pk.description,
pk.market_id,
pk.currency_code,
pk.currency_id,
pk.symbol,
max(pk.base_price_id) base_price_id,
min(pk.fulldate) from_date,
--max(pk.fulldate) to_date,
DATEADD(DAY,1,max(pk.fulldate)) to_date,
(DATEDIFF(DAY,min(pk.fulldate),max(pk.fulldate))+1) nights,

sum(case @adults 
when 1 then pk.price_single
when 2 then coalesce(pk.price_twin,pk.price_single)
when 3 then coalesce(pk.price_triple,pk.price_single)
when 4 then coalesce(pk.price_quadruple,pk.price_single)
else pk.price_single end) total_per_adult,

sum(coalesce(pk.price_honeymoon,pk.price_twin, pk.price_single)) total_per_honeymooner,
sum(coalesce(pk.price_infant,0.0)) total_per_infant,
sum(coalesce(pk.price_child,0.0)) total_per_child,
sum(coalesce(pk.price_teen,0.0)) total_per_teen,



sum(coalesce(pk.chg_adult,0.0)) charges_adult,
sum(coalesce(pk.chg_infant,0.0)) charges_infant,
sum(coalesce(pk.chg_child,0.0)) charges_child,
sum(coalesce(pk.chg_teen,0.0)) charges_teen,



max(coalesce(pk.price_ground_handling_adult,0.0)) ground_handling_adult,
max(coalesce(pk.price_ground_handling_teen,0.0)) ground_handling_teen,
max(coalesce(pk.price_ground_handling_child,0.0)) ground_handling_child,



pk.incl_grnd_handling includes_ground_handling,

case @is_honeymoon when 1 then

sum(coalesce(pk.price_honeymoon,pk.price_twin, pk.price_single)) 
+ max(coalesce(pk.price_ground_handling_adult,0.0)) 
+ sum(coalesce(pk.chg_adult,0.0)) 
else 

sum(case @adults 
when 1 then pk.price_single
when 2 then coalesce(pk.price_twin,pk.price_single)
when 3 then coalesce(pk.price_triple,pk.price_single)
when 4 then coalesce(pk.price_quadruple,pk.price_single)
else pk.price_single end)  

+ max(coalesce(pk.price_ground_handling_adult,0.0)) 

+ sum(coalesce(pk.chg_adult,0.0)) 

end

as total_price,

 max(coalesce(pk.price_ground_handling_adult,0.0)) 
+ max(coalesce(pk.price_ground_handling_teen,0.0)) 
+ max(coalesce(pk.price_ground_handling_child,0.0))
as total_cost_ground_handling,
@room_num as room_sequence,
	@adults num_adults,
	max(gt.infant) num_infants,
	max(gt.child) num_children,
	max(gt.teen) num_teens,
	case when sum(case when una.room_id is not null then 1 else 0 end )>0 then 'Y' else 'N' end as unavailable_flag,
	
--GL:05/08/17 added as part of change for package allotment
max(ra.room_allotment_id) allotment_id, max(ra.allotment_amount) allotments

from v_package pk


inner join [date] dt
on dt.fulldate between @start and DATEADD(DAY,pk.number_nights-1,@start)
and dt.fulldate = pk.fulldate


inner join room r 
on pk.room_id = r.room_id


left join room_occupancy ro
on r.room_id = ro.room_id

left join occupancy o 
on o.occupancy_id = ro.occupancy_id


left join dbo.v_unavailability una 
on una.room_id=pk.room_id
and una.unavailable_date=pk.fulldate

--GL:05/08/17 added as part of change for package allotment
--left join dbo.package_allotment pa
--on pa.allotment_date = @start
--and pa.package_id = pk.package_id
--and pa.room_id = pk.room_id

--GL:01/09/17 changed to match room allotment instead
left join dbo.room_allotment ra
on ra.allotment_date = @start
and ra.room_id = r.room_id


Cross apply  [dbo].[fn_GetGuestDefinition](r.provider_id,@age1,@age2,@age3,@age4,@age5) gt


where r.is_active = 1
and pk.is_active = 1
and (@nights = 0 or (@nights = pk.number_nights))
and (@currency_id = 0 or (@currency_id = pk.currency_id))
and (@hotel_id = 0 or (@hotel_id = pk.provider_id))
and (@package_id = 0 or (@package_id = pk.package_id))

and o.adult = @adults
 and o.infant = gt.infant
 and o.teen = gt.teen
 and o.child = gt.child


group by 
pk.package_id,
pk.package_name,
pk.room_id,
pk.room_name,
pk.provider_id,
pk.provider_name,
pk.description,
pk.market_id,
pk.currency_code,
pk.currency_id,
--pk.base_price_id,
pk.symbol,
pk.incl_grnd_handling,
pk.number_nights 


having  DATEDIFF(DAY,min(pk.fulldate),max(pk.fulldate)) >= (pk.number_nights -1)

) as T

) as OT
where 
	(@onlyBestPrice = 0  or OT.rn = @onlyBestPrice)

END