﻿CREATE PROCEDURE [dbo].[GetPromotionsWithSets](
	-- Add the parameters for the stored procedure here
	@start date,
	@end date,	
	@provider_id int ,
	@room_id int =0 ,
	@market_id int =0 ,
	@is_honeymoon int = 0
	)
AS
BEGIN
with promo_data as 
(
select distinct
p.promotion_id, 
p.name,
p.code, 
p.description, 
p.contracted_offer, 
p.cancellation_policy,
markets.market_id,
markets.market_name,
rooms.room_id,
rooms.room_name,
p.min_nights,
p.free_nights,
p.discount,
p.is_honeymoon,
p.apply_all_rooms,
p.apply_all_markets,
p.flat_single,
p.flat_twin,
p.flat_triple,
p.flat_quadruple,
p.flat_child1,
p.flat_child2,
p.flat_teen1,
p.flat_teen2,
coalesce(c.conf_key,'NA') promo_categ,
coalesce(ps.promotion_set_id,-1 * p.promotion_id) promotion_set_id, --added a * -1 to make all promotions mutually exclusive by default without overlapping with any other promoset
coalesce(ps.name,p.name + '(Default)') promotion_set_name
 from promotion p 
 inner join  --get all markets applicable 
(
	select 
	p.promotion_id,
	p.apply_all_markets,
	m.market_id,
	m.market_name,
	m.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_market] pm
	on p.promotion_id=pm.promotion_id
	right join [dbo].[market] m
	on ( (p.apply_all_markets=0 and  m.market_id=pm.market_id)  --either it applies to a specific market or 
	or  p.apply_all_markets=1   --to all markets 
	)
	and p.provider_id=m.provider_id
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) markets 
on markets.provider_id=p.provider_id
inner join   --get all rooms applicable 
(
	select 
	p.promotion_id,
	p.apply_all_rooms,
	r.room_id,
	r.name room_name,
	r.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_room] pr
	on p.promotion_id=pr.promotion_id
	right join [dbo].[room] r 
	on (
	(r.room_id=pr.room_id and p.apply_all_rooms=0)  --either it applies to a specific room or 
	or  p.apply_all_rooms=1   ---or to all rooms 
	)
	and r.provider_id=p.provider_id
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
	
) rooms 
on rooms.promotion_id=markets.promotion_id
and rooms.provider_id=p.provider_id
and p.promotion_id=rooms.promotion_id

--START OF SECTION USED TO REMOVE DUPLICATE PROMO CATEGORIZED IN GROUPS
left join dbo.[configuration] c
on  p.promotion_group_id= c.configuration_id
and c.conf_type='PROMOCATEG'


left join dbo.promo_set_promotions psp
on psp.promotion_id=p.promotion_id

left join dbo.promotion_set ps
on ps.promotion_set_id=psp.promotion_set_id
and ps.is_active=1

where  p.provider_id = @provider_id
and p.is_active = 1
and getDate() between coalesce(book_from_date,'1900-01-01') and coalesce(book_until_date,'2500-12-31')
and @start >= coalesce(travel_from_date,'1900-01-01') 
and @end <= coalesce(travel_until_date,'2500-12-31')
and @start >= valid_from
and (coalesce(min_nights,0) = 
case when coalesce(free_nights,0) > 0 then 
(
SELECT  top(1) min_nights FROM [promotion] where provider_id = @provider_id and coalesce(min_nights,0) <= DATEDIFF(day,@start,@end) order by min_nights desc) 
end
or
coalesce(min_nights,0) <=  case when coalesce(free_nights,0)=0 then
DATEDIFF(day,@start,@end)
end
)
and @end <= valid_to
--and (DATEDIFF(day,@start,@end) >= coalesce(min_nights,0) and  coalesce(free_nights,0)=0) 
--and (min_nights = 6 or coalesce(min_nights,0)=0)
and Datediff(day,GetDate(),@start) between coalesce(p.book_before_days_min,0) and coalesce(p.book_before_days_max,Datediff(day,GetDate(),@start))
and (@room_id = 0 or @room_id=rooms.room_id) --if room is not specified take all else filter 
and (@market_id =0 or @market_id = markets.market_id) --if market is not specified take all else filter 
and (is_honeymoon = @is_honeymoon or @is_honeymoon= 1)

),

--for all sets containing only one promotion, return only the first set, to eliminate duplicates at booking level
uniquepromosets as 
(

select 
promotion_set_id,
promotion_id
from 
(
select 
promotion_set_id,
promotion_id,
row_number() over ( partition by promotion_id order by promotion_set_id,promotion_id) rec_cnt

from
(
	SELECT 
	p.[promotion_set_id]
	,p.[promotion_id]
	,count(p.promotion_id) over (partition by p.[promotion_set_id]) num_promo
	FROM 
			(
			select 
			distinct
			p.[promotion_set_id]
			,p.[promotion_id]
			from
			promo_data p
			) p
	)
 a 
 where a.num_promo=1
 ) b 
 where rec_cnt>1

)


select 
p.promotion_id, 
p.name,
p.code, 
p.description, 
p.contracted_offer, 
p.cancellation_policy,
p.market_id,
p.market_name,
p.room_id,
p.room_name,
p.min_nights,
p.free_nights,
p.discount,
p.is_honeymoon,
p.apply_all_rooms,
p.apply_all_markets,
p.flat_single,
p.flat_twin,
p.flat_triple,
p.flat_quadruple,
p.flat_child1,
p.flat_child2,
p.flat_teen1,
p.flat_teen2,
p.promo_categ,
p.promotion_set_id,
p.promotion_set_name
 from promo_data p 
 --join with unique promotions and matching records 
 left join uniquepromosets up
 on up.promotion_set_id=p.promotion_set_id
 and up.promotion_id=up.promotion_id
 
left join
(
select
promotion_id,promo_categ,room_id,market_id,promotion_set_id
from
(
select promotion_id,room_id,market_id,p.promotion_set_id,coalesce(promo_categ,'NA') promo_categ, ((coalesce(free_nights,0)/DATEDIFF(day,@start,@end))*100) + coalesce(discount,0) score,
ROW_NUMBER() 
	over (partition by room_id,market_id,p.promotion_set_id,coalesce(promo_categ,'NA') 
	order by ((coalesce(free_nights,0)/DATEDIFF(day,@start,@end))*100) + coalesce(discount,0) desc)
as  score_rank 
from promo_data p
where  coalesce(promo_categ,'NA')<>'NA'

) top_promo 
where top_promo.score_rank=1 --get only the first one

) catpromo
on 
	catpromo.promotion_id=p.promotion_id
and catpromo.promo_categ=p.promo_categ
and catpromo.room_id=p.room_id
and catpromo.market_id=p.market_id
and catpromo.promotion_set_id=p.promotion_set_id
where  
((catpromo.promo_categ is null and coalesce(p.promo_categ,'NA')='NA') or (catpromo.promo_categ is not null ))
and up.promotion_id is null --eliminate all promo returned in the uniquepromo dataset
END