﻿
CREATE PROCEDURE [dbo].[GetPromotions](
	-- Add the parameters for the stored procedure here
	@start date,
	@end date,	
	@provider_id int ,
	@room_id int =0 ,
	@market_id int =0 ,
	@is_honeymoon int = 0
	)
AS
BEGIN
with promo_data as 
(
select distinct
p.promotion_id, 
p.name, 
p.description, 
p.contracted_offer, 
p.cancellation_policy,
markets.market_id,
markets.market_name,
rooms.room_id,
rooms.room_name,
p.min_nights,
p.free_nights,
p.discount,
p.is_honeymoon,
p.apply_all_rooms,
p.apply_all_markets,
coalesce(c.conf_key,'NA') promo_categ
 from promotion p 
 inner join  --get all markets applicable 
(
	select 
	p.promotion_id,
	p.apply_all_markets,
	m.market_id,
	m.market_name,
	m.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_market] pm
	on p.promotion_id=pm.promotion_id
	right join [dbo].[market] m
	on (m.market_id=pm.market_id  --either it applies to a specific market or 
	or  p.apply_all_markets=1   --to all markets 
	)
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) markets 
on markets.provider_id=p.provider_id
inner join   --get all rooms applicable 
(
	select 
	p.promotion_id,
	p.apply_all_rooms,
	r.room_id,
	r.name room_name,
	r.provider_id
	from 
	[dbo].[promotion] p
	left join [dbo].[promotion_room] pr
	on p.promotion_id=pr.promotion_id
	right join [dbo].[room] r 
	on (r.room_id=pr.room_id  --either it applies to a specific room or 
	or  p.apply_all_rooms=1   ---or to all rooms 
	)
	where p.promotion_id is not null --there must be a promotion associated
	and p.is_active=1
) rooms 
on rooms.promotion_id=markets.promotion_id
and rooms.provider_id=p.provider_id


--START OF SECTION USED TO REMOVE DUPLICATE PROMO CATEGORIZED IN GROUPS
left join dbo.[configuration] c
on  p.promotion_group_id= c.configuration_id
and c.conf_type='PROMOCATEG'


where  p.provider_id = @provider_id
and is_active = 1
and getDate() between coalesce(book_from_date,'1900-01-01') and coalesce(book_until_date,'2500-12-31')
and @start >= coalesce(travel_from_date,'1900-01-01') 
and @end <= coalesce(travel_until_date,'2500-12-31')
and @start >= valid_from
and @end <= valid_to
and DATEDIFF(day,@start,@end) >= coalesce(min_nights,0)
and Datediff(day,GetDate(),@start) > coalesce(book_before_days,0)
and (@room_id = 0 or @room_id=rooms.room_id) --if room is not specified take all else filter 
and (@market_id =0 or @market_id = markets.market_id) --if market is not specified take all else filter 
and (is_honeymoon = @is_honeymoon or @is_honeymoon= 1)

)


select 
p.promotion_id, 
p.name, 
p.description, 
p.contracted_offer, 
p.cancellation_policy,
p.market_id,
p.market_name,
p.room_id,
p.room_name,
p.min_nights,
p.free_nights,
p.discount,
p.is_honeymoon,
p.apply_all_rooms,
p.apply_all_markets

 from promo_data p 
left join
(
select
promotion_id,promo_categ,room_id,market_id
from
(
select promotion_id,room_id,market_id,coalesce(promo_categ,'NA') promo_categ, (coalesce(free_nights,0)*100) + coalesce(discount,0) score,
ROW_NUMBER() 
	over (partition by room_id,market_id,coalesce(promo_categ,'NA') 
	order by (coalesce(free_nights,0)*100) + coalesce(discount,0) desc)
as  score_rank 
from promo_data p
where  coalesce(promo_categ,'NA')<>'NA'

) top_promo 
where top_promo.score_rank=1 --get only the first one

) catpromo
on 
	catpromo.promotion_id=p.promotion_id
and catpromo.promo_categ=p.promo_categ
and catpromo.room_id=p.room_id
and catpromo.market_id=p.market_id
where  
((catpromo.promo_categ is null and coalesce(p.promo_categ,'NA')='NA') or (catpromo.promo_categ is not null ))

END