﻿SET IDENTITY_INSERT   [dbo].[hotel]  ON

insert into [dbo].[hotel] ([hotel_id]      ,[provider_id]      ,[star_rating]) values (100,117,3);
insert into [dbo].[hotel] ([hotel_id]      ,[provider_id]      ,[star_rating]) values (101,118,3);
insert into [dbo].[hotel] ([hotel_id]      ,[provider_id]      ,[star_rating]) values (102,119,3);
insert into [dbo].[hotel] ([hotel_id]      ,[provider_id]      ,[star_rating]) values (103,120,3);

SET IDENTITY_INSERT   [dbo].[hotel]  OFF
SET IDENTITY_INSERT [dbo].[market]  ON
insert into [dbo].[market]([market_id]      ,[market_name]      ,[currency]      ,[provider_id]      ,[is_active]) values (100,'default','MUR',117,'Y');
SET IDENTITY_INSERT [dbo].[market]  OFF



SET IDENTITY_INSERT [dbo].[room_type] ON
insert into [dbo].[room_type]([room_type_id]      ,[hotel_id]      ,[name]      ,[description]      ,[min_adult]      ,[max_adult]      ,[room_size]) values (100,	100	,'Standard Room'	,'standard room'	,1	,4	,100);
insert into [dbo].[room_type]([room_type_id]      ,[hotel_id]      ,[name]      ,[description]      ,[min_adult]      ,[max_adult]      ,[room_size]) values (101,	100,	'Deluxe Room',	'deluxe room',	1	,3	,120);
SET IDENTITY_INSERT [dbo].[room_type] OFF

SET IDENTITY_INSERT [dbo].[occupant] ON
insert into [dbo].[occupant] ([occupant_id]      ,[hotel_id]      ,[market_id]      ,[name]      ,[description]      ,[min_adult]      ,[ground_handling_price])values(100,100,100,'Single','One Adult',1,65                   );
insert into [dbo].[occupant] ([occupant_id]      ,[hotel_id]      ,[market_id]      ,[name]      ,[description]      ,[min_adult]      ,[ground_handling_price])values(101,100,100,'Twin Shared','Two adults sharing',2,65     );
insert into [dbo].[occupant] ([occupant_id]      ,[hotel_id]      ,[market_id]      ,[name]      ,[description]      ,[min_adult]      ,[ground_handling_price])values(102,100,100,'Triple Shared','3 adults sharing',3,65     );
insert into [dbo].[occupant] ([occupant_id]      ,[hotel_id]      ,[market_id]      ,[name]      ,[description]      ,[min_adult]      ,[ground_handling_price])values(103,100,100,'Child','child less than 13',0,35           );
insert into [dbo].[occupant] ([occupant_id]      ,[hotel_id]      ,[market_id]      ,[name]      ,[description]      ,[min_adult]      ,[ground_handling_price])values(104,100,100,'Teen','teen between 13 an d 17',0,65       );
SET IDENTITY_INSERT [dbo].[occupant] OFF

SET IDENTITY_INSERT  [dbo].[package]  ON

insert into [dbo].[package]
( [package_id]      ,[hotel_id]      ,[market_id]      ,[room_type_id]      ,[occupant_id]      ,[name]      ,[description]      ,[valid_from]      ,[valid_to]      ,[free_night]      ,[paid_night]      ,[price_per_person])
values ('100','100','100','100','100','packX','package X','2016-01-01','2016-02-28','0','4','100');

SET IDENTITY_INSERT  [dbo].[package]  ON
insert into [dbo].[package]
( [package_id]      ,[hotel_id]    ,[name]      ,[description]      ,[valid_from]      ,[valid_to]      ,[free_night]      ,[paid_night]   )
values ('100','100','PackageEasyDeal','Easy 4 night package at reasonable price','2016-01-01','2016-02-28','0','4');
insert into [dbo].[package]
( [package_id]      ,[hotel_id]    ,[name]      ,[description]      ,[valid_from]      ,[valid_to]      ,[free_night]      ,[paid_night]   )
values ('101','100','NiceMarchPackage','3 night package with one free night','2016-02-29','2016-03-30','1','3');
insert into [dbo].[package]
( [package_id]      ,[hotel_id]    ,[name]      ,[description]      ,[valid_from]      ,[valid_to]      ,[free_night]      ,[paid_night]   )
values ('102','100','Normal Rate','Normal Rate','2016-01-01','2016-03-30','0','1');
SET IDENTITY_INSERT  [dbo].[package]  OFF

insert into Package_pricing values (100,100,100,100,100);
insert into Package_pricing values (100,100,100,101,80);
insert into Package_pricing values (100,100,100,102,60);
insert into Package_pricing values (100,100,100,103,45);


insert into Package_pricing values (102,100,100,100,110);
insert into Package_pricing values (102,100,100,101,90);
insert into Package_pricing values (102,100,100,102,65);
insert into Package_pricing values (102,100,100,103,50);

SET IDENTITY_INSERT [dbo].[promotion] ON

insert into [dbo].[promotion] ([promotion_id]      ,[hotel_id]      ,[market_id]      ,[name]      ,[description]      ,[valid_from]      ,[valid_to]      ,[travel_from]      ,[travel_to]      ,[booking_from]      ,[booking_to]      ,[rateOrAmount]      ,[rate]) 
values 
('100','100','100','low season','low season','2016-01-01',	'2016-02-28',NULL,NULL,'2016-01-01','2016-02-28','R','0.1')

SET IDENTITY_INSERT [dbo].[promotion] OFF




